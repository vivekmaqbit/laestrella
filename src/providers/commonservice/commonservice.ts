import { Injectable } from '@angular/core';

import { AlertController, LoadingController, ToastController } from 'ionic-angular';

import { HttpClient } from '@angular/common/http';


@Injectable()

export class CommonserviceProvider {
    
    loading:any;
    headers:any;
    HttpHeaders:any;

   
    //serviceurl:any="http://www.omsoftware.us/overachievers/laestrellas/api/webservice/";
    //serviceurl:any = "http://www.omsoftware.us/overachievers/laestrellas/api/"
    //serviceurl:any="https://www.omsoftware.org/laestrellas/api/"
    serviceurl:any="https://laestrella.com.sa/laestrellas/api/";
  //  serviceurl:any='https://laravel.cppatidar.com/laestrella/laestrellas/api/';
    
    servicedataformated:any;

    constructor(private alertCtrl: AlertController, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public http: HttpClient) {
        console.log('Hello CommonserviceProvider Provider');
    }


    erroralert(msg)
    {
        let alerterror = this.alertCtrl.create({
            title: 'Error!',
            subTitle: msg,
            buttons: [
                {
                    text: 'Ok',
                    role: 'cancel',
                    handler: () => {
                        console.log('Ok clicked');
                    }
                }
            ]
        });
        alerterror.present();
    } 

    successalert(msg)
    {
        let alerterror = this.alertCtrl.create({
            title: 'Success!',
            subTitle: msg,
            buttons: [
                {
                    text: 'Ok',
                    role: 'cancel',
                    handler: () => {
                        console.log('Ok clicked');
                    }
                }
            ]
        });
        alerterror.present();
    } 


    toastalert(msg)
    {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });

        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });

        toast.present();
    }

    waitloadershow()
    {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });

        this.loading.present();
    }
    
    waitloaderhide()
    {
        this.loading.dismiss();
    }

    serverdataget(servicedata)
    {
        if(servicedata)
        {
            return this.http.get(this.serviceurl+servicedata,{});
        }
       
        
        
    }
    serverdatapost(servicedataa)
    {
        console.log(servicedataa);
        

        if(servicedataa)
        {
            return this.http.post(this.serviceurl+servicedataa,{});
            
        }
        
    }
    

}
