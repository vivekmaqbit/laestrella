import { Component } from '@angular/core';
import { ListPage } from '../list/list';
import { HomePage } from '../home/home';
import { SearchPage } from '../search/search';
import { MyAccountPage } from '../myAccount/myAccount';
import { myCartPage } from '../mycart/myCart';



@Component({
  templateUrl: 'tab.html'
})
export class TabPage {

  tab1Root = HomePage;
  tab2Root = SearchPage;
  tab3Root = HomePage;
  tab4Root = myCartPage;
  tab5Root = MyAccountPage;
  
  
 

    constructor() {

    }
}