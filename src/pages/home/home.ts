import { Component } from '@angular/core';
import { NavController,AlertController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { Nav, Platform } from 'ionic-angular';
import { CommonserviceProvider } from '../../providers/commonservice/commonservice';
import { CategoryDetailsPage } from '../categorydetails/categoryDetails';
// import { CategoryDetailsPage } from '../categorydetails/categorydetails';
import { SearchPage } from '../search/search';
import { MyAccountPage } from '../myAccount/myAccount';
// import { myCartPage } from '../mycart/mycart';
import { CartitemPage } from '../cartitem/cartitem';
import { DashboardPage } from '../dashboard/dashboard';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
    error:any="";
    
    servicedata:any;
    data:any;
    catlist=[];
    tabBarElement:any;


    constructor(public platform: Platform,public navCtrl: NavController,private commonservice: CommonserviceProvider,private alertCtrl: AlertController,public events: Events) 
    {
        events.publish('allpagecommon');  
        this.loadpage(); 
        
        this.platform.registerBackButtonAction(() => {
            this.navCtrl.setRoot(DashboardPage);
        });
    }
  
   
    
    openNav()
    {
        document.getElementById("mySidenav").style.display = "block";
    }

    closeNav() 
    {
        document.getElementById("mySidenav").style.display = "none";
    }
    chooseNav(id,name) 
    {
        document.getElementById("mySidenav").style.display = "none";
        localStorage.setItem("cat_id",id);
        localStorage.setItem("cat_name",name);
        this.navCtrl.setRoot(CategoryDetailsPage);
    }
    loadpage()
    {
        //this.commonservice.waitloadershow();
        console.log(localStorage.getItem("setcatlist"));
        
        var allcatlist =JSON.parse(localStorage.getItem("setcatlist"));
       
        if(allcatlist!=null)
        {
            if(allcatlist.length>0)
            {
               
                let i=0;
                if(localStorage.getItem("applanguage")=="er"){
                    var setLang = 2;
                 }else{
                    var setLang = 1;
                }
                for (let alt of allcatlist) 
                { 
                    if(alt.language_id == setLang){ 
                  
                   this.catlist.push({"category_id":alt.category_id,"image":alt.image,"name":alt.name});
                    i++;
                    }
                }

            }
        }
        console.log("categorypage" + this.catlist);
        
    }
    gocatgorydetails(id,name)
    {
         if(localStorage.getItem("applanguage")=="ar"){
             var setLang = 2;
             localStorage.setItem("cat_id",id);
             localStorage.setItem("cat_name",name);
         }else{
          var setLang = 1;
          localStorage.setItem("cat_id",id);
          localStorage.setItem("cat_name",name);
          }

      
        this.navCtrl.setRoot(CategoryDetailsPage);
    }
    
    gotosearch()
    {       
        localStorage.setItem('lastpageserch','HomePage');  
        this.navCtrl.setRoot(SearchPage);
    }    
    gotohome()
    {
        this.navCtrl.setRoot(DashboardPage);
    }
    gotobag()
    {
        this.navCtrl.setRoot(CartitemPage);
    }
    gotoaccount()
    {
        this.navCtrl.setRoot(MyAccountPage);
    }
    gotoback()
    {       
        this.navCtrl.setRoot(DashboardPage);
    }
    
    


}
