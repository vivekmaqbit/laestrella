import { Component, ComponentFactoryResolver } from '@angular/core';
import { NavController,AlertController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { CommonserviceProvider } from '../../providers/commonservice/commonservice';
import { CategoryDetailsPage } from '../categorydetails/categoryDetails';
// import { CategoryDetailsPage } from '../categorydetails/categorydetails';
import { SearchPage } from '../search/search';
import { MyAccountPage } from '../myAccount/myAccount';
// import { myCartPage } from '../mycart/mycart';
import { AddtocartPage } from '../addtocart/addToCart';
// import { AddtocartPage } from '../addtocart/addtocart';
import { CartitemPage } from '../cartitem/cartitem';
import { HomePage } from '../home/home';
import { Nav, Platform } from 'ionic-angular';



@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
export class DashboardPage {
    error:any="";    
    servicedata:any;
    data:any;
    catlist=[];
    tabBarElement:any;
    productlist=[];
    bannerlist:any='';
    cartvalue="0";


    constructor(public platform: Platform,public navCtrl: NavController,private commonservice: CommonserviceProvider,private alertCtrl: AlertController,public events: Events) 
    {
        events.publish('allpagecommon');  
        this.platform.registerBackButtonAction(() => {
            let alert = this.alertCtrl.create
        ({
            title: 'Confirm',
            message: 'Do you really want to exit ?',
            buttons: 
        [{
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              
            }
            },
            {
                text: 'Ok',
                handler: () => 
                {
                 // this.deleteitem(id);
                 this.platform.exitApp();
                }
            }
        ]});
        alert.present();
            
          });
        this.bannerlistfunction();
        console.log(localStorage.getItem("cartproductcount"));
        if(localStorage.getItem("cartproductcount") == null || localStorage.getItem("cartproductcount") == ''){
            this.cartvalue="0";
        }
        else{
            this.cartvalue=localStorage.getItem("cartproductcount");
        }
        //this.cartvalue=localStorage.getItem("cartproductcount");
        if(localStorage.getItem("setcatlist") == '1')
        {
            console.log('2');
            this.allservicecalldata();
        }
        else
        {
             console.log('1');
            this.loadpage();
            
        }
    }
    
    
    
    
    allservicecalldata()
    {
        console.log('category_list');
        this.servicedata="category_list/"+localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
              //  console.log('333category_list');
                this.data=res;                
              //   console.log('1212category_list');
                localStorage.setItem("setcatlist", JSON.stringify(this.data));
               // console.log('setcat');
                this.allproductdata();
            }
            
        ) 
        
    }
    allproductdata()
    {
        this.servicedata="allcategory_product/"+localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                this.data=res;
                console.log('allproductdata');
                console.log(this.data);
                this.allproductdatatwo();
                localStorage.setItem("allcategoryproductlist", JSON.stringify(this.data.data));   
            }
        )   
    }
     allproductdatatwo()
    {
        this.servicedata="allproduct_list/"+localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                this.data=res;
               // console.log('allproductdatatwo');
                console.log(this.data); 
                localStorage.setItem("allproductlist", JSON.stringify(this.data.data));
                this.loadpage();
            }
        )  
    }
  
    bannerlistfunction()
    {
        if(localStorage.getItem("applanguage")=="er"){
            var setLang = 2;
         }else{
            var setLang = 1;
        }
        this.servicedata="city?lang="+localStorage.getItem("applanguage");;
        var bannerlist =JSON.parse(localStorage.getItem("banner"));
        console.log("deom"+bannerlist); 
        this.servicedata="banner?lang="+setLang;
        console.log(this.servicedata);
        if(localStorage.getItem("applanguage")=="er"){
            var setLang = 2;
         }else{
            var setLang = 1;
        }
            this.commonservice.serverdataget(this.servicedata).subscribe(
                res => {
                    this.data=res;              
                  
                        this.bannerlist=this.data.data;           
                    
                    console.log(this.bannerlist); 
                }
            )  
    }
    
    openNav()
    {
        document.getElementById("mySidenav").style.display = "block";
    }

    closeNav() 
    {
        document.getElementById("mySidenav").style.display = "none";
    }

    chooseNav(id,name) 
    {
        document.getElementById("mySidenav").style.display = "none";
        localStorage.setItem("cat_id",id);
        localStorage.setItem("cat_name",name);
        this.navCtrl.setRoot(CategoryDetailsPage);
    }

    loadpage()
    {
        console.log(JSON.parse(localStorage.getItem("setcatlist")));
        var allcatlist =JSON.parse(localStorage.getItem("setcatlist"));
        if(localStorage.getItem("applanguage")=="er"){
            var setLang = 2;
         }else{
            var setLang = 1;
        }
        if(allcatlist!=null)
        {
            if(allcatlist.length>0)
            {
                let i=0;
                for (let alt of allcatlist) 
                {                   
               //   console.log("dblang"+ alt.language_id);
                 // console.log("setLang"+setLang);
                  if(alt.language_id == setLang){                                          
                    this.catlist.push({"category_id":alt.category_id,"image":alt.image,"name":alt.name});
                    i++;
                  }                
                   //this.catlist.push({"category_id":alt.category_id,"image":alt.image,"name":alt.name});
                    //i++;
                }
            }
            this.Allproductlist();
        } 
    }
    Allproductlist()
    {
         var allproductlist =JSON.parse(localStorage.getItem("allcategoryproductlist"));
     
         if(allproductlist!=null)
         {
            if(allproductlist.length>0)
            {
                for (let cat of this.catlist) 
                {
                   //console.log(this.catlist); 
                    var catname=cat.name;
                    // var discount_price =cat.discount_price;
                    // var is_discount = cat.is_discount;
                    //console.log("dashcatname"+catname);
                    var catid=cat.category_id;
                    var catimage=cat.image;
                    var catlanguageid =cat.language_id;
                    var catproduct=[];
                     let i=0;
                     //console.log(localStorage.getItem("applanguage"));
                     if(localStorage.getItem("applanguage")=="er"){
                        var setLang = 2;
                        
                     }else{
                        var setLang = 1;
                    }

                    for (let pt of allproductlist) 
                    {
                       if(pt.category_id == catid)
                       {
                           i++; 
                            if(i<=4)
                            {
                                if(pt.language_id == setLang){ 
                                 catproduct.push({pt})  
                                }
                            }
                       }   
                    }                    
                    this.productlist.push({'catname':catname,'catid':catid,'catimage':catimage,'product':catproduct,'catlanguageid':catproduct})
                  //   console.log("sdsd");  language_id
                } 
               // this.loadpage();
              //  console.log("categoryproduct list ");
              console.log(this.productlist);
            }   
         }  
    }
     gocatgorydetails(id,name)
     {
       
        localStorage.setItem("cat_id",id);
        localStorage.setItem("cat_name",name);
         this.navCtrl.setRoot(CategoryDetailsPage);
     }
    
    gotosearch()
    {
        localStorage.setItem('lastpageserch','DashboardPage');
        this.navCtrl.setRoot(SearchPage);
    }
    goproductetails(id)
    {
       
        localStorage.setItem("details_pdid",id);
        localStorage.setItem("Addtocartback",'DashboardPage');
        this.navCtrl.setRoot(AddtocartPage);
    }
    
    
    gotobag()
    {
        this.navCtrl.setRoot(CartitemPage);
    }
    gotoaccount()
    {
        this.navCtrl.setRoot(MyAccountPage);
    }
    gotocat()
    {
        this.navCtrl.setRoot(HomePage);
    }
    


}
