import { Component } from '@angular/core';
import { NavController ,AlertController} from 'ionic-angular';
import { Events } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { DashboardPage } from '../dashboard/dashboard';
import { SearchPage } from '../search/search';
import { MyAccountPage } from '../myAccount/myAccount';
import { myCartPage } from '../mycart/myCart';
import { MyOrderPage } from '../myorder/myOrder';
import { AddtocartPage } from '../addtocart/addToCart';
import { CommonserviceProvider } from '../../providers/commonservice/commonservice';
import { CartitemPage } from '../cartitem/cartitem';
import { HomePage } from '../home/home';
import { Nav, Platform } from 'ionic-angular';

@Component({ 
  selector: 'page-orderDetails',
  templateUrl: 'orderDetails.html'
})
export class OrderDetailsPage {    
    
    servicedata:any;
    data:any;
    date:any;
    tr_id:any;
    productlength:any;
    address="";
    amount=0;
    totalamount:any;
    products:any;
    orderstatus:any;
    subtotal=0;
    tax=0;
    taxablevalue:any;  
    coupon_code:any;
    cancelcheck=0;
    coupon_amount=0;
    is_cancelled=0;
    shipping_charge=0;
    constructor(public platform: Platform,private alertCtrl: AlertController,public navCtrl: NavController,public events: Events,private commonservice: CommonserviceProvider) {

        events.publish('allpagecommon'); 
        this.loadpage();
        this.platform.registerBackButtonAction(() => {
            this.navCtrl.setRoot(MyOrderPage);
        });
    }
    
    loadpage() 
    {
       
        this.commonservice.waitloadershow();
        this.servicedata="order_history_detail?order_id="+localStorage.getItem("orderid")+"&lang="+localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                this.commonservice.waitloaderhide();
                this.data=res;
                console.log(this.data);
                this.date=this.data.data.date;
                this.tr_id=this.data.data.order_id;
                this.products=this.data.data.product;
                this.cancelcheck= this.data.data.is_cancelled;
                this.totalamount=this.data.data.total;
                this.subtotal =this.data.data.sub_total;
                this.is_cancelled =this.data.data.is_cancelled
                console.log(this.is_cancelled);
                this.address=this.data.data.delivery_address;
                this.orderstatus=this.data.data.orderstatus; 
                this.tax =this.data.data.tax;      
                this.shipping_charge=this.data.data.shipping;
                console.log(this.shipping_charge);
                if(this.data.data.coupon_code == null || this.data.data.coupon_code == ''){
                    this.coupon_code=0;
                }  
                else{
                    this.coupon_code = this.data.data.coupon_code;
                    this.coupon_amount=this.data.data.coupon_amount;
                }  
            }     
            
        )
            
      //  var amount= (this.totalamount)-(this.tax);
   
        
    } 
    
    gotocancel(){
        let alert = this.alertCtrl.create
        ({
            title: 'Confirm',
            message: 'Do you really want to cancel your order? You can cancel your order within 24Hrs',
            buttons: 
        [{
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              
            }
            },
            {
                text: 'Ok',
                handler: () => 
                {
                  this.cancleorder();
                }
            }
        ]});
        alert.present();
        }
        cancleorder(){
            this.servicedata="cancel?order_id="+localStorage.getItem("orderid") + "&orderstatus=7";
            this.navCtrl.setRoot(OrderDetailsPage);
        
            this.commonservice.serverdataget(this.servicedata).subscribe(
                res => {
                    this.data=res;
                    console.log(this.data.cancel);
                    // console.log(this.data); 
                    // console.log(this.data.cancel);                 
                    // if(this.data.cancel == 1){
                    //     this.commonservice.successalert("Your Order Has Been Canceled"); 
                    // }else{
                    //     this.commonservice.erroralert("You Can Cancel Your Order Within 24hrs. Please Contact To Customer Care");  
                    // }               
                }     
                       
                )
            }
    
    goOrderDetails()
    {       
        this.navCtrl.setRoot(SignupPage);
    }
    gotoback()
    {       
        this.navCtrl.setRoot(MyOrderPage);
    }
    gotohome()
    {
        this.navCtrl.setRoot(DashboardPage);
    }    
    gotosearch()
    {
        localStorage.setItem('lastpageserch','orderdetilaspage');
        console.log(localStorage.getItem('lastpageserch'));
        this.navCtrl.setRoot(SearchPage);
    }
    details(id)
    {       
        localStorage.setItem("details_pdid",id);
        this.navCtrl.setRoot(AddtocartPage);
    }    
    gotocat()
    {
        this.navCtrl.setRoot(HomePage);
    }
    gotobag()
    {
        this.navCtrl.setRoot(CartitemPage);
    }
    gotoaccount()
    {
        this.navCtrl.setRoot(MyAccountPage);
    }
   

}
