import { Component } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CommonserviceProvider } from '../../providers/commonservice/commonservice';
import { VerificationPage  } from '../verification/verification';
import { PaymentPage } from '../payment/payment';

@IonicPage()
@Component({
  selector: 'page-mobile',
  templateUrl: 'mobile.html',
})
export class MobilePage {
    error:any="";
    
    servicedata:any;
    data:any;
    countrylist:any;
    mobilenumber='+966';
    region:any
    numbervalue:any='';
    constructor(public platform: Platform,public navCtrl: NavController, public navParams: NavParams,private commonservice: CommonserviceProvider) 
    {
        this.platform.registerBackButtonAction(() => {

        });
        
    }
    

    ionViewDidLoad() {
        this.loadpage();
    }
    
    loadpage()
    {
        this.commonservice.waitloadershow();
        this.servicedata="country/"+localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                this.commonservice.waitloaderhide();
                this.data=res;
                
                this.countrylist=this.data.data;
                console.log(this.data.data);
                
            }
        )    
    }
    login()
    {
         if(this.mobilenumber!=null)
        {
            let  number =this.mobilenumber;
            let numbertostr = number.toString()
            let numberlength=numbertostr.length;
            console.log(numberlength);
            if(Number(numberlength)  < 11  )
            {
                this.numbervalue = '1';
                
            }
            else if( Number(numberlength)  > 16)
            {
                this.numbervalue = '1';
            }
            else
            {
                this.numbervalue = '0';
            }
            
        }
        console.log(this.numbervalue);
        if(!this.mobilenumber)
        {
            this.error="Mobile Number is required";
        }
        /*else if(this.numbervalue == '1')
        { 
            this.error="Please enter valid mobile number";
        }*/
        if(this.error=="")
        {
            localStorage.setItem("ship_mobile", this.mobilenumber);
            this.navCtrl.setRoot(PaymentPage);
           /* this.commonservice.waitloadershow();
            this.servicedata="login?mobile="+this.mobilenumber;
            this.commonservice.serverdataget(this.servicedata).subscribe(
                res => {
                    this.commonservice.waitloaderhide();
                    this.data=res;


                    console.log(this.data);
                    if(this.data.status="true")
                    {
                        localStorage.setItem("loginotp", this.data.otp);
                        localStorage.setItem("loginuserid", this.data.userid);
                        this.navCtrl.setRoot(VerificationPage);
                    }
                    else
                    {
                        this.commonservice.erroralert("Something went wrong");   
                    }


                }
            )*/
        }
        else
        {
            this.commonservice.erroralert(this.error);    
        } 
        
    }

}
