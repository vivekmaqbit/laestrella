import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { SearchPage } from '../search/search';
import { MyAccountPage } from '../myAccount/myAccount';
import { myCartPage } from '../mycart/myCart';

@Component({
  selector: 'page-splishpage',
  templateUrl: 'splishpage.html'
})
export class SplishPage {

    constructor(public navCtrl: NavController) {

    }
    gotohome()
    {
        this.navCtrl.setRoot(HomePage);
    }
    
    gotosearch()
    {
       
        this.navCtrl.setRoot(SearchPage);
    }
    
    gotocat()
    {
        this.navCtrl.setRoot(HomePage);
    }
    gotobag()
    {
        this.navCtrl.setRoot(myCartPage);
    }
    gotoaccount()
    {
        this.navCtrl.setRoot(MyAccountPage);
    }
    

}