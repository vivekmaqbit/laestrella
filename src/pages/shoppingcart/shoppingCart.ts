import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { CommonserviceProvider } from '../../providers/commonservice/commonservice';
import { AddtocartPage } from '../addtocart/addToCart';
import { myCartPage } from '../mycart/myCart';
import { DashboardPage } from '../dashboard/dashboard';
import { SearchPage } from '../search/search';
import { MyAccountPage } from '../myAccount/myAccount';
import { CartitemPage } from '../cartitem/cartitem';
import { HomePage } from '../home/home';
import { Nav, Platform } from 'ionic-angular';
import * as $ from 'jquery'


@Component({
  selector: 'page-shoppingCart',
  templateUrl: 'shoppingCart.html'
})
export class ShoppingCartPage {
    error:any="";
    servicedata:any;
    data:any;
    image="assets/imgs/product-des.png";
    name="ambuj";
    protype="SAR 139 / Meter";
    promaterial="slikone";
    color="black";
    proquenty="1";
    price="1000";
    categorydetils="";
    sku="";
    jan="";
    productitem="";
    productid="";

    constructor(public platform: Platform,public navCtrl: NavController,private commonservice: CommonserviceProvider,public events: Events) {
    
        events.publish('allpagecommon');
        this.loadpage(); 
        this.platform.registerBackButtonAction(() => {
            this.navCtrl.setRoot(AddtocartPage);
        });
    }
    
    
    loadpage()
    {
        let i=0;
        this.productitem=JSON.parse(localStorage.getItem("cart"+"11")); 
        console.log(this.productitem);
        if(this.productitem!=null)
        {
            for (let obj of this.productitem) 
            {
                if(obj[0]==localStorage.getItem("details_pdid"))
                {
                    
                    this.data=obj;
                    
                }
               
                i++;
            }
            console.log(this.data[8]);
            this.productid=this.data[0];
            this.name=this.data[1];
            this.price=this.data[2];
            this.image=this.data[3];
            this.proquenty=this.data[4];
            this.promaterial=this.data[5];
            this.protype=this.data[6];
            this.color=this.data[7];           
            this.sku="BRI43535";
            this.jan="78";
            this.description();
           
        }
        
        //his.data=localStorage.getItem("cart"+"11");
        
               
        
    }
    description()
    {
        this.servicedata="productDetail/"+localStorage.getItem("details_pdid")+"?lang="+localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                this.data=res;
                this.description=this.data.data.description;
                $("#categorydetils").append(this.data.data.description);
            }
        )    
    }
    gotoaddtocart()
    {
        this.navCtrl.setRoot(CartitemPage);
    }
     
    gotomycart()
    {
       this.navCtrl.setRoot(myCartPage);
    }
       gotohome()
    {
        this.navCtrl.setRoot(DashboardPage);
    }
    
    gotosearch()
    {
       
        this.navCtrl.setRoot(SearchPage);
    }
    
    gotocat()
    {
        this.navCtrl.setRoot(HomePage);
    }
    gotobag()
    {
        this.navCtrl.setRoot(myCartPage);
    }
    gotoaccount()
    {
        this.navCtrl.setRoot(MyAccountPage);
    }
    gotoback()
    {
        this.navCtrl.setRoot(AddtocartPage);
    }
    addwishlist()
    {
        this.commonservice.waitloadershow();
            
        this.servicedata="add_wishlist?product_id="+localStorage.getItem("details_pdid")+"&userid="+localStorage.getItem("loginuserid")+"/"+localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                console.log(res);
                this.data = res;
                this.commonservice.waitloaderhide(); 
                if(this.data.status="true")
                {
                    this.commonservice.successalert("Product added in your wishlist sucessfully"); 
                }
                else
                {
                    this.commonservice.erroralert("Something went wrong");   
                }

            }
        )
        
        
    }
}
