import { Component } from '@angular/core';
import { NavController, ToastController ,AlertController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { CommonserviceProvider } from '../../providers/commonservice/commonservice';
import { VerificationPage  } from '../verification/verification';
import { OrderDetailsPage } from '../orderDetails/orderDetails';
import { OrderConfirmedPage } from '../orderConfirmed/orderConfirmed';
import { HomePage } from '../home/home';
import { DashboardPage } from '../dashboard/dashboard';
import * as $ from 'jquery';
import { CartitemPage } from '../cartitem/cartitem';
import * as bootstrap from "bootstrap";
import { Nav, Platform } from 'ionic-angular';

@Component({
  selector: 'page-payment', 
  templateUrl: 'payment.html'
})
export class PaymentPage {
    ct=0;
    coupontype:any;
    error:any="";
    servicedata:any;
    data:any;
    fname:any="";
    nocart=0;
    lname:any="";
    email:any="";
    emailone:any="";
    emailtwo:any="@gmail.com"
    mobile:any= "";
    address:any="";
    city:any="";
    countrycode:any;
    emailtwoto:any=''; 
    postcode:any="";
    country:any="";
    information:boolean;
    bcngemail:any="0";
    bcngaddress:any="0";
    bcngemaillast:any="0";
    bcngaddresslast:any="0";
    addressship:any;
    emailship:any;
    emailmetod:any;
    addressmethod:any;
    updateaddress=0; 
    shippingmethod=0;
    cartitems=[];
    productitem=[];
    totalamount=0;
    tax=0;
    countrylist=[];
    citylist=[];
    opendom=1;
    couponshow=0;
    total_weight=0;
    couponamount=0;
    couponcode="";
    subtotalpay=0;
    taxablevalue=0;
    discount=0;
    c_address="";
    c_zipcode="";
    c_city="";
    c_country="";
    c_id="0";
    c_a_array=[];
    c_status="0";
    c_all_address=[];
    coupon_code:any;
    total_address=0;
    $ :any;
    shipping_charge=0;
    orderconfirmarray=[];
    


    constructor(public platform: Platform,private alertCtrl: AlertController,public navCtrl: NavController,public events: Events,private commonservice: CommonserviceProvider, private toastCtrl: ToastController) {
     
        
        events.publish('allpagecommon'); 
        this.loadpage();
        this.mobile=localStorage.getItem("ship_mobile");
        this.postcode = "";
        this.platform.registerBackButtonAction(() => {
            this.navCtrl.setRoot(CartitemPage);
        });
    }
    isValidEmailAddress(emailAddress) 
    {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    }
    /*opentab(tabnumber)
    {
        if(tabnumber == 1)
        {
            $("#two").css("display","none"); 
            $("#three").css("display","none"); 
            $("#one").css("display","block"); 
            $(".two").css("border-bottom","none"); 
            $(".three").css("border-bottom","none"); 
            $(".one").css("border-bottom","3px solid"); 
            $("#twofooter").css("display","none"); 
            $("#threefooter").css("display","none"); 
            $("#onefooter").css("display","block"); 
        }
        else if(tabnumber == 2)
        {
            if(this.updateaddress == 1)
            {
                $("#one").css("display","none"); 
                $("#three").css("display","none");
                $("#two").css("display","block");
                $(".one").css("border-bottom","none"); 
                $(".three").css("border-bottom","none"); 
                $(".two").css("border-bottom","3px solid"); 
                $("#onefooter").css("display","none"); 
                $("#threefooter").css("display","none");
                $("#twofooter").css("display","block"); 
            }
        }
        else
        {
            if(this.updateaddress == 1)
            {
                $("#two").css("display","none"); 
                $("#one").css("display","none");
                $("#three").css("display","block"); 
                $(".one").css("border-bottom","none"); 
                $(".two").css("border-bottom","none"); 
                $(".three").css("border-bottom","3px solid");
                $("#twofooter").css("display","none"); 
                $("#onefooter").css("display","none");
                $("#threefooter").css("display","block");
            }
        }
    }*/
       opentab(tabnumber)
    {
        if(tabnumber == 1)
        {
            $("#three").css("display","none"); 
            $("#one").css("display","block"); 
            $(".three").css("border-bottom","none"); 
            $(".one").css("border-bottom","3px solid"); 
            $("#threefooter").css("display","none"); 
            $("#onefooter").css("display","block"); 
            $(".two").css("border-bottom","none"); 
            $("#twofooter").css("display","none"); 
            $("#two").css("display","none");
        }
        else if(tabnumber == 2)
        {
            if(this.updateaddress == 1)
            {
                //this.commonservice.waitloadershow();
               // this.commonservice.waitloaderhide();
              //  this.cartitem();
                $("#one").css("display","none"); 
                $("#three").css("display","none");
                $("#two").css("display","block");
                $(".one").css("border-bottom","none"); 
                $(".three").css("border-bottom","none"); 
                $(".two").css("border-bottom","3px solid"); 
                $("#onefooter").css("display","none"); 
                $("#threefooter").css("display","none");
                $("#twofooter").css("display","block"); 
                
            }
        }
        else
        {
            if(this.updateaddress == 1)
            {
                $("#one").css("display","none");
                $("#three").css("display","block"); 
                $("#two").css("display","none");
                $(".one").css("border-bottom","none"); 
                $(".three").css("border-bottom","3px solid");
                $("#onefooter").css("display","none");
                $("#threefooter").css("display","block");
                $(".two").css("border-bottom","none"); 
                $("#twofooter").css("display","none"); 
            }
        }
    }
    gotoadresssection()
    {
        $("#three").css("display","none"); 
        $("#two").css("display","none");
        $("#one").css("display","block"); 
        $(".three").css("border-bottom","none"); 
        $(".two").css("border-bottom","none"); 
        $(".one").css("border-bottom","3px solid"); 
        $("#threefooter").css("display","none");
        $("#twofooter").css("display","none"); 
        $("#onefooter").css("display","block"); 
        this.fname="";
        this.lname="";
        this.email="";
        this.emailone="";
        this.emailtwo="@gmail.com";       
        this.address="";
        this.city="";        
        this.emailtwoto='';
        this.postcode="";
        this.country="";
        this.updateaddress=0;        
    }
    goorderconfirm()
    {
        this.navCtrl.setRoot(VerificationPage);
    }
    gotoback()
    {
       this.navCtrl.setRoot(CartitemPage);
        // this.navCtrl.setRoot(DashboardPage);
    }
    goshippingmethod()
    {
        this.updateaddress=1;
        this.shippingmethod=1;
        this.opentab(3);
    }
    changedom()
    {
       
       if(this.emailtwo=='other')
       {
           this.opendom=0;  
        //    alert("dfdfdf");
       }
    }
    
    
    loadpage()
    {
        if(localStorage.getItem("islogin"))
        {
            this.commonservice.waitloadershow();
            this.servicedata="view_profile?userid="+localStorage.getItem("loginuserid")+"&lang="+localStorage.getItem("applanguage");
            this.commonservice.serverdataget(this.servicedata).subscribe(
                res => {
                    this.commonservice.waitloaderhide();
                    this.data=res;
                    console.log(this.data.data);
                    this.fname=this.data.data.firstname;
                    this.lname=this.data.data.lastname;
                    //this.email=this.data.data.email;
                    this.mobile=this.data.data.telephone;
                    this.address=this.data.data.address;
                    this.addressship=this.data.data.address;
                    this.emailship=this.data.data.email;
                    this.emailmetod=this.data.data.email;
                    this.addressmethod=this.data.data.address;
                    if(localStorage.getItem("ship_saveinformation"))
                    {
                        this.fname=localStorage.getItem("ship_fname");
                        this.lname=localStorage.getItem("ship_lname");
                        this.emailone=localStorage.getItem("ship_emailone");
                        this.emailtwo=localStorage.getItem("ship_emailtwo");
                        this.mobile=localStorage.getItem("ship_mobile");
                        this.address=localStorage.getItem("ship_address");
                        this.information=true;
                        this.city=localStorage.getItem("ship_city");
                        this.country=localStorage.getItem("ship_country");
                        this.postcode=localStorage.getItem("ship_postcode");
                        this.addressship=localStorage.getItem("ship_address")
                        this.emailship=localStorage.getItem("ship_email");
                        this.emailmetod=localStorage.getItem("ship_email");
                        this.addressmethod=localStorage.getItem("ship_address");
                        this.email=localStorage.getItem("ship_email");
                        this.address=localStorage.getItem("ship_address");
                    }
                  //  this.cartitem();
                    this.countrylistad();
                }
            ) 
        }
        else
        {
            if(localStorage.getItem("ship_saveinformation"))
            {
                this.fname=localStorage.getItem("ship_fname");
                this.lname=localStorage.getItem("ship_lname");
                this.emailone=localStorage.getItem("ship_emailone");
                this.emailtwo=localStorage.getItem("ship_emailtwo");
                this.mobile=localStorage.getItem("ship_mobile");
                this.address=localStorage.getItem("ship_address");
                this.information=true;
                this.city=localStorage.getItem("ship_city");
                this.country=localStorage.getItem("ship_country");
                this.postcode=localStorage.getItem("ship_postcode");
                this.addressship=localStorage.getItem("ship_address")
                this.emailship=localStorage.getItem("ship_email");
                this.emailmetod=localStorage.getItem("ship_email");
                this.addressmethod=localStorage.getItem("ship_address");
                this.email=localStorage.getItem("ship_email");
                this.address=localStorage.getItem("ship_address");
            }
               // this.cartitem();
                this.countrylistad();
            
        }
         
    }
    countrylistad()
    {
       
        this.servicedata="country?lang="+localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                this.data=res;
                
                this.countrylist=this.data.data;
                this.countrycode="";
                this.citylistad();
            }
        )    
    }
    underprocess() {
		let toast = this.toastCtrl.create({
			message: 'In Process',
			duration: 3000,
			position: 'bottom'
		});
		toast.onDidDismiss(() => {
			//console.log('Dismissed toast');
		});
		toast.present();
	}
    citylistad()
    {       
        this.servicedata="city?lang="+localStorage.getItem("applanguage");;
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                this.data=res;                
                this.citylist=this.data.data;
                //console.log(this.citylist);
            }
        )    
    }
    // getTotal ()
    // {
    //     var total = 0;
    //     for(var i = 0; i < this.cartitems.length; i++)
    //     {
            
    //         var product = this.cartitems[i];
    //         total += (product.price* product.quen);
    //     }
    //     this.tax=(total * 0.05);
    //     this.totalamount=(total * 0.05) + total;
    //     console.log(this.tax);
    // }
    getTotal () 
    {  
   
        var total = 0;
        var taxprice=0;
        var weight=0;
       
        for(var i = 0; i < this.cartitems.length; i++)
        {
            var product = this.cartitems[i];
            if(product.is_discount == 1){
                total += (product.discount_price* product.quen);              
             weight += (product.weight* product.quen);    
            }
            else{
                total += (product.price* product.quen);
                weight += (product.weight* product.quen);    
            } 
        }        
        this.total_weight=weight;

        console.log(this.total_weight);
        this.servicedata="shippingratecal?user_id="+localStorage.getItem("loginuserid")+"&product_array="+JSON.stringify(this.cartitems)+"&city="+this.city+"&country_code="+this.countrycode+"&total_weight="+this.total_weight;
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res=>{
                this.data=res;
                console.log(this.data);
                console.log(this.couponcode);
                this.shipping_charge= this.data.TotalAmount.Value;  
                // if(localStorage.getItem("couponcode") == "0" || localStorage.getItem("couponcode") == "" || localStorage.getItem("couponcode") == null || localStorage.getItem("couponcode") == undefined ){  
                    if(this.coupon_code == null || this.coupon_code == ''){
                console.log("null enter");
                    this.couponcode="";
                    this.ct=0;
                    this.couponshow=0;    
                    this.coupon_code="";    
                    this.totalamount=total;           
                    this.taxablevalue = this.totalamount *0.05; 
                    this.subtotalpay=  this.taxablevalue + this.totalamount + this.shipping_charge;
            
                  }else{
                    console.log("coupn enter enter");
                    this.couponshow=1;       
                    this.totalamount=total;           
                    this.taxablevalue = this.totalamount *0.05;
                    if(this.coupontype == "P"){
                        console.log("percentgae enter");          
                        this.ct = this.totalamount * this.couponamount;
                        console.log(this.shipping_charge);                      
                        this.subtotalpay=  (this.taxablevalue + this.totalamount + this.shipping_charge) - this.ct;
                     //   localStorage.setItem("couponamount",this.ct);
                        }
                        else{
                            console.log("flat amount enter");
                           this.ct = this.couponamount;                          
                            this.subtotalpay=  (this.taxablevalue + this.totalamount + this.shipping_charge) - this.ct;
                            //localStorage.setItem("couponamount",this.ct);
                        }
                    // this.couponcode=localStorage.getItem("couponcode");
                    // this.servicedata="coupon_code?coupon_code="+localStorage.getItem("couponcode")+"&user_id="+localStorage.getItem("loginuserid");
                    // this.commonservice.serverdataget(this.servicedata).subscribe(
                    //      res => {
                    //          this.data=res;
                    //          console.log(this.data);
                    //          this.coupontype =this.data.Response.type;
                    //          this.couponamount=this.data.Response.amount;
                             
                    //          if(this.coupontype == "P"){
                    //             console.log("percentgae enter");          
                    //             this.ct = this.totalamount * this.couponamount;
                    //             console.log(this.shipping_charge);                      
                    //             this.subtotalpay=  (this.taxablevalue + this.totalamount + this.shipping_charge) - this.ct;
                    //          //   localStorage.setItem("couponamount",this.ct);
                    //             }
                    //             else{
                    //                 console.log("flat amount enter");
                    //                this.ct = this.couponamount;                          
                    //                 this.subtotalpay=  (this.taxablevalue + this.totalamount + this.shipping_charge) - this.ct;
                    //                 //localStorage.setItem("couponamount",this.ct);
                    //             }
                    //      }
                    // )
                  }             
            }         
        ) 
       
        localStorage.setItem("subtotalpay",JSON.stringify(this.subtotalpay));
        localStorage.setItem("tax",JSON.stringify(this.taxablevalue));        
    }
    minusitem(val,id,pr, color)
    {
        console.log(color);
        console.log(val);
        console.log(id);
        console.log(pr);
       var min = localStorage.getItem(pr);
       console.log('crt_min'+min);
       var nid = id;
        if(val>min)
        {
            var m = parseFloat(val)-parseFloat(min);
        }
        else
        {
            var m = parseFloat(val);
        }
        nid[''+pr] = m;
        console.log(m);
        console.log(pr);
        this.updatequenty(m,pr,color);
    }
    plusitem(val,id,pr,stock,color)
    {
        console.log(color);
        console.log(val);
        console.log(id);
        console.log(pr);
        var quantity_stock =stock;
        var min = localStorage.getItem(pr); 

        if(parseFloat(val) < parseFloat(stock)){
            console.log("if call");
            var nid = id;
            var m = parseFloat(val) +parseFloat(min);
            nid[''+pr] = m;
        }
        else{
            console.log("else call");
            this.commonservice.erroralert("You reach max quntity");
            var nid = id;
            var m = parseFloat(val);
            nid[''+pr] = m;
        }
      //  var m = parseFloat(val) +parseFloat(min);
        console.log('crt_pls'+min);
        var nid = id;
       // var m = parseFloat(val) +parseFloat(min);
        nid[''+pr] = m;
        console.log(m);
        console.log(pr);
        this.updatequenty(m,pr,color);
    }
    updatequenty(m,pid,color)
    {
        let a=[];
        let b = JSON.parse(localStorage.getItem("cart"+"11"));
        for (let val of b) 
        {
           
            if(pid==val[0])
            {  
              if(color == val[7] ){
                val[4] = m;
                a.push(val); 
              }else{
                a.push(val); 
              }
                
            }
            else
            {
                a.push(val); 

            }
        };
        localStorage.setItem("cart"+"11", JSON.stringify(a));
        this.allitem();
    }
    allitem()
    {
        console.log("allitem function call start;");
        this.cartitems=[];
        this.productitem=JSON.parse(localStorage.getItem("cart"+"11")); 
        console.log(this.productitem);
        if(this.productitem!=null)
        {
            if(this.productitem.length>0)
            {
              console.log(this.productitem.length);
                let i=0; 
                for (let val of this.productitem) 
                {
                   i++;                  
                    this.cartitems.push({"id":val[0],"name":val[1],"price":val[2],"image":val[3],"quen":val[4],"material":val[5],"color":val[7],"is_discount":val[9],"discount_price":val[10],"quantity_stock":val[11],"weight":val[12],"coupon_amount":val[13]});
                   console.log(this.cartitems);
                }
                this.nocart=1; 
            }
            else
            {
                this.nocart=0; 
            }
        }
        else
        {
            this.nocart=0; 
        }
       
        this.getTotal();
       // this.shiipingratecal();
       console.log(this.nocart);
        localStorage.setItem("cartproductcount",JSON.stringify(this.cartitems.length));
        console.log("allitem function call end;");
    }
    deleteConfirm(id,color) 
    {
        let alert = this.alertCtrl.create
        ({
            title: 'Confirm',
            message: 'Do you want to Delete this product in your cart?',
            buttons: 
        [{
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              
            }
            },
            {
                text: 'Ok',
                handler: () => 
                {
                  this.deleteitem(id,color);
                }
            }
        ]});
        alert.present();
    }
    
    deleteitem(id,color)
    { 
        let a=[];
        let b = JSON.parse(localStorage.getItem("cart"+"11"));
        for (let val of b) 
        {
            if(id!=val[0] || (val[7]!= color))
            {  
                a.push(val); 
            }
            
        };
        localStorage.setItem("cart"+"11", JSON.stringify(a));
      //  localStorage.removeItem(id);
        this.allitem();
    }

    coupon(){          
                if(this.couponcode == null || this.couponcode==''){
                    this.couponshow=0;                            
                            this.coupon_code="";
                            this.couponcode="";
                    this.getTotal();
                    
                    //localStorage.setItem("couponcode","");
                  }else{           
                 console.log(this.couponcode);   
                 this.commonservice.waitloadershow();
                 this.servicedata="coupon_code?coupon_code="+this.couponcode+"&user_id="+localStorage.getItem("loginuserid");
                 this.commonservice.serverdataget(this.servicedata).subscribe(
                      res => {
                          this.data=res;
                          this.commonservice.waitloaderhide();
                          console.log(this.data);
                           if(this.data.status == 1){ 
                               this.coupontype =this.data.Response.type;
                            this.couponamount=this.data.Response.amount;
                            this.couponshow=1;  
                            this.coupon_code=this.couponcode;
                           // localStorage.setItem("couponcode",this.couponcode);  
                                      
                          }
                          else{
                            this.couponshow=0;                            
                            this.coupon_code="";
                            this.couponcode="";
                          this.commonservice.erroralert("Invalid coupon code");
                         
                          }   
                          this.getTotal();
                      })
                  }

            }
         
    removecoupon(){
        this.couponshow=0;
        this.coupon_code="";
        this.couponcode="";
        this.getTotal();

    }
    cartitem()
    {
        this.productitem=JSON.parse(localStorage.getItem("cart"+"11")); 
        console.log(this.productitem);
       
        if(this.productitem!=null)
        {
            if(this.productitem.length>0)
            {
              
                let i=0; 
                for(let k= 0;k<this.productitem.length;k++){
                    let temp = this.productitem[k];
                    this.cartitems.push({"id":temp.pid,"name":temp.name,
                    "price":temp.price,"image":temp.image,
                    "quen":temp.qty,"material":'',"color":temp.color,
                    "type":temp.type,
                    "is_discount":temp.isDiscount,"discount_price":temp.discount_price,
                    "quantity_stock":temp.stock,"weight":temp.weight});
                }
               this.nocart=1;
            }
            else{
                this.nocart=0;
            }
            console.log(this.cartitems);
        }
        else{
            this.nocart=0;
        }
        
        this.getTotal();
       // this.shiipingratecal();
    }   
    
    savetoinformation()
    {
        
        
        if(this.emailtwo=='other')
        {
            this.email=this.emailone+""+this.emailtwoto;
            // alert("sdsdsdsdsdsdsd");
        }
        else
        {
            this.email=this.emailone+""+this.emailtwo;
        }
       // console.log(this.email);
        this.error="";
        
      

        if(!this.fname)
        {
            this.error="First Name is required";
        }
        else if(!this.lname)
        {
            this.error="Last Name is required";
        }
        else if(!this.email)
        {
            this.error="Email is required";
        }
        else if(this.isValidEmailAddress(this.email) == false)
        {
            this.error="Please Enter Valid Email Address And For other porvide Full E-Mail Address E.x admin@admin.com";
        }
        else if(!this.mobile)
        {
            this.error="Mobile Number is required";
        }
        else if(!this.address)
        {
            this.error="Address is required";
        }
        else if(!this.city)
        {
            this.error="City is required";
        }      
        else if(!this.country)
        {
            this.error="Country is required";
        }
        else if(!this.postcode ||  !($.isNumeric(this.postcode))){
            this.error="Post Code is required";
        }
        if(this.error=="")
        {
            localStorage.setItem("ship_fname",this.fname);
            localStorage.setItem("ship_lname",this.lname);
            localStorage.setItem("ship_emailone",this.emailone);
            localStorage.setItem("ship_emailtwo",this.emailtwo);
            localStorage.setItem("ship_mobile",this.mobile);
            localStorage.setItem("ship_address",this.address);
            localStorage.setItem("ship_city",this.city);
            localStorage.setItem("ship_country",this.country);
            localStorage.setItem("ship_postcode",this.postcode);
        }
        else
        {
            
            this.commonservice.erroralert(this.error); 
            this.information=false;
           
           
        }        
    }
     savetoinformationto()
    {
        this.cartitem();
        if(this.emailtwo == "other"){
            this.email=this.emailone;
            console.log(this.email);

        }
        else{
            this.email=this.emailone+''+this.emailtwo;
            console.log(this.email);
        }
      
        //console.log(this.email);
        this.error="";

        if(this.fname=="")
        {
            this.error="First Name is required";
        }
        else if(this.lname=="")
        {
            this.error="Last Name is required";
        }
        else if(this.email=="")
        {
            this.error="Email is required";
        }
        else if(this.isValidEmailAddress(this.email) == false)
        {
            this.error="For other porvide Full E-Mail Address Like E.x admin@admin.com";
            // this.error="Please Enter Valid Email Address";
        }
        else if(this.mobile=="")
        {
            this.error="Mobile Number is required";
        }
        else if(this.address=="")
        {
            this.error="Address is required";
        }
        else if(this.city=="")
        {
            this.error="City is required";
        }
        else if(this.country=="")
        {
            this.error="Country is required";
        }
        // else if(this.postcode=="")
        // {
        //     this.error="Post Code is required";
        // }
        if(this.error=="")
        {
            localStorage.setItem("ship_fname",this.fname);
            localStorage.setItem("ship_lname",this.lname);
            localStorage.setItem("ship_emailone",this.emailone);
            localStorage.setItem("ship_emailtwo",this.emailtwo);
            localStorage.setItem("ship_mobile",this.mobile);
            localStorage.setItem("ship_address",this.address);
            localStorage.setItem("ship_city",this.city);
            localStorage.setItem("ship_country",this.country);
            localStorage.setItem("ship_postcode",this.postcode);
           this.shiipingratecal();
           
            
        }
        else
        {
            
            this.commonservice.erroralert(this.error); 
            this.information=false;
           
           
        }        
    }
    changeinemail()
    {
        this.bcngemail="1";
    }
    
    changeinaddress()
    {
        this.bcngaddress="1";
    }
    
    savechangeinemail()
    {
        if(this.email=="")
        {
            this.error="Email is required";
        }
        else if(this.isValidEmailAddress(this.email) == false)
        {
            this.error="Please Enter Valid Email Address";
        }
        if(this.error=="")
        {
            localStorage.setItem("ship_email",this.email);
            this.bcngemail="0";
            this.addressship=localStorage.getItem("ship_address")
            this.emailship=localStorage.getItem("ship_email");
            this.emailmetod=localStorage.getItem("ship_email");
            this.addressmethod=localStorage.getItem("ship_address");
            this.email=localStorage.getItem("ship_email");
            this.address=localStorage.getItem("ship_address");
        }
        else
        {
            this.commonservice.erroralert(this.error); 
        } 
    }
    
    savechangeinaddress()
    {
        
        //console.log("changeinaddress");
         
        if(this.address=="")
        {
            this.error="Address is required";
        }
        if(this.error=="")
        {
            localStorage.setItem("ship_address",this.address);
            this.bcngaddress="0";
            this.addressship=localStorage.getItem("ship_address")
            this.emailship=localStorage.getItem("ship_email");
            this.emailmetod=localStorage.getItem("ship_email");
            this.addressmethod=localStorage.getItem("ship_address");
            this.email=localStorage.getItem("ship_email");
            this.address=localStorage.getItem("ship_address");
        }
        else
        {
            this.commonservice.erroralert(this.error); 
        } 
    }
    changeinemaillast()
    {
        this.bcngemaillast="1";
    }
    
    changeinaddresslast()
    {
        this.bcngaddresslast="1";
    }
    savechangeinemaillast()
    {
        
        if(this.email=="")
        {
            this.error="Email is required";
        }
        else if(this.isValidEmailAddress(this.email) == false)
        {
            this.error="Please Enter Valid Email Address";
        }
        if(this.error=="")
        {
            localStorage.setItem("ship_email",this.email);            
            this.bcngemaillast="0";
            this.addressship=localStorage.getItem("ship_address")
            this.emailship=localStorage.getItem("ship_email");
            this.emailmetod=localStorage.getItem("ship_email");
            this.addressmethod=localStorage.getItem("ship_address");
            this.email=localStorage.getItem("ship_email");
            this.address=localStorage.getItem("ship_address");
        }
        else
        {
            this.commonservice.erroralert(this.error); 
        } 
    }
    
    savechangeinaddresslast()
    {
       
        if(this.address=="")
        {
            this.error="Address is required";
        }
        if(this.error=="")
        {
            localStorage.setItem("ship_address",this.address);
           
            this.bcngaddresslast="0";
            this.addressship=localStorage.getItem("ship_address")
            this.emailship=localStorage.getItem("ship_email");
            this.emailmetod=localStorage.getItem("ship_email");
            this.addressmethod=localStorage.getItem("ship_address");
            this.email=localStorage.getItem("ship_email");
            this.address=localStorage.getItem("ship_address");
        }
        else
        {
            this.commonservice.erroralert(this.error); 
        } 
    }
    completeorder()
    {
       // this.commonservice.waitloadershow();
        this.completeorderto();
            // this.servicedata="login?mobile="+this.mobile+"&lang="+localStorage.getItem("applanguage");
            // this.commonservice.serverdataget(this.servicedata).subscribe(
            //     res => {
            //         this.commonservice.waitloaderhide();
            //         this.data=res;


            //         console.log(this.data);
            //         if(this.data.status="true")
            //         {
            //             localStorage.setItem("loginotp", this.data.otp);
            //             localStorage.setItem("loginuserid", this.data.userid);
                        
            //         }
            //         else
            //         {
            //             this.commonservice.erroralert("Something went wrong");   
            //         }
            //     }
            // )
        
    }
    
    completeorderto()
    {
        if(localStorage.getItem("ship_emailtwo") == "other"){
            this.email=localStorage.getItem("ship_emailone");
            console.log(this.email);
            // alert("dfdsdssf");
        }
        else{
            this.email=localStorage.getItem("ship_emailone")+localStorage.getItem("ship_emailtwo");
            console.log(this.email);
        }    
    
      // this.commonservice.waitloadershow();
    //    this.shipping_charge= "00";
       console.log(JSON.stringify(this.cartitems));
       this.getTotal();
    this.orderconfirmarray.push({"tax":this.taxablevalue,"totalamount":this.subtotalpay,"shipping_charge": this.shipping_charge,"subtotal":this.totalamount,"email":this.email,"coupon_amount":this.ct,"coupon_code":this.coupon_code,"cartitems":JSON.stringify(this.cartitems)});           
    console.log(this.orderconfirmarray);
    localStorage.setItem("orderconifrmdetails", JSON.stringify(this.orderconfirmarray));
    //this.commonservice.successalert("The order cannot be cancelled after 24 hours of confirmation");
    this.navCtrl.setRoot(OrderConfirmedPage);
       // this.servicedata="placeorder?firstname="+localStorage.getItem("ship_fname")+"&tax="+m.tax+ "&totalamount="+m.subtotalpay + "&shipping_charge="+m.shipping_charge +"&subtotal=" + m.totalamount + "&lastname="+localStorage.getItem("ship_lname")+"&telephone="+localStorage.getItem("ship_mobile")+"&email="+m.email+"&address="+localStorage.getItem("ship_address")+"&postcode="+localStorage.getItem("ship_postcode")+"&city="+localStorage.getItem("ship_city")+"&shipping_country="+localStorage.getItem("ship_country")+"&customer_id="+localStorage.getItem("loginuserid")+"&product_array="+m.cartitems+"&lang="+localStorage.getItem("applanguage")+"&coupon_amount="+m.ct+"&coupon_code="+m.couponcode;    
    }

    newaddress(){

        if(this.c_country == null || this.c_country == '' || this.c_address == '' || this.c_address == null || this.c_city == null || this.c_city == '' || this.c_zipcode == null || this.c_zipcode == '' || !($.isNumeric(this.c_zipcode))){
            
            if( this.c_address == '' || this.c_address == null){
                this.commonservice.erroralert("New Address Required");
            }            
            else if(this.c_city == null || this.c_city == ''){
                this.commonservice.erroralert("City Name Required");
            }
            else if(this.c_country == null || this.c_country == ''){
                this.commonservice.erroralert("Country Name Required");
            }
            else if(this.c_zipcode == null || this.c_zipcode == ''){
                this.commonservice.erroralert("Postal Code Required");
            }
            else if(!($.isNumeric(this.c_zipcode))){
                this.commonservice.erroralert("Postal Code Have Only Numeric Value");
            }
        }
        else{
        console.log(this.c_country);
        this.servicedata="add_user_address?user_id="+localStorage.getItem("loginuserid")+"&lang="+localStorage.getItem("applanguage")+"&city="+this.c_city+"&country="+this.c_country+"&zipcode="+this.c_zipcode+"&address="+this.c_address;
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res=>{
                this.data=res;
                console.log(this.data);
            }
        )
      //  this.address = this.c_address;
      //  this.city =this.c_city ;
      //  this.country =this.c_country ;
       // this.postcode = this.c_zipcode;
       $(".tab-content #menu1").removeClass("active");
       $(".tab-content #menu1").removeClass("in");
       $(".tab-content #home").addClass("active");
       $(".tab-content #home").addClass("in");
       $(".modal-body ul li").eq(1).addClass("active");
       $(".modal-body ul li").eq(2).removeClass("active");
       $("#firsttab").addClass("active");
       $("#secounftab").removeClass("active");
       this.custom_address();
       this.c_zipcode="";
       this.c_city="";
       this.c_address="";
       this.c_country="";
       //$(".tab-content #home").addClass("active");
    
        }
    }
    custom_address(){
        // $("body").addClass("modal-open");
        this.servicedata="isAddressAvailable?user_id="+localStorage.getItem("loginuserid");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res=>{
                this.data=res;
                console.log(this.data.total_address);
                this.total_address= this.data.total_address;
                if(this.data.status == 1){
                    this.c_status=this.data.status;
                    this.c_all_address=this.data.Response;
                    // for(let b of this.data.Response){
                    // this.c_address=b.address;
                    // this.c_id=b.id;
                    // this.c_city=b.city;
                    // this.c_country=b.country;
                    // this.c_zipcode=b.zipcode;                   
                    // console.log(this.c_address);
                    
                    // }
                    
                }
                else{
                    this.total_address= 0;
                    if(this.data.total_address >= 2){
                        this.commonservice.erroralert("You Can Add Only 2 Address");
                        $("#secounftab").css("display","none");
                    }
                    this.c_status=this.data.status;
                   // this.commonservice.erroralert("something went wrong");
                }
                console.log(this.data);
            }
        )
    }
    selectaddress(id){
        this.servicedata="isAddressAvailable?user_id="+localStorage.getItem("loginuserid");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res=>{
                this.data=res;
                if(this.data.status == 1){
                    for(let b of this.data.Response){
                    this.c_id=b.id;
                    // console.log(id);
                    // console.log(b.id);
                    if( id == b.id){
                        this.address=b.address;
                        this.city=b.city;
                        this.country=b.country;
                        console.log(this.country);
                        this.postcode=b.zipcode;  
                        this.countrycode=b.iso;

                    // this.shiipingratecal();
                       //$('#exampleModa789l').hide();
                      // var $ :any;
                    //    var el = document.getElementById('#exampleModa789l') as HTMLDivElement;
                    //    el. modal('hide');
                       
                    //   document.getElementById("exampleModa789l").style.display = "none";
               //  $('#exampleModa789l').modal('hide');
                     //location.reload();
                    }
                    }
                   
                }
                else{
                    this.c_status=this.data.status;
                  //  this.commonservice.erroralert("something went wrong");
                }
                console.log(this.data);
                
            }
        ) 
        // $(".modalcancel").append("<button type='button' data-dismiss='modal'> </button>"); 
        
        // let alert = this.alertCtrl.create
        // ({
        //     title: 'Confirm',
        //     message: 'Confirm Your Shippping Address',
        //     buttons: 
        // [{
        //     text: 'Cancel',
        //     role: 'cancel',
        //     handler: () => {
              
        //     }
        //     },
        //     {
        //         text: 'Ok',
        //         cssClass: 'modalcancel',
        //         handler: () => 
        //         {
        //             //$('.modalcancel').html("<div id='mySecondDiv'></div>");
        //            // $(".modalcancel").append("<button type='button' data-dismiss='modal'> </button>"); 
        //         // $(".alert-button").append("<button type='button' data-dismiss='modal'> </button>"); 
        //           this.confirmnewaddress(id);
                  
        //         }
        //     }
        // ]});
        // alert.present();

    }
    // confirmnewaddress(id){        
    //     this.servicedata="isAddressAvailable?user_id="+localStorage.getItem("loginuserid");
    //     this.commonservice.serverdataget(this.servicedata).subscribe(
    //         res=>{
    //             this.data=res;
    //             if(this.data.status == 1){
    //                 for(let b of this.data.Response){
    //                 this.c_id=b.id;
    //                 // console.log(id);
    //                 // console.log(b.id);
    //                 if( id == b.id){
    //                     this.address=b.address;
    //                     this.city=b.city;
    //                     this.country=b.country;
    //                     this.postcode=b.zipcode;  
    //                    $('#exampleModa789l').hide();
    //                   // var $ :any;
    //                 //    var el = document.getElementById('#exampleModa789l') as HTMLDivElement;
    //                 //    el. modal('hide');
                       
    //                 //   document.getElementById("exampleModa789l").style.display = "none";
    //            //  $('#exampleModa789l').modal('hide');
    //                  //location.reload();
    //                 }
    //                 }
                   
    //             }
    //             else{
    //                 this.c_status=this.data.status;
    //               //  this.commonservice.erroralert("something went wrong");
    //             }
    //             console.log(this.data);
    //         }
    //     ) 
    // }
    selectcountry(id){
        this.countrycode=id;
    }
    shiipingratecal(){   
        // for(var i = 0; i < this.cartitems.length; i++)
        // {
        //     var weight;
        //     var product = this.cartitems[i];
        //     weight += (product.weight* product.quen);      
        // }   
        // this.total_weight=weight;  
        this.commonservice.waitloadershow();
        
        this.servicedata="shippingratecal?user_id="+localStorage.getItem("loginuserid")+"&product_array="+JSON.stringify(this.cartitems)+"&city="+this.city+"&country_code="+this.countrycode+"&total_weight="+this.total_weight;
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res=>{
                this.data=res;
                console.log(this.data);
                this.commonservice.waitloaderhide();
                if(this.data.HasErrors == false){
                    this.shipping_charge= this.data.TotalAmount.Value;
                    this.updateaddress=1;   
                    this.opentab(2);
                }
                else{
                    this.opentab(1);
                    this.commonservice.erroralert("Enter valid city and country name");
                }
               
            }         
        )      
        console.log(this.shipping_charge);
     
    }
    deleteaddress(id){
        let alert = this.alertCtrl.create
        ({
            title: 'Confirm',
            message: 'Do you want to delete this address?',
            buttons: 
        [{
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              
            }
            },
            {
                text: 'Ok',
                handler: () => 
                {
                  this.deltaddress(id);
                }
            }
        ]});
        alert.present();

       
    }
    
    deltaddress(id){
        this.servicedata="delete_user_address?user_id="+localStorage.getItem("loginuserid")+"&address_id="+id;
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res=>{
                this.data=res;
                if(this.data.status == 1){
                    this.commonservice.successalert("Deleted succussfully");
                }
                else{
                   // this.commonservice.erroralert("Something Went Wrong");
                }
            }
        )
        this.custom_address();
    }

    gohome(){
        this.navCtrl.setRoot(DashboardPage); 
    }
}