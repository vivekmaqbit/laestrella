import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { Nav, Platform } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
import { SearchPage } from '../search/search';
import { MyAccountPage } from '../myAccount/myAccount';
// import { myCartPage } from '../mycart/mycart';
import { CommonserviceProvider } from '../../providers/commonservice/commonservice';
// import { ShoppingCartPage } from '../shoppingcart/shoppingcart';
import { CartitemPage } from '../cartitem/cartitem';
import { HomePage } from '../home/home';
// import { AddtocartPage } from '../addtocart/addtocart';
import { AddtocartPage } from '../addtocart/addToCart';

@Component({
  selector: 'page-mywishlist',
  templateUrl: 'mywishlist.html'
})
export class MyWishlistPage {
    
    
    
    error:any="";
    
    servicedata:any;
    data:any;
    image="assets/imgs/product-des.png";
    name="Black/Green Leaf Cotton Silk Screen Printed Kalamkari Fabric";
    protype="SAR 139 / Meter";
    promaterial="slikone";
    color="black";
    proquenty="1";
    price="1000";
    sku:any;
    jan:any;
    description:any;
    wishlist:any;
    materiallist:any;
    constructor(public platform: Platform,public navCtrl: NavController,private commonservice: CommonserviceProvider,public events: Events) {
        
        this.loadpage(); 
       events.publish('allpagecommon');  
        if(localStorage.getItem("loginuserid")=="" || localStorage.getItem("loginuserid")=="null")
        {
           this.navCtrl.setRoot(DashboardPage); 
        }
        // else if(localStorage.getItem("loginuserid")=="null")
        // {
        //     this.navCtrl.setRoot(DashboardPage);
        // }
        this.platform.registerBackButtonAction(() => {
            this.navCtrl.setRoot(DashboardPage);
        });
    }
    
    gotoMoveToCart()
    {       
        this.navCtrl.setRoot(AddtocartPage);
    }
    gotoback()
    {       
        this.navCtrl.setRoot(DashboardPage);
    }
    loadpage()
    {
        if(localStorage.getItem("applanguage")=="er"){
            var setLang = 2;
         }else{
            var setLang = 1;
        }
        this.commonservice.waitloadershow();
        this.servicedata="wish_list?userid="+localStorage.getItem("loginuserid")+"&lang="+setLang;
        console.log(this.servicedata);
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                this.commonservice.waitloaderhide();
                this.data=res;
                console.log(this.data);
                if(this.data.status ==true)
                {
                    this.wishlist=this.data.data;
                }
                else
                {
                    this.wishlist=[];
                }
            }
        )    
    }
    gotohome()
    {
        this.navCtrl.setRoot(DashboardPage);
    }
    
    gotosearch()
    {       
        this.navCtrl.setRoot(SearchPage);
    }
    
    gotocat()
    {
        this.navCtrl.setRoot(HomePage);
    }
    gotobag()
    {
        this.navCtrl.setRoot(CartitemPage);
    }
    gotoaccount()
    {
        this.navCtrl.setRoot(MyAccountPage);
    }
    deletewishlist(product_id)
    {
        console.log(product_id);
        this.commonservice.waitloadershow();
            
        this.servicedata="remove_wishlist?product_id="+product_id+"&userid="+localStorage.getItem("loginuserid")+"&lang="+localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                this.data = res;
                console.log(this.data);
                this.commonservice.waitloaderhide(); 
                if(this.data.status="true")
                {
                    this.wishlist=[];
                    this.commonservice.successalert("Product removed from wishlist sucessfully"); 
                    this.loadpage(); 
                    
                }
                else
                {
                    this.commonservice.erroralert("Something went wrong");   
                }

            }
        )
        
    }
     loadpageto()
    {
        this.wishlist=[];
        this.servicedata="wish_list?userid="+localStorage.getItem("loginuserid")+"&lang="+localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                
                this.data=res;
                console.log(this.data);
                if(this.data.status ==true)
                {
                    this.wishlist=this.data.data;
                }
                else
                {
                    this.wishlist=[];
                }
            }
        )    
    }
     

}