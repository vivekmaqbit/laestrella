import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-delieveryTimeline',
  templateUrl: 'delieveryTimeline.html'
})
export class DelieveryTimelinePage {

  constructor(public navCtrl: NavController) {

  }

}