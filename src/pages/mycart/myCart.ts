import { Component } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { NavController,AlertController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { CommonserviceProvider } from '../../providers/commonservice/commonservice';
// import { AddtocartPage } from '../addtocart/addtocart';
import { AddtocartPage } from '../addtocart/addToCart';
import { PaymentPage } from '../payment/payment';
import { HomePage } from '../home/home';
import { CartitemPage } from '../cartitem/cartitem';

@Component({
  selector: 'page-myCart',
  templateUrl: 'myCart.html'
})
export class myCartPage {
     error:any="";
    
    servicedata:any;
    data:any;
    image="";
    name="";
    protype="";
    promaterial="";
    color="";
    proquenty="";
    price="";
    categorydetils="";
    sku="";
    jan='';
    productitem:any;  
    productid:any;
    totalamount=0;
    discount_price="0";
    is_discount=0;
    quantity_stock="0";

    constructor(public platform: Platform,public navCtrl: NavController,public events: Events,private commonservice: CommonserviceProvider,private alertCtrl: AlertController) {
        events.publish('allpagecommon');  
        this.loadpage(); 
        this.platform.registerBackButtonAction(() => {
            this.navCtrl.setRoot(AddtocartPage);
        });
    }   
    
    loadpage()
    {
        let i=0;
        this.productitem=JSON.parse(localStorage.getItem("cart"+"11")); 
        
        if(this.productitem!=null)
        {
            console.log('leng '+this.productitem.length)
            let length = this.productitem.length;
            length = length - 1;
            this.data = this.productitem[length];
            console.log('test data '+JSON.stringify(this.data))

            // for (let obj of this.productitem) 
            // {
            //     if(obj[0]==localStorage.getItem("details_pdid"))
            //     {                   
            //         this.data=obj;                    
            //     }
               
            //     i++;
            // }
           
        //    console.log(this.data);
            this.productid=this.data.pid;
            this.name=this.data.name;
            this.price=this.data.price; 
            this.image=this.data.image;
            this.proquenty=this.data.qty;
            // this.promaterial=this.data.;
            // this.protype=this.data[6];
            this.discount_price=this.data.discount_price;
            this.is_discount=this.data.isDiscount;
            this.quantity_stock=this.data.qty;
           
             this.color= this.data.color;


            this.sku="BRI43535";
            this.jan="78";
             this.description();
             if(this.is_discount == 1){
                this.totalamount=parseFloat(this.proquenty)* parseFloat(this.discount_price);  
            }
            else{
                this.totalamount=parseFloat(this.proquenty)* parseFloat(this.price);  
            }
           
        }
        else
        {
            this.commonservice.erroralert("Product not add in your cart"); 
            this.navCtrl.setRoot(HomePage);
        }
        
        
        // this.totalamount=parseFloat(this.proquenty)* parseFloat(this.price);
        //his.data=localStorage.getItem("cart"+"11");
        
               
        
    }
    description()
    {
        this.servicedata="productDetail/"+localStorage.getItem("details_pdid");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                this.data=res;
                this.description=this.data.data.description;
                $("#categorydetils").append(this.data.data.description);
            }
        )    
    }
    
     gotoaddtocart()
    {
        this.navCtrl.setRoot(AddtocartPage);
    }
     
    gotomycart()
    {
       this.navCtrl.setRoot(CartitemPage);
    }
    gotoback()
    {
        this.navCtrl.setRoot(AddtocartPage);
    }
    deletecrtitem()
    {
        
         let alert = this.alertCtrl.create
        ({
            title: 'Confirm',
            message: 'Do you want to Delete this product in your cart?',
            buttons: 
        [{
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              
            }
            },
            {
                text: 'Ok',
                handler: () => 
                {
                  this.deleteitem(localStorage.getItem("details_pdid"));
                }
            }
        ]});
        alert.present();
        
        //
    }
     deleteitem(id)
    { 
   
        let a=[];
        let b = JSON.parse(localStorage.getItem("cart"+"11"));
        for (let val of b) 
        {
            if(id!=val[0])
            {  
                a.push(val); 
            }
            
        };
        localStorage.setItem("cart"+"11", JSON.stringify(a));
       this.navCtrl.setRoot(CartitemPage);
    }
    addwishlist()
    {
        this.commonservice.waitloadershow();
            
        this.servicedata="add_wishlist?product_id="+localStorage.getItem("details_pdid")+"&userid="+localStorage.getItem("loginuserid")+"&lang="+localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                this.data = res;
                this.commonservice.waitloaderhide(); 
                if(this.data.status="true")
                {
                    this.commonservice.successalert("Product add in your wishlist sucessfully"); 
                }
                else
                {
                    this.commonservice.erroralert("Something went wrong");   
                }

            }
        )
        
        
    }


}
