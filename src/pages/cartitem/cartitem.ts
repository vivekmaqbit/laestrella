import { Component } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { AddtocartPage } from '../addtocart/addToCart';
import { CommonserviceProvider } from '../../providers/commonservice/commonservice';
import { DashboardPage } from '../dashboard/dashboard';
import { MyAccountPage } from '../myAccount/myAccount';
import { myCartPage } from '../mycart/myCart';
import { MobilePage } from '../mobile/mobile';
import { PaymentPage } from '../payment/payment';
import { SignupPage } from '../signup/signup';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-cartitem',
  templateUrl: 'cartitem.html',
})
export class CartitemPage {
    ct=0;
    cartitems=[];
    itemcount=0;
    subtotal=0;
    totalamount=0;
    nocart=0;
    taxablevalue=0;
    productitem=[];
    subtotalpay=0;
    couponshow=0;
    couponamount=0;
    couponcode="";
    servicedata:any;
    data:any;
    error:any;
    discount=0;
    paymentarray=[];
    coupontype:any;
    type = "METER"
 

    constructor(public platform: Platform,public navCtrl: NavController, public navParams: NavParams,private commonservice: CommonserviceProvider,private alertCtrl: AlertController) {
        this.platform.registerBackButtonAction(() => {
           // localStorage.setItem("backtosigninpage",'PaymentPage');
            this.navCtrl.setRoot(HomePage); 
        });
        
    }

    ionViewDidLoad() {
        this.loadpage();
    }
    
    loadpage()
    {
        this.productitem=JSON.parse(localStorage.getItem("cart"+"11")); 

        console.log('cart '+JSON.stringify(this.productitem))
        
        if(this.productitem!=null)
        {
            if(this.productitem.length>0)
            {
                for(let k=0;k<this.productitem.length;k++){
                    this.data = this.productitem[k];
                    this.cartitems.push({"id":this.data.pid,"name":this.data.name,
                    "price":this.data.price,"image":this.data.image,
                    "quen":this.data.qty,"material":'',"color":this.data.color,
                    "type":this.data.type,
                    "is_discount":this.data.isDiscount,"discount_price":this.data.discount_price,
                    "quantity_stock":this.data.stock,"weight":this.data.weight});
                }   
                this.nocart=1;
            }
            else
            {
               this.nocart=0; 
            }
        }
        else
        {
            this.nocart=0; 
        }
        this.getTotal ();
        localStorage.setItem("cartproductcount",JSON.stringify(this.cartitems.length));
        console.log("loadpage function call and;");
    }
    minusitem(val,id,pr, color)
    {
        console.log(color);
        console.log(val);
        console.log(id);
        console.log(pr);
       var min = localStorage.getItem(pr);
       console.log('crt_min'+min);
       var nid = id;
        if(val>min)
        {
            var m = parseFloat(val)-parseFloat(min);
        }
        else
        {
            var m = parseFloat(val);
        }
        nid[''+pr] = m;
        console.log(m);
        console.log(pr);
        this.updatequenty(m,pr,color);
    }
    plusitem(val,id,pr,stock,color)
    {
        console.log(color);
        console.log(val);
        console.log(id);
        console.log(pr);
        var quantity_stock =stock;
        var min = localStorage.getItem(pr); 

        if( parseFloat(val) <  parseFloat(stock)){
            console.log("if call");
            var nid = id;
            var m = parseFloat(val) +parseFloat(min);
            nid[''+pr] = m;
        }
        else{
            console.log("else call");
            this.commonservice.erroralert("You reach max quntity");
            var nid = id;
            var m = parseFloat(val);
            nid[''+pr] = m;
        }
      //  var m = parseFloat(val) +parseFloat(min);
        console.log('crt_pls'+min);
        var nid = id;
       // var m = parseFloat(val) +parseFloat(min);
        nid[''+pr] = m;
        console.log(m);
        console.log(pr);
        this.updatequenty(m,pr,color);
    }
    updatequenty(m,pid,color)
    { 
        let a=[];
        let b = JSON.parse(localStorage.getItem("cart"+"11"));
        for(let k =0;k<b.length;k++){
            let temp = this.productitem[k];
            if(pid == temp.pid){
                temp.qty = m
            }
            a.push(temp)
        }

        localStorage.setItem("cart"+"11", JSON.stringify(a));
        this.allitem();
    }
    allitem()
    {
        console.log("allitem function call start;");
        this.cartitems=[];
        this.productitem=JSON.parse(localStorage.getItem("cart"+"11")); 
        
        if(this.productitem!=null)
        {
            if(this.productitem.length>0)
            {
                for(let k=0;k<this.productitem.length;k++){
                    this.data = this.productitem[k];
                    this.cartitems.push({"id":this.data.pid,"name":this.data.name,
                    "price":this.data.price,"image":this.data.image,
                    "quen":this.data.qty,"material":'',"color":this.data.color,
                    "type":this.data.type,
                    "is_discount":this.data.isDiscount,"discount_price":this.data.discount_price,
                    "quantity_stock":this.data.stock,"weight":this.data.weight});
                }   
                this.nocart=1;
            }
            else
            {
               this.nocart=0; 
            }
        }
        else
        {
            this.nocart=0; 
        }
        this.getTotal ();
        localStorage.setItem("cartproductcount",JSON.stringify(this.cartitems.length));
        console.log("allitem function call end;");
    }
    getTotal ()
    {  
        //this.subtotalpay= 0;
        var total = 0;
        var taxprice=0;
        for(var i = 0; i < this.cartitems.length; i++)
        {
            var product = this.cartitems[i];
            if(product.is_discount == 1){
                total += (product.discount_price* product.quen);
            }
            else{
                total += (product.price* product.quen);
            } 
        }
        
        console.log(this.couponamount);
        if(this.couponcode == null || this.couponcode == ''){  
            console.log("null enter");
            this.couponshow=0;
            this.totalamount=total; 
            this.taxablevalue = this.totalamount *0.05;
            this.subtotalpay=  this.taxablevalue + this.totalamount;
            
    
          }else{
            console.log("coupn enter enter");
            this.couponshow=1;
            this.totalamount=total; 
            this.taxablevalue = this.totalamount *0.05;
            
            if(this.coupontype == "P"){
            console.log("percentgae enter");          
            this.ct = this.totalamount * this.couponamount;
            this.subtotalpay=  (this.taxablevalue + this.totalamount) - this.ct;
         //   localStorage.setItem("couponamount",this.ct);
            }
            else{
                console.log("flat amount enter");
               this.ct = this.couponamount;
                this.subtotalpay=  (this.taxablevalue + this.totalamount) - this.ct;
                //localStorage.setItem("couponamount",this.ct);
            }
           
            
           
          }
        // this.totalamount=total;    
        // this.taxablevalue= total*0.05;
        // this.subtotalpay =this.totalamount +this.taxablevalue;
        localStorage.setItem("subtotalpay",JSON.stringify(this.subtotalpay));
        localStorage.setItem("tax",JSON.stringify(this.taxablevalue));
        console.log(localStorage.getItem("tax"));
    }
    
    coupon(){
        console.log(this.couponcode);
console.log(localStorage.getItem("islogin"));
        if(localStorage.getItem("islogin") == "1" )
            {               
                if(this.couponcode == null || this.couponcode==''){
                    this.getTotal();
                    localStorage.setItem("couponcode","0");
                  }else{           
                      console.log(this.couponcode);   
                 this.servicedata="coupon_code?coupon_code="+this.couponcode+"&user_id="+localStorage.getItem("loginuserid");
                 this.commonservice.serverdataget(this.servicedata).subscribe(
                      res => {
                          this.data=res;
                          
                          console.log(this.data);
                           if(this.data.status == 1){ 
                               this.coupontype =this.data.Response.type;
                            this.couponamount=this.data.Response.amount;
                            localStorage.setItem("couponcode",this.couponcode);              
                          }
                          else{
                          this.commonservice.erroralert("Invalid coupon code");
                          }   
                          this.getTotal();
                      })
                  }

            }
            else
            {                
                this.navCtrl.setRoot(SignupPage); 
            }
        // console.log(localStorage.getItem("loginuserid"));
         
       } 
 
    
    
    gotohome()
    {
        this.navCtrl.setRoot(DashboardPage);
    }
    
    gotopayment()
    {
console.log( localStorage.getItem("cart"+"11"));
console.log(this.cartitems);
// this.paymentarray.push({"tax":this.taxablevalue,"productarray":this.cartitems,"subtoalamount":this.totalamount,"finalamount":this.subtotalpay,"coupnamount":this.discount,"couponcode":this.couponcode});
// console.log(this.paymentarray);
        if(this.cartitems.length > 0)
        {
            console.log(localStorage.getItem("islogin"));
            if(localStorage.getItem("islogin"))
            {
               this.navCtrl.setRoot(PaymentPage); 
            }
            else
            {
                localStorage.setItem("backtosigninpage",'PaymentPage');
                this.navCtrl.setRoot(SignupPage); 
            }
        }
        else
        {
            this.commonservice.erroralert("Products add in your cart");  
            this.navCtrl.setRoot(DashboardPage);  
        }
       
    }    
   
    
    deleteConfirm(id,color) 
    {
        let alert = this.alertCtrl.create
        ({
            title: 'Confirm',
            message: 'Do you want to Delete this product in your cart?',
            buttons: 
        [{
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              
            }
            },
            {
                text: 'Ok',
                handler: () => 
                {
                  this.deleteitem(id,color);
                }
            }
        ]});
        alert.present();
    }
    
    deleteitem(id,color)
    { 

        let a=[];
        let b = JSON.parse(localStorage.getItem("cart"+"11"));
        for(let k =0;k<b.length;k++){
            let temp = this.productitem[k];
            if(id == temp.pid){
               
            }else{
                a.push(temp)
            }
           
        }

        localStorage.setItem("cart"+"11", JSON.stringify(a));

      
      //  localStorage.removeItem(id);
        this.allitem();
    }

}
