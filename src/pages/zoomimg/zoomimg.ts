import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
import { SearchPage } from '../search/search';
import { MyAccountPage } from '../myAccount/myAccount';
import { myCartPage } from '../mycart/myCart';
import { ImageViewerController } from 'ionic-img-viewer';
import { AddtocartPage } from '../addtocart/addToCart';
import { Nav, Platform } from 'ionic-angular';



@Component({
  selector: 'page-zoomimg',
  templateUrl: 'zoomimg.html'
})
export class ZoomimgPage {

    _imageViewerCtrl: ImageViewerController;
    mainimage:any="assets/imgs/product-des.png";
    singleimg:any="assets/imgs/product-des.png";
    zoomingimg:any=[];
    addimage:any=[];
    addimageto:any=[];
    showsingleimg='0';
    numberimage=0;
    constructor(public platform: Platform,public navCtrl: NavController ,public imageViewerCtrl: ImageViewerController) 
    {
        this.platform.registerBackButtonAction(() => {
            this.navCtrl.setRoot(AddtocartPage);
        });
        this._imageViewerCtrl = imageViewerCtrl;
        this.addimage=JSON.parse(localStorage.getItem("scrollimage")); 
        console.log("zoomingpage");
        console.log(this.addimage);
        if(this.addimage != null)
        {
           let i=0;
            for (let bp of this.addimage) 
            {
                this.zoomingimg.push({"image":bp,"quen":Number(i)+1});
                i++;
            }
            
        }
         if(this.addimage != null)
        {
           let i=0;
            for (let bp of this.addimage) 
            {
                this.addimageto.push({"img":bp});
                i++;
            }
            
        }
      
        this.mainimage=this.zoomingimg[0].image;
    }
    presentImage(myImage)
    {
        const imageViewer = this._imageViewerCtrl.create(myImage);
        imageViewer.present();
     }
    
    gotoback()
    {

        this.navCtrl.setRoot(AddtocartPage);
    }
    opennewimg(img,id)
    {
        console.log(img);
        this.mainimage=img;
        this.numberimage=id;
        
    }
   /* opennewimg(img)
    {
        this.addimageto=[];
        let newslider =[];
        newslider.push({img});
        
        this.newfunction(newslider);
        console.log("a1");
        console.log(this.addimageto);
         console.log("a");
    }
    newfunction(data)
    { 
        this.addimageto=[];
        this.addimageto = data;
         console.log(this.addimageto);
         console.log("aa");
    }*/
    closenewimg()
    {
        this. showsingleimg='0';
        this.addimageto=[];
         if(this.addimage != null)
        {
           let i=0;
            for (let bp of this.addimage) 
            {
                this.addimageto.push({"img":bp});
                i++;
            }
        }
        console.log(this.addimageto);
        
    }
    Closesfunction()
    {
        console.log("dsad");
    }
    rightfuction()
    {
        console.log(this.addimage.length);
        console.log(this.numberimage);
        if(this.addimage.length != this.numberimage)
        {
            let imagenumber = Number(this.numberimage) + 1;
            this.numberimage = imagenumber;
            for (let bp of this.zoomingimg) 
            {
                if(bp.quen == imagenumber)
                {
                     this.mainimage=bp.image;
                }
            }
        }
    }
    leftfunction()
    {
        if(this.addimage.length != 1)
        {
            let imagenumber = Number(this.numberimage) - 1;
            this.numberimage = imagenumber;
            for (let bp of this.zoomingimg) 
            {
                if(bp.quen == imagenumber)
                {
                     this.mainimage=bp.image;
                }
            }
        }
    }
   

}