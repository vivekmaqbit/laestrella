import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MyOrderPage } from '../myorder/myOrder';
import { PaymentPage } from '../payment/payment';
import { myCartPage } from '../mycart/myCart';
import { Nav, Platform } from 'ionic-angular';
@Component({
  selector: 'page-verification',
  templateUrl: 'verification.html'
})
export class VerificationPage {
    otp:any;
    otpone:any;
    otptwo:any;
    otpthree:any;
    otpfour:any;
    
    constructor(public platform: Platform,public navCtrl: NavController) {
    
        this.otp=localStorage.getItem("loginotp");
       
        var str = this.otp;
    
        
        this.otpone= str.charAt(0);
        this.otptwo=str.charAt(1);
        this.otpthree=str.charAt(2);
        this.otpfour=str.charAt(3);

        this.platform.registerBackButtonAction(() => {
            localStorage.setItem("loginotp", "");
            localStorage.setItem("loginuserid", "");
           
            this.navCtrl.setRoot(myCartPage);
        });
    }
    
    
    moveto(currentpos)
    {
        let nextpos=parseInt(currentpos)+1;
        
        let enteredotpdigit=(<HTMLInputElement>document.getElementById("otp"+currentpos)).value;
        
        if(enteredotpdigit=='')
        {
            return false;
        }
        
        if(enteredotpdigit>='10')
        {
            let olddigit=String(enteredotpdigit).charAt(0);
            (<HTMLInputElement>document.getElementById("otp"+currentpos)).value=olddigit;
        }
        
        
        
        if(currentpos=='4')
        {
            document.getElementById("otp"+currentpos).blur();
        }
        else
        {
            document.getElementById("otp"+nextpos).focus();
        }
    }
    gomyorder()
    {
        this.navCtrl.setRoot(PaymentPage);
    }
    gotoback()
    {
        localStorage.setItem("loginotp", "");
        localStorage.setItem("loginuserid", "");
       
        this.navCtrl.setRoot(myCartPage);
    }

}