import { Component } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { Nav, Platform } from 'ionic-angular';
import { AddtocartPage } from '../addtocart/addToCart';
import { CommonserviceProvider } from '../../providers/commonservice/commonservice';
import { DashboardPage } from '../dashboard/dashboard';
import { SearchPage } from '../search/search';
import { MyAccountPage } from '../myAccount/myAccount';
import { myCartPage } from '../mycart/myCart';
import { CartitemPage } from '../cartitem/cartitem';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-categoryDetails',
  templateUrl: 'categoryDetails.html'
})
export class CategoryDetailsPage {

    error:any="";
    checkColor:any;
    checkedId:any;
    servicedata:any;
    data:any;
    catdetailslist=[];
    catname:any;
    
    constructor(public platform: Platform,public navCtrl: NavController,public navParams:NavParams
        ,private commonservice: CommonserviceProvider,public events: Events) {
        events.publish('allpagecommon');  
        this.loadpage(); 
        console.log(localStorage.getItem("cat_name")); 
        this.platform.registerBackButtonAction(() => {
            this.navCtrl.setRoot(HomePage);
        });
    }

    ionViewDidEnter(){
        let id = this.navParams.get("id") || null;
      

        if(id != null && id != undefined){
            document.getElementById(id).scrollIntoView();
            this.navParams.data.id = null

        }
    }

    gotoback()
    {
        this.navCtrl.setRoot(HomePage);
    }
    gocatgorydetails(id)
    {
        localStorage.setItem("Addtocartback",'CategoryDetailsPage');
        localStorage.setItem("details_pdid",id);
        this.navCtrl.setRoot(AddtocartPage);   
    }
    /*loadpage()
    {
        this.commonservice.waitloadershow();
        this.servicedata="productList/"+localStorage.getItem("cat_id");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                this.commonservice.waitloaderhide();
                this.data=res;
                console.log(res);
                this.catdetailslist=this.data.data;
            }
        )     
    }*/
    loadpage()
    {
        var b = JSON.parse(localStorage.getItem("allcategoryproductlist"));
        console.log("allcategoryproductlist"+b);
        if(localStorage.getItem("applanguage")=="er"){
            var setLang = 2;
         }else{
            var setLang = 1;
        }
        if(b != null)
        {
            let i=0;
            for (let bp of b) 
            {
               
               if(bp.category_id == localStorage.getItem("cat_id"))
              
               {
                if(bp.language_id == setLang){         
                   this.catdetailslist.push({"product_id":bp.product_id,"image":bp.image,"name":bp.name,"price":bp.price,"model":bp.model,"sku":bp.sku,"jan":bp.jan,"language_id":bp.language_id,"colorImages":bp.mycolorImg,"isColorSelected":false}); 
                }
                }
               
            }
            
        }
        this.catname=localStorage.getItem("cat_name");
       
       
       
    }
    gotohome()
    {
        
        this.navCtrl.setRoot(DashboardPage);
    }
    
    gotosearch()
    {        
        localStorage.setItem('lastpageserch','CategoryDetailsPage');  
        this.navCtrl.setRoot(SearchPage);
    }    
    gotocat()
    {
        this.navCtrl.setRoot(HomePage);
    }
    gotobag()
    {
        this.navCtrl.setRoot(CartitemPage);
    }
    gotoaccount()
    {
        // this.navCtrl.setRoot(HomePage);
        this.navCtrl.setRoot(MyAccountPage);
    }

    onColorSelect(col, prod_id){
     
        this.checkColor = col;
       for(let i = 0; i < this.catdetailslist.length;i++){
           if(prod_id == this.catdetailslist[i].product_id){
               for(let j = 0; j < this.catdetailslist[i].colorImages.length;j++){
                   if(col == this.catdetailslist[i].colorImages[j].color){
                   
                    this.checkedId = prod_id;
                    this.catdetailslist[i].image = "";
                    this.catdetailslist[i].image = this.catdetailslist[i].colorImages[j].img
                   }
               } 
           }
       }
    }
}