import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Nav, Platform } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
import { SearchPage } from '../search/search';
import { MyAccountPage } from '../myAccount/myAccount';
import {myCartPage} from '../mycart/myCart';
// import { myCartPage } from '../mycart/mycart';
import { CartitemPage } from '../cartitem/cartitem';
import { CommonserviceProvider } from '../../providers/commonservice/commonservice';
// import { OrderDetailsPage } from '../orderdetails/orderdetails';
import { OrderDetailsPage } from '../orderDetails/orderDetails';
import { HomePage } from '../home/home';
// import { MyOrderPage } from '../myOrder/myOrder';
import { PaymentPage } from '../payment/payment';

@Component({
  selector: 'page-orderConfirmed',
  templateUrl: 'orderConfirmed.html'
})
export class OrderConfirmedPage {
    error:any="";
    servicedata:any;
    data:any;
    username:any;
    otp:any;
    otpone:any;
    otptwo:any;
    otpthree:any;
    otpfour:any;
    orderid:any;
    otpone1:any;
    otptwo2:any;
    otpthree3:any;
    otpfour4:any;
    m:any;

    constructor(public platform: Platform,public navCtrl: NavController,public events: Events,private commonservice: CommonserviceProvider) {

        events.publish('allpagecommon');
        // this.otp=localStorage.getItem("loginotp");
        // var str = this.otp;
        // this.otpone= str.charAt(0);
        // this.otptwo=str.charAt(1);
        // this.otpthree=str.charAt(2);
        // this.otpfour=str.charAt(3); 
        this.orderid=localStorage.getItem("orderid"); 
        this.loadpage();
        this.platform.registerBackButtonAction(() => {
            this.navCtrl.setRoot(HomePage);
        });
        document.addEventListener('paste', (e: ClipboardEvent) => {
            console.log(e.clipboardData.getData('Text'));
            this.m = e.clipboardData.getData('Text');
            if(this.m.length == 4){
        this.otpone= this.m.charAt(0);
        console.log(this.otpone);
        this.otptwo=this.m.charAt(1);
        this.otpthree=this.m.charAt(2);
        this.otpfour=this.m.charAt(3);   
        $('#otp1').val(this.otpone).trigger('input'); 
        $('#otp2').val(this.otptwo).trigger('input'); 
        $('#otp3').val(this.otpthree).trigger('input'); 
        $('#otp4').val(this.otpfour).trigger('input'); 
            }
           
            e.preventDefault();
            e.stopPropagation();
           
          }); 
    }
     loadpage()
    {
        localStorage.removeItem("orderconfirmotp");
       
        //this.commonservice.waitloadershow();
        this.otpss();
        this.servicedata="view_profile?userid="+localStorage.getItem("loginuserid")+"&lang="+localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
            //    this.commonservice.waitloaderhide();
                this.data=res;
                console.log(this.data.data);
                this.username=this.data.data.firstname +' '+this.data.data.lastname;
               
               
            }
        )  
    }
    otpss(){
        this.servicedata="orderconfirmotp?userid="+localStorage.getItem("loginuserid");
            this.commonservice.serverdataget(this.servicedata).subscribe( res => {
                    this.data=res;
                    console.log(this.data);
                    if(this.data.status="true")
                    {
                         localStorage.setItem("orderconfirmotp", this.data.otp);

                         this.otp=localStorage.getItem("orderconfirmotp");       
                         var str = this.otp;         
                        //  this.otpone= str.charAt(0);
                        //  this.otptwo=str.charAt(1);
                        //  this.otpthree=str.charAt(2);
                        //  this.otpfour=str.charAt(3);
                    }
                    else
                    {
                        this.commonservice.erroralert("Something went wrong");   
                    }


                }
            )
    }
    
      moveto(currentpos)
    {
        let nextpos=parseInt(currentpos)+1;
        
        let enteredotpdigit=(<HTMLInputElement>document.getElementById("otp"+currentpos)).value;
        
        if(enteredotpdigit=='')
        {
            return false;
        }
        
        if(enteredotpdigit>='10')
        {
            let olddigit=String(enteredotpdigit).charAt(0);
            (<HTMLInputElement>document.getElementById("otp"+currentpos)).value=olddigit;
        }
        
        
        
        if(currentpos=='4')
        {
            document.getElementById("otp"+currentpos).blur();
        }
        else
        {
            document.getElementById("otp"+nextpos).focus();
        }
    }
    // continueShopping()
    // {      
    //     this.commonservice.successalert("Your order has been confirm successfully"); 
    //     this.sendmail();
    //     this.navCtrl.setRoot(OrderDetailsPage);
    // }
    continueShopping()
    {         
        this.otp=localStorage.getItem("orderconfirmotp");       
        var str = this.otp;         
        this.otpone1= str.charAt(0);
        this.otptwo2=str.charAt(1);
        this.otpthree3=str.charAt(2);
        this.otpfour4=str.charAt(3);
        if(this.otpone == this.otpone1 &&  this.otptwo2== this.otptwo && this.otpthree3== this.otpthree && this.otpfour == this.otpfour4){
          
            var m =JSON.parse(localStorage.getItem("orderconifrmdetails"));       
            console.log(m[0]);            
            this.servicedata="placeorder?firstname="+localStorage.getItem("ship_fname")+"&tax="+m[0].tax+ "&totalamount="+m[0].totalamount + "&shipping_charge="+m[0].shipping_charge +"&subtotal=" + m[0].subtotal + "&lastname="+localStorage.getItem("ship_lname")+"&telephone="+localStorage.getItem("ship_mobile")+"&email="+m[0].email+"&address="+localStorage.getItem("ship_address")+"&postcode="+localStorage.getItem("ship_postcode")+"&city="+localStorage.getItem("ship_city")+"&shipping_country="+localStorage.getItem("ship_country")+"&customer_id="+localStorage.getItem("loginuserid")+"&product_array="+m[0].cartitems+"&lang="+localStorage.getItem("applanguage")+"&coupon_amount="+m[0].coupon_amount+"&coupon_code="+m[0].couponcode;
            console.log(this.servicedata);
            this.commonservice.serverdataget(this.servicedata).subscribe(
                    res => {
                        this.data = res;
                      //  this.commonservice.waitloaderhide(); 
                        
                        if(this.data.status="true")
                        {
                            
                            localStorage.setItem("orderid",this.data.data); 
                            localStorage.removeItem("ship_fname");
                            localStorage.removeItem("ship_lname");
                            localStorage.removeItem("ship_mobile");
                            localStorage.removeItem("ship_email");
                            localStorage.removeItem("ship_emailone")
                            localStorage.removeItem("ship_emailtwo")
                            localStorage.removeItem("ship_address");
                            localStorage.removeItem("ship_postcode");
                            localStorage.removeItem("ship_city");
                            localStorage.removeItem("ship_country");
                            localStorage.removeItem("cart"+"11");
                            localStorage.removeItem("cartproductcount");
                            localStorage.removeItem("couponcode");
                            this.commonservice.successalert("Your order has been confirmed successfully"); 
                            localStorage.removeItem("orderconfirmotp");
                            localStorage.removeItem("orderconifrmdetails");
                            this.navCtrl.setRoot(OrderDetailsPage);                            
                        }
                        else
                        {
                            localStorage.removeItem("orderconfirmotp");
                            this.commonservice.erroralert("Something went wrong"); 
                        }
                        
                         
                    }
                )
          


 

            
        }
        else{
            localStorage.removeItem("orderconfirmotp");
            this.commonservice.erroralert("Incorrect OTP");
            
        }   
    }
    sendmail(){
        
    }
    s1(){
        this.otpone="";
    }
    s2(){
        this.otptwo="";
    }
    s3(){
        this.otpthree="";
    }
    s4(){
        this.otpfour="";
    }
    gotohome()
    {
        localStorage.removeItem("orderconfirmotp");
        this.navCtrl.setRoot(DashboardPage);
    }
    
    gotosearch()
    {
        localStorage.removeItem("orderconfirmotp");
        this.navCtrl.setRoot(SearchPage);
    }
    
    gotocat()
    {
        localStorage.removeItem("orderconfirmotp");
        this.navCtrl.setRoot(HomePage);
    }
    gotobag()
    {
        localStorage.removeItem("orderconfirmotp");
        this.navCtrl.setRoot(CartitemPage);
    }
    gotoaccount()
    {
        localStorage.removeItem("orderconfirmotp");
        this.navCtrl.setRoot(MyAccountPage);
    }      
    gotoback()
    {
        localStorage.removeItem("orderconfirmotp");
        this.navCtrl.setRoot(PaymentPage);
        //this.navCtrl.setRoot(MyOrderPage);
    }

}