import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Nav, Platform } from 'ionic-angular';
import { SearchPage } from '../search/search';
import { MyAccountPage } from '../myAccount/myAccount';
// import { myCartPage } from '../mycart/mycart';
import {myCartPage} from '../mycart/myCart';

@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html'
})
export class NotificationPage {

    constructor(public platform: Platform,public navCtrl: NavController) {
        this.platform.registerBackButtonAction(() => {
            this.navCtrl.setRoot(HomePage);
        });
    }
    gotohome()
    {
        this.navCtrl.setRoot(HomePage);
    }
    
    gotosearch()
    {
       
        this.navCtrl.setRoot(SearchPage);
    }
    
    gotocat()
    {
        this.navCtrl.setRoot(HomePage);
    }
    gotobag()
    {
        this.navCtrl.setRoot(myCartPage);
    }
    gotoaccount()
    {
        this.navCtrl.setRoot(MyAccountPage);
    }
    

}