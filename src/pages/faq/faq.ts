import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Nav, Platform } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
import { SearchPage } from '../search/search';
import { MyAccountPage } from '../myAccount/myAccount';
import { myCartPage } from '../mycart/myCart';
// import { myCartPage } from '../mycart/mycart';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html'
})
export class FaqPage {

    constructor(public platform: Platform,public navCtrl: NavController) {
        this.platform.registerBackButtonAction(() => {
            this.navCtrl.setRoot(DashboardPage);
        });
    }
    
    gotoback()
    {

        this.navCtrl.setRoot(DashboardPage);
    }
    gotohome()
    {
        this.navCtrl.setRoot(DashboardPage);
    }
    
    gotosearch()
    {
       
        this.navCtrl.setRoot(SearchPage);
    }
    
    gotocat()
    {
        this.navCtrl.setRoot(HomePage);
    }
    gotobag()
    {
        this.navCtrl.setRoot(myCartPage);
    }
    gotoaccount()
    {
        this.navCtrl.setRoot(MyAccountPage);
    }

}