import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
import { SignupPage } from '../signup/signup';
import { MyAccountPage } from '../myAccount/myAccount';

import { PaymentPage } from '../payment/payment';
import { CommonserviceProvider } from '../../providers/commonservice/commonservice';

@Component({
  selector: 'page-signinOtp',
  templateUrl: 'signinOtp.html'
})
export class SigninOtpPage {
    otp:any;
    otpone:any;
    otptwo:any;
    otpthree:any;
    otpfour:any;
    otpone1:any;
    otptwo2:any;
    otpthree3:any;
    otpfour4:any;
    otpfive:any;
    m:any;

    constructor(public navCtrl: NavController,private commonservice: CommonserviceProvider) {
        this.otp=localStorage.getItem("loginotp");       
        var str = this.otp;         
        document.addEventListener('paste', (e: ClipboardEvent) => {
            console.log(e.clipboardData.getData('Text'));
            this.m = e.clipboardData.getData('Text');
             if(this.m.length == 4){
        this.otpone= this.m.charAt(0);
        console.log(this.otpone);
        this.otptwo=this.m.charAt(1);
        this.otpthree=this.m.charAt(2);
        this.otpfour=this.m.charAt(3);   
        $('#otp1').val(this.otpone).trigger('input'); 
        $('#otp2').val(this.otptwo).trigger('input'); 
        $('#otp3').val(this.otpthree).trigger('input'); 
        $('#otp4').val(this.otpfour).trigger('input'); 
             }
          
            e.preventDefault();
            e.stopPropagation();
           
           // this.otpone= m.charAt(0);
           
          this.clickpgeauto();
          
          });   
       
       // console.log(x);
    }
    
    // performSearch(e){
    //     console.log(e);
    // }
   
   
    clickpgeauto(){
       $("#hidebutton").click();
        console.log("hi");
      //  this.moveto(4);          
    }
   
    fileChange(){
        this.otpone= this.m.charAt(0);
        console.log(this.otpone);
        this.otptwo=this.m.charAt(1);
        this.otpthree=this.m.charAt(2);
        this.otpfour=this.m.charAt(3);  
    }
   
    // omp(e){
    //     console.log(this.otpone);
    //     console.log(e);
    //     var m =e.target.value;
    //     console.log(m);
    //     console.log(e.target);
    //     var strd = this.otpone; 
    //     var x = document.getElementById("otp1");
    //     console.log(x);
    //     console.log(strd);
    //     console.log(this.otpone);
    //     console.log(strd.charAt(0));    
    //     this.otpone= strd[0];      
    //     this.otptwo=strd[1];
    //     this.otpthree=strd[2];
    //     this.otpfour=strd[3];
        
    // }
    moveto(currentpos)
    {
        //var x = document.getElementById("searchparkinglocations");
        // var x = $("#otp1").val();
        // console.log(x);
        let nextpos=parseInt(currentpos)+1;
        
        let enteredotpdigit=(<HTMLInputElement>document.getElementById("otp"+currentpos)).value;
        
        if(enteredotpdigit=='')
        {
            return false;
        }
        
        if(enteredotpdigit>='10')
        {
            let olddigit=String(enteredotpdigit).charAt(0);
            (<HTMLInputElement>document.getElementById("otp"+currentpos)).value=olddigit;
        }
        
        
        
        if(currentpos=='4')
        {
            document.getElementById("otp"+currentpos).blur();
        }
        else
        {
            document.getElementById("otp"+nextpos).focus();
        }
    }
    test(e){
        console.log(e);
        console.log("input");
    }
    om(e){
        console.log(e);
        console.log("pesss");
    }
    focusOutFunction(){
        
    }
    // modhlcg(){
    //     let str1 = this.otpthree;     
    //     console.log(this.otpthree.length);
    //     console.log(str1.length);
    //     if(str1 >= 4){
                  
    //         this.otpone1= str1.charAt(0);
    //         this.otptwo2=str1.charAt(1);
    //         this.otpthree3=str1.charAt(2);
    //         this.otpfour4=str1.charAt(3);
    //     }
    // }
    // runTimeChange(){
        
    //     var str1 = this.otptwo; 
    //     console.log(this.otptwo);
    //     console.log(str1.length);
    //     if(str1.length >= 4){
                  
    //         this.otpone1= str1.charAt(0);
    //         this.otptwo2=str1.charAt(1);
    //         this.otpthree3=str1.charAt(2);
    //         this.otpfour4=str1.charAt(3);
    //     }
        
    // }
    // doSomething(){
    //     console.log(this.otpone);
    //     var str1 = this.otpone; 
    //     if(str1.length >= 4){
                  
    //         this.otpone1= str1.charAt(0);
    //         this.otptwo2=str1.charAt(1);
    //         this.otpthree3=str1.charAt(2);
    //         this.otpfour4=str1.charAt(3);
    //     }
    //     console.log(str1.length);
        
    // }
    s1(){
        // console.log(this.otpone);
        this.otpone="";
    }
    s2(){
        this.otptwo="";
    }
    s3(){
        this.otpthree="";
    }
    s4(){
        this.otpfour="";
    }
    s5(){
     this.otpfive="";   
    }
    gohome()
    {
        this.otp=localStorage.getItem("loginotp");       
        var str = this.otp;         
        this.otpone1= str.charAt(0);
        this.otptwo2=str.charAt(1);
        this.otpthree3=str.charAt(2);
        this.otpfour4=str.charAt(3);
        
     
        if(this.otpone == this.otpone1 &&  this.otptwo2== this.otptwo && this.otpthree3== this.otpthree && this.otpfour == this.otpfour4){
            localStorage.setItem("islogin", "1");       
            if(localStorage.getItem("backtosigninpage"))
            {
                this.navCtrl.setRoot(PaymentPage);
                this.commonservice.successalert("Login sucessfully"); 
            }
            else
            {
                this.navCtrl.setRoot(DashboardPage);
                this.commonservice.successalert("Login sucessfully");
            }
        }else{
            this.commonservice.erroralert("Incorrect OTP");
        }
        
    }
    goback()
    {
        localStorage.removeItem("loginotp");
        this.navCtrl.setRoot(SignupPage);
    }
    
   

}