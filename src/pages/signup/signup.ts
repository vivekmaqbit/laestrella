import { Component,Directive, HostListener } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SigninOtpPage } from '../signinOtp/signinOtp';
import { CommonserviceProvider } from '../../providers/commonservice/commonservice';
import { DashboardPage } from '../dashboard/dashboard';
import { Events } from 'ionic-angular';
import { Nav, Platform } from 'ionic-angular';
import { HomePage } from '../home/home';
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {

    error:any="";
    
    servicedata:any;
    data:any;
    countrylist:any;
    mobilenumber="";
    region:any;
    numbervalue :any='';
    countrycode:any="966"; 
    mg:any;
    

    constructor(public platform: Platform,public navCtrl: NavController,private commonservice: CommonserviceProvider,public events: Events) {
    
        this.loadpage();
        this.platform.registerBackButtonAction(() => {
            this.navCtrl.setRoot(DashboardPage);
        }); 
        document.addEventListener('paste', (e: ClipboardEvent) => {
            console.log(e.clipboardData.getData('Text'));
            this.mg = e.clipboardData.getData('Text');        
            $('#telephone').val(this.mg).trigger('.telephone'); 
            e.preventDefault();
            e.stopPropagation();
          
          });
      
    }
  
    loadpage()
    {
        this.commonservice.waitloadershow();
        this.servicedata="country?lang="+localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                this.commonservice.waitloaderhide();
                this.data=res;
                
                this.countrylist=this.data.data;
               // this.countrycode="191";
               this.countrycode=this.data.data[2].phonecode;
                console.log(this.data);
                console.log(this.countrycode);
                
            }
        )    
    }
   
    login()
    {
         if(this.mobilenumber!=null)
        {
            let  number =this.mobilenumber;
            let numbertostr = number.toString()
            let numberlength=numbertostr.length;
            console.log(numberlength);
            if(Number(numberlength)  <= 9  )
            {
                this.numbervalue = '1';
                
            }
            else if( Number(numberlength)  >= 13)
            {
                this.numbervalue = '1';
            }
            else
            {
                this.numbervalue = '0';
            }
            
        }
        console.log(this.numbervalue);
        if(!this.mobilenumber)
        {
            this.error="Mobile number is required";
        }
       
        if(this.error=="")
        {
            this.commonservice.waitloadershow();
            this.servicedata="login?mobile="+this.mobilenumber+"&countrycode="+this.countrycode+"&lang="+localStorage.getItem("applanguage");

            this.commonservice.serverdataget(this.servicedata).subscribe(
                res => {
                    this.commonservice.waitloaderhide();
                    this.data=res;


                    console.log(this.data);
                    if(this.data.status="true")
                    {
                        localStorage.setItem("loginotp", this.data.otp);
                        localStorage.setItem("loginuserid", this.data.userid);
                        this.navCtrl.setRoot(SigninOtpPage);
                    }
                    else
                    {
                        this.commonservice.erroralert("Something went wrong");   
                    }


                }
            )
        }
        else
        {
            this.commonservice.erroralert(this.error);    
        } 
        
    }
    gohome()
    {
         this.events.publish('allpagecommon');  
        this.navCtrl.setRoot(DashboardPage);
    }
    checkFocus(e){            
        
     $("#telephone").on("click", function(e) {
            $(".login_footer").addClass("active");
            console.log("active btnb");
    e.stopPropagation();
  });

  $(document).on("click", function(e) {
    e.stopPropagation();
    if ($(e.target).is(".ogin_footer") === false) {
        console.log("remove");
        $(".login_footer").removeClass("active");
    }
  });
    }

} 