import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Nav, Platform } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
import { CommonserviceProvider } from '../../providers/commonservice/commonservice';
import { SearchPage } from '../search/search';
// import { myCartPage } from '../mycart/mycart';
import { CartitemPage } from '../cartitem/cartitem';
import { SignupPage } from '../signup/signup';
import { HomePage } from '../home/home';


@Component({
  selector: 'page-myAccount',
  templateUrl: 'myAccount.html'
})
export class MyAccountPage {
    error:any="";
    
    servicedata:any;
    data:any;
    fname:any;
    lname:any;
    email:any;
    mobilenumber:any;
    address:any;
   
    
    constructor(public platform: Platform,public navCtrl: NavController,private commonservice: CommonserviceProvider,public events: Events) {
        events.publish('allpagecommon');  
        
        if(!localStorage.getItem("islogin"))
        {
           this.navCtrl.setRoot(SignupPage); 
           localStorage.setItem("backtosigninpage",'MyAccountPage');
        }
        else
        {
            this.loadpage();
        }
        this.platform.registerBackButtonAction(() => {
            this.navCtrl.setRoot(DashboardPage);
        });
        
    }
    isValidEmailAddress(emailAddress) 
    {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    }
   
    loadpage()
    {
       
        this.commonservice.waitloadershow();
        this.servicedata="view_profile?userid="+localStorage.getItem("loginuserid");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                this.commonservice.waitloaderhide();
                this.data=res;
                console.log(this.data.data);
                this.fname=this.data.data.firstname;
                this.lname=this.data.data.lastname;
                this.email=this.data.data.email;
                this.mobilenumber=this.data.data.telephone;
                this.address=this.data.data.address;
            }
        )  
    }
    
    gotoback()
    {

        this.navCtrl.setRoot(DashboardPage);
    }
     
    updateprofile()
    {
        this.error="";

        if(this.fname=="")
        {
            this.error="First Name is required";
        }
        else if(this.lname=="")
        {
            this.error="Last Name is required";
        }
        else if(this.email=="")
        {
            this.error="Email is required";
        }
         else if(this.isValidEmailAddress(this.email) == false)
        {
            this.error="Please Enter Valid Email Address";
        }
        else if(this.mobilenumber=="")
        {
            this.error="Mobile Number is required";
        }

        if(this.error=="")
        {
            this.commonservice.waitloadershow();
            
            this.servicedata="update_profile?firstname="+this.fname+"&lastname="+this.lname+"&telephone="+this.mobilenumber+"&email="+this.email+"&address="+this.address+"&userid="+localStorage.getItem("loginuserid");
            this.commonservice.serverdataget(this.servicedata).subscribe(
                res => {
                    this.data = res;
                    this.commonservice.waitloaderhide(); 
                    console.log(this.data);
                    if(this.data.status="true")
                    {
                        this.events.publish('allpagecommon'); 
                        this.navCtrl.setRoot(DashboardPage);
                        this.commonservice.successalert("Profile updated sucessfully"); 
                    }
                    else
                    {
                        this.commonservice.erroralert("Something went wrong");   
                    }
                     
                }
            )
        }
        else
        {
            this.commonservice.erroralert(this.error);    
        }        
    }
    
     gotohome()
    {
        this.navCtrl.setRoot(DashboardPage);
    }
    
    gotosearch()
    {
       
        this.navCtrl.setRoot(SearchPage);
    }
    
    gotocat()
    {
        this.navCtrl.setRoot(HomePage);
    }
    gotobag()
    {
        this.navCtrl.setRoot(CartitemPage);
    }
    gotoaccount()
    {
    }

}