import { Component,ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { PaymentPage } from '../payment/payment';
import { SignupPage } from '../signup/signup';
import { CartitemPage } from '../cartitem/cartitem';
import { CommonserviceProvider } from '../../providers/commonservice/commonservice';
import { ShoppingCartPage } from '../shoppingcart/shoppingCart';
import { myCartPage } from '../mycart/myCart';
import { CategoryDetailsPage} from '../categorydetails/categoryDetails';
import { DashboardPage } from '../dashboard/dashboard';
import { ZoomimgPage } from '../zoomimg/zoomimg';
import * as $ from 'jquery';
import { Nav, Platform } from 'ionic-angular';

@Component({
    
  selector: 'page-addtocart',
  templateUrl: 'addToCart.html'
})
export class AddtocartPage {


    error:any="";    
    servicedata:any;
    data:any;
    image="assets/imgs/product-des.png";
    name="Black/Green Leaf Cotton Silk Screen Printed Kalamkari Fabric";
    purchase_count = 0;
    viewusers = 0;
    protype="SAR 139 / Meter";
    promaterial="slikone";
    color="black";
    quantity="quantity"
    quantity_stock="0";
    proquenty="quantity";
    wishliststatus=0;
    slingleimageshow=1;
    language_id:any;
    singleimg="";
    price="1000";
    is_discount=0;
    discount_price="0";
    sku:any;
    jan:any;
    weight=0;
    description:any;
    colorlist:any;
    quantitylist:any;
    materiallist:any;
    wishbuttonshow=0;
    wishlist=0;
    addimage=[];
    addimageto=[];
    addimagetolist=[];
    selectedColor = '';
    isColorFilterSelected:boolean = false;
    currentImgInSlide;
    sliderWidth ="100%";
     colorImageList = [];
     newColorArray =[];
     newnameArray=[];
     newdescriptionArray=[];
     newQualityArray =[];
     forAllImages = [];
     checkColor;
     type = "METER"
     typeStock = "METERSTOCK"
     showContent:boolean = false;

    @ViewChild(Slides) slides:Slides;

    constructor(
        public platform: Platform,  public navCtrl: NavController,public alertCtrl: AlertController,private commonservice: CommonserviceProvider,public events: Events
         ) {
    
        events.publish('allpagecommon');  
        this.loadpage(); 
        this.platform.registerBackButtonAction(() => {
            if( localStorage.getItem("Addtocartback") == 'CategoryDetailsPage')
            {
                this.navCtrl.setRoot(CategoryDetailsPage);
            }
            else
            {
               this.navCtrl.setRoot(DashboardPage); 
            }
        });
    }

    ionViewWillLeave(){
        this.navCtrl.getPrevious().data.id = localStorage.getItem("details_pdid");
     
    }


    gotoback()
    {
        if( localStorage.getItem("Addtocartback") == 'CategoryDetailsPage')
        {
            this.navCtrl.setRoot(CategoryDetailsPage);
        }
        else
        {
           this.navCtrl.setRoot(DashboardPage); 
        }
    }
   
    
    gopaymentqw()
    {
        this.navCtrl.setRoot(PaymentPage);
    }
    loginuser()
    {
      
        this.servicedata="check_wishlist?product_id="+localStorage.getItem("details_pdid")+"&userid="+localStorage.getItem("loginuserid")+"&lang="+localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                this.data=res;
                
                // if(this.wishliststatus==1)
                // {
                //     this.wishliststatus=0; 
                // }
                // else
                // {
                //     this.wishliststatus=1;
                // }
                if(this.data.status==true)
                {
                    this.wishliststatus=1;
                    
                }
                else
                {
                    this.wishliststatus=0;
                    
                }
                
               
            }
        )  
        
    }
    openmodel(image)
    {
        
         this.navCtrl.setRoot(ZoomimgPage);
    }
    openmodelto()
    {
        this.navCtrl.setRoot(ZoomimgPage);
    }
    clickimage(image)
    {
        this.slingleimageshow=0;
        this.singleimg=image; 
    }
    closesingleimg()
    {
        console.log("sdsa");
       this.slingleimageshow=1;  
    }
   
    closepopup()
    {
        $("#myModal").css("display","none"); 
    }
    opentomodel(image)
    {
        console.log(image);
        
        this.navCtrl.setRoot(ZoomimgPage);
    }
    
    loadpage()
    {

        console.log('test');
        console.log(localStorage.getItem("loginuserid"));
        this.commonservice.waitloadershow();
        if(localStorage.getItem("applanguage")=="er"){
            var setLang = 2;
         }else{
            var setLang = 1;
       }
        this.servicedata="productDetail/"+localStorage.getItem("details_pdid")+"?lang="+setLang;
    
   
    //  this.servicedata="productDetail/"+localStorage.getItem("details_pdid")+"?lang="+localStorage.getItem("applanguage");
    console.log(localStorage.getItem("applanguage"));    
    console.log(this.servicedata);
    console.log(localStorage.getItem("details_pdid"));
    
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {    
                this.showContent =true
                this.commonservice.waitloaderhide();
                this.data = res;  
                console.log(this.data.data.type);
                if(this.data.data.type == 'clothes'){
                    this.type = "METER"
                    this.typeStock = "METERSTOCK"
                }else{
                    this.type = "PIECES"
                    this.typeStock = "PIECESSTOCK"
                }
                console.log(setLang);
                console.log(this.data.data.language_id);
               // if(this.data.data.language_id = setLang){ 
                    this.purchase_count  = this.data.data.purchase_count;                   
                    // this.purchase_count  = 0;                
                    this.viewusers = Math.floor(Math.random() * 101);               
                    this.quantity_stock = this.data.data.stock;                      
                    this.weight = this.data.data.weight;
                    // this.colorlist = this.data.data.color;
                    this.quantitylist = this.data.data.quantity_varients;
                    //alert(this.quantitylist);
                    this.materiallist=this.data.data.material;
                    this.image=this.data.data.image;
                    this.colorImageList = this.data.data.mycolorImg;
                    console.log(this.data.data.mycolorImg);                 
                    if(this.colorImageList){
                        this.color= this.colorImageList[0].color
                        console.log(this.color);
                        for (let i = 0; i < this.colorImageList.length; i++){
                        
                            this.newColorArray.push(this.colorImageList[i].color);
                            
                        //  console.log(this.colorImageList[i].color);
                        }
                    }         

                    this.name=this.data.data.name;
                    if(this.name){
                        for (let i = 0; i < this.name.length; i++){
                            if(this.data.data.language_id = setLang){ 
                            this.newnameArray.push(this.name[i]);
                            }
                        //  console.log(this.colorImageList[i].color);
                        }
                    }  
                    this.protype=this.data.data.quantity;                    
                    localStorage.setItem("scrollimage", JSON.stringify(this.data.data.additionalimage));
            
                    // if(this.colorlist)
                    // {
                    //     this.color=this.data.data.color[0];
                    //     console.log(this.color);
                    // }
                    if(this.quantitylist)
                    {
                        this.quantity=this.data.data.quantity[0];
                    }              
                    
                    if(this.materiallist)
                    {
                        this.promaterial=this.data.data.material[0];
                    }
                    this.price=this.data.data.price;
                    this.discount_price= this.data.data.discount_price;
                    this.is_discount= this.data.data.is_discount;
                    console.log(this.discount_price);
                    this.proquenty="";
                    this.sku=this.data.data.sku;
                    this.jan=this.data.data.jan; //customer view 
                    this.description=this.data.data.description;
                    // if(this.description){
                    //     for (let i = 0; i < this.description.length; i++){
                    //         if(this.data.data.language_id = setLang){ 
                    //         this.newdescriptionArray.push(this.description[i].description);
                    //         }
                    //     //  console.log(this.colorImageList[i].color);
                    //     }
                    // } 
                    $("#description").append(this.data.data.description);
                    this.language_id=this.data.data.language_id
                    console.log(this.data.data.language_id);
                  
                    if(localStorage.getItem("islogin"))
                    {
                    this.loginuser(); 
                    this.wishbuttonshow=1;
                    }
                    this.scrollimage();
                //}
            }
        )    
    }
    Addtocartproduct()
    {
        console.log(this.proquenty);
        if(this.proquenty == "" || this.proquenty == null || this.color =="" || this.color == null){
            this.commonservice.erroralert("Please select quantity");   
        }
        else{
            console.log(localStorage.getItem("details_pdid"));
            var  details_pdid = localStorage.getItem("details_pdid");
            console.log(details_pdid);
            localStorage.setItem(details_pdid, this.quantitylist[this.quantitylist.length-1]);
            var min = localStorage.getItem(details_pdid);
            console.log('atc_min'+min);
    
    
            if(this.price == "" && this.price == null)
            {
              this.price = "0"; 
            }
            
            let device_id="11";
            var a=[];
             let prev_cart = localStorage.getItem("cart"+device_id);
          
            if(prev_cart != null && prev_cart != undefined ){
               let temp = JSON.parse(localStorage.getItem("cart"+device_id));
             
               if(temp.length>0){
              
                   for(let j=0;j<temp.length;j++){
                       if(localStorage.getItem("details_pdid") != temp[j].pid){
                        a.push(temp[j]);
                       }
                      
                   }
               }
            }

            let obj = {
                'name': this.name,
                'pid' : localStorage.getItem("details_pdid"),
                'desc':this.description,
                'stock':this.quantity_stock,
                'weight':this.weight,
                'color':this.color,
                'image':this.image,
                'price':this.price,
                'discount_price':this.discount_price,
                'isDiscount':this.is_discount,
                'qty':this.proquenty,
                'type':this.type
            }
            a.push(obj);

            localStorage.setItem("cart"+device_id, JSON.stringify(a));
            localStorage.setItem("cartproductcount",JSON.stringify(a.length));
          this.navCtrl.setRoot(myCartPage);
        }  
    }
    removewishlist(){
        this.commonservice.waitloadershow();            
        this.servicedata="remove_wishlist?product_id="+localStorage.getItem("details_pdid")+"&userid="+localStorage.getItem("loginuserid");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {               
                this.data = res;              
                this.commonservice.waitloaderhide(); 
                if(this.data.status="true")
                {
                    this.loginuser();
                    this.commonservice.successalert("Product removed from wishlist sucessfully"); 
                }
                else
                {
                    this.commonservice.erroralert("Something went wrong");   
                }
            }
        )
    }
    addwishlist()
    {
        if(localStorage.getItem("islogin")){
            this.commonservice.waitloadershow();            
            this.servicedata="add_wishlist?product_id="+localStorage.getItem("details_pdid")+"&userid="+localStorage.getItem("loginuserid");
            this.commonservice.serverdataget(this.servicedata).subscribe(
                res => {
                   
                    this.data = res;
                  
                    this.commonservice.waitloaderhide(); 
                    if(this.data.status="true")
                    {
                        this.loginuser();
                        this.commonservice.successalert("Product added in your wishlist sucessfully"); 
                    }
                    else
                    {
                        this.commonservice.erroralert("Something went wrong");   
                    }
                }
            )
        }
        else{
            this.commonservice.erroralert("Login first");
            this.navCtrl.setRoot(SignupPage); 
        }
      
    }
    scrollimage()
    {
         this.addimage=JSON.parse(localStorage.getItem("scrollimage")); 
         this.twoscrollimage();
        
    }
    twoscrollimage()
    {
         this.addimage=JSON.parse(localStorage.getItem("scrollimage")); 
        console.log(this.addimage,"add image");
        if(this.addimage != null)
        {
           let i=0;
            for (let bp of this.addimage) 
            {
                this.addimageto.push({"image":bp,"quen":i});
                this.addimagetolist.push({"image":bp,"quen":i});
                i++;
            }
            this.forAllImages = this.addimageto;
        }
       console.log(this.addimageto,"add image to");  
    }
   
    // onColorSelect(evt){
    //    let color = evt.target.value;
    //    this.selectedColor = color;
    //    let anyColorFound = false;
    //     let colorImages=[];
    //     for(var i =0;i < this.data.data.colorImg.length;i++){
    //         if(this.data.data.colorImg[i].color ==color){
    //             anyColorFound = true;
    //           colorImages.push({'image':this.data.data.colorImg[i].image,"quen":i})
    //         }
    //     }
    //     if(anyColorFound){
    //         this.addimagetolist = colorImages;
    //         this.addimageto = colorImages;
    //         if(colorImages.length == 1){
    //             this.sliderWidth = "40%"
    //         }
    //         if(this.addimageto.length == 1){
    //            this.slides.slideTo(0);
    //         }
    //         this.slides.update();
    //     }
    //     else{
    //         this.commonservice.erroralert("No item available");   
    //     }
    // }
    onColorChoose(col){
        let anyColorFound = false;
        this.checkColor = col;
        console.log(col);
        let colorImages=[];
        if (this.colorImageList) {
            for (let i = 0; i < this.colorImageList.length; i++) {
                if (col == this.colorImageList[i].color) {
                    anyColorFound = true;
                  //localStorage.setItem(localStorage.getItem("details_pdid"), JSON.stringify(this.colorImageList[i].color));
                  //localStorage.setItem("colorselect", JSON.stringify(this.colorImageList[i].color));
                  // console.log(localStorage.getItem("details_pdid"), JSON.stringify(this.colorImageList[i].color));
                   // console.log(JSON.parse(localStorage.getItem("colorselect")));
                    this.color=this.colorImageList[i].color;
                        console.log(this.color);
                    if (this.colorImageList[i].img) {
                        console.log(this.colorImageList[i].img.length);
                        for (let j = 0; j < this.colorImageList[i].img.length; j++) {
                            colorImages.push({ 'image': this.colorImageList[i].img[j], "quen": i })
                        }
                    }
                }
            }
        }
      
        if (anyColorFound) {
            this.addimagetolist = colorImages;
            this.addimageto = colorImages;
            if (colorImages.length == 1) {
                this.sliderWidth = "40%";
                this.slides.update();
            }
            if (colorImages.length == 2) {
                this.sliderWidth = "60%";
                this.slides.update();
            }
            if (this.slides) {
                if (this.addimageto.length == 1) {
                    this.slides.slideTo(0);
                }
                this.slides.update();
            }
        }
        else {
            this.commonservice.erroralert("No item available");
        }
    }

    getAllImageData(){
    
        this.checkColor = '';
        this.addimageto = this.forAllImages;
        this.addimagetolist = this.forAllImages;
        this.sliderWidth = "100%";
        this.slides.update();
    }

    activeColor(){
        return {
            active: true,  
        };
    }

    onOtherSelect(evt){
     //   alert("hhiihi");
        console.log(evt.target.value)
        if(evt.target.value == 6){
            const prompt = this.alertCtrl.create({
                title: 'Cloth length',
                message: "Please enter the lenth of clothes(in meters)",
                inputs: [
                  {
                    name: 'length',
                    type: 'number',
                    id:'maxLength10'
                  },
                ],
                buttons: [
                  {
                    text: 'Cancel',
                    handler: data => {
                      console.log('Cancel clicked');
                    }
                  },
                  {
                    text: 'Save',
                    handler: data => {
                   
                      console.log(data,'Saved clicked');
                    }
                  }
                ]
              });
              prompt.present().then(result =>{
                document.getElementById('maxLength10').setAttribute('max','99');
                document.getElementById('maxLength10').addEventListener('keyup', function(r){
                    // if(r.target.value.length>2){
                    //     return false;
                    // }
                    console.log(r);
                    console.log(r.target);
                })
              });
          }
    
          }    
}
