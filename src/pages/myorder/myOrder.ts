import { Component } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { NavController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { OrderDetailsPage } from '../orderDetails/orderDetails';
// import { OrderDetailsPage } from '../orderdetails/orderdetails';
import { DashboardPage } from '../dashboard/dashboard';
import { SearchPage } from '../search/search';
import { MyAccountPage } from '../myAccount/myAccount';
// import { myCartPage } from '../mycart/mycart';
import { CommonserviceProvider } from '../../providers/commonservice/commonservice';
import { CartitemPage } from '../cartitem/cartitem';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-myOrder',
  templateUrl: 'myOrder.html'
})
export class MyOrderPage {
    
    error:any="";
    servicedata:any;
    data:any;
    orderlist:any=[];
    norecourd:any='1';
    constructor(public platform: Platform,public navCtrl: NavController,public events: Events,private commonservice: CommonserviceProvider) {
         events.publish('allpagecommon');
         this.loadpage();  
         this.platform.registerBackButtonAction(() => {
            this.navCtrl.setRoot(DashboardPage);
        });
    }
    
    loadpage()
    {
       
        this.commonservice.waitloadershow();
        this.servicedata="order_history?user_id="+localStorage.getItem("loginuserid")+"&lang="+localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                this.commonservice.waitloaderhide();
                
                this.data=res;
                console.log(this.data);
                console.log(this.data.data);
                console.log(this.data.order_status);
                if(this.data.status!=false)
                {
                     console.log('1');
                    this.norecourd='0';
                    
                    this.orderlist=this.data.data;
                }
                else
                {
                     console.log('2');
                     this.orderlist=[];
                }
               
            }
        ) 
         
    }
   
    goOrderDetails(id)
    {
        localStorage.setItem("orderid",id); 
        this.navCtrl.setRoot(OrderDetailsPage);
    }
    gotoback()
    {       
        this.navCtrl.setRoot(DashboardPage);
    }
      gotohome()
    {       
        this.navCtrl.setRoot(DashboardPage);
    }    
    gotosearch()
    {
        localStorage.setItem('lastpageserch','myorderPage');
        console.log(localStorage.getItem('lastpageserch'));
        this.navCtrl.setRoot(SearchPage);
    }    
    gotocat()
    {
         this.navCtrl.setRoot(HomePage);
    }
    gotobag()
    {
        this.navCtrl.setRoot(CartitemPage);
    }
    gotoaccount()
    {
        this.navCtrl.setRoot(MyAccountPage);
    }

}