import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { AddtocartPage } from '../addtocart/addToCart';
import { CommonserviceProvider } from '../../providers/commonservice/commonservice';
import { DashboardPage } from '../dashboard/dashboard';
import { MyAccountPage } from '../myAccount/myAccount';
import { CategoryDetailsPage } from '../categorydetails/categoryDetails';
import { myCartPage } from '../mycart/myCart';
import { CartitemPage } from '../cartitem/cartitem';
import { CompleterService, CompleterData } from 'ng2-completer';
import * as $ from 'jquery'



@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {

    error:any="";
    servicedata:any;
    data:any;
    productlist=[];
    tabBarElement:any;
    searchText:any;
    
    protected searchStr: string;
    protected captain: string;
    protected dataService: CompleterData;
    protected searchData = [ ];
    protected captains = [ ];
    constructor(private completerService: CompleterService,public navCtrl: NavController,private commonservice: CommonserviceProvider,public events: Events) {
        
         var allproductlist =JSON.parse(localStorage.getItem("allproductlist"));
        console.log(allproductlist);
        if(localStorage.getItem("applanguage")=="er"){
            var setLang = 2;
            
         }else{
            var setLang = 1;
        }
        
        if(allproductlist!=null)
        {
            if(allproductlist.length>0)
            {
               
                let i=0;
                for (let alt of allproductlist) 
                {
                    if(alt.language_id == setLang){ 
                    this.productlist.push({"product_id":alt.product_id,"image":alt.image,"name":alt.name,"price":alt.price,"model":alt.model});
                    i++;
                    }
                }

            }
           
        }
        console.log(this.productlist);
           this.dataService = completerService.local(this.productlist, 'name', 'name'); 
        events.publish('allpagecommon');
         this.dataService = completerService.local(this.productlist, 'name', 'name');
      
       
    }
   

     
    
    gocatgorydetails(id)
    {
       
        localStorage.setItem("details_pdid",id);
        this.navCtrl.setRoot(AddtocartPage);
    }
   
    gotohome()
    {
        this.navCtrl.setRoot(DashboardPage);
    }
    
    gotosearch()
    {
       
    }
    
    gotocat()
    {
        this.navCtrl.setRoot(DashboardPage);
    }
    gotobag()
    {
        this.navCtrl.setRoot(CartitemPage);
    }
    gotoaccount()
    {
        this.navCtrl.setRoot(MyAccountPage);
    }
    gotofunction()
    {
        this.searchText=this.searchStr;
    }
    goback()
    {
        if(localStorage.getItem('lastpageserch')=='DashboardPage')
        {
           this.navCtrl.setRoot(DashboardPage); 
        }
        else
        {
            this.navCtrl.setRoot(CategoryDetailsPage); 
        }
    }
    
   
    

}