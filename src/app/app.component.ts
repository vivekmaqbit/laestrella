import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MenuController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { CommonserviceProvider } from '../providers/commonservice/commonservice';
import { TranslateService } from '@ngx-translate/core';
import { HomePage } from '../pages/home/home';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { ListPage } from '../pages/list/list';
import { FaqPage } from '../pages/faq/faq';
import { SignupPage } from '../pages/signup/signup';
import { MyWishlistPage } from '../pages/mywishlist/mywishlist';
import { MyOrderPage } from '../pages/myorder/myOrder';
// import { MyOrderPage } from '../pages/myOrder/myOrder';
import { CartitemPage } from '../pages/cartitem/cartitem';
import { MyAccountPage } from '../pages/myAccount/myAccount';
import { SplishPage } from '../pages/splishpage/splishpage';
import { format } from 'url';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;  
    rootPage: any = SplishPage;
    username="Guest user";
    useraddress="15 3C, DRM Rd, Saket Nagar, Indore";
    userimage="assets/imgs/thumb.png";
    pages: Array<{title: string, component: any}>;
    error:any="";
    lang:any="en";
    removesplash:any="any";
    logoutshow:number=0;    
    servicedata:any;
    data:any;

    constructor(private alertCtrl: AlertController,public menuCtrl: MenuController,public events: Events,public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,private commonservice: CommonserviceProvider,public translate: TranslateService) {
        this.initializeApp();
        this.allservicecalldata();
        localStorage.setItem("setcatlist", '1');
        localStorage.setItem("setinterval", '0');        
        this.statusBar.backgroundColorByHexString('#000000');
        //console.log(this.nav.getActive());

        if(localStorage.getItem("applanguage"))
        {
            if(localStorage.getItem("applanguage") == 'en')
            {
                this.translate.setDefaultLang('en');
                localStorage.setItem("applanguage", 'en');
                this.lang=false;
                console.log(this.lang);
            }
            else
            {
                console.log(this.lang);
                this.translate.setDefaultLang('er');
                localStorage.setItem("applanguage", 'er');
                this.lang=true;
            }
        }
        else
        {
            
            this.translate.setDefaultLang('en');
            localStorage.setItem("applanguage", 'en');
            console.log(localStorage.getItem("applanguage"));
        }
        
        events.subscribe('allpagecommon', () => {
            this.allpagecommon();
           
        });
        
        this.removesplash = setInterval(() => {
                if(localStorage.getItem("setinterval")== '1')
                {
                    if(localStorage.getItem("islogin"))
                    {
                        this.rootPage = DashboardPage;
                        this.logoutshow=1;
                        clearInterval(this.removesplash);
                    }
                    else
                    {
                        this.rootPage = DashboardPage;
                        this.logoutshow=0;
                        clearInterval(this.removesplash);
                    }
                }
                else
                {
                    localStorage.setItem("setinterval", '1');
                }
            }, 3000);
    }
  

    initializeApp() {
            this.platform.ready().then(() => {
            this.statusBar.styleDefault();      
            this.splashScreen.hide();
            this.statusBar.backgroundColorByHexString('#000000');
            this.statusBar.overlaysWebView(false);
            this.statusBar.backgroundColorByHexString('#000000');
          //  $("#checkboxid").removeAttr("checked");
            $(".switch:not([checked])").on('change' , function(){
                $(".switch").not(this).prop("checked" , false);
                console.log("wistch");
              });
        });
        this.platform.registerBackButtonAction(() => {
            let alert = this.alertCtrl.create
        ({
            title: 'Confirm',
            message: 'Do you really want to exit ?',
            buttons: 
        [{
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              
            }
            },
            {
                text: 'Ok',
                handler: () => 
                {
                 // this.deleteitem(id);
                 this.platform.exitApp();
                }
            }
        ]});
        alert.present();
            
          });
        
     
    }
 

    gotohomepage(page) {
        
        this.nav.setRoot(DashboardPage);
        this.menuCtrl.close();
        
    }
    gotocategorypage(page) {
        
        this.nav.setRoot(HomePage);
        this.menuCtrl.close();
        
    }
      
    
    gotoorderpage(page) {
        this.nav.setRoot(MyOrderPage);
        this.menuCtrl.close();
        
    }
    gotocartpage(page) {
       this.nav.setRoot(CartitemPage);
       this.menuCtrl.close();
       
    }
    gotowishlistpage() {
        this.nav.setRoot(MyWishlistPage);
        this.menuCtrl.close();
        
    }
    gotosignpage() {
        localStorage.removeItem("loginotp");
        localStorage.removeItem("loginuserid");
        localStorage.removeItem("islogin");
        localStorage.removeItem("backtosigninpage");      
        this.menuCtrl.close();       
        this.nav.setRoot(SignupPage);
        this.logoutshow=0;
        this.commonservice.successalert("Logout sucessfully"); 
        this.events.publish('allpagecommon');
        
    }
    signpage() {
       
        this.nav.setRoot(SignupPage);
        this.menuCtrl.close();
       
    }
    gotomyaccount() {
        this.nav.setRoot(MyAccountPage);
        this.menuCtrl.close();
       
    }
    gotofaq() {
        this.nav.setRoot(FaqPage);
        this.menuCtrl.close();
       
    }
    allpagecommon()
    {
        if(localStorage.getItem("islogin"))
        {
            this.servicedata="view_profile?userid="+localStorage.getItem("loginuserid")+"&lang="+localStorage.getItem("applanguage");
            this.commonservice.serverdataget(this.servicedata).subscribe(
                res => {

                    this.data=res;
                    console.log(this.data);
                    console.log(this.data.data.firstname);
                    if(this.data.data.firstname == "" || this.data.data.firstname == null ){
                        this.username="Guest user";
                    }else{
                        this.username=this.data.data.firstname +" "+ this.data.data.lastname;
                    }
                    this.useraddress=this.data.data.address;
                    this.logoutshow=1;
                }
            )     
        }
        else{
            this.username="Guest user";  
            this.logoutshow=0;
            //this.email="admmin@gmail.com";
          }
    }
    
    allservicecalldata()
    {
       
        this.servicedata="category_list/"+localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                
                this.data=res;
                console.log(this.data);
               //  console.log('category_list');
                
                localStorage.setItem("setcatlist", JSON.stringify(this.data));
                this.allproductdata();
            }
        ) 
        
    }
    allproductdata()
    {
        this.servicedata="allcategory_product/"+localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                this.data=res;
             //   console.log('allproductdata');
              console.log(this.data);
                this.allproductdatatwo();
                localStorage.setItem("allcategoryproductlist", JSON.stringify(this.data.data));
                
            }
        )  
        
    }
     allproductdatatwo()
    {
        this.servicedata="allproduct_list/"+localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                this.data=res;
              //  console.log('allproductdatatwo');
                console.log(this.data);                
                localStorage.setItem("allproductlist", JSON.stringify(this.data.data));
                
                
            }
        )  
        
    }
    changelaguagefunction()
    {        
        this.allservicecalldata();
        if(this.lang == false)
        {
            this.translate.setDefaultLang('en');
            localStorage.setItem("applanguage", 'en');
            this.nav.setRoot(DashboardPage);
            //this.nav.setRoot(DashboardPage);
            this.menuCtrl.close();
        }
        else
        {
            this.translate.setDefaultLang('er');
            localStorage.setItem("applanguage", 'er');
            this.nav.setRoot(DashboardPage);   
          //  this.nav.setRoot(DashboardPage);
            this.menuCtrl.close();        
            
        }
    }
   
}
