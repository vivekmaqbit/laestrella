import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule,HttpClient } from '@angular/common/http';
import { CommonserviceProvider } from '../providers/commonservice/commonservice';
import { TranslateModule, TranslateLoader} from '@ngx-translate/core';
import { TranslateHttpLoader} from '@ngx-translate/http-loader';
import { FilterPipe} from './filter.pipe';
import { IonicImageViewerModule } from 'ionic-img-viewer';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { ListPage } from '../pages/list/list';
import { SplishPage } from '../pages/splishpage/splishpage';
import { PinchZoomModule } from 'ngx-pinch-zoom';



import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SearchPage } from '../pages/search/search';
import { SignupPage } from '../pages/signup/signup';
import { VerificationPage  } from '../pages/verification/verification';
import { FaqPage } from '../pages/faq/faq';
import { CategoryDetailsPage } from '../pages/categorydetails/categoryDetails';
import {AddtocartPage} from '../pages/addtocart/addToCart';
import { ShoppingCartPage } from '../pages/shoppingcart/shoppingCart';
import { myCartPage } from '../pages/mycart/myCart';
import { OrderDetailsPage } from '../pages/orderDetails/orderDetails';
// import { CategoryDetailsPage } from '../pages/categorydetails/categorydetails';
// import { AddtocartPage } from '../pages/addtocart/addtocart';
// import { ShoppingCartPage } from '../pages/shoppingcart/shoppingcart';
// import { myCartPage } from '../pages/mycart/mycart';
import { PaymentPage } from '../pages/payment/payment';
import { OrderConfirmedPage } from '../pages/orderConfirmed/orderConfirmed';
import { DelieveryTimelinePage } from '../pages/delieveryTimeline/delieveryTimeline';
// import { OrderDetailsPage } from '../pages/orderdetails/orderdetails';
import { MyAccountPage } from '../pages/myAccount/myAccount';
import { NotificationPage } from '../pages/notification/notification';
import { MyWishlistPage } from '../pages/mywishlist/mywishlist';
import { MyOrderPage } from '../pages/myorder/myOrder';
import { SigninOtpPage } from '../pages/signinOtp/signinOtp';
import { CartitemPage } from '../pages/cartitem/cartitem';
import { MobilePage } from '../pages/mobile/mobile';
import { ZoomimgPage } from '../pages/zoomimg/zoomimg';

import { Ng2CompleterModule } from "ng2-completer";

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/language/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    SearchPage,
    SignupPage,
    VerificationPage,
    FaqPage,
    CategoryDetailsPage,
    AddtocartPage,
    ShoppingCartPage,
    myCartPage,
    PaymentPage,
    OrderConfirmedPage,
    DelieveryTimelinePage,
    OrderDetailsPage,
    MyAccountPage,
    NotificationPage,
    MyWishlistPage,
    MyOrderPage,
    CartitemPage,
    FilterPipe,
    MobilePage,
    ZoomimgPage,
    DashboardPage,
    SplishPage,
    SigninOtpPage    
  ],
  imports: [
   
    BrowserModule,
    HttpClientModule,
    Ng2CompleterModule,
    IonicModule.forRoot(MyApp, {statusbarPadding: true}),
    IonicImageViewerModule,
     PinchZoomModule,
  
    TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    SearchPage,
    SignupPage,
    VerificationPage,
    FaqPage,
    CategoryDetailsPage,
    AddtocartPage,
    ShoppingCartPage,
    myCartPage,
    PaymentPage,
    OrderConfirmedPage,
    DelieveryTimelinePage,
    OrderDetailsPage,
    MyAccountPage,
    NotificationPage,
    MyWishlistPage,
    CartitemPage,
    MyOrderPage,
    ZoomimgPage,
    MobilePage,
    DashboardPage,
    SplishPage, 
    SigninOtpPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CommonserviceProvider
  ]
})
export class AppModule {}
