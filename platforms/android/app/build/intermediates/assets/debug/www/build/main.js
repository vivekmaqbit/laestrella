webpackJsonp([2],{

/***/ 14:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommonserviceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CommonserviceProvider = /** @class */ (function () {
    function CommonserviceProvider(alertCtrl, loadingCtrl, toastCtrl, http) {
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.http = http;
        //serviceurl:any="http://www.omsoftware.us/overachievers/laestrellas/api/webservice/";
        //serviceurl:any = "http://www.omsoftware.us/overachievers/laestrellas/api/"
        //serviceurl:any="https://www.omsoftware.org/laestrellas/api/"
        this.serviceurl = "https://laestrella.com.sa/laestrellas/api/";
        console.log('Hello CommonserviceProvider Provider');
    }
    CommonserviceProvider.prototype.erroralert = function (msg) {
        var alerterror = this.alertCtrl.create({
            title: 'Error!',
            subTitle: msg,
            buttons: [
                {
                    text: 'Ok',
                    role: 'cancel',
                    handler: function () {
                        console.log('Ok clicked');
                    }
                }
            ]
        });
        alerterror.present();
    };
    CommonserviceProvider.prototype.successalert = function (msg) {
        var alerterror = this.alertCtrl.create({
            title: 'Success!',
            subTitle: msg,
            buttons: [
                {
                    text: 'Ok',
                    role: 'cancel',
                    handler: function () {
                        console.log('Ok clicked');
                    }
                }
            ]
        });
        alerterror.present();
    };
    CommonserviceProvider.prototype.toastalert = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    CommonserviceProvider.prototype.waitloadershow = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.loading.present();
    };
    CommonserviceProvider.prototype.waitloaderhide = function () {
        this.loading.dismiss();
    };
    CommonserviceProvider.prototype.serverdataget = function (servicedata) {
        if (servicedata) {
            return this.http.get(this.serviceurl + servicedata, {});
        }
    };
    CommonserviceProvider.prototype.serverdatapost = function (servicedataa) {
        console.log(servicedataa);
        if (servicedataa) {
            return this.http.post(this.serviceurl + servicedataa, {});
        }
    };
    CommonserviceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* ToastController */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], CommonserviceProvider);
    return CommonserviceProvider;
}());

//# sourceMappingURL=commonservice.js.map

/***/ }),

/***/ 154:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 154;

/***/ }),

/***/ 17:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_commonservice_commonservice__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__categorydetails_categoryDetails__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__search_search__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__myAccount_myAccount__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__addtocart_addToCart__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__cartitem_cartitem__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__home_home__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// import { CategoryDetailsPage } from '../categorydetails/categorydetails';


// import { myCartPage } from '../mycart/mycart';

// import { AddtocartPage } from '../addtocart/addtocart';



var DashboardPage = /** @class */ (function () {
    function DashboardPage(platform, navCtrl, commonservice, alertCtrl, events) {
        var _this = this;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.commonservice = commonservice;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.error = "";
        this.catlist = [];
        this.productlist = [];
        this.bannerlist = '';
        this.cartvalue = "0";
        events.publish('allpagecommon');
        this.platform.registerBackButtonAction(function () {
            var alert = _this.alertCtrl.create({
                title: 'Confirm',
                message: 'Do you really want to exit ?',
                buttons: [{
                        text: 'Cancel',
                        role: 'cancel',
                        handler: function () {
                        }
                    },
                    {
                        text: 'Ok',
                        handler: function () {
                            // this.deleteitem(id);
                            _this.platform.exitApp();
                        }
                    }
                ]
            });
            alert.present();
        });
        this.bannerlistfunction();
        console.log(localStorage.getItem("cartproductcount"));
        if (localStorage.getItem("cartproductcount") == null || localStorage.getItem("cartproductcount") == '') {
            this.cartvalue = "0";
        }
        else {
            this.cartvalue = localStorage.getItem("cartproductcount");
        }
        //this.cartvalue=localStorage.getItem("cartproductcount");
        if (localStorage.getItem("setcatlist") == '1') {
            console.log('2');
            this.allservicecalldata();
        }
        else {
            console.log('1');
            this.loadpage();
        }
    }
    DashboardPage.prototype.allservicecalldata = function () {
        var _this = this;
        console.log('category_list');
        this.servicedata = "category_list/" + localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            //  console.log('333category_list');
            _this.data = res;
            //   console.log('1212category_list');
            localStorage.setItem("setcatlist", JSON.stringify(_this.data));
            // console.log('setcat');
            _this.allproductdata();
        });
    };
    DashboardPage.prototype.allproductdata = function () {
        var _this = this;
        this.servicedata = "allcategory_product/" + localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            console.log('allproductdata');
            console.log(_this.data);
            _this.allproductdatatwo();
            localStorage.setItem("allcategoryproductlist", JSON.stringify(_this.data.data));
        });
    };
    DashboardPage.prototype.allproductdatatwo = function () {
        var _this = this;
        this.servicedata = "allproduct_list/" + localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            // console.log('allproductdatatwo');
            console.log(_this.data);
            localStorage.setItem("allproductlist", JSON.stringify(_this.data.data));
            _this.loadpage();
        });
    };
    DashboardPage.prototype.bannerlistfunction = function () {
        var _this = this;
        if (localStorage.getItem("applanguage") == "er") {
            var setLang = 2;
        }
        else {
            var setLang = 1;
        }
        this.servicedata = "city?lang=" + localStorage.getItem("applanguage");
        ;
        var bannerlist = JSON.parse(localStorage.getItem("banner"));
        console.log("deom" + bannerlist);
        this.servicedata = "banner?lang=" + setLang;
        console.log(this.servicedata);
        if (localStorage.getItem("applanguage") == "er") {
            var setLang = 2;
        }
        else {
            var setLang = 1;
        }
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            _this.bannerlist = _this.data.data;
            console.log(_this.bannerlist);
        });
    };
    DashboardPage.prototype.openNav = function () {
        document.getElementById("mySidenav").style.display = "block";
    };
    DashboardPage.prototype.closeNav = function () {
        document.getElementById("mySidenav").style.display = "none";
    };
    DashboardPage.prototype.chooseNav = function (id, name) {
        document.getElementById("mySidenav").style.display = "none";
        localStorage.setItem("cat_id", id);
        localStorage.setItem("cat_name", name);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__categorydetails_categoryDetails__["a" /* CategoryDetailsPage */]);
    };
    DashboardPage.prototype.loadpage = function () {
        console.log(JSON.parse(localStorage.getItem("setcatlist")));
        var allcatlist = JSON.parse(localStorage.getItem("setcatlist"));
        if (localStorage.getItem("applanguage") == "er") {
            var setLang = 2;
        }
        else {
            var setLang = 1;
        }
        if (allcatlist != null) {
            if (allcatlist.length > 0) {
                var i = 0;
                for (var _i = 0, allcatlist_1 = allcatlist; _i < allcatlist_1.length; _i++) {
                    var alt = allcatlist_1[_i];
                    //   console.log("dblang"+ alt.language_id);
                    // console.log("setLang"+setLang);
                    if (alt.language_id == setLang) {
                        this.catlist.push({ "category_id": alt.category_id, "image": alt.image, "name": alt.name });
                        i++;
                    }
                    //this.catlist.push({"category_id":alt.category_id,"image":alt.image,"name":alt.name});
                    //i++;
                }
            }
            this.Allproductlist();
        }
    };
    DashboardPage.prototype.Allproductlist = function () {
        var allproductlist = JSON.parse(localStorage.getItem("allcategoryproductlist"));
        if (allproductlist != null) {
            if (allproductlist.length > 0) {
                for (var _i = 0, _a = this.catlist; _i < _a.length; _i++) {
                    var cat = _a[_i];
                    //console.log(this.catlist); 
                    var catname = cat.name;
                    // var discount_price =cat.discount_price;
                    // var is_discount = cat.is_discount;
                    //console.log("dashcatname"+catname);
                    var catid = cat.category_id;
                    var catimage = cat.image;
                    var catlanguageid = cat.language_id;
                    var catproduct = [];
                    var i = 0;
                    //console.log(localStorage.getItem("applanguage"));
                    if (localStorage.getItem("applanguage") == "er") {
                        var setLang = 2;
                    }
                    else {
                        var setLang = 1;
                    }
                    for (var _b = 0, allproductlist_1 = allproductlist; _b < allproductlist_1.length; _b++) {
                        var pt = allproductlist_1[_b];
                        if (pt.category_id == catid) {
                            i++;
                            if (i <= 4) {
                                if (pt.language_id == setLang) {
                                    catproduct.push({ pt: pt });
                                }
                            }
                        }
                    }
                    this.productlist.push({ 'catname': catname, 'catid': catid, 'catimage': catimage, 'product': catproduct, 'catlanguageid': catproduct });
                    //   console.log("sdsd");  language_id
                }
                // this.loadpage();
                //  console.log("categoryproduct list ");
                console.log(this.productlist);
            }
        }
    };
    DashboardPage.prototype.gocatgorydetails = function (id, name) {
        localStorage.setItem("cat_id", id);
        localStorage.setItem("cat_name", name);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__categorydetails_categoryDetails__["a" /* CategoryDetailsPage */]);
    };
    DashboardPage.prototype.gotosearch = function () {
        localStorage.setItem('lastpageserch', 'DashboardPage');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__search_search__["a" /* SearchPage */]);
    };
    DashboardPage.prototype.goproductetails = function (id) {
        localStorage.setItem("details_pdid", id);
        localStorage.setItem("Addtocartback", 'DashboardPage');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__addtocart_addToCart__["a" /* AddtocartPage */]);
    };
    DashboardPage.prototype.gotobag = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__cartitem_cartitem__["a" /* CartitemPage */]);
    };
    DashboardPage.prototype.gotoaccount = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__myAccount_myAccount__["a" /* MyAccountPage */]);
    };
    DashboardPage.prototype.gotocat = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__home_home__["a" /* HomePage */]);
    };
    DashboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-dashboard',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/dashboard/dashboard.html"*/'\n<ion-header>\n    <div class="headerbg_bl">\n            <div class="container" style="background-color: black !important;">\n                <div style="    display: flex;\n                justify-content: center;\n                align-items: center;">\n                    <div style="flex: 1;order: 0;" menuToggle> \n                        <!-- width: 20%;float: left; -->\n                        <a  style="background-color: #000000 !important;" ><img name="menu" class="LefFix" src="assets/images/menu.png"> </a>\n                    </div>\n                    <!-- width: 40%;float: left; -->\n                    <div style="flex: 2;text-align: center;position: relative">\n                        <img src="assets/images/logo1.png" class="MiddleFix">   \n                    </div>\n                    <!-- width: 30%;float: right; -->\n                    <div  (click)="gotobag()" style="flex: 1;position: relative;">\n                        <img src="assets/images/carrt.png" class="RightFix"><span class="catvalue">{{cartvalue}}</span>\n                    </div>\n                </div>\n            </div>\n    </div>\n</ion-header>\n\n<ion-content padding >\n    <div id="mySidenav" class="sidenav" style="background-color: white;    z-index: 999999;top:  -2.1%;position: absolute;left: 45%;  height: 55vh; border-bottom-left-radius: 18px; border-top-left-radius: 15px;">\n        <a  (click)="closeNav()" style="    font-size: 23px;float: left;cursor: pointer; padding-top: 12px;color: white !important;padding-left: 3px;padding-right: 16px;font-weight: 700; ">&times;</a>\n        <h4 class="sidenavh4" >{{"MOSTRECOMMENDED" | translate}}</h4>\n        <div *ngFor="let scl of catlist" ><a  (click)="chooseNav(scl.category_id,scl.name)"  class="cet" > {{scl.name}}</a>\n            <hr></div>\n    </div>\n    <div class="main">\n        <div class="ContentBody">\n            \n            <ion-row (click)="gotosearch()">\n                <ion-col>\n                    <div class="form-group" style="width:100%">\n                        <div class="input-group">\n                            <span class="input-group-addon Caddon"><img class="searchimg" src="assets/images/search.png"></span>\n                            <input id="text" type="text" class="form-control Ccontrol" name="text"\n                                placeholder="{{\'SEARCHYOURPRODUCT\' | translate}}" style="height: 42px;" required>\n                        </div>\n                    </div>\n                </ion-col>\n            </ion-row>\n            <ion-row>\n                \n                <ion-slides slider="slider" *ngIf="bannerlist && bannerlist.length" autoplay="2000" class="slideroption" pager="true" loop="true"\n                    speed="300">\n                    <ion-slide *ngFor="let slides of bannerlist">\n                        <img src="{{slides.image}}" />\n                    </ion-slide>\n                </ion-slides>\n            </ion-row>\n            <br>\n            <div class="">\n                <div class="row" *ngFor="let pt of productlist">\n                    <div class="col-xs-12">\n                        <div style="margin-bottom: 10px;"> \n                            <div style="display:inline-block;text-align:left"><b>{{pt.catname}} </b></div>\n                            <div style="display:inline-block;float:right;color: #337ab7;" (click)="gocatgorydetails(pt.catid,pt.catname)">{{"VIEWALL" | translate}}</div>\n                        </div>\n                    </div>\n                    <div class="col-xs-6" style="padding: 0 5px;" *ngFor="let prot of pt.product" (click)="goproductetails(prot.pt.product_id)">\n                        <div class="card" style="padding:5px;background-color:white">\n                            <img src="{{prot.pt.image}}" class="card-img-top image">\n                            <p class="text-center" style="margin: 5px 0;line-height: 20px;"><strong >{{prot.pt.name |  slice:0:30}}</strong></p>\n                           <div style="text-align: center;" *ngIf="prot.pt.is_discount == 1">\n                            <span class="text-center" style="    color: #000;\n                            margin: 0;\n                            line-height: 20px;\n                            font-size: 14px;\n                            font-weight: 500;">SAR {{prot.pt.discount_price}}</span>\n                            <span class="text-center" style="font-size: 13px;text-decoration: line-through;\n                            color: grey;\n                            margin: 0 0 0 10px;\n                            line-height: 20px;">SAR {{prot.pt.price}}</span>            \n                           </div>\n                            <div *ngIf="prot.pt.is_discount == 0">\n                            <p class="text-center" style="color: grey;margin: 0; line-height: 20px;">SAR {{prot.pt.price}}</p>\n                           </div>\n            <div class="color_boxes" style="margin: 5px 0 10px;">\n<div class="color-box"  [ngStyle]="{\'background-color\':clorr.color,\'border\':none}" *ngFor="let clorr of prot.pt.mycolorImg" ></div>\n                            </div>                            \n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>  \n    </div>  \n</ion-content>\n\n<ion-footer>\n         <div style="display:flex;">\n            <div style=\'flex:1;text-align:center\'>\n                <a class="iconname"  style="color:black;font-size: 10px;"> \n                    <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/home.png">\n                    <br>\n                    {{"HOME" | translate}} \n                </a>\n            </div> \n            <div (click)="gotosearch()"  style=\'flex:1;text-align:center\'>\n                <a class="iconname" style="color:black;font-size: 10px;"> \n                    <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/search.png">\n                    <br>\n                    {{"SEARCH" | translate}}</a>\n            </div>\n            <div (click)="gotocat()"  style=\'flex:1;text-align:center\'>\n                <a class="iconname" style="color:black;font-size: 10px;">  \n                    <img  style="    height: 18px;" src="assets/imgs/icon-images/list.png">\n                    <br>\n                    {{"CATEGORY" | translate}} </a>\n            </div>\n            <div (click)="gotobag()"  style=\'flex:1;text-align:center\'>\n                <a class="iconname" style="color:black;font-size: 10px;"> \n                    <img class="img-fluid" style="height: 18px;" src="assets/imgs/icon-images/shopping-bag.png">\n                    <br>\n                    {{"BAG" | translate}}\n                </a>\n            </div>\n            <div (click)="gotoaccount()"  style=\'flex:1;text-align:center\'>\n                <a   class="iconname" style="color:black;font-size: 10px;"> \n                    <img class="img-fluid" style="height: 18px;"  src="assets/imgs/icon-images/round-account-button-with-user-inside.png">\n                    <br>\n                    {{"MYACCOUNT" | translate}} </a>\n            </div>\n         </div>\n</ion-footer>\n\n'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/dashboard/dashboard.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_commonservice_commonservice__["a" /* CommonserviceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */]])
    ], DashboardPage);
    return DashboardPage;
}());

//# sourceMappingURL=dashboard.js.map

/***/ }),

/***/ 18:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_commonservice_commonservice__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__categorydetails_categoryDetails__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__search_search__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__myAccount_myAccount__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__cartitem_cartitem__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__dashboard_dashboard__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






// import { CategoryDetailsPage } from '../categorydetails/categorydetails';


// import { myCartPage } from '../mycart/mycart';


var HomePage = /** @class */ (function () {
    function HomePage(platform, navCtrl, commonservice, alertCtrl, events) {
        var _this = this;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.commonservice = commonservice;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.error = "";
        this.catlist = [];
        events.publish('allpagecommon');
        this.loadpage();
        this.platform.registerBackButtonAction(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__dashboard_dashboard__["a" /* DashboardPage */]);
        });
    }
    HomePage.prototype.openNav = function () {
        document.getElementById("mySidenav").style.display = "block";
    };
    HomePage.prototype.closeNav = function () {
        document.getElementById("mySidenav").style.display = "none";
    };
    HomePage.prototype.chooseNav = function (id, name) {
        document.getElementById("mySidenav").style.display = "none";
        localStorage.setItem("cat_id", id);
        localStorage.setItem("cat_name", name);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__categorydetails_categoryDetails__["a" /* CategoryDetailsPage */]);
    };
    HomePage.prototype.loadpage = function () {
        //this.commonservice.waitloadershow();
        console.log(localStorage.getItem("setcatlist"));
        var allcatlist = JSON.parse(localStorage.getItem("setcatlist"));
        if (allcatlist != null) {
            if (allcatlist.length > 0) {
                var i = 0;
                if (localStorage.getItem("applanguage") == "er") {
                    var setLang = 2;
                }
                else {
                    var setLang = 1;
                }
                for (var _i = 0, allcatlist_1 = allcatlist; _i < allcatlist_1.length; _i++) {
                    var alt = allcatlist_1[_i];
                    if (alt.language_id == setLang) {
                        this.catlist.push({ "category_id": alt.category_id, "image": alt.image, "name": alt.name });
                        i++;
                    }
                }
            }
        }
        console.log("categorypage" + this.catlist);
    };
    HomePage.prototype.gocatgorydetails = function (id, name) {
        if (localStorage.getItem("applanguage") == "ar") {
            var setLang = 2;
            localStorage.setItem("cat_id", id);
            localStorage.setItem("cat_name", name);
        }
        else {
            var setLang = 1;
            localStorage.setItem("cat_id", id);
            localStorage.setItem("cat_name", name);
        }
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__categorydetails_categoryDetails__["a" /* CategoryDetailsPage */]);
    };
    HomePage.prototype.gotosearch = function () {
        localStorage.setItem('lastpageserch', 'HomePage');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__search_search__["a" /* SearchPage */]);
    };
    HomePage.prototype.gotohome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__dashboard_dashboard__["a" /* DashboardPage */]);
    };
    HomePage.prototype.gotobag = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__cartitem_cartitem__["a" /* CartitemPage */]);
    };
    HomePage.prototype.gotoaccount = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__myAccount_myAccount__["a" /* MyAccountPage */]);
    };
    HomePage.prototype.gotoback = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__dashboard_dashboard__["a" /* DashboardPage */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/home/home.html"*/'\n<ion-header>\n        <div class="headerbg_wh">\n    <div class="container">\n            <!-- style=" padding-top: 20px; padding-bottom: 20px;" -->\n        <span (click)="gotoback()" style="font-size:20px; cursor:pointer; " ><i class="fa fa-arrow-left"  aria-hidden="true" style="font-size: 20px;  padding-left: 10px;"></i> &nbsp; &nbsp; &nbsp;<small style="font-weight: 500;">{{"CATEGORY" | translate}}</small> </span>\n        <span (click)="gotosearch()" style="font-size:20px; cursor:pointer;float: right;"  ><i class="fa fa-search"  aria-hidden="true" style="font-size: 20px;  padding-left: 10px;"></i></span>\n    </div>\n    </div>\n</ion-header>\n\n<ion-content padding style="background-color: rgb(245, 244, 244);" >\n    <div id="mySidenav" class="sidenav" style="background-color: white;top: 0.9%;position: absolute;left: 45%;  height: 55vh; border-bottom-left-radius: 18px; border-top-left-radius: 15px;">\n        <a  (click)="closeNav()" style="    font-size: 23px;float: left;cursor: pointer; padding-top: 12px;color: white !important;padding-left: 3px;padding-right: 16px;font-weight: 700; ">&times;</a>\n        <h4 class="sidenavh4" >{{"MOSTRECOMMENDED" | translate}}</h4>\n        <div *ngFor="let scl of catlist" ><a  (click)="chooseNav(scl.category_id,scl.name)"  class="cet" > {{scl.name}}</a>\n            <hr></div>\n        \n    </div>\n    <div class="container-fluid" style="padding: 0px;">\n        <div class="container" style="padding: 0px;">\n            <div class="row" style="width: 100%;margin-right: 0px;margin-left: 0px; ">           \n                <div *ngFor="let cl of catlist" (click)="gocatgorydetails(cl.category_id,cl.name)" class="col-xs-6 col-sm-6" style="flex-basis: 49%;-webkit-flex-basis: 49%;padding: 4px;margin-bottom:15px;border: 0.4px solid #e4dbdb">\n                    <div class="card" >\n                        <a >  <img class="card-img-top image" style="height: 25vh;" src="{{cl.image}}" alt="Photo of sunset"></a>\n                        <div class="middle">\n                        </div> \n                        <div class="card-body" style="display: flex;\n                        justify-content: center;\n                        min-height: 10vh;\n                        align-items: center;">\n                            <p class="text-center font-weight-bold" style="padding:5px;line-height:21px;font-size: 15px;font-weight: 600;margin:0;">{{cl.name | translate }}</p>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>    \n</ion-content>\n\n<ion-footer>\n        <div style="display:flex;">\n           <div (click)="gotohome()" style=\'flex:1;text-align:center\'>\n               <a class="iconname"  style="color:black;font-size: 10px;"> \n                   <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/home.png">\n                   <br>\n                   {{"HOME" | translate}} \n               </a>\n           </div> \n           <div (click)="gotosearch()"  style=\'flex:1;text-align:center\'>\n               <a class="iconname" style="color:black;font-size: 10px;"> \n                   <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/search.png">\n                   <br>\n                   {{"SEARCH" | translate}}</a>\n           </div>\n           <div   style=\'flex:1;text-align:center\'>\n            <!-- (click)="gotocatpage()" -->\n               <a class="iconname" style="color:black;font-size: 10px;">  \n                   <img  style="    height: 18px;" src="assets/imgs/icon-images/list.png">\n                   <br>\n                   {{"CATEGORY" | translate}} </a>\n           </div>\n           <div (click)="gotobag()"  style=\'flex:1;text-align:center\'>\n               <a class="iconname" style="color:black;font-size: 10px;"> \n                   <img class="img-fluid" style="height: 18px;" src="assets/imgs/icon-images/shopping-bag.png">\n                   <br>\n                   {{"BAG" | translate}}\n               </a>\n           </div>\n           <div (click)="gotoaccount()"  style=\'flex:1;text-align:center\'>\n               <a   class="iconname" style="color:black;font-size: 10px;"> \n                   <img class="img-fluid" style="height: 18px;"  src="assets/imgs/icon-images/round-account-button-with-user-inside.png">\n                   <br>\n                   {{"MYACCOUNT" | translate}} </a>\n           </div>\n        </div>\n</ion-footer>'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_commonservice_commonservice__["a" /* CommonserviceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 19:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartitemPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_commonservice_commonservice__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__payment_payment__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__signup_signup__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__home_home__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var CartitemPage = /** @class */ (function () {
    function CartitemPage(platform, navCtrl, navParams, commonservice, alertCtrl) {
        var _this = this;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.commonservice = commonservice;
        this.alertCtrl = alertCtrl;
        this.ct = 0;
        this.cartitems = [];
        this.itemcount = 0;
        this.subtotal = 0;
        this.totalamount = 0;
        this.nocart = 0;
        this.taxablevalue = 0;
        this.productitem = [];
        this.subtotalpay = 0;
        this.couponshow = 0;
        this.couponamount = 0;
        this.couponcode = "";
        this.discount = 0;
        this.paymentarray = [];
        this.type = "METER";
        this.platform.registerBackButtonAction(function () {
            // localStorage.setItem("backtosigninpage",'PaymentPage');
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__home_home__["a" /* HomePage */]);
        });
    }
    CartitemPage.prototype.ionViewDidLoad = function () {
        this.loadpage();
    };
    CartitemPage.prototype.loadpage = function () {
        this.productitem = JSON.parse(localStorage.getItem("cart" + "11"));
        console.log('cart ' + JSON.stringify(this.productitem));
        if (this.productitem != null) {
            if (this.productitem.length > 0) {
                for (var k = 0; k < this.productitem.length; k++) {
                    this.data = this.productitem[k];
                    this.cartitems.push({ "id": this.data.pid, "name": this.data.name,
                        "price": this.data.price, "image": this.data.image,
                        "quen": this.data.qty, "material": '', "color": this.data.color,
                        "type": this.data.type,
                        "is_discount": this.data.isDiscount, "discount_price": this.data.discount_price,
                        "quantity_stock": this.data.stock, "weight": this.data.weight });
                }
                this.nocart = 1;
            }
            else {
                this.nocart = 0;
            }
        }
        else {
            this.nocart = 0;
        }
        this.getTotal();
        localStorage.setItem("cartproductcount", JSON.stringify(this.cartitems.length));
        console.log("loadpage function call and;");
    };
    CartitemPage.prototype.minusitem = function (val, id, pr, color) {
        console.log(color);
        console.log(val);
        console.log(id);
        console.log(pr);
        var min = localStorage.getItem(pr);
        console.log('crt_min' + min);
        var nid = id;
        if (val > min) {
            var m = parseFloat(val) - parseFloat(min);
        }
        else {
            var m = parseFloat(val);
        }
        nid['' + pr] = m;
        console.log(m);
        console.log(pr);
        this.updatequenty(m, pr, color);
    };
    CartitemPage.prototype.plusitem = function (val, id, pr, stock, color) {
        console.log(color);
        console.log(val);
        console.log(id);
        console.log(pr);
        var quantity_stock = stock;
        var min = localStorage.getItem(pr);
        if (parseFloat(val) < parseFloat(stock)) {
            console.log("if call");
            var nid = id;
            var m = parseFloat(val) + parseFloat(min);
            nid['' + pr] = m;
        }
        else {
            console.log("else call");
            this.commonservice.erroralert("You reach max quntity");
            var nid = id;
            var m = parseFloat(val);
            nid['' + pr] = m;
        }
        //  var m = parseFloat(val) +parseFloat(min);
        console.log('crt_pls' + min);
        var nid = id;
        // var m = parseFloat(val) +parseFloat(min);
        nid['' + pr] = m;
        console.log(m);
        console.log(pr);
        this.updatequenty(m, pr, color);
    };
    CartitemPage.prototype.updatequenty = function (m, pid, color) {
        var a = [];
        var b = JSON.parse(localStorage.getItem("cart" + "11"));
        for (var k = 0; k < b.length; k++) {
            var temp = this.productitem[k];
            if (pid == temp.pid) {
                temp.qty = m;
            }
            a.push(temp);
        }
        localStorage.setItem("cart" + "11", JSON.stringify(a));
        this.allitem();
    };
    CartitemPage.prototype.allitem = function () {
        console.log("allitem function call start;");
        this.cartitems = [];
        this.productitem = JSON.parse(localStorage.getItem("cart" + "11"));
        if (this.productitem != null) {
            if (this.productitem.length > 0) {
                for (var k = 0; k < this.productitem.length; k++) {
                    this.data = this.productitem[k];
                    this.cartitems.push({ "id": this.data.pid, "name": this.data.name,
                        "price": this.data.price, "image": this.data.image,
                        "quen": this.data.qty, "material": '', "color": this.data.color,
                        "type": this.data.type,
                        "is_discount": this.data.isDiscount, "discount_price": this.data.discount_price,
                        "quantity_stock": this.data.stock, "weight": this.data.weight });
                }
                this.nocart = 1;
            }
            else {
                this.nocart = 0;
            }
        }
        else {
            this.nocart = 0;
        }
        this.getTotal();
        localStorage.setItem("cartproductcount", JSON.stringify(this.cartitems.length));
        console.log("allitem function call end;");
    };
    CartitemPage.prototype.getTotal = function () {
        //this.subtotalpay= 0;
        var total = 0;
        var taxprice = 0;
        for (var i = 0; i < this.cartitems.length; i++) {
            var product = this.cartitems[i];
            if (product.is_discount == 1) {
                total += (product.discount_price * product.quen);
            }
            else {
                total += (product.price * product.quen);
            }
        }
        console.log(this.couponamount);
        if (this.couponcode == null || this.couponcode == '') {
            console.log("null enter");
            this.couponshow = 0;
            this.totalamount = total;
            this.taxablevalue = this.totalamount * 0.05;
            this.subtotalpay = this.taxablevalue + this.totalamount;
        }
        else {
            console.log("coupn enter enter");
            this.couponshow = 1;
            this.totalamount = total;
            this.taxablevalue = this.totalamount * 0.05;
            if (this.coupontype == "P") {
                console.log("percentgae enter");
                this.ct = this.totalamount * this.couponamount;
                this.subtotalpay = (this.taxablevalue + this.totalamount) - this.ct;
                //   localStorage.setItem("couponamount",this.ct);
            }
            else {
                console.log("flat amount enter");
                this.ct = this.couponamount;
                this.subtotalpay = (this.taxablevalue + this.totalamount) - this.ct;
                //localStorage.setItem("couponamount",this.ct);
            }
        }
        // this.totalamount=total;    
        // this.taxablevalue= total*0.05;
        // this.subtotalpay =this.totalamount +this.taxablevalue;
        localStorage.setItem("subtotalpay", JSON.stringify(this.subtotalpay));
        localStorage.setItem("tax", JSON.stringify(this.taxablevalue));
        console.log(localStorage.getItem("tax"));
    };
    CartitemPage.prototype.coupon = function () {
        var _this = this;
        console.log(this.couponcode);
        console.log(localStorage.getItem("islogin"));
        if (localStorage.getItem("islogin") == "1") {
            if (this.couponcode == null || this.couponcode == '') {
                this.getTotal();
                localStorage.setItem("couponcode", "0");
            }
            else {
                console.log(this.couponcode);
                this.servicedata = "coupon_code?coupon_code=" + this.couponcode + "&user_id=" + localStorage.getItem("loginuserid");
                this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
                    _this.data = res;
                    console.log(_this.data);
                    if (_this.data.status == 1) {
                        _this.coupontype = _this.data.Response.type;
                        _this.couponamount = _this.data.Response.amount;
                        localStorage.setItem("couponcode", _this.couponcode);
                    }
                    else {
                        _this.commonservice.erroralert("Invalid coupon code");
                    }
                    _this.getTotal();
                });
            }
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__signup_signup__["a" /* SignupPage */]);
        }
        // console.log(localStorage.getItem("loginuserid"));
    };
    CartitemPage.prototype.gotohome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__["a" /* DashboardPage */]);
    };
    CartitemPage.prototype.gotopayment = function () {
        console.log(localStorage.getItem("cart" + "11"));
        console.log(this.cartitems);
        // this.paymentarray.push({"tax":this.taxablevalue,"productarray":this.cartitems,"subtoalamount":this.totalamount,"finalamount":this.subtotalpay,"coupnamount":this.discount,"couponcode":this.couponcode});
        // console.log(this.paymentarray);
        if (this.cartitems.length > 0) {
            console.log(localStorage.getItem("islogin"));
            if (localStorage.getItem("islogin")) {
                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__payment_payment__["a" /* PaymentPage */]);
            }
            else {
                localStorage.setItem("backtosigninpage", 'PaymentPage');
                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__signup_signup__["a" /* SignupPage */]);
            }
        }
        else {
            this.commonservice.erroralert("Products add in your cart");
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__["a" /* DashboardPage */]);
        }
    };
    CartitemPage.prototype.deleteConfirm = function (id, color) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm',
            message: 'Do you want to Delete this product in your cart?',
            buttons: [{
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Ok',
                    handler: function () {
                        _this.deleteitem(id, color);
                    }
                }
            ]
        });
        alert.present();
    };
    CartitemPage.prototype.deleteitem = function (id, color) {
        var a = [];
        var b = JSON.parse(localStorage.getItem("cart" + "11"));
        for (var k = 0; k < b.length; k++) {
            var temp = this.productitem[k];
            if (id == temp.pid) {
            }
            else {
                a.push(temp);
            }
        }
        localStorage.setItem("cart" + "11", JSON.stringify(a));
        //  localStorage.removeItem(id);
        this.allitem();
    };
    CartitemPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-cartitem',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/cartitem/cartitem.html"*/'\n<ion-header>\n        <div class="headerbg_wh">\n    <div class="container" >\n            <!-- style=" padding-top: 20px; padding-bottom: 20px;" -->\n        <span (click)="gotohome()" style="font-size:20px; cursor:pointer; " ><i class="fa fa-arrow-left"  aria-hidden="true" style="font-size: 20px;  padding-left: 10px;"></i> &nbsp; &nbsp; &nbsp;<small style="font-weight: 500;">{{"YOURCARTITEM" | translate}}</small> </span>\n    </div>\n    </div>\n</ion-header>\n\n\n<ion-content style="background-color: rgb(245, 244, 244);">\n   \n    <div class="container-fluid" *ngIf="nocart==1" style="padding-top: 20px; padding-bottom: 10px;height: 100vh;">\n        <div  style="width: 100%; display: inline-block;">\n            <div class="cart_bl" *ngFor="let ci of cartitems">\n                <div class="col-xs-4" style="padding: 0px;width:20%;">\n                    <img src="{{ci.image}}" style="height: 105px; width: 105px;">\n                </div>\n                <div class="col-xs-4" style="padding:0 10px;width:40%;">\n                    <div  style="    margin: 0 0 3px;\n                    line-height: 18px;">\n                        <span style="width: 60%; font-weight: bold;">{{ci.name | slice: 0:25}}</span>  \n                    </div>\n                    <!-- <p style=\'margin:0;\'>\n                        <span style="color: red;font-weight: bold;">{{"SAR" | translate}} {{ci.price}}</span>\n                    </p>  -->\n                    <div *ngIf="ci.is_discount == 1">\n                        <p  style="color: red;\n                        font-weight: bold;\n                        margin: 0;\n                        line-height: 20px;\n                        font-size: 14px;">{{"SAR" | translate}} {{ci.discount_price}}</p>\n                        <span  style="text-decoration: line-through;\n                        color: grey;\n                        font-size: 12px;\n                        margin: 0;\n                        line-height: 16px;"> {{"SAR" | translate}} {{ci.price}}  </span>  <span style="font-size:12px;">/{{type | translate}}</span>                        \n                       </div>\n                        <div *ngIf="ci.is_discount == 0">\n                        <p class="text-center" style="color: red;font-weight: bold; line-height: 20px;"> {{"SAR" | translate}} {{ci.price}} </p>/{{type | translate}}\n                       </div>\n                    <p style=\'margin:0;\'>\n                        <span style="font-weight: bold;">{{"COLOR" | translate}} : <span  style="width: 15px;height: 15px;padding: 0px 8px;\n                            margin: 0 5px; " [ngStyle]="{\'background-color\': ci.color}"></span> </span>\n                    </p> \n                </div>\n                <div class="col-xs-4" style="width:40%;padding:5px 0 0;">\n                    <a (click)="deleteConfirm(ci.id,ci.color)" ><i  class="fa fa-trash-o" aria-hidden="true" style="font-size:20px; margin-left: 60px;color: black;"></i></a>\n                    <a class="addtocart1">\n                        <span class="minus bg-dark" (click)="minusitem(ci.quen,ci,ci.id,ci.color);"  style="display: inline-block; width:22%;text-align: center;">-</span>\n                        <input type="tel" class="count" name="qty"  placeholder="{{ci.quen | number : \'1.1-2\'}}" style="background-color:#fadf12;width: 50%;text-align: center;border:#5f5b5b" readonly>\n                        <span class="plus bg-dark" (click)="plusitem(ci.quen,ci,ci.id,ci.quantity_stock,ci.color);" style="display: inline-block; width:22%;text-align: center;">+</span>\n                                            </a>\n                                            {{ci.type | translate}}\n                </div>\n\n                <!-- <div class="col-xs-12" style=\'padding: 0;\'><hr style="border-color: #c3a1a1 !important;"></div> -->\n            </div>           \n        </div>\n        <!-- coupn code start -->\n        <!-- <label class = "coupon item item-input">\n            <ion-input  class="custom_input" [(ngModel)]="couponcode" placeholder="Enter Coupan Code" name="coupon" autocorrect="on" clearInput="true" clearOnEdit="true" type="text" inputmode="text"></ion-input>\n            <button  (click)="coupon()"> {{"ApplyCouponCode" | translate}}</button>\n         </label>\n         -->\n       \n        \n        <!--  coupon code end-->\n<p style="border-bottom: 1px solid #5f5b5b;color: #000;font-weight: 600;font-size: 15px;padding: 0px 0 5px;margin: 0px 0 5px;">{{"COSTSTRUCTURE" | translate}} </p>\n        <div style="width: 100%">\n            <div class="col-xs-6" style="padding: 0px;">\n                <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"SUBTOTAL" | translate}}</h4>\n                <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"TAXCART" | translate}}</h4>\n                <!-- <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"ShippingCharges" | translate}}</h4>\n                <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"COUPONAMOUNT" | translate}}</h4> -->\n                <h4 style="    border-top: 1px solid #5f5b5b;\n                padding-top: 8px;\n                font-size: 15px;" class="myorderh4 font-weight-bold">{{"TOTALAMOUNT" | translate}}</h4>\n            </div>\n            <div class="col-xs-6" style="padding: 0px;">\n                <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"SAR" | translate}} : {{totalamount | number:\'1.1-2\'}}</h4>\n                <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"SAR" | translate}} : {{taxablevalue | number:\'1.1-2\'}} </h4>\n                <!-- <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"SAR" | translate}} : 0.00 </h4>\n                <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"SAR" | translate}} : - {{ct | number:\'1.1-2\'}} </h4> -->\n                <h4 style="    border-top: 1px solid #5f5b5b;\n                padding-top: 8px;\n                font-size: 15px;" class="myorderh4 font-weight-bold">{{"SAR" | translate}} : {{subtotalpay | number:\'1.1-2\'}}</h4>\n            </div>\n           <p style="color: red; font-size: 15px;">*{{"SHIPPINGCONTENT" | translate}}</p> \n        </div>        \n    </div> \n    <div *ngIf="nocart==0" style="    text-align: center; margin-top: 83px; font-size: 20px; color: #848484;">{{"NOITEMINCART" | translate}}</div>\n</ion-content>\n<ion-footer style="padding: 0;">\n   \n    <div style="background-color:#ffffff;    z-index: 1;    width: 100%;">\n        <div style="width: 100%">\n            <div class="col" style=" margin:0px;" style="width: 50%;float: left;padding:0px;">\n                <button  class="btn btn-primary btn-block Update-Cart-btn" style="height:50px;" (click)="gotohome()">{{"ADDMOREITEM" | translate}}</button> \n            </div>\n            <div class="col"  style="width: 50%;float: left;padding:0px;">\n                <button  class="btn btn-primary btn-block CheckOut-btn" style="height:50px;" (click)="gotopayment()">{{"PROCEED" | translate}}</button>  \n            </div>\n        </div>\n    </div>\n</ion-footer>\n'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/cartitem/cartitem.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_commonservice_commonservice__["a" /* CommonserviceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], CartitemPage);
    return CartitemPage;
}());

//# sourceMappingURL=cartitem.js.map

/***/ }),

/***/ 197:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/cartitem/cartitem.module": [
		441,
		1
	],
	"../pages/mobile/mobile.module": [
		440,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 197;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VerificationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__payment_payment__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__mycart_myCart__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var VerificationPage = /** @class */ (function () {
    function VerificationPage(platform, navCtrl) {
        var _this = this;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.otp = localStorage.getItem("loginotp");
        var str = this.otp;
        this.otpone = str.charAt(0);
        this.otptwo = str.charAt(1);
        this.otpthree = str.charAt(2);
        this.otpfour = str.charAt(3);
        this.platform.registerBackButtonAction(function () {
            localStorage.setItem("loginotp", "");
            localStorage.setItem("loginuserid", "");
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__mycart_myCart__["a" /* myCartPage */]);
        });
    }
    VerificationPage.prototype.moveto = function (currentpos) {
        var nextpos = parseInt(currentpos) + 1;
        var enteredotpdigit = document.getElementById("otp" + currentpos).value;
        if (enteredotpdigit == '') {
            return false;
        }
        if (enteredotpdigit >= '10') {
            var olddigit = String(enteredotpdigit).charAt(0);
            document.getElementById("otp" + currentpos).value = olddigit;
        }
        if (currentpos == '4') {
            document.getElementById("otp" + currentpos).blur();
        }
        else {
            document.getElementById("otp" + nextpos).focus();
        }
    };
    VerificationPage.prototype.gomyorder = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__payment_payment__["a" /* PaymentPage */]);
    };
    VerificationPage.prototype.gotoback = function () {
        localStorage.setItem("loginotp", "");
        localStorage.setItem("loginuserid", "");
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__mycart_myCart__["a" /* myCartPage */]);
    };
    VerificationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-verification',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/verification/verification.html"*/'<ion-header >\n\n        <div class="headerbg_wh">\n\n    <div class="container" style="width: 100%;">\n\n        <button style="width: 20%;float: left;background-color: transparent;" icon-only >\n\n            <i (click)="gotoback()" class="fa fa-arrow-left" aria-hidden="true" style="font-size: 20px;"></i>\n\n        </button>\n\n        <ion-title style="width: 80%;float: left;    margin-top: -4px;">{{"VERIFICATION" | translate}}</ion-title>\n\n    </div>\n\n    </div>\n\n</ion-header>\n\n<ion-content style="background-color: rgb(245, 244, 244);">\n\n    <div class="container-fluid  " >\n\n        <h4 style="color:black; font-size:20px; font-weight:600; padding-bottom:0px; margin-bottom:15px; margin-top:10px; text-align:center;">\n\n                    {{"ENTERRECEIVEDOTP" | translate}}</h4>\n\n        <!--<div style="text-align: -webkit-center;" ><img class="card-img-top image" src="assets/imgs/icon-images/Untitled.png" style="    width: 25%;"></div>\n\n        <h4 style="color:black; font-size:20px; font-weight:600; padding-bottom:0px; margin-bottom:15px; margin-top:10px; text-align:center;">\n\n                    Order #59165 </h4>\n\n         <h3 style="color: #1b1a1a;; font-size:13px; text-align:center;">Thank You Ambuj</h3>\n\n        <h3 style="color: #1b1a1a; font-size:13px; text-align:center;">Please enter recevied otp </h3>-->\n\n        <div class="form-group form-inline" align="center" style="width: 100%;    margin-top: 125px;">\n\n            <div><input type="number"  class="OptULli" id="otp1" maxlength="1" [(ngModel)]="otpone" (keyup)="moveto(1)"/></div>\n\n             <div><input type="number" class="OptULli"  id="otp2" maxlength="1" [(ngModel)]="otptwo" (keyup)="moveto(2)"/></div>\n\n             <div><input type="number" class="OptULli"  id="otp3" maxlength="1" [(ngModel)]="otpthree" (keyup)="moveto(3)"/></div>\n\n             <div><input type="number" class="OptULli"  id="otp4"  maxlength="1" [(ngModel)]="otpfour" (keyup)="moveto(4)"/></div>\n\n        </div>\n\n        <br><br>\n\n        <div class="row" style="width: 100%;padding-left: 32px;">\n\n            <div  (click)="gomyorder()" class="col-md-12 col-lg-12" style="float: left;width: 50%">\n\n                <button class="btn btn-primary  btn-block register"  style="    background-color: #171615 !important;    border-radius: 21px;">{{"SUBMIT" | translate}}</button>\n\n            </div>\n\n            <div (click)="gomyorder()" class="col-md-12 col-lg-12" style="float: left;width: 50%">\n\n                <button class="btn btn-primary  btn-block register"  style="border-radius: 21px;">{{"CANCELORDER" | translate}}</button>\n\n            </div>\n\n        </div>\n\n    </div>\n\n</ion-content >\n\n'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/verification/verification.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */]])
    ], VerificationPage);
    return VerificationPage;
}());

//# sourceMappingURL=verification.js.map

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SigninOtpPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__signup_signup__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__payment_payment__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_commonservice_commonservice__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SigninOtpPage = /** @class */ (function () {
    function SigninOtpPage(navCtrl, commonservice) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.commonservice = commonservice;
        this.otp = localStorage.getItem("loginotp");
        var str = this.otp;
        document.addEventListener('paste', function (e) {
            console.log(e.clipboardData.getData('Text'));
            _this.m = e.clipboardData.getData('Text');
            if (_this.m.length == 4) {
                _this.otpone = _this.m.charAt(0);
                console.log(_this.otpone);
                _this.otptwo = _this.m.charAt(1);
                _this.otpthree = _this.m.charAt(2);
                _this.otpfour = _this.m.charAt(3);
                $('#otp1').val(_this.otpone).trigger('input');
                $('#otp2').val(_this.otptwo).trigger('input');
                $('#otp3').val(_this.otpthree).trigger('input');
                $('#otp4').val(_this.otpfour).trigger('input');
            }
            e.preventDefault();
            e.stopPropagation();
            // this.otpone= m.charAt(0);
            _this.clickpgeauto();
        });
        // console.log(x);
    }
    // performSearch(e){
    //     console.log(e);
    // }
    SigninOtpPage.prototype.clickpgeauto = function () {
        $("#hidebutton").click();
        console.log("hi");
        //  this.moveto(4);          
    };
    SigninOtpPage.prototype.fileChange = function () {
        this.otpone = this.m.charAt(0);
        console.log(this.otpone);
        this.otptwo = this.m.charAt(1);
        this.otpthree = this.m.charAt(2);
        this.otpfour = this.m.charAt(3);
    };
    // omp(e){
    //     console.log(this.otpone);
    //     console.log(e);
    //     var m =e.target.value;
    //     console.log(m);
    //     console.log(e.target);
    //     var strd = this.otpone; 
    //     var x = document.getElementById("otp1");
    //     console.log(x);
    //     console.log(strd);
    //     console.log(this.otpone);
    //     console.log(strd.charAt(0));    
    //     this.otpone= strd[0];      
    //     this.otptwo=strd[1];
    //     this.otpthree=strd[2];
    //     this.otpfour=strd[3];
    // }
    SigninOtpPage.prototype.moveto = function (currentpos) {
        //var x = document.getElementById("searchparkinglocations");
        // var x = $("#otp1").val();
        // console.log(x);
        var nextpos = parseInt(currentpos) + 1;
        var enteredotpdigit = document.getElementById("otp" + currentpos).value;
        if (enteredotpdigit == '') {
            return false;
        }
        if (enteredotpdigit >= '10') {
            var olddigit = String(enteredotpdigit).charAt(0);
            document.getElementById("otp" + currentpos).value = olddigit;
        }
        if (currentpos == '4') {
            document.getElementById("otp" + currentpos).blur();
        }
        else {
            document.getElementById("otp" + nextpos).focus();
        }
    };
    SigninOtpPage.prototype.test = function (e) {
        console.log(e);
        console.log("input");
    };
    SigninOtpPage.prototype.om = function (e) {
        console.log(e);
        console.log("pesss");
    };
    SigninOtpPage.prototype.focusOutFunction = function () {
    };
    // modhlcg(){
    //     let str1 = this.otpthree;     
    //     console.log(this.otpthree.length);
    //     console.log(str1.length);
    //     if(str1 >= 4){
    //         this.otpone1= str1.charAt(0);
    //         this.otptwo2=str1.charAt(1);
    //         this.otpthree3=str1.charAt(2);
    //         this.otpfour4=str1.charAt(3);
    //     }
    // }
    // runTimeChange(){
    //     var str1 = this.otptwo; 
    //     console.log(this.otptwo);
    //     console.log(str1.length);
    //     if(str1.length >= 4){
    //         this.otpone1= str1.charAt(0);
    //         this.otptwo2=str1.charAt(1);
    //         this.otpthree3=str1.charAt(2);
    //         this.otpfour4=str1.charAt(3);
    //     }
    // }
    // doSomething(){
    //     console.log(this.otpone);
    //     var str1 = this.otpone; 
    //     if(str1.length >= 4){
    //         this.otpone1= str1.charAt(0);
    //         this.otptwo2=str1.charAt(1);
    //         this.otpthree3=str1.charAt(2);
    //         this.otpfour4=str1.charAt(3);
    //     }
    //     console.log(str1.length);
    // }
    SigninOtpPage.prototype.s1 = function () {
        // console.log(this.otpone);
        this.otpone = "";
    };
    SigninOtpPage.prototype.s2 = function () {
        this.otptwo = "";
    };
    SigninOtpPage.prototype.s3 = function () {
        this.otpthree = "";
    };
    SigninOtpPage.prototype.s4 = function () {
        this.otpfour = "";
    };
    SigninOtpPage.prototype.s5 = function () {
        this.otpfive = "";
    };
    SigninOtpPage.prototype.gohome = function () {
        this.otp = localStorage.getItem("loginotp");
        var str = this.otp;
        this.otpone1 = str.charAt(0);
        this.otptwo2 = str.charAt(1);
        this.otpthree3 = str.charAt(2);
        this.otpfour4 = str.charAt(3);
        if (this.otpone == this.otpone1 && this.otptwo2 == this.otptwo && this.otpthree3 == this.otpthree && this.otpfour == this.otpfour4) {
            localStorage.setItem("islogin", "1");
            if (localStorage.getItem("backtosigninpage")) {
                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__payment_payment__["a" /* PaymentPage */]);
                this.commonservice.successalert("Login sucessfully");
            }
            else {
                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__["a" /* DashboardPage */]);
                this.commonservice.successalert("Login sucessfully");
            }
        }
        else {
            this.commonservice.erroralert("Incorrect OTP");
        }
    };
    SigninOtpPage.prototype.goback = function () {
        localStorage.removeItem("loginotp");
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__signup_signup__["a" /* SignupPage */]);
    };
    SigninOtpPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-signinOtp',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/signinOtp/signinOtp.html"*/'<ion-header>\n\n        <div class="headerbg_wh">\n\n    <div class="container" style="width: 100%; ">\n\n       <!-- padding-top: 20px; padding-bottom: 20px;" -->\n\n        <button style="width: 20%;float: left;background-color: transparent;" icon-only >\n\n            <i (click)="goback()" class="fa fa-arrow-left" aria-hidden="true" style="font-size: 20px;"></i>\n\n        </button>\n\n        <ion-title style="width: 80%;float: left;">{{"VERIFICATION" | translate}}</ion-title>\n\n    </div>\n\n    </div>\n\n</ion-header>\n\n<ion-content style="background-color: rgb(245, 244, 244);">\n\n    \n\n    <div class="container-fluid " style="margin-top: 40px;">\n\n        <br><br>\n\n        \n\n        <h4 style="color:black; font-size:20px; font-weight:600; padding-bottom:0px; margin-bottom:15px; margin-top:10px; text-align:center;">\n\n                    {{"VERIFYMOBILE" | translate}} </h4>\n\n       \n\n        <h3 style="color: #7f7979; font-size:13px; text-align:center;">{{"ENTERYOUROTP" | translate}}</h3>\n\n        <br><br>\n\n        <div class="form-group form-inline" style="width: 100%">\n\n            <!-- (change)="fileChange()" (ionInput)="test($event)" (paste)="omp($event)" (keypress)="om($event)" -->\n\n             <div><input   (click)="clickpgeauto()"  (focus)="s1()" [attr.maxlength]="1" pattern="[0-9]*" decimal="true" allow-multiple-decimals="true"  type="number"  class="OptULli" id="otp1" maxlength="1" [(ngModel)]="otpone" (keyup)="moveto(1)"  /></div>\n\n             <div><input   (focus)="s2()"  [attr.maxlength]="1" pattern="[0-9]*" decimal="true" allow-multiple-decimals="true"  type="number" class="OptULli"  id="otp2" maxlength="1" [(ngModel)]="otptwo" (keyup)="moveto(2)"  /></div>\n\n             <div><input  (focus)="s3()"  [attr.maxlength]="1" pattern="[0-9]*" decimal="true" allow-multiple-decimals="true" type="number" class="OptULli"  id="otp3" maxlength="1" [(ngModel)]="otpthree" (keyup)="moveto(3)" /></div>\n\n             <div><input  (focus)="s4()" [attr.maxlength]="1" pattern="[0-9]*" decimal="true" allow-multiple-decimals="true" type="number" class="OptULli"  id="otp4"  maxlength="1" [(ngModel)]="otpfour" (keyup)="moveto(4)" /></div>\n\n        </div>\n\n        <br><br>\n\n        <br><br>\n\n        <br><br>\n\n        <p id="hidebutton"></p>\n\n        <div class="row" style="width: 100%;padding-left: 32px;">\n\n            <div class="col-md-12 col-lg-12" style="float: left;width:100%">\n\n                <button class="btn btn-primary  btn-block register" style="border-radius: 21px;" (click)="gohome()">{{"VERIFYNOW" | translate}}</button>\n\n            </div>            \n\n        </div>\n\n    </div>\n\n</ion-content>\n\n'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/signinOtp/signinOtp.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */], __WEBPACK_IMPORTED_MODULE_5__providers_commonservice_commonservice__["a" /* CommonserviceProvider */]])
    ], SigninOtpPage);
    return SigninOtpPage;
}());

//# sourceMappingURL=signinOtp.js.map

/***/ }),

/***/ 22:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyAccountPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_commonservice_commonservice__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__search_search__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__cartitem_cartitem__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__signup_signup__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__home_home__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







// import { myCartPage } from '../mycart/mycart';



var MyAccountPage = /** @class */ (function () {
    function MyAccountPage(platform, navCtrl, commonservice, events) {
        var _this = this;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.commonservice = commonservice;
        this.events = events;
        this.error = "";
        events.publish('allpagecommon');
        if (!localStorage.getItem("islogin")) {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__signup_signup__["a" /* SignupPage */]);
            localStorage.setItem("backtosigninpage", 'MyAccountPage');
        }
        else {
            this.loadpage();
        }
        this.platform.registerBackButtonAction(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__["a" /* DashboardPage */]);
        });
    }
    MyAccountPage.prototype.isValidEmailAddress = function (emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    };
    MyAccountPage.prototype.loadpage = function () {
        var _this = this;
        this.commonservice.waitloadershow();
        this.servicedata = "view_profile?userid=" + localStorage.getItem("loginuserid");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.commonservice.waitloaderhide();
            _this.data = res;
            console.log(_this.data.data);
            _this.fname = _this.data.data.firstname;
            _this.lname = _this.data.data.lastname;
            _this.email = _this.data.data.email;
            _this.mobilenumber = _this.data.data.telephone;
            _this.address = _this.data.data.address;
        });
    };
    MyAccountPage.prototype.gotoback = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__["a" /* DashboardPage */]);
    };
    MyAccountPage.prototype.updateprofile = function () {
        var _this = this;
        this.error = "";
        if (this.fname == "") {
            this.error = "First Name is required";
        }
        else if (this.lname == "") {
            this.error = "Last Name is required";
        }
        else if (this.email == "") {
            this.error = "Email is required";
        }
        else if (this.isValidEmailAddress(this.email) == false) {
            this.error = "Please Enter Valid Email Address";
        }
        else if (this.mobilenumber == "") {
            this.error = "Mobile Number is required";
        }
        if (this.error == "") {
            this.commonservice.waitloadershow();
            this.servicedata = "update_profile?firstname=" + this.fname + "&lastname=" + this.lname + "&telephone=" + this.mobilenumber + "&email=" + this.email + "&address=" + this.address + "&userid=" + localStorage.getItem("loginuserid");
            this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
                _this.data = res;
                _this.commonservice.waitloaderhide();
                console.log(_this.data);
                if (_this.data.status = "true") {
                    _this.events.publish('allpagecommon');
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__["a" /* DashboardPage */]);
                    _this.commonservice.successalert("Profile updated sucessfully");
                }
                else {
                    _this.commonservice.erroralert("Something went wrong");
                }
            });
        }
        else {
            this.commonservice.erroralert(this.error);
        }
    };
    MyAccountPage.prototype.gotohome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__["a" /* DashboardPage */]);
    };
    MyAccountPage.prototype.gotosearch = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__search_search__["a" /* SearchPage */]);
    };
    MyAccountPage.prototype.gotocat = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__home_home__["a" /* HomePage */]);
    };
    MyAccountPage.prototype.gotobag = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__cartitem_cartitem__["a" /* CartitemPage */]);
    };
    MyAccountPage.prototype.gotoaccount = function () {
    };
    MyAccountPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-myAccount',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/myAccount/myAccount.html"*/'<ion-header >\n\n        <div class="headerbg_wh">\n\n        <div class="container" style="width: 100%; ">\n\n    <div style="width: 100%;display: flex ;">\n\n            <!-- padding-top: 20px; padding-bottom: 20px; \n\n             style="width: 20%;float: left;background-color: transparent;" -->\n\n        <button icon-only style="text-align:left; flex: 1;background-color: transparent;" >\n\n            <i (click)="gotoback()" class="fa fa-arrow-left" aria-hidden="true" style="font-size: 20px;"></i>\n\n        </button>\n\n        <span style="flex:6; font-size: 20px">{{"MYACCOUNT" | translate}}</span>\n\n        <!-- style="width: 80%;float: left;    margin-top: -4px;" -->\n\n    </div>\n\n    </div>\n\n    </div>\n\n</ion-header>\n\n\n\n<ion-content style="background-color: rgb(245, 244, 244);" class="myaccount">    \n\n   \n\n    \n\n    <div class="container-fluid pt-5 align_content" style="margin-top: 10px">\n\n        <form class="loginpage "style="width:90%; margin:0 auto;" >\n\n            <div class="media" >\n\n                <div class="media-body" style="text-align: -webkit-center;    margin-top: 14px;" >\n\n                    <img src="assets/imgs/thumb.png" class="align-self-center" style="width:60px">\n\n                </div>\n\n            </div>\n\n            <div class="form-group form-inline" style="width: 100%;">\n\n                <!-- <label style="width:10%;float: left"></label> -->\n\n                <input name="fname" class="line-animation" placeholder=\'{{"FNAME" | translate}}\'type="text" style="color:black;float: left" [(ngModel)]="fname" >\n\n                <div class="line"></div>\n\n            </div>\n\n         \n\n            <div class="form-group form-inline" style="width: 100%;">\n\n                <!-- <label style="width:10%;float: left"></label> -->\n\n                <input name="lname" class="line-animation" placeholder=\'{{"LNAME" | translate}}\' type="text" style="color:black;float: left" [(ngModel)]="lname" >\n\n                <div class="line"></div>\n\n            </div>\n\n            \n\n            <div class="form-group form-inline" style="width: 100%;">\n\n                <!-- <label style="width:10%;float: left"></label> -->\n\n                <input name="email" class="line-animation" placeholder=\'{{"EMAIL" | translate}}\' type="text" style="color:black;float: left" [(ngModel)]="email" >\n\n                <div class="line"></div>\n\n            </div>\n\n            \n\n            <div class="form-group form-inline" style="width: 100%;">\n\n                <!-- <label style="width:10%;float: left"></label> -->\n\n                <input name="mobile" class="line-animation" placeholder=\'{{"MOBILENUMBER" | translate}}\' type="number" style="color:black;float: left" [(ngModel)]="mobilenumber" >\n\n                <div class="line"></div>\n\n            </div>\n\n            <div class="form-group form-inline" style="width: 100%;">\n\n                <!-- <label style="width:10%;float: left"></label> -->\n\n                <input name="adress" class="line-animation" placeholder=\'{{"ADDRESS" | translate}}\' type="text" style="color:black;float: left" [(ngModel)]="address" >\n\n                <div class="line"></div>\n\n            </div>\n\n            <br>\n\n           <!--<h3 style="color:#666; font-size:14px; text-align:center;color: #292727;">Already Registerd ? Login Here </h3>-->\n\n        </form>\n\n        <div class="col-md-12 col-lg-12" style="padding: 0 50px;">\n\n                <button class="btn btn-primary  btn-block register" style="border-radius: 21px;" (click)="updateprofile()" > {{"UPDATEPROFILE" | translate}}</button>\n\n            </div>\n\n    </div>\n\n</ion-content>\n\n<ion-footer>\n\n        <div style="display:flex;">\n\n           <div (click)="gotohome()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname"  style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/home.png">\n\n                   <br>\n\n                   {{"HOME" | translate}} \n\n               </a>\n\n           </div> \n\n           <div (click)="gotosearch()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/search.png">\n\n                   <br>\n\n                   {{"SEARCH" | translate}}</a>\n\n           </div>\n\n           <div (click)="gotocat()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;">  \n\n                   <img  style="    height: 18px;" src="assets/imgs/icon-images/list.png">\n\n                   <br>\n\n                   {{"CATEGORY" | translate}} </a>\n\n           </div>\n\n           <div (click)="gotobag()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="height: 18px;" src="assets/imgs/icon-images/shopping-bag.png">\n\n                   <br>\n\n                   {{"BAG" | translate}}\n\n               </a>\n\n           </div>\n\n           <div (click)="gotoaccount()"  style=\'flex:1;text-align:center\'>\n\n               <a   class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="height: 18px;"  src="assets/imgs/icon-images/round-account-button-with-user-inside.png">\n\n                   <br>\n\n                   {{"MYACCOUNT" | translate}} </a>\n\n           </div>\n\n        </div>\n\n</ion-footer>\n\n<!-- <ion-footer>\n\n    <ion-toolbar>\n\n            <div (click)="gotohome()" style="text-align:center; width:20%; float:left;">\n\n                <a class="iconname"  style="color:black;font-size: 12px;"> \n\n                    <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/home.png">\n\n                    <br>\n\n                    {{"HOME" | translate}}  \n\n                </a>\n\n            </div> \n\n            <div (click)="gotosearch()"  style="text-align:center; width:20%; float:left;">\n\n                <a class="iconname" style="color:black;font-size: 12px;"> \n\n                    <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/search.png">\n\n                    <br>\n\n                    {{"SEARCH" | translate}}</a>\n\n            </div>\n\n            <div (click)="gotocat()"   style="text-align:center; width:20%; float:left;">\n\n                <a class="iconname" style="color:black;font-size: 12px;">  \n\n                    <img  style="    height: 18px;" src="assets/imgs/icon-images/list.png">\n\n                    <br>\n\n                    {{"CATEGORY" | translate}} </a>\n\n            </div>\n\n            <div (click)="gotobag()"  style="text-align:center; width:20%; float:left;">\n\n                <a class="iconname" style="color:black;font-size: 12px;"> \n\n                    <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/shopping-bag.png">\n\n                    <br>\n\n                    {{"BAG" | translate}}\n\n                </a>\n\n            </div>\n\n            <div (click)="gotoaccount()"  style="text-align:center; width:20%; float:left;">\n\n                <a   class="iconname" style="color:black;font-size: 12px;"> \n\n                    <img class="img-fluid" style="    height: 18px;"  src="assets/imgs/icon-images/round-account-button-with-user-inside.png">\n\n                    <br>\n\n                    {{"MYACCOUNT" | translate}} </a>\n\n            </div>\n\n    </ion-toolbar>\n\n</ion-footer> -->'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/myAccount/myAccount.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__providers_commonservice_commonservice__["a" /* CommonserviceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */]])
    ], MyAccountPage);
    return MyAccountPage;
}());

//# sourceMappingURL=myAccount.js.map

/***/ }),

/***/ 222:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ZoomimgPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_img_viewer__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__addtocart_addToCart__ = __webpack_require__(37);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ZoomimgPage = /** @class */ (function () {
    function ZoomimgPage(platform, navCtrl, imageViewerCtrl) {
        var _this = this;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.imageViewerCtrl = imageViewerCtrl;
        this.mainimage = "assets/imgs/product-des.png";
        this.singleimg = "assets/imgs/product-des.png";
        this.zoomingimg = [];
        this.addimage = [];
        this.addimageto = [];
        this.showsingleimg = '0';
        this.numberimage = 0;
        this.platform.registerBackButtonAction(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__addtocart_addToCart__["a" /* AddtocartPage */]);
        });
        this._imageViewerCtrl = imageViewerCtrl;
        this.addimage = JSON.parse(localStorage.getItem("scrollimage"));
        console.log("zoomingpage");
        console.log(this.addimage);
        if (this.addimage != null) {
            var i = 0;
            for (var _i = 0, _a = this.addimage; _i < _a.length; _i++) {
                var bp = _a[_i];
                this.zoomingimg.push({ "image": bp, "quen": Number(i) + 1 });
                i++;
            }
        }
        if (this.addimage != null) {
            var i = 0;
            for (var _b = 0, _c = this.addimage; _b < _c.length; _b++) {
                var bp = _c[_b];
                this.addimageto.push({ "img": bp });
                i++;
            }
        }
        this.mainimage = this.zoomingimg[0].image;
    }
    ZoomimgPage.prototype.presentImage = function (myImage) {
        var imageViewer = this._imageViewerCtrl.create(myImage);
        imageViewer.present();
    };
    ZoomimgPage.prototype.gotoback = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__addtocart_addToCart__["a" /* AddtocartPage */]);
    };
    ZoomimgPage.prototype.opennewimg = function (img, id) {
        console.log(img);
        this.mainimage = img;
        this.numberimage = id;
    };
    /* opennewimg(img)
     {
         this.addimageto=[];
         let newslider =[];
         newslider.push({img});
         
         this.newfunction(newslider);
         console.log("a1");
         console.log(this.addimageto);
          console.log("a");
     }
     newfunction(data)
     {
         this.addimageto=[];
         this.addimageto = data;
          console.log(this.addimageto);
          console.log("aa");
     }*/
    ZoomimgPage.prototype.closenewimg = function () {
        this.showsingleimg = '0';
        this.addimageto = [];
        if (this.addimage != null) {
            var i = 0;
            for (var _i = 0, _a = this.addimage; _i < _a.length; _i++) {
                var bp = _a[_i];
                this.addimageto.push({ "img": bp });
                i++;
            }
        }
        console.log(this.addimageto);
    };
    ZoomimgPage.prototype.Closesfunction = function () {
        console.log("dsad");
    };
    ZoomimgPage.prototype.rightfuction = function () {
        console.log(this.addimage.length);
        console.log(this.numberimage);
        if (this.addimage.length != this.numberimage) {
            var imagenumber = Number(this.numberimage) + 1;
            this.numberimage = imagenumber;
            for (var _i = 0, _a = this.zoomingimg; _i < _a.length; _i++) {
                var bp = _a[_i];
                if (bp.quen == imagenumber) {
                    this.mainimage = bp.image;
                }
            }
        }
    };
    ZoomimgPage.prototype.leftfunction = function () {
        if (this.addimage.length != 1) {
            var imagenumber = Number(this.numberimage) - 1;
            this.numberimage = imagenumber;
            for (var _i = 0, _a = this.zoomingimg; _i < _a.length; _i++) {
                var bp = _a[_i];
                if (bp.quen == imagenumber) {
                    this.mainimage = bp.image;
                }
            }
        }
    };
    ZoomimgPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-zoomimg',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/zoomimg/zoomimg.html"*/'<!-- <ion-header style="z-index: 99999999999999999999999;">\n\n    <ion-navbar >\n\n        <div class="container" >\n\n            <div  (click)="gotoback()"  class="container" style=" padding-top: 10px; padding-bottom: 10px;">\n\n                <span style="font-size:20px; cursor:pointer;" ><i class="fa fa-arrow-left" aria-hidden="true" style="font-size: 20px;"></i> &nbsp;<small style="font-weight: 500;">&nbsp;&nbsp; {{"ADDTOCRT" | translate}} </small></span>\n\n               \n\n            </div>\n\n        </div>\n\n    </ion-navbar>\n\n</ion-header> -->\n\n\n\n<ion-header>\n\n    <div class="headerbg_wh">\n\n    <div class="container">\n\n            <!-- style=" padding-top: 20px; padding-bottom: 20px;" -->\n\n        <!-- <span (click)="gotoback()"  style="font-size:20px; cursor:pointer; " ><i class="fa fa-arrow-left" aria-hidden="true" style="font-size: 20px;  padding-left: 10px;"></i> &nbsp; &nbsp; &nbsp;<small style="font-weight: 500;">{{"ADDTOCRT" | translate}}</small> </span>\n\n        <span  (click)="removewishlist()" style="cursor:pointer;float: right;  " ><i  *ngIf="wishliststatus==1" class="fa fa-heart" aria-hidden="true" style="margin-right:1px;    font-size: 26px;color:#FF0000;"></i></span>\n\n        <span (click)="addwishlist()"  style="cursor:pointer;float: right;  " ><i  *ngIf="wishliststatus!=1" class="fa fa-heart" aria-hidden="true" style="margin-right:1px;    font-size: 26px;color:#d6c9a8;"></i></span> -->\n\n        <div  (click)="gotoback()"  class="container">\n\n            <span style="font-size:20px; cursor:pointer;" ><i class="fa fa-arrow-left" aria-hidden="true" style="font-size: 20px;"></i> &nbsp;<small style="font-weight: 500;">&nbsp;&nbsp; {{"ADDTOCRT" | translate}} </small></span>\n\n           \n\n        </div>\n\n    </div>\n\n</div>\n\n</ion-header>\n\n\n\n<ion-content style="background-color: rgb(245, 244, 244);">\n\n  \n\n    <!--<div style="text-align: -webkit-center;margin-top: 44px;">   <img style="    height: auto; width: auto;" #myImage (click)="presentImage(myImage)"  src="{{mainimage}}" /></div>-->\n\n    \n\n    <a (click)="leftfunction()" style="    position: fixed;\n\n    left: 0;\n\n    z-index: 999999999;\n\n    top: 20%;\n\n    width: 40px;\n\n    margin: 0 auto;\n\n    align-items: center;\n\n    height: 100px;\n\n    display: flex;\n\n    background-color: rgba(255, 255, 255, 0.5);\n\n    text-align: center;">\n\n     <img src="assets/images/left-arrow.png" style="    height: 20px;\n\n     margin: 0 auto;" />\n\n    </a>\n\n    \n\n    <div style="text-align: -webkit-center;"> \n\n        <pinch-zoom style=\' width: auto;margin: 50px 0 0;\'>\n\n            <img src="{{mainimage}}" /> \n\n        </pinch-zoom>\n\n    </div>\n\n    <a (click)="rightfuction()" style="    position: fixed;\n\n    right: 0;\n\n    z-index: 999999999;\n\n    top: 20%;\n\n    width: 40px;\n\n    margin: 0 auto;\n\n    align-items: center;\n\n    height: 100px;\n\n    display: flex;\n\n    background-color: rgba(255, 255, 255, 0.5);\n\n    text-align: center;"> <img src="assets/images/right-arrow.png" style="height: 20px;\n\n    margin: 0 auto;" /></a>\n\n\n\n    <div class="tab1" style="margin: 0 auto; position: fixed; bottom: 75px;left: 0;right:0;text-align: center;z-index: 9999;	">\n\n        <ion-slides  autoplay="200000" class="slideroption" pager="true"   loop="true" speed="300">\n\n            <ion-slide >\n\n                <a  *ngFor="let slide of zoomingimg"  (click)="opennewimg(slide.image,slide.quen)"  class="tablinks" >\n\n                    <img src="{{slide.image}}" style="width:45px;">\n\n                </a>\n\n            </ion-slide>\n\n        </ion-slides>\n\n     \n\n        <!-- <a  *ngFor="let slide of zoomingimg"  (click)="opennewimg(slide.image,slide.quen)"  class="tablinks" >\n\n            <img src="{{slide.image}}" style="width:45px;">\n\n        </a> -->\n\n    </div>\n\n </ion-content >\n\n \n\n \n\n \n\n <!--<ion-content style="background-color: rgb(245, 244, 244);">\n\n\n\n        <ion-slides   *ngIf="showsingleimg==\'0\'"  style="text-align: -webkit-center;margin-top: -44px;" zoom="true">\n\n            <ion-slide *ngFor="let slide of addimageto" >\n\n                <div class="swiper-zoom-container" >\n\n                    <img  src="{{slide.img}}"  style="height: auto; width: auto;">\n\n                </div>\n\n            </ion-slide>\n\n        </ion-slides>\n\n    \n\n        <ion-slides   *ngIf="showsingleimg==\'1\'"    style="text-align: -webkit-center;margin-top: -44px;" zoom="true">\n\n            <ion-slide >\n\n                <div class="swiper-zoom-container"  >\n\n                    <img (click)="closenewimg()" src="{{singleimg}}"  style="height: auto; width: auto;">\n\n                </div>\n\n            </ion-slide>\n\n        </ion-slides>\n\n   \n\n   \n\n    <div class="tab1" style="margin: 0 auto; position: fixed; bottom: 75px;left: 25%;   z-index: 9999;	">\n\n        <a  *ngFor="let slide of zoomingimg"  (click)="opennewimg(slide.image)"  class="tablinks" >\n\n            <img src="{{slide.image}}" style="width:45px;">\n\n        </a>\n\n       \n\n    </div>\n\n    \n\n </ion-content >-->\n\n'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/zoomimg/zoomimg.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_img_viewer__["a" /* ImageViewerController */]])
    ], ZoomimgPage);
    return ZoomimgPage;
}());

//# sourceMappingURL=zoomimg.js.map

/***/ }),

/***/ 227:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderConfirmedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__search_search__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__myAccount_myAccount__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__cartitem_cartitem__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_commonservice_commonservice__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__orderDetails_orderDetails__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__home_home__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__payment_payment__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







// import { myCartPage } from '../mycart/mycart';


// import { OrderDetailsPage } from '../orderdetails/orderdetails';


// import { MyOrderPage } from '../myOrder/myOrder';

var OrderConfirmedPage = /** @class */ (function () {
    function OrderConfirmedPage(platform, navCtrl, events, commonservice) {
        var _this = this;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.events = events;
        this.commonservice = commonservice;
        this.error = "";
        events.publish('allpagecommon');
        // this.otp=localStorage.getItem("loginotp");
        // var str = this.otp;
        // this.otpone= str.charAt(0);
        // this.otptwo=str.charAt(1);
        // this.otpthree=str.charAt(2);
        // this.otpfour=str.charAt(3); 
        this.orderid = localStorage.getItem("orderid");
        this.loadpage();
        this.platform.registerBackButtonAction(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__home_home__["a" /* HomePage */]);
        });
        document.addEventListener('paste', function (e) {
            console.log(e.clipboardData.getData('Text'));
            _this.m = e.clipboardData.getData('Text');
            if (_this.m.length == 4) {
                _this.otpone = _this.m.charAt(0);
                console.log(_this.otpone);
                _this.otptwo = _this.m.charAt(1);
                _this.otpthree = _this.m.charAt(2);
                _this.otpfour = _this.m.charAt(3);
                $('#otp1').val(_this.otpone).trigger('input');
                $('#otp2').val(_this.otptwo).trigger('input');
                $('#otp3').val(_this.otpthree).trigger('input');
                $('#otp4').val(_this.otpfour).trigger('input');
            }
            e.preventDefault();
            e.stopPropagation();
        });
    }
    OrderConfirmedPage.prototype.loadpage = function () {
        var _this = this;
        localStorage.removeItem("orderconfirmotp");
        //this.commonservice.waitloadershow();
        this.otpss();
        this.servicedata = "view_profile?userid=" + localStorage.getItem("loginuserid") + "&lang=" + localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            //    this.commonservice.waitloaderhide();
            _this.data = res;
            console.log(_this.data.data);
            _this.username = _this.data.data.firstname + ' ' + _this.data.data.lastname;
        });
    };
    OrderConfirmedPage.prototype.otpss = function () {
        var _this = this;
        this.servicedata = "orderconfirmotp?userid=" + localStorage.getItem("loginuserid");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            console.log(_this.data);
            if (_this.data.status = "true") {
                localStorage.setItem("orderconfirmotp", _this.data.otp);
                _this.otp = localStorage.getItem("orderconfirmotp");
                var str = _this.otp;
                //  this.otpone= str.charAt(0);
                //  this.otptwo=str.charAt(1);
                //  this.otpthree=str.charAt(2);
                //  this.otpfour=str.charAt(3);
            }
            else {
                _this.commonservice.erroralert("Something went wrong");
            }
        });
    };
    OrderConfirmedPage.prototype.moveto = function (currentpos) {
        var nextpos = parseInt(currentpos) + 1;
        var enteredotpdigit = document.getElementById("otp" + currentpos).value;
        if (enteredotpdigit == '') {
            return false;
        }
        if (enteredotpdigit >= '10') {
            var olddigit = String(enteredotpdigit).charAt(0);
            document.getElementById("otp" + currentpos).value = olddigit;
        }
        if (currentpos == '4') {
            document.getElementById("otp" + currentpos).blur();
        }
        else {
            document.getElementById("otp" + nextpos).focus();
        }
    };
    // continueShopping()
    // {      
    //     this.commonservice.successalert("Your order has been confirm successfully"); 
    //     this.sendmail();
    //     this.navCtrl.setRoot(OrderDetailsPage);
    // }
    OrderConfirmedPage.prototype.continueShopping = function () {
        var _this = this;
        this.otp = localStorage.getItem("orderconfirmotp");
        var str = this.otp;
        this.otpone1 = str.charAt(0);
        this.otptwo2 = str.charAt(1);
        this.otpthree3 = str.charAt(2);
        this.otpfour4 = str.charAt(3);
        if (this.otpone == this.otpone1 && this.otptwo2 == this.otptwo && this.otpthree3 == this.otpthree && this.otpfour == this.otpfour4) {
            var m = JSON.parse(localStorage.getItem("orderconifrmdetails"));
            console.log(m[0]);
            this.servicedata = "placeorder?firstname=" + localStorage.getItem("ship_fname") + "&tax=" + m[0].tax + "&totalamount=" + m[0].totalamount + "&shipping_charge=" + m[0].shipping_charge + "&subtotal=" + m[0].subtotal + "&lastname=" + localStorage.getItem("ship_lname") + "&telephone=" + localStorage.getItem("ship_mobile") + "&email=" + m[0].email + "&address=" + localStorage.getItem("ship_address") + "&postcode=" + localStorage.getItem("ship_postcode") + "&city=" + localStorage.getItem("ship_city") + "&shipping_country=" + localStorage.getItem("ship_country") + "&customer_id=" + localStorage.getItem("loginuserid") + "&product_array=" + m[0].cartitems + "&lang=" + localStorage.getItem("applanguage") + "&coupon_amount=" + m[0].coupon_amount + "&coupon_code=" + m[0].couponcode;
            console.log(this.servicedata);
            this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
                _this.data = res;
                //  this.commonservice.waitloaderhide(); 
                if (_this.data.status = "true") {
                    localStorage.setItem("orderid", _this.data.data);
                    localStorage.removeItem("ship_fname");
                    localStorage.removeItem("ship_lname");
                    localStorage.removeItem("ship_mobile");
                    localStorage.removeItem("ship_email");
                    localStorage.removeItem("ship_emailone");
                    localStorage.removeItem("ship_emailtwo");
                    localStorage.removeItem("ship_address");
                    localStorage.removeItem("ship_postcode");
                    localStorage.removeItem("ship_city");
                    localStorage.removeItem("ship_country");
                    localStorage.removeItem("cart" + "11");
                    localStorage.removeItem("cartproductcount");
                    localStorage.removeItem("couponcode");
                    _this.commonservice.successalert("Your order has been confirmed successfully");
                    localStorage.removeItem("orderconfirmotp");
                    localStorage.removeItem("orderconifrmdetails");
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__orderDetails_orderDetails__["a" /* OrderDetailsPage */]);
                }
                else {
                    localStorage.removeItem("orderconfirmotp");
                    _this.commonservice.erroralert("Something went wrong");
                }
            });
        }
        else {
            localStorage.removeItem("orderconfirmotp");
            this.commonservice.erroralert("Incorrect OTP");
        }
    };
    OrderConfirmedPage.prototype.sendmail = function () {
    };
    OrderConfirmedPage.prototype.s1 = function () {
        this.otpone = "";
    };
    OrderConfirmedPage.prototype.s2 = function () {
        this.otptwo = "";
    };
    OrderConfirmedPage.prototype.s3 = function () {
        this.otpthree = "";
    };
    OrderConfirmedPage.prototype.s4 = function () {
        this.otpfour = "";
    };
    OrderConfirmedPage.prototype.gotohome = function () {
        localStorage.removeItem("orderconfirmotp");
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__["a" /* DashboardPage */]);
    };
    OrderConfirmedPage.prototype.gotosearch = function () {
        localStorage.removeItem("orderconfirmotp");
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__search_search__["a" /* SearchPage */]);
    };
    OrderConfirmedPage.prototype.gotocat = function () {
        localStorage.removeItem("orderconfirmotp");
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__home_home__["a" /* HomePage */]);
    };
    OrderConfirmedPage.prototype.gotobag = function () {
        localStorage.removeItem("orderconfirmotp");
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__cartitem_cartitem__["a" /* CartitemPage */]);
    };
    OrderConfirmedPage.prototype.gotoaccount = function () {
        localStorage.removeItem("orderconfirmotp");
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__myAccount_myAccount__["a" /* MyAccountPage */]);
    };
    OrderConfirmedPage.prototype.gotoback = function () {
        localStorage.removeItem("orderconfirmotp");
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__payment_payment__["a" /* PaymentPage */]);
        //this.navCtrl.setRoot(MyOrderPage);
    };
    OrderConfirmedPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-orderConfirmed',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/orderConfirmed/orderConfirmed.html"*/'<ion-header>\n\n        <div class="headerbg_wh">\n\n    <div class="container" >\n\n            <!-- style=" padding-top: 20px; padding-bottom: 20px;" -->\n\n        <span style="font-size:20px; cursor:pointer;" ><i class="fa fa-arrow-left" (click)="gotoback()" aria-hidden="true" style="font-size: 20px;"></i> &nbsp; {{"MYORDERCONFIRMED" | translate}}</span>\n\n        <span style="font-size: 20px;cursor:pointer;float: right;" ><i class="fa fa-search" (click)="gotosearch()" aria-hidden="true" style="font-size: 20px;"></i></span>\n\n    </div>\n\n    </div>\n\n</ion-header>\n\n\n\n<ion-content style="background-color: rgb(245, 244, 244);">\n\n    <div class="container-fluid">\n\n        <div class="container">\n\n            <div class="row" >\n\n                <h4 style="color:black; font-size:16px; font-weight:bold; padding-bottom:0px; margin-bottom:0px; margin-top:10px;">\n\n                    <!--<i class="fa fa-arrow-left" aria-hidden="true" style="margin-right:30px;"></i>-->\n\n                </h4>\n\n            </div>\n\n        </div>\n\n\n\n        <div align="center">\n\n            <img src="assets/imgs/icon-images/Untitled.png" style="    width: 23%;">\n\n        </div>\n\n        <h4 style="color:black; font-size:16px; font-weight:600; padding-bottom:0px; margin-bottom:10px; margin-top:10px; text-align:center;">\n\n            {{"ORDER" | translate}} {{orderid}} </h4>\n\n\n\n        <h3 style="color:#666; font-size:14px; text-align:center;">	{{"THANKYOU" | translate}} {{username}} </h3>\n\n        <h3 style="color:#666; font-size:14px; text-align:center;">	{{"RECEIVEDOTP" | translate}} </h3>\n\n        <div class="form-group form-inline" align="center" style="width: 100%">\n\n             <!-- <div><input (paste)="false" (focus)="s1()"  [attr.maxlength]="1" pattern="[0-9]*" decimal="true" allow-multiple-decimals="true" type="number"  class="OptULli" id="otp1" maxlength="1" [(ngModel)]="otpone" (keyup)="moveto(1)"/></div>\n\n             <div><input (paste)="false" (focus)="s2()"  [attr.maxlength]="1" pattern="[0-9]*" decimal="true" allow-multiple-decimals="true" type="number" class="OptULli"  id="otp2" maxlength="1" [(ngModel)]="otptwo" (keyup)="moveto(2)"/></div>\n\n             <div><input (paste)="false" (focus)="s3()"  [attr.maxlength]="1" pattern="[0-9]*" decimal="true" allow-multiple-decimals="true" type="number" class="OptULli"  id="otp3" maxlength="1" [(ngModel)]="otpthree" (keyup)="moveto(3)"/></div>\n\n             <div><input (paste)="false" (focus)="s4()"  [attr.maxlength]="1" pattern="[0-9]*" decimal="true" allow-multiple-decimals="true"  type="number" class="OptULli"  id="otp4"  maxlength="1" [(ngModel)]="otpfour" (keyup)="moveto(4)"/></div> -->\n\n               <div><input  (focus)="s1()"  [attr.maxlength]="1" pattern="[0-9]*" decimal="true" allow-multiple-decimals="true" type="number"  class="OptULli" id="otp1" maxlength="1" [(ngModel)]="otpone" (keyup)="moveto(1)"/></div>\n\n             <div><input  (focus)="s2()"  [attr.maxlength]="1" pattern="[0-9]*" decimal="true" allow-multiple-decimals="true" type="number" class="OptULli"  id="otp2" maxlength="1" [(ngModel)]="otptwo" (keyup)="moveto(2)"/></div>\n\n             <div><input  (focus)="s3()"  [attr.maxlength]="1" pattern="[0-9]*" decimal="true" allow-multiple-decimals="true" type="number" class="OptULli"  id="otp3" maxlength="1" [(ngModel)]="otpthree" (keyup)="moveto(3)"/></div>\n\n             <div><input  (focus)="s4()"  [attr.maxlength]="1" pattern="[0-9]*" decimal="true" allow-multiple-decimals="true"  type="number" class="OptULli"  id="otp4"  maxlength="1" [(ngModel)]="otpfour" (keyup)="moveto(4)"/></div>\n\n        </div>\n\n        <br>\n\n\n\n        <div class="row" style="margin-top: 20px;">\n\n            <div class="col">\n\n                <button class="btn btn-primary  btn-block updatebtn" (click)="continueShopping()" >{{"SUBMIT" | translate}}</button>\n\n            </div>  \n\n            <div class="col">\n\n                <button class="btn btn-primary  btn-block Add-Cart-btn" (click)="otpss()" >{{"Resendotp" | translate}}</button>\n\n                <!-- {{"CANCELORDER" | translate}} -->\n\n            </div>\n\n        </div> \n\n        <br>\n\n        <h3 style="color:#666; font-size:14px; text-align:center;">	{{"CONFIRMEDORDER" | translate}} !! </h3>\n\n        \n\n    </div>	\n\n</ion-content >\n\n\n\n'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/orderConfirmed/orderConfirmed.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */], __WEBPACK_IMPORTED_MODULE_6__providers_commonservice_commonservice__["a" /* CommonserviceProvider */]])
    ], OrderConfirmedPage);
    return OrderConfirmedPage;
}());

//# sourceMappingURL=orderConfirmed.js.map

/***/ }),

/***/ 24:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__addtocart_addToCart__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_commonservice_commonservice__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__myAccount_myAccount__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__categorydetails_categoryDetails__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__cartitem_cartitem__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_completer__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__home_home__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__myorder_myOrder__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__orderDetails_orderDetails__ = __webpack_require__(74);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var SearchPage = /** @class */ (function () {
    function SearchPage(platform, completerService, navCtrl, commonservice, events) {
        var _this = this;
        this.platform = platform;
        this.completerService = completerService;
        this.navCtrl = navCtrl;
        this.commonservice = commonservice;
        this.events = events;
        this.error = "";
        this.productlist = [];
        this.searchData = [];
        this.captains = [];
        this.platform.registerBackButtonAction(function () {
            if (localStorage.getItem('lastpageserch') == 'DashboardPage') {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__["a" /* DashboardPage */]);
            }
            else {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__categorydetails_categoryDetails__["a" /* CategoryDetailsPage */]);
            }
        });
        var allproductlist = JSON.parse(localStorage.getItem("allproductlist"));
        console.log(allproductlist);
        if (localStorage.getItem("applanguage") == "er") {
            var setLang = 2;
        }
        else {
            var setLang = 1;
        }
        if (allproductlist != null) {
            if (allproductlist.length > 0) {
                var i = 0;
                for (var _i = 0, allproductlist_1 = allproductlist; _i < allproductlist_1.length; _i++) {
                    var alt = allproductlist_1[_i];
                    if (alt.language_id == setLang) {
                        this.productlist.push({ "product_id": alt.product_id, "image": alt.image, "name": alt.name, "price": alt.price, "model": alt.model, "is_discount": alt.is_discount, "discount_price": alt.discount_price });
                        i++;
                    }
                }
            }
        }
        console.log(this.productlist);
        this.dataService = completerService.local(this.productlist, 'name', 'name');
        events.publish('allpagecommon');
        this.dataService = completerService.local(this.productlist, 'name', 'name');
    }
    SearchPage.prototype.gocatgorydetails = function (id) {
        localStorage.setItem("details_pdid", id);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__addtocart_addToCart__["a" /* AddtocartPage */]);
    };
    SearchPage.prototype.gotohome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__["a" /* DashboardPage */]);
    };
    SearchPage.prototype.gotosearch = function () {
    };
    SearchPage.prototype.gotocat = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__home_home__["a" /* HomePage */]);
    };
    SearchPage.prototype.gotobag = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__cartitem_cartitem__["a" /* CartitemPage */]);
    };
    SearchPage.prototype.gotoaccount = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__myAccount_myAccount__["a" /* MyAccountPage */]);
    };
    SearchPage.prototype.gotofunction = function () {
        this.searchText = this.searchStr;
    };
    SearchPage.prototype.goback = function () {
        console.log(localStorage.getItem('lastpageserch'));
        if (localStorage.getItem('lastpageserch') == 'DashboardPage') {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__["a" /* DashboardPage */]);
        }
        else if (localStorage.getItem('lastpageserch') == 'HomePage') {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__home_home__["a" /* HomePage */]);
        }
        else if (localStorage.getItem('lastpageserch') == 'myorderPage') {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__myorder_myOrder__["a" /* MyOrderPage */]);
        }
        else if (localStorage.getItem('lastpageserch') == 'orderdetilaspage') {
            // localStorage.setItem("orderid",id); 
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_11__orderDetails_orderDetails__["a" /* OrderDetailsPage */]);
        }
        else if (localStorage.getItem('lastpageserch') == 'CategoryDetailsPage') {
            // localStorage.setItem("orderid",id); 
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__categorydetails_categoryDetails__["a" /* CategoryDetailsPage */]);
        }
        else {
            //  this.navCtrl.setRoot(CategoryDetailsPage); 
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__["a" /* DashboardPage */]);
        }
    };
    SearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-search',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/search/search.html"*/'<ion-header>\n\n    <!-- <div class="CustomTop"> -->\n\n            <div class="headerbg_bl">\n\n            <div class="container">\n\n                <div class="row" style="display: flex; justify-content: center;align-items: center;">\n\n                    <div class="col-xs-2" style="width: 16.66666667%;">\n\n                        <a (click)="goback()"><img class="LefFix" src="assets/images/left.png"> </a>\n\n                    </div>\n\n                    <div class="col-xs-10" style="width:80%;">\n\n                            \n\n                       <ng2-completer [(ngModel)]="searchStr"  [datasource]="dataService" [minSearchLength]="0" placeholder=\'{{"Search" | translate}}\'></ng2-completer>\n\n                    </div>\n\n                   \n\n                </div>\n\n            </div>\n\n            </div>\n\n    <!-- </div> -->\n\n</ion-header>\n\n	\n\n	\n\n\n\n<ion-content>\n\n   \n\n    <div class="container-fluid" style="padding-top: 10px; padding-bottom: 10px;">\n\n     \n\n        <div class="row" style="margin-top: 14px;" >\n\n            <div *ngFor="let pl of productlist | filter: searchStr"   (click)="gocatgorydetails(pl.product_id)" style="    width: 100%;">\n\n                <div class="col-xs-4" style="padding:19px;padding-top: 0px;">\n\n                    <img src="{{pl.image}}" style="    height: 72px;width: 200px;">\n\n                </div>\n\n                <div class="col-xs-8" style="padding:0;">\n\n                    <p style="margin:0;">\n\n                        <span style="width: 60%; font-weight: bold;">{{pl.name}}</span>  \n\n                    </p>\n\n                    <div>\n\n                        <!-- <span style="color: red;font-weight: bold;">SAR {{pl.price}}</span> -->\n\n                        <div *ngIf="pl.is_discount == 1">\n\n                            <span  style="color: rgb(223, 11, 11);margin: 0;line-height: 20px;font-size: 15px;font-weight: 500;">SAR {{pl.discount_price}}</span>\n\n                            <span  style="font-size: 13px;text-decoration: line-through;color: grey;margin: 0 0 0 10px;line-height: 20px;">SAR {{pl.price}}</span>            \n\n                        </div>\n\n                        <div *ngIf="pl.is_discount == 0">\n\n                            <div  style="color: grey;margin: 0; line-height: 20px;">SAR {{pl.price}}</div>\n\n                        </div> \n\n                    </div>\n\n                </div>\n\n                <div class="col-xs-12" style=\'padding: 0;\'><hr></div>\n\n            </div>\n\n        </div>\n\n\n\n    </div>  \n\n</ion-content>\n\n<!-- <ion-footer>\n\n    <ion-toolbar>\n\n        <div (click)="gotohome()" style="text-align:center; width:20%; float:left;">\n\n            <a class="iconname"  style="color:black;font-size: 12px;"> \n\n                <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/home.png">\n\n                <br>\n\n                {{"HOME" | translate}}  \n\n            </a>\n\n        </div> \n\n        <div (click)="gotosearch()" style="text-align:center; width:20%; float:left;">\n\n            <a class="iconname"  style="color:black;font-size: 12px;"> \n\n                <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/search.png">\n\n                <br>\n\n                {{"SEARCH" | translate}}</a>\n\n        </div>\n\n        <div  (click)="gotocat()"  style="text-align:center; width:20%; float:left;">\n\n            <a class="iconname" style="color:black;font-size: 12px;">  \n\n                <img  style="    height: 18px;" src="assets/imgs/icon-images/list.png">\n\n                <br>\n\n                {{"CATEGORY" | translate}} </a>\n\n        </div>\n\n        <div (click)="gotobag()"  style="text-align:center; width:20%; float:left;">\n\n            <a class="iconname" style="color:black;font-size: 12px;"> \n\n                <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/shopping-bag.png">\n\n                <br>\n\n                {{"BAG" | translate}}\n\n            </a>\n\n        </div>\n\n        <div (click)="gotoaccount()"   style="text-align:center; width:20%; float:left;">\n\n            <a  class="iconname" style="color:black;font-size: 12px;"> \n\n                <img class="img-fluid" style="    height: 18px;"  src="assets/imgs/icon-images/round-account-button-with-user-inside.png">\n\n                <br>\n\n               {{"MYACCOUNT" | translate}} </a>\n\n        </div>\n\n    </ion-toolbar>\n\n</ion-footer> -->\n\n\n\n<!-- <ion-footer>\n\n        <div style="display:flex;background-color: #f0f0f0;padding-top:8px;padding-bottom:8px;">\n\n           <div (click)="gotohome()" style=\'flex:1;text-align:center\'>\n\n               <a class="iconname"  style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/home.png">\n\n                   <br>\n\n                   {{"HOME" | translate}} \n\n               </a>\n\n           </div> \n\n           <div (click)="gotosearch()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/search.png">\n\n                   <br>\n\n                   {{"SEARCH" | translate}}</a>\n\n           </div>\n\n           <div (click)="gotocatpage()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;">  \n\n                   <img  style="    height: 18px;" src="assets/imgs/icon-images/list.png">\n\n                   <br>\n\n                   {{"CATEGORY" | translate}} </a>\n\n           </div>\n\n           <div (click)="gotobag()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="height: 18px;" src="assets/imgs/icon-images/shopping-bag.png">\n\n                   <br>\n\n                   {{"BAG" | translate}}\n\n               </a>\n\n           </div>\n\n           <div (click)="gotoaccount()"  style=\'flex:1;text-align:center\'>\n\n               <a   class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="height: 18px;"  src="assets/imgs/icon-images/round-account-button-with-user-inside.png">\n\n                   <br>\n\n                   {{"MYACCOUNT" | translate}} </a>\n\n           </div>\n\n        </div>\n\n</ion-footer> -->\n\n\n\n<ion-footer>\n\n        <div style="display:flex;">\n\n           <div style=\'flex:1;text-align:center\'>\n\n               <a class="iconname"  style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/home.png">\n\n                   <br>\n\n                   {{"HOME" | translate}} \n\n               </a>\n\n           </div> \n\n           <div (click)="gotosearch()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/search.png">\n\n                   <br>\n\n                   {{"SEARCH" | translate}}</a>\n\n           </div>\n\n           <div (click)="gotocat()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;">  \n\n                   <img  style="    height: 18px;" src="assets/imgs/icon-images/list.png">\n\n                   <br>\n\n                   {{"CATEGORY" | translate}} </a>\n\n           </div>\n\n           <div (click)="gotobag()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="height: 18px;" src="assets/imgs/icon-images/shopping-bag.png">\n\n                   <br>\n\n                   {{"BAG" | translate}}\n\n               </a>\n\n           </div>\n\n           <div (click)="gotoaccount()"  style=\'flex:1;text-align:center\'>\n\n               <a   class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="height: 18px;"  src="assets/imgs/icon-images/round-account-button-with-user-inside.png">\n\n                   <br>\n\n                   {{"MYACCOUNT" | translate}} </a>\n\n           </div>\n\n        </div>\n\n</ion-footer>\n\n'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/search/search.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_8_ng2_completer__["a" /* CompleterService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__providers_commonservice_commonservice__["a" /* CommonserviceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */]])
    ], SearchPage);
    return SearchPage;
}());

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 272:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FaqPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__search_search__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__myAccount_myAccount__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__mycart_myCart__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__home_home__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







// import { myCartPage } from '../mycart/mycart';

var FaqPage = /** @class */ (function () {
    function FaqPage(platform, navCtrl) {
        var _this = this;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.platform.registerBackButtonAction(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__["a" /* DashboardPage */]);
        });
    }
    FaqPage.prototype.gotoback = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__["a" /* DashboardPage */]);
    };
    FaqPage.prototype.gotohome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__["a" /* DashboardPage */]);
    };
    FaqPage.prototype.gotosearch = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__search_search__["a" /* SearchPage */]);
    };
    FaqPage.prototype.gotocat = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__home_home__["a" /* HomePage */]);
    };
    FaqPage.prototype.gotobag = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__mycart_myCart__["a" /* myCartPage */]);
    };
    FaqPage.prototype.gotoaccount = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__myAccount_myAccount__["a" /* MyAccountPage */]);
    };
    FaqPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-faq',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/faq/faq.html"*/'<ion-content >\n\n    <div class="container" style="    height: 8vh;">\n\n        <h4 style="color:black; font-size:16px; font-weight:bold; padding-bottom:0px; margin-bottom:0px; margin-top:17px;">\n\n            <a (click)="gotoback()"><i class="fa fa-arrow-left" aria-hidden="true" style="margin-right:30px;"></i></a>{{"FAQ" | translate}}\n\n        </h4>\n\n    </div>\n\n    <hr style="color:black;">\n\n    <h2 style="text-align: center">FAQs</h2>\n\n    <div style="padding: 40px;padding-top: 10px;">\n\n        <p>Why do I see different prices for the same product?</p>\n\n        <p>Why do I see different prices for the same product?</p>\n\n        <p>Credit card EMI option, you can choose to pay in easy installments of 3, 6, 9, 12, 18, or 24 months, with credit cards from the following banks:</p>\n\n        <p>The delivery of my order is delayed. What should I do?</p>\n\n        <p>I had a valid Plus Coupon but I cancelled the order. Can I get the coupon again?</p>\n\n        <p>What are the complete terms and condition for a particular Plus Coupon?</p>\n\n             \n\n    </div>\n\n </ion-content >\n\n<ion-footer>\n\n    <ion-toolbar >\n\n            <div (click)="gotohome()" style="text-align:center; width:20%; float:left;">\n\n                <a class="iconname" (click)="gotohome()" style="color:black;font-size: 12px;"> \n\n                    <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/home.png">\n\n                    <br>\n\n                    {{"HOME" | translate}} \n\n                </a>\n\n            </div> \n\n            <div style="text-align:center; width:20%; float:left;">\n\n                <a class="iconname" (click)="gotosearch()" style="color:black;font-size: 12px;"> \n\n                    <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/search.png">\n\n                    <br>\n\n                    {{"SEARCH" | translate}}</a>\n\n            </div>\n\n            <div  style="text-align:center; width:20%; float:left;">\n\n                <a class="iconname" (click)="gotocat()" style="color:black;font-size: 12px;">  \n\n                    <img  style="    height: 18px;" src="assets/imgs/icon-images/list.png">\n\n                    <br>\n\n                    {{"CATEGORY" | translate}}</a>\n\n            </div>\n\n            <div style="text-align:center; width:20%; float:left;">\n\n                <a (click)="gotobag()" class="iconname" style="color:black;font-size: 12px;"> \n\n                    <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/shopping-bag.png">\n\n                    <br>\n\n                    {{"BAG" | translate}}\n\n                </a>\n\n            </div>\n\n            <div  style="text-align:center; width:20%; float:left;">\n\n                <a  (click)="gotoaccount()"  class="iconname" style="color:black;font-size: 12px;"> \n\n                    <img class="img-fluid" style="    height: 18px;"  src="assets/imgs/icon-images/round-account-button-with-user-inside.png">\n\n                    <br>\n\n                    {{"MYACCOUNT" | translate}} </a>\n\n            </div>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/faq/faq.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */]])
    ], FaqPage);
    return FaqPage;
}());

//# sourceMappingURL=faq.js.map

/***/ }),

/***/ 273:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyWishlistPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__search_search__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__myAccount_myAccount__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_commonservice_commonservice__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__cartitem_cartitem__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__home_home__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__addtocart_addToCart__ = __webpack_require__(37);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







// import { myCartPage } from '../mycart/mycart';

// import { ShoppingCartPage } from '../shoppingcart/shoppingcart';


// import { AddtocartPage } from '../addtocart/addtocart';

var MyWishlistPage = /** @class */ (function () {
    function MyWishlistPage(platform, navCtrl, commonservice, events) {
        var _this = this;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.commonservice = commonservice;
        this.events = events;
        this.error = "";
        this.image = "assets/imgs/product-des.png";
        this.name = "Black/Green Leaf Cotton Silk Screen Printed Kalamkari Fabric";
        this.protype = "SAR 139 / Meter";
        this.promaterial = "slikone";
        this.color = "black";
        this.proquenty = "1";
        this.price = "1000";
        this.loadpage();
        events.publish('allpagecommon');
        if (localStorage.getItem("loginuserid") == "" || localStorage.getItem("loginuserid") == "null") {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__["a" /* DashboardPage */]);
        }
        // else if(localStorage.getItem("loginuserid")=="null")
        // {
        //     this.navCtrl.setRoot(DashboardPage);
        // }
        this.platform.registerBackButtonAction(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__["a" /* DashboardPage */]);
        });
    }
    MyWishlistPage.prototype.gotoMoveToCart = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__addtocart_addToCart__["a" /* AddtocartPage */]);
    };
    MyWishlistPage.prototype.gotoback = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__["a" /* DashboardPage */]);
    };
    MyWishlistPage.prototype.loadpage = function () {
        var _this = this;
        if (localStorage.getItem("applanguage") == "er") {
            var setLang = 2;
        }
        else {
            var setLang = 1;
        }
        this.commonservice.waitloadershow();
        this.servicedata = "wish_list?userid=" + localStorage.getItem("loginuserid") + "&lang=" + setLang;
        console.log(this.servicedata);
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.commonservice.waitloaderhide();
            _this.data = res;
            console.log(_this.data);
            if (_this.data.status == true) {
                _this.wishlist = _this.data.data;
            }
            else {
                _this.wishlist = [];
            }
        });
    };
    MyWishlistPage.prototype.gotohome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__["a" /* DashboardPage */]);
    };
    MyWishlistPage.prototype.gotosearch = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__search_search__["a" /* SearchPage */]);
    };
    MyWishlistPage.prototype.gotocat = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__home_home__["a" /* HomePage */]);
    };
    MyWishlistPage.prototype.gotobag = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__cartitem_cartitem__["a" /* CartitemPage */]);
    };
    MyWishlistPage.prototype.gotoaccount = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__myAccount_myAccount__["a" /* MyAccountPage */]);
    };
    MyWishlistPage.prototype.deletewishlist = function (product_id) {
        var _this = this;
        console.log(product_id);
        this.commonservice.waitloadershow();
        this.servicedata = "remove_wishlist?product_id=" + product_id + "&userid=" + localStorage.getItem("loginuserid") + "&lang=" + localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            console.log(_this.data);
            _this.commonservice.waitloaderhide();
            if (_this.data.status = "true") {
                _this.wishlist = [];
                _this.commonservice.successalert("Product removed from wishlist sucessfully");
                _this.loadpage();
            }
            else {
                _this.commonservice.erroralert("Something went wrong");
            }
        });
    };
    MyWishlistPage.prototype.loadpageto = function () {
        var _this = this;
        this.wishlist = [];
        this.servicedata = "wish_list?userid=" + localStorage.getItem("loginuserid") + "&lang=" + localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            console.log(_this.data);
            if (_this.data.status == true) {
                _this.wishlist = _this.data.data;
            }
            else {
                _this.wishlist = [];
            }
        });
    };
    MyWishlistPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mywishlist',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/mywishlist/mywishlist.html"*/'\n\n<ion-header >\n\n        <div class="headerbg_wh">\n\n    <div class="container">\n\n        <span style="margin-right: 20px; font-size:20px; cursor:pointer;" ><i class="fa fa-arrow-left" (click)="gotoback()" aria-hidden="true" style="font-size: 20px;"></i> &nbsp; {{"MYWISHLIST" | translate}}</span>\n\n        <span style="font-size:20px;cursor:pointer;float: right;" ><i class="fa fa-search" (click)="gotosearch()" aria-hidden="true" style="font-size: 20px;"></i></span>\n\n    </div> \n\n    </div> \n\n</ion-header>\n\n\n\n<ion-content style="background-color: rgb(245, 244, 244);">\n\n    <br>\n\n    <br>\n\n    <div class="container-fluid">\n\n        <div *ngFor="let wl of wishlist"  class="container-fluid">\n\n            <div class="row">    \n\n                <div class="col-6" style="width: 35%;    ">\n\n                     <img class="" src="{{wl.image}}" style="height: 20vh;border-radius: 7px;">			\n\n                </div>\n\n                <div class="col-6" style="width: 64%;    padding-left: 10px;">\n\n                    <div class="card product" style="box-shadow:none; border:none;">\n\n                        <h3 class="notificationh3" style="font-size: 15px;"> {{wl.name}}</h3>\n\n                        <span style="font-size: 16px;"></span>\n\n                        <span style="font-size: 16px;">\n\n                            <div *ngIf="wl.is_discount == 1">\n\n                                <span class="text-center" style="    color: #000;\n\n                                margin: 0;\n\n                                line-height: 20px;\n\n                                font-size: 15px !important;\n\n                                font-weight: 500;">SAR {{wl.discount_price}}</span>\n\n                                <span class="text-center" style="    font-size: 13px;\n\n                                text-decoration: line-through;\n\n                                color: grey;\n\n                                font-size: 14px !important;\n\n                                margin: 0 0 0 6px;\n\n                                line-height: 20px;">SAR {{wl.price}}</span>            \n\n                               </div>\n\n                                <div *ngIf="wl.is_discount == 0">\n\n                                <p class="text-center" style="color: grey;margin: 0; line-height: 20px;">SAR {{prot.pt.price}}</p>\n\n                               </div>\n\n                            <!-- {{"SAR" | translate}}.{{wl.price}} -->\n\n                        </span>\n\n                    </div>\n\n\n\n                    <div class="row">\n\n                        <div class="col-md-12">\n\n                            <button (click)="gotoMoveToCart()" class="btn btn-primary  btn-block Add-Cart-btn cust_cart_btn" style="font-size:12px;    border-radius: 22px;width: 100%;">{{"MOVETOCART" | translate}}</button>\n\n                        </div>\n\n                    </div> \n\n                </div>\n\n                <div style="width: 100%; margin: 15px auto; text-align: center;">\n\n                    <div class="wshbtn" style="    position: relative;\n\n                    width: 45%;\n\n                    margin: 0 5px;\n\n                    display: inline-block;">\n\n                        <button class="btn btn-primary  btn-block updatebtn" (click)="gotoback()" style="border-radius: 22px;">{{"ADDNEWPRODUCT" | translate}}</button>\n\n                    </div>  \n\n                    <div class="wshbtn" style="    position: relative;\n\n                    width: 45%;\n\n                    margin: 0 5px;\n\n                    display: inline-block;">\n\n                        <button class="btn btn-primary  btn-block Add-Cart-btn" (click)="deletewishlist(wl.product_id)" style="border-radius: 22px;">{{"DELETE" | translate}}</button>\n\n                    </div>\n\n                </div>\n\n            </div>\n\n            <hr>\n\n           \n\n        \n\n         \n\n        </div> \n\n         <div *ngIf="wishlist==0" style="    text-align: center; margin-top: 83px; font-size: 20px; color: #848484;">{{"NOITEMINWISHLIST" | translate}}</div>\n\n       \n\n    </div>\n\n</ion-content>\n\n<!-- <ion-footer>\n\n        <div style="display:flex;">\n\n           <div style=\'flex:1;text-align:center\'>\n\n               <a class="iconname"  style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/home.png">\n\n                   <br>\n\n                   {{"HOME" | translate}} \n\n               </a>\n\n           </div> \n\n           <div (click)="gotosearch()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/search.png">\n\n                   <br>\n\n                   {{"SEARCH" | translate}}</a>\n\n           </div>\n\n           <div (click)="gotocatpage()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;">  \n\n                   <img  style="    height: 18px;" src="assets/imgs/icon-images/list.png">\n\n                   <br>\n\n                   {{"CATEGORY" | translate}} </a>\n\n           </div>\n\n           <div (click)="gotobag()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="height: 18px;" src="assets/imgs/icon-images/shopping-bag.png">\n\n                   <br>\n\n                   {{"BAG" | translate}}\n\n               </a>\n\n           </div>\n\n           <div (click)="gotoaccount()"  style=\'flex:1;text-align:center\'>\n\n               <a   class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="height: 18px;"  src="assets/imgs/icon-images/round-account-button-with-user-inside.png">\n\n                   <br>\n\n                   {{"MYACCOUNT" | translate}} </a>\n\n           </div>\n\n        </div>\n\n</ion-footer> -->\n\n<ion-footer>\n\n    <div style="display:flex;">\n\n       <div (click)="gotohome()"  style=\'flex:1;text-align:center\'>\n\n           <a class="iconname"  style="color:black;font-size: 10px;"> \n\n               <img class="img-fluid" style="height: 18px;" src="assets/imgs/icon-images/home.png">\n\n               <br>\n\n               {{"HOME" | translate}} \n\n           </a>\n\n       </div> \n\n       <div (click)="gotosearch()"  style=\'flex:1;text-align:center\'>\n\n           <a class="iconname" style="color:black;font-size: 10px;"> \n\n               <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/search.png">\n\n               <br>\n\n               {{"SEARCH" | translate}}</a>\n\n       </div>\n\n       <div (click)="gotocat()"  style=\'flex:1;text-align:center\'>\n\n           <a class="iconname" style="color:black;font-size: 10px;">  \n\n               <img  style="    height: 18px;" src="assets/imgs/icon-images/list.png">\n\n               <br>\n\n               {{"CATEGORY" | translate}} </a>\n\n       </div>\n\n       <div (click)="gotobag()"  style=\'flex:1;text-align:center\'>\n\n           <a class="iconname" style="color:black;font-size: 10px;"> \n\n               <img class="img-fluid" style="height: 18px;" src="assets/imgs/icon-images/shopping-bag.png">\n\n               <br>\n\n               {{"BAG" | translate}}\n\n           </a>\n\n       </div>\n\n       <div (click)="gotoaccount()"  style=\'flex:1;text-align:center\'>\n\n           <a   class="iconname" style="color:black;font-size: 10px;"> \n\n               <img class="img-fluid" style="height: 18px;"  src="assets/imgs/icon-images/round-account-button-with-user-inside.png">\n\n               <br>\n\n               {{"MYACCOUNT" | translate}} </a>\n\n       </div>\n\n    </div>\n\n</ion-footer>\n\n'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/mywishlist/mywishlist.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */], __WEBPACK_IMPORTED_MODULE_5__providers_commonservice_commonservice__["a" /* CommonserviceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */]])
    ], MyWishlistPage);
    return MyWishlistPage;
}());

//# sourceMappingURL=mywishlist.js.map

/***/ }),

/***/ 274:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SplishPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__search_search__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__myAccount_myAccount__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__mycart_myCart__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SplishPage = /** @class */ (function () {
    function SplishPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    SplishPage.prototype.gotohome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    SplishPage.prototype.gotosearch = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__search_search__["a" /* SearchPage */]);
    };
    SplishPage.prototype.gotocat = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    SplishPage.prototype.gotobag = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__mycart_myCart__["a" /* myCartPage */]);
    };
    SplishPage.prototype.gotoaccount = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__myAccount_myAccount__["a" /* MyAccountPage */]);
    };
    SplishPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-splishpage',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/splishpage/splishpage.html"*/'<ion-content>\n\n    <div style="background-color: black">\n\n        <img src="assets/imgs/Splash.gif" class="img-responsive" style="height: 100vh;width: 100%">\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/splishpage/splishpage.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */]])
    ], SplishPage);
    return SplishPage;
}());

//# sourceMappingURL=splishpage.js.map

/***/ }),

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MobilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_commonservice_commonservice__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__payment_payment__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MobilePage = /** @class */ (function () {
    function MobilePage(platform, navCtrl, navParams, commonservice) {
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.commonservice = commonservice;
        this.error = "";
        this.mobilenumber = '+966';
        this.numbervalue = '';
        this.platform.registerBackButtonAction(function () {
        });
    }
    MobilePage.prototype.ionViewDidLoad = function () {
        this.loadpage();
    };
    MobilePage.prototype.loadpage = function () {
        var _this = this;
        this.commonservice.waitloadershow();
        this.servicedata = "country/" + localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.commonservice.waitloaderhide();
            _this.data = res;
            _this.countrylist = _this.data.data;
            console.log(_this.data.data);
        });
    };
    MobilePage.prototype.login = function () {
        if (this.mobilenumber != null) {
            var number = this.mobilenumber;
            var numbertostr = number.toString();
            var numberlength = numbertostr.length;
            console.log(numberlength);
            if (Number(numberlength) < 11) {
                this.numbervalue = '1';
            }
            else if (Number(numberlength) > 16) {
                this.numbervalue = '1';
            }
            else {
                this.numbervalue = '0';
            }
        }
        console.log(this.numbervalue);
        if (!this.mobilenumber) {
            this.error = "Mobile Number is required";
        }
        /*else if(this.numbervalue == '1')
        {
            this.error="Please enter valid mobile number";
        }*/
        if (this.error == "") {
            localStorage.setItem("ship_mobile", this.mobilenumber);
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__payment_payment__["a" /* PaymentPage */]);
            /* this.commonservice.waitloadershow();
             this.servicedata="login?mobile="+this.mobilenumber;
             this.commonservice.serverdataget(this.servicedata).subscribe(
                 res => {
                     this.commonservice.waitloaderhide();
                     this.data=res;
 
 
                     console.log(this.data);
                     if(this.data.status="true")
                     {
                         localStorage.setItem("loginotp", this.data.otp);
                         localStorage.setItem("loginuserid", this.data.userid);
                         this.navCtrl.setRoot(VerificationPage);
                     }
                     else
                     {
                         this.commonservice.erroralert("Something went wrong");
                     }
 
 
                 }
             )*/
        }
        else {
            this.commonservice.erroralert(this.error);
        }
    };
    MobilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mobile',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/mobile/mobile.html"*/'<ion-header style="background-color: white;">\n    <!-- <ion-navbar > -->\n            <div class="headerbg_wh">\n                    <div class="container">\n        <ion-title >{{"MOBILEVERIFICATION" | translate}}</ion-title>\n        </div>\n        </div>\n    <!-- </ion-navbar> -->\n</ion-header>\n\n\n<ion-content style="background-color: rgb(245, 244, 244);">\n    <div class="container-fluid pt-5 align_content" style="margin-top: 100px;">\n        <div class="loginpage " style="padding:0px 15px;padding-top: 0px;">\n             <div class="form-group form-inline" style="width: 100%">\n                <label style="width:10%;float: left"><img src="assets/imgs/icon-images/flag.png" style="color: #292727; margin-top: -4px; height:24px;"></label>\n                <!--<input name="userCcountry" class="line-animation" placeholder="Saudi Arabia" type="text"  style="color:black; width:90%;float: left" >-->\n                <select class="line-animation" style="color:black; width:80%;float: left;font-size: 17px;" >\n                    <option *ngFor="let co of countrylist" value="{{co}}">{{co}}</option>\n                </select>\n                <div class="line"></div>\n            </div>\n           \n            <br><br>\n            <div class="form-group form-inline" style="width: 100%;margin-top: 25px;">\n                <label style="width:10%;float: left;font-size: 17px;"><img src="assets/imgs/icon-images/call-answer.png" style="    margin-top: 0px; height:24px;"></label>\n                <input name="userCcountry" class="line-animation" placeholder="" type="tel" style="color:black; width:80%;float: left;font-size: 17px;" [(ngModel)]="mobilenumber" >\n                <div class="line"></div>\n            </div>\n            <br><br>\n            <div class="row" style="width: 100%;padding-left: 32px;padding-top: 30px;">\n                <div class="col-md-12 col-lg-12" style="float: left;width:100%">\n                    <button class="btn btn-primary  btn-block register" style="border-radius: 21px;" (click)="login()" >{{"SUBMIT" | translate}}</button>\n                </div>\n            </div>\n            <!--<div class="row" style="width: 100%;padding-left: 32px;    margin-top: 35px;">\n                <div class="col-md-12 col-lg-12" style="float: left;width:100%">\n                    <button class="btn btn-primary  btn-block register" style="border-radius: 21px;background-color: white !important;color: black" (click)="gohome()">Skip</button>\n                </div>\n            </div>-->\n            <br>\n        </div>\n    </div>\n    \n</ion-content>\n\n'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/mobile/mobile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_commonservice_commonservice__["a" /* CommonserviceProvider */]])
    ], MobilePage);
    return MobilePage;
}());

//# sourceMappingURL=mobile.js.map

/***/ }),

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(284);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 284:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_commonservice_commonservice__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_http_loader__ = __webpack_require__(424);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__filter_pipe__ = __webpack_require__(426);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ionic_img_viewer__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_component__ = __webpack_require__(427);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_home_home__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_dashboard_dashboard__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_list_list__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_splishpage_splishpage__ = __webpack_require__(274);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_ngx_pinch_zoom__ = __webpack_require__(436);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_status_bar__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_splash_screen__ = __webpack_require__(271);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_search_search__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_signup_signup__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_verification_verification__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_faq_faq__ = __webpack_require__(272);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_categorydetails_categoryDetails__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_addtocart_addToCart__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_shoppingcart_shoppingCart__ = __webpack_require__(437);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_mycart_myCart__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_orderDetails_orderDetails__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_payment_payment__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_orderConfirmed_orderConfirmed__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_delieveryTimeline_delieveryTimeline__ = __webpack_require__(438);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_myAccount_myAccount__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_notification_notification__ = __webpack_require__(439);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_mywishlist_mywishlist__ = __webpack_require__(273);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_myorder_myOrder__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_signinOtp_signinOtp__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_cartitem_cartitem__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_mobile_mobile__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_zoomimg_zoomimg__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37_ng2_completer__ = __webpack_require__(200);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


























// import { CategoryDetailsPage } from '../pages/categorydetails/categorydetails';
// import { AddtocartPage } from '../pages/addtocart/addtocart';
// import { ShoppingCartPage } from '../pages/shoppingcart/shoppingcart';
// import { myCartPage } from '../pages/mycart/mycart';



// import { OrderDetailsPage } from '../pages/orderdetails/orderdetails';









function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_6__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/language/', '.json');
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_10__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_search_search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_verification_verification__["a" /* VerificationPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_faq_faq__["a" /* FaqPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_categorydetails_categoryDetails__["a" /* CategoryDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_addtocart_addToCart__["a" /* AddtocartPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_shoppingcart_shoppingCart__["a" /* ShoppingCartPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_mycart_myCart__["a" /* myCartPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_payment_payment__["a" /* PaymentPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_orderConfirmed_orderConfirmed__["a" /* OrderConfirmedPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_delieveryTimeline_delieveryTimeline__["a" /* DelieveryTimelinePage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_orderDetails_orderDetails__["a" /* OrderDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_myAccount_myAccount__["a" /* MyAccountPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_notification_notification__["a" /* NotificationPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_mywishlist_mywishlist__["a" /* MyWishlistPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_myorder_myOrder__["a" /* MyOrderPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_cartitem_cartitem__["a" /* CartitemPage */],
                __WEBPACK_IMPORTED_MODULE_7__filter_pipe__["a" /* FilterPipe */],
                __WEBPACK_IMPORTED_MODULE_35__pages_mobile_mobile__["a" /* MobilePage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_zoomimg_zoomimg__["a" /* ZoomimgPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_dashboard_dashboard__["a" /* DashboardPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_splishpage_splishpage__["a" /* SplishPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_signinOtp_signinOtp__["a" /* SigninOtpPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_37_ng2_completer__["b" /* Ng2CompleterModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* MyApp */], { statusbarPadding: true }, {
                    links: [
                        { loadChildren: '../pages/mobile/mobile.module#MobilePageModule', name: 'MobilePage', segment: 'mobile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/cartitem/cartitem.module#CartitemPageModule', name: 'CartitemPage', segment: 'cartitem', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_8_ionic_img_viewer__["b" /* IonicImageViewerModule */],
                __WEBPACK_IMPORTED_MODULE_14_ngx_pinch_zoom__["a" /* PinchZoomModule */],
                __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: createTranslateLoader,
                        deps: [__WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]]
                    }
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_10__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_search_search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_verification_verification__["a" /* VerificationPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_faq_faq__["a" /* FaqPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_categorydetails_categoryDetails__["a" /* CategoryDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_addtocart_addToCart__["a" /* AddtocartPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_shoppingcart_shoppingCart__["a" /* ShoppingCartPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_mycart_myCart__["a" /* myCartPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_payment_payment__["a" /* PaymentPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_orderConfirmed_orderConfirmed__["a" /* OrderConfirmedPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_delieveryTimeline_delieveryTimeline__["a" /* DelieveryTimelinePage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_orderDetails_orderDetails__["a" /* OrderDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_myAccount_myAccount__["a" /* MyAccountPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_notification_notification__["a" /* NotificationPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_mywishlist_mywishlist__["a" /* MyWishlistPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_cartitem_cartitem__["a" /* CartitemPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_myorder_myOrder__["a" /* MyOrderPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_zoomimg_zoomimg__["a" /* ZoomimgPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_mobile_mobile__["a" /* MobilePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_dashboard_dashboard__["a" /* DashboardPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_splishpage_splishpage__["a" /* SplishPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_signinOtp_signinOtp__["a" /* SigninOtpPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_4__providers_commonservice_commonservice__["a" /* CommonserviceProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 37:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddtocartPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__payment_payment__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__signup_signup__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_commonservice_commonservice__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__mycart_myCart__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__categorydetails_categoryDetails__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__dashboard_dashboard__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__zoomimg_zoomimg__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_jquery__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_jquery__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var AddtocartPage = /** @class */ (function () {
    function AddtocartPage(platform, navCtrl, alertCtrl, commonservice, events) {
        var _this = this;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.commonservice = commonservice;
        this.events = events;
        this.error = "";
        this.image = "assets/imgs/product-des.png";
        this.name = "Black/Green Leaf Cotton Silk Screen Printed Kalamkari Fabric";
        this.purchase_count = 0;
        this.viewusers = 0;
        this.protype = "SAR 139 / Meter";
        this.promaterial = "slikone";
        this.color = "black";
        this.quantity = "quantity";
        this.quantity_stock = "0";
        this.proquenty = "quantity";
        this.wishliststatus = 0;
        this.slingleimageshow = 1;
        this.singleimg = "";
        this.price = "1000";
        this.is_discount = 0;
        this.discount_price = "0";
        this.weight = 0;
        this.wishbuttonshow = 0;
        this.wishlist = 0;
        this.addimage = [];
        this.addimageto = [];
        this.addimagetolist = [];
        this.selectedColor = '';
        this.isColorFilterSelected = false;
        this.sliderWidth = "100%";
        this.colorImageList = [];
        this.newColorArray = [];
        this.newnameArray = [];
        this.newdescriptionArray = [];
        this.newQualityArray = [];
        this.forAllImages = [];
        this.type = "METER";
        this.typeStock = "METERSTOCK";
        this.showContent = false;
        events.publish('allpagecommon');
        this.loadpage();
        this.platform.registerBackButtonAction(function () {
            if (localStorage.getItem("Addtocartback") == 'CategoryDetailsPage') {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__categorydetails_categoryDetails__["a" /* CategoryDetailsPage */]);
            }
            else {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__dashboard_dashboard__["a" /* DashboardPage */]);
            }
        });
    }
    AddtocartPage.prototype.ionViewWillLeave = function () {
        this.navCtrl.getPrevious().data.id = localStorage.getItem("details_pdid");
    };
    AddtocartPage.prototype.gotoback = function () {
        if (localStorage.getItem("Addtocartback") == 'CategoryDetailsPage') {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__categorydetails_categoryDetails__["a" /* CategoryDetailsPage */]);
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__dashboard_dashboard__["a" /* DashboardPage */]);
        }
    };
    AddtocartPage.prototype.gopaymentqw = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__payment_payment__["a" /* PaymentPage */]);
    };
    AddtocartPage.prototype.loginuser = function () {
        var _this = this;
        this.servicedata = "check_wishlist?product_id=" + localStorage.getItem("details_pdid") + "&userid=" + localStorage.getItem("loginuserid") + "&lang=" + localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            // if(this.wishliststatus==1)
            // {
            //     this.wishliststatus=0; 
            // }
            // else
            // {
            //     this.wishliststatus=1;
            // }
            if (_this.data.status == true) {
                _this.wishliststatus = 1;
            }
            else {
                _this.wishliststatus = 0;
            }
        });
    };
    AddtocartPage.prototype.openmodel = function (image) {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__zoomimg_zoomimg__["a" /* ZoomimgPage */]);
    };
    AddtocartPage.prototype.openmodelto = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__zoomimg_zoomimg__["a" /* ZoomimgPage */]);
    };
    AddtocartPage.prototype.clickimage = function (image) {
        this.slingleimageshow = 0;
        this.singleimg = image;
    };
    AddtocartPage.prototype.closesingleimg = function () {
        console.log("sdsa");
        this.slingleimageshow = 1;
    };
    AddtocartPage.prototype.closepopup = function () {
        __WEBPACK_IMPORTED_MODULE_9_jquery__("#myModal").css("display", "none");
    };
    AddtocartPage.prototype.opentomodel = function (image) {
        console.log(image);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__zoomimg_zoomimg__["a" /* ZoomimgPage */]);
    };
    AddtocartPage.prototype.loadpage = function () {
        var _this = this;
        console.log('test');
        console.log(localStorage.getItem("loginuserid"));
        this.commonservice.waitloadershow();
        if (localStorage.getItem("applanguage") == "er") {
            var setLang = 2;
        }
        else {
            var setLang = 1;
        }
        this.servicedata = "productDetail/" + localStorage.getItem("details_pdid") + "?lang=" + setLang;
        //  this.servicedata="productDetail/"+localStorage.getItem("details_pdid")+"?lang="+localStorage.getItem("applanguage");
        console.log(localStorage.getItem("applanguage"));
        console.log(this.servicedata);
        console.log(localStorage.getItem("details_pdid"));
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.showContent = true;
            _this.commonservice.waitloaderhide();
            _this.data = res;
            console.log(_this.data.data.type);
            if (_this.data.data.type == 'clothes') {
                _this.type = "METER";
                _this.typeStock = "METERSTOCK";
            }
            else {
                _this.type = "PIECES";
                _this.typeStock = "PIECESSTOCK";
            }
            console.log(setLang);
            console.log(_this.data.data.language_id);
            // if(this.data.data.language_id = setLang){ 
            _this.purchase_count = _this.data.data.purchase_count;
            // this.purchase_count  = 0;                
            _this.viewusers = Math.floor(Math.random() * 101);
            _this.quantity_stock = _this.data.data.stock;
            _this.weight = _this.data.data.weight;
            // this.colorlist = this.data.data.color;
            _this.quantitylist = _this.data.data.quantity_varients;
            //alert(this.quantitylist);
            _this.materiallist = _this.data.data.material;
            _this.image = _this.data.data.image;
            _this.colorImageList = _this.data.data.mycolorImg;
            console.log(_this.data.data.mycolorImg);
            if (_this.colorImageList) {
                _this.color = _this.colorImageList[0].color;
                console.log(_this.color);
                for (var i = 0; i < _this.colorImageList.length; i++) {
                    _this.newColorArray.push(_this.colorImageList[i].color);
                    //  console.log(this.colorImageList[i].color);
                }
            }
            _this.name = _this.data.data.name;
            if (_this.name) {
                for (var i = 0; i < _this.name.length; i++) {
                    if (_this.data.data.language_id = setLang) {
                        _this.newnameArray.push(_this.name[i]);
                    }
                    //  console.log(this.colorImageList[i].color);
                }
            }
            _this.protype = _this.data.data.quantity;
            localStorage.setItem("scrollimage", JSON.stringify(_this.data.data.additionalimage));
            // if(this.colorlist)
            // {
            //     this.color=this.data.data.color[0];
            //     console.log(this.color);
            // }
            if (_this.quantitylist) {
                _this.quantity = _this.data.data.quantity[0];
            }
            if (_this.materiallist) {
                _this.promaterial = _this.data.data.material[0];
            }
            _this.price = _this.data.data.price;
            _this.discount_price = _this.data.data.discount_price;
            _this.is_discount = _this.data.data.is_discount;
            console.log(_this.discount_price);
            _this.proquenty = "";
            _this.sku = _this.data.data.sku;
            _this.jan = _this.data.data.jan; //customer view 
            _this.description = _this.data.data.description;
            // if(this.description){
            //     for (let i = 0; i < this.description.length; i++){
            //         if(this.data.data.language_id = setLang){ 
            //         this.newdescriptionArray.push(this.description[i].description);
            //         }
            //     //  console.log(this.colorImageList[i].color);
            //     }
            // } 
            __WEBPACK_IMPORTED_MODULE_9_jquery__("#description").append(_this.data.data.description);
            _this.language_id = _this.data.data.language_id;
            console.log(_this.data.data.language_id);
            if (localStorage.getItem("islogin")) {
                _this.loginuser();
                _this.wishbuttonshow = 1;
            }
            _this.scrollimage();
            //}
        });
    };
    AddtocartPage.prototype.Addtocartproduct = function () {
        console.log(this.proquenty);
        if (this.proquenty == "" || this.proquenty == null || this.color == "" || this.color == null) {
            this.commonservice.erroralert("Please select quantity");
        }
        else {
            console.log(localStorage.getItem("details_pdid"));
            var details_pdid = localStorage.getItem("details_pdid");
            console.log(details_pdid);
            localStorage.setItem(details_pdid, this.quantitylist[this.quantitylist.length - 1]);
            var min = localStorage.getItem(details_pdid);
            console.log('atc_min' + min);
            if (this.price == "" && this.price == null) {
                this.price = "0";
            }
            var device_id = "11";
            var a = [];
            var prev_cart = localStorage.getItem("cart" + device_id);
            if (prev_cart != null && prev_cart != undefined) {
                var temp = JSON.parse(localStorage.getItem("cart" + device_id));
                if (temp.length > 0) {
                    for (var j = 0; j < temp.length; j++) {
                        if (localStorage.getItem("details_pdid") != temp[j].pid) {
                            a.push(temp[j]);
                        }
                    }
                }
            }
            var obj = {
                'name': this.name,
                'pid': localStorage.getItem("details_pdid"),
                'desc': this.description,
                'stock': this.quantity_stock,
                'weight': this.weight,
                'color': this.color,
                'image': this.image,
                'price': this.price,
                'discount_price': this.discount_price,
                'isDiscount': this.is_discount,
                'qty': this.proquenty,
                'type': this.type
            };
            a.push(obj);
            localStorage.setItem("cart" + device_id, JSON.stringify(a));
            localStorage.setItem("cartproductcount", JSON.stringify(a.length));
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__mycart_myCart__["a" /* myCartPage */]);
        }
    };
    AddtocartPage.prototype.removewishlist = function () {
        var _this = this;
        this.commonservice.waitloadershow();
        this.servicedata = "remove_wishlist?product_id=" + localStorage.getItem("details_pdid") + "&userid=" + localStorage.getItem("loginuserid");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            _this.commonservice.waitloaderhide();
            if (_this.data.status = "true") {
                _this.loginuser();
                _this.commonservice.successalert("Product removed from wishlist sucessfully");
            }
            else {
                _this.commonservice.erroralert("Something went wrong");
            }
        });
    };
    AddtocartPage.prototype.addwishlist = function () {
        var _this = this;
        if (localStorage.getItem("islogin")) {
            this.commonservice.waitloadershow();
            this.servicedata = "add_wishlist?product_id=" + localStorage.getItem("details_pdid") + "&userid=" + localStorage.getItem("loginuserid");
            this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
                _this.data = res;
                _this.commonservice.waitloaderhide();
                if (_this.data.status = "true") {
                    _this.loginuser();
                    _this.commonservice.successalert("Product added in your wishlist sucessfully");
                }
                else {
                    _this.commonservice.erroralert("Something went wrong");
                }
            });
        }
        else {
            this.commonservice.erroralert("Login first");
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__signup_signup__["a" /* SignupPage */]);
        }
    };
    AddtocartPage.prototype.scrollimage = function () {
        this.addimage = JSON.parse(localStorage.getItem("scrollimage"));
        this.twoscrollimage();
    };
    AddtocartPage.prototype.twoscrollimage = function () {
        this.addimage = JSON.parse(localStorage.getItem("scrollimage"));
        console.log(this.addimage, "add image");
        if (this.addimage != null) {
            var i = 0;
            for (var _i = 0, _a = this.addimage; _i < _a.length; _i++) {
                var bp = _a[_i];
                this.addimageto.push({ "image": bp, "quen": i });
                this.addimagetolist.push({ "image": bp, "quen": i });
                i++;
            }
            this.forAllImages = this.addimageto;
        }
        console.log(this.addimageto, "add image to");
    };
    // onColorSelect(evt){
    //    let color = evt.target.value;
    //    this.selectedColor = color;
    //    let anyColorFound = false;
    //     let colorImages=[];
    //     for(var i =0;i < this.data.data.colorImg.length;i++){
    //         if(this.data.data.colorImg[i].color ==color){
    //             anyColorFound = true;
    //           colorImages.push({'image':this.data.data.colorImg[i].image,"quen":i})
    //         }
    //     }
    //     if(anyColorFound){
    //         this.addimagetolist = colorImages;
    //         this.addimageto = colorImages;
    //         if(colorImages.length == 1){
    //             this.sliderWidth = "40%"
    //         }
    //         if(this.addimageto.length == 1){
    //            this.slides.slideTo(0);
    //         }
    //         this.slides.update();
    //     }
    //     else{
    //         this.commonservice.erroralert("No item available");   
    //     }
    // }
    AddtocartPage.prototype.onColorChoose = function (col) {
        var anyColorFound = false;
        this.checkColor = col;
        console.log(col);
        var colorImages = [];
        if (this.colorImageList) {
            for (var i = 0; i < this.colorImageList.length; i++) {
                if (col == this.colorImageList[i].color) {
                    anyColorFound = true;
                    //localStorage.setItem(localStorage.getItem("details_pdid"), JSON.stringify(this.colorImageList[i].color));
                    //localStorage.setItem("colorselect", JSON.stringify(this.colorImageList[i].color));
                    // console.log(localStorage.getItem("details_pdid"), JSON.stringify(this.colorImageList[i].color));
                    // console.log(JSON.parse(localStorage.getItem("colorselect")));
                    this.color = this.colorImageList[i].color;
                    console.log(this.color);
                    if (this.colorImageList[i].img) {
                        console.log(this.colorImageList[i].img.length);
                        for (var j = 0; j < this.colorImageList[i].img.length; j++) {
                            colorImages.push({ 'image': this.colorImageList[i].img[j], "quen": i });
                        }
                    }
                }
            }
        }
        if (anyColorFound) {
            this.addimagetolist = colorImages;
            this.addimageto = colorImages;
            if (colorImages.length == 1) {
                this.sliderWidth = "40%";
                this.slides.update();
            }
            if (colorImages.length == 2) {
                this.sliderWidth = "60%";
                this.slides.update();
            }
            if (this.slides) {
                if (this.addimageto.length == 1) {
                    this.slides.slideTo(0);
                }
                this.slides.update();
            }
        }
        else {
            this.commonservice.erroralert("No item available");
        }
    };
    AddtocartPage.prototype.getAllImageData = function () {
        this.checkColor = '';
        this.addimageto = this.forAllImages;
        this.addimagetolist = this.forAllImages;
        this.sliderWidth = "100%";
        this.slides.update();
    };
    AddtocartPage.prototype.activeColor = function () {
        return {
            active: true,
        };
    };
    AddtocartPage.prototype.onOtherSelect = function (evt) {
        //   alert("hhiihi");
        console.log(evt.target.value);
        if (evt.target.value == 6) {
            var prompt_1 = this.alertCtrl.create({
                title: 'Cloth length',
                message: "Please enter the lenth of clothes(in meters)",
                inputs: [
                    {
                        name: 'length',
                        type: 'number',
                        id: 'maxLength10'
                    },
                ],
                buttons: [
                    {
                        text: 'Cancel',
                        handler: function (data) {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: 'Save',
                        handler: function (data) {
                            console.log(data, 'Saved clicked');
                        }
                    }
                ]
            });
            prompt_1.present().then(function (result) {
                document.getElementById('maxLength10').setAttribute('max', '99');
                document.getElementById('maxLength10').addEventListener('keyup', function (r) {
                    // if(r.target.value.length>2){
                    //     return false;
                    // }
                    console.log(r);
                    console.log(r.target);
                });
            });
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* Slides */])
    ], AddtocartPage.prototype, "slides", void 0);
    AddtocartPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-addtocart',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/addtocart/addToCart.html"*/'\n\n<ion-header>\n\n    <div class="headerbg_wh">\n\n    <div class="container">\n\n            <!-- style=" padding-top: 20px; padding-bottom: 20px;" -->\n\n        <span (click)="gotoback()"  style="font-size:20px; cursor:pointer; " ><i class="fa fa-arrow-left" aria-hidden="true" style="font-size: 20px;  padding-left: 10px;"></i> &nbsp; &nbsp; &nbsp;<small style="font-weight: 500;">{{"ADDTOCRT" | translate}}</small> </span>\n\n        <span  (click)="removewishlist()" style="cursor:pointer;float: right;  " ><i  *ngIf="wishliststatus==1" class="fa fa-heart" aria-hidden="true" style="margin-right:1px;    font-size: 26px;color:#FF0000;"></i></span>\n\n        <span (click)="addwishlist()"  style="cursor:pointer;float: right;  " ><i  *ngIf="wishliststatus!=1" class="fa fa-heart" aria-hidden="true" style="margin-right:1px;    font-size: 26px;color:#d6c9a8;"></i></span>\n\n    </div>\n\n</div>\n\n</ion-header>\n\n\n\n<ion-content style="background-color: rgb(245, 244, 244);">\n\n    <div *ngIf="showContent == true" class="container-fluid" style="margin-top: 25px; "> \n\n            <!-- height: 100vh;padding-left: 0px; style="padding-left: 9px;" -->\n\n    <div>\n\n            <div *ngIf="slingleimageshow==1" >\n\n                <ion-slides *ngIf="addimageto && addimageto.length" autoplay="200000" class="slideroption" pager="true"   loop="true" speed="300">\n\n                    <ion-slide   *ngFor="let slide of addimageto">\n\n                        <img (click)="opentomodel(addimageto)" src="{{slide.image}}" style="width: 100%;height: 41vh;" />\n\n                    </ion-slide>\n\n                </ion-slides>\n\n            </div>\n\n            <div (click)="closesingleimg()"  *ngIf="slingleimageshow==0" style="text-align: -webkit-center"><img (click)="openmodelto()"  src="{{singleimg}}" style="width: 100%;height: 41vh;" /></div>\n\n            <div class="row" style="padding:0px;padding-top: 11px;">\n\n                <div *ngFor="let slide of addimagetolist"    class="col imgslidesp" style="height: 7vh;text-align:center"> <img (click)="clickimage(slide.image)" src="{{slide.image}}" alt="Los Angeles" [ngStyle]="{\'width\': sliderWidth}"  style="height:7vh;margin:0 auto"></div>\n\n            </div>\n\n   \n\n        \n\n            \n\n            <div class="row" style="padding-bottom: 15px;text-align:center;margin:0px;"> \n\n                <!-- <div></div> --> \n\n                <div style="width:100%;">\n\n                        <h2 style="font-size: 18px;font-weight: 800" > {{name}}</h2> \n\n                    <h3 style="font-size: 15px;">{{protype}}</h3>\n\n                   <!-- <i><h3 *ngIf="purchase_count != \'0\'" style="font-size: 15px; margin-top: 1px;color:red;"  >{{purchase_count}} User recently purchase this item <sup>*</sup></h3></i>\n\n                    <i><h3 *ngIf="purchase_count == \'0\'" style="font-size: 15px; margin-top: 1px;color:red;"  >{{"CURRENTLY" | translate}} {{jan}} {{"USERVIEW" | translate}}<sup>*</sup></h3></i> -->\n\n                    <i><h3 style="font-size: 15px; margin-top: 1px;color:red;"  >{{"CURRENTLY" | translate}} {{jan}} {{"USERVIEW" | translate}}<sup>*</sup></h3></i>  \n\n                    <div *ngIf="is_discount == 1">\n\n                        <span class="text-center" style="    color: #000000;\n\n                        margin: 0;\n\n                        line-height: 20px;\n\n                        font-weight: 600;\n\n                        font-size: 16px;">SAR {{discount_price}}</span>\n\n                        <span class="text-center" style="    text-decoration: line-through;\n\n                        color: grey;\n\n                        font-size: 13px;\n\n                        line-height: 20px;\n\n                        margin: 0 10px;"> SAR {{price}}  </span>   <span style="color: #000;">/{{type | translate}} </span>                      \n\n                       </div>\n\n                        <div *ngIf="is_discount == 0">\n\n                        <p class="text-center" style="color: grey;margin: 0; line-height: 20px;"> SAR {{price}} /{{type | translate}} </p>\n\n                       </div>\n\n                    \n\n                       <!-- SAR {{price}} /{{"METER" | translate}}  -->\n\n                    <h4 style="font-size: 15px;"> ( {{quantity_stock}} {{typeStock | translate}} )</h4>\n\n                </div> \n\n            </div>\n\n            <!-- below comment for color select -->\n\n           <!-- <div class="form-group form-inline" style="margin-bottom: 0px; margin-top: -16px;">\n\n                <label style="width:12%; font-size:13px; font-weight:400; float: left;">{{\'COLOR\' | translate}}</label>\n\n                <select class="collor" (change)="onColorSelect($event)"style="width:25%;float: left; font-size:13px; font-weight:400; margin-left:2px;" [(ngModel)]="color" >\n\n                 \n\n                    <option *ngFor="let cl of colorlist" value="{{cl}}" >{{cl}}</option>\n\n                </select>\n\n                <label style="width:19%; float: left; margin-left: 63px; font-size:13px; font-weight:400;">{{\'MATERIAL\' | translate}}</label>\n\n                <select class="collor" style="width:25%; font-size:13px;float: right; font-weight:400;     margin-left: -9px;" [(ngModel)]="promaterial" >\n\n                    <option  *ngFor="let ml of materiallist" value="{{ml}}">{{ml}}</option>\n\n                </select>\n\n                <span *ngIf="selectedColor" style="margin-left:50px">{{selectedColor}}</span>\n\n            </div> -->\n\n            <div class="color_boxes">\n\n               <div class="color-box" [ngClass]="{\'active\': checkColor == color}"  [ngStyle]="{\'background-color\':color,\'border\':none}" *ngFor = "let color of newColorArray" (click)="onColorChoose(color)">\n\n     \n\n               </div>\n\n               <div (click)="getAllImageData()" style="margin-top:2px;">ALL</div>\n\n            </div>\n\n            <div >\n\n                <div class="card" style="padding: 25px 0;">\n\n                    <div class="card-body text-center" style="background-color: #eae5e5;padding: 10px; border-radius: 5px;">\n\n                        <h4 class="myorderh4 font-weight-bold"> <strong>{{"SKU" | translate}} </strong>: {{sku}} </h4>\n\n                        <h4 class="myorderh4 font-weight-bold">{{"DESCRIPTION" | translate}}\n\n                            <span style="font-weight:400;" id="description"> </span>\n\n                        </h4>\n\n                    </div>\n\n                </div> \n\n            </div>\n\n        </div>\n\n    </div>\n\n</ion-content> \n\n<ion-footer style="padding:0;">\n\n       \n\n    <div *ngIf="showContent == true" style="background-color:#ffffff;    z-index: 1;    width: 100%;">\n\n        <div style="display:flex;">\n\n            <div class="col s_quntity" style="padding:0px;">                 \n\n                    <ion-item>\n\n                            <ion-label>{{"SelectQuantity" | translate}} </ion-label>                 \n\n                            <ion-select required class="custom_select" placeholder=\'{{"QUANTITY" | translate}}\' [(ngModel)]="proquenty"  (change)="onOtherSelect($event)">                                   \n\n                                <ion-option *ngFor="let ql of quantitylist" [value]="ql">{{ql}} {{type | translate}}</ion-option>\n\n                            </ion-select>\n\n                        </ion-item>           \n\n            </div>\n\n            <div class="col" (click)="Addtocartproduct()" style="padding:0px; ">\n\n                <button  class="btn btn-primary btn-block CheckOut-btn" style="height:50px;width: 100%;"  >{{"ADDTOCRTBUTTON" | translate}}</button> \n\n            </div>\n\n        </div>\n\n    </div>\n\n</ion-footer>'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/addtocart/addToCart.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__providers_commonservice_commonservice__["a" /* CommonserviceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */]])
    ], AddtocartPage);
    return AddtocartPage;
}());

//# sourceMappingURL=addToCart.js.map

/***/ }),

/***/ 426:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FilterPipe = /** @class */ (function () {
    function FilterPipe() {
    }
    FilterPipe.prototype.transform = function (items, searchText) {
        if (!items)
            return [];
        if (!searchText)
            return items;
        searchText = searchText.toLowerCase();
        return items.filter(function (it) {
            return it.name.toLowerCase().includes(searchText);
        });
    };
    FilterPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'filter'
        })
    ], FilterPipe);
    return FilterPipe;
}());

//# sourceMappingURL=filter.pipe.js.map

/***/ }),

/***/ 427:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(271);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_commonservice_commonservice__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_dashboard_dashboard__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_faq_faq__ = __webpack_require__(272);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_signup_signup__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_mywishlist_mywishlist__ = __webpack_require__(273);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_myorder_myOrder__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_cartitem_cartitem__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_myAccount_myAccount__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_splishpage_splishpage__ = __webpack_require__(274);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














// import { MyOrderPage } from '../pages/myOrder/myOrder';




var MyApp = /** @class */ (function () {
    function MyApp(alertCtrl, menuCtrl, events, platform, statusBar, splashScreen, commonservice, translate) {
        var _this = this;
        this.alertCtrl = alertCtrl;
        this.menuCtrl = menuCtrl;
        this.events = events;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.commonservice = commonservice;
        this.translate = translate;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_14__pages_splishpage_splishpage__["a" /* SplishPage */];
        this.username = "Guest user";
        this.useraddress = "15 3C, DRM Rd, Saket Nagar, Indore";
        this.userimage = "assets/imgs/thumb.png";
        this.error = "";
        this.lang = "en";
        this.removesplash = "any";
        this.logoutshow = 0;
        this.initializeApp();
        this.allservicecalldata();
        localStorage.setItem("setcatlist", '1');
        localStorage.setItem("setinterval", '0');
        this.statusBar.backgroundColorByHexString('#000000');
        //console.log(this.nav.getActive());
        if (localStorage.getItem("applanguage")) {
            if (localStorage.getItem("applanguage") == 'en') {
                this.translate.setDefaultLang('en');
                localStorage.setItem("applanguage", 'en');
                this.lang = false;
                console.log(this.lang);
            }
            else {
                console.log(this.lang);
                this.translate.setDefaultLang('er');
                localStorage.setItem("applanguage", 'er');
                this.lang = true;
            }
        }
        else {
            this.translate.setDefaultLang('en');
            localStorage.setItem("applanguage", 'en');
            console.log(localStorage.getItem("applanguage"));
        }
        events.subscribe('allpagecommon', function () {
            _this.allpagecommon();
        });
        this.removesplash = setInterval(function () {
            if (localStorage.getItem("setinterval") == '1') {
                if (localStorage.getItem("islogin")) {
                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_dashboard_dashboard__["a" /* DashboardPage */];
                    _this.logoutshow = 1;
                    clearInterval(_this.removesplash);
                }
                else {
                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_dashboard_dashboard__["a" /* DashboardPage */];
                    _this.logoutshow = 0;
                    clearInterval(_this.removesplash);
                }
            }
            else {
                localStorage.setItem("setinterval", '1');
            }
        }, 3000);
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            _this.statusBar.backgroundColorByHexString('#000000');
            _this.statusBar.overlaysWebView(false);
            _this.statusBar.backgroundColorByHexString('#000000');
            //  $("#checkboxid").removeAttr("checked");
            $(".switch:not([checked])").on('change', function () {
                $(".switch").not(this).prop("checked", false);
                console.log("wistch");
            });
        });
        this.platform.registerBackButtonAction(function () {
            var alert = _this.alertCtrl.create({
                title: 'Confirm',
                message: 'Do you really want to exit ?',
                buttons: [{
                        text: 'Cancel',
                        role: 'cancel',
                        handler: function () {
                        }
                    },
                    {
                        text: 'Ok',
                        handler: function () {
                            // this.deleteitem(id);
                            _this.platform.exitApp();
                        }
                    }
                ]
            });
            alert.present();
        });
    };
    MyApp.prototype.gotohomepage = function (page) {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_dashboard_dashboard__["a" /* DashboardPage */]);
        this.menuCtrl.close();
    };
    MyApp.prototype.gotocategorypage = function (page) {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */]);
        this.menuCtrl.close();
    };
    MyApp.prototype.gotoorderpage = function (page) {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_11__pages_myorder_myOrder__["a" /* MyOrderPage */]);
        this.menuCtrl.close();
    };
    MyApp.prototype.gotocartpage = function (page) {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_12__pages_cartitem_cartitem__["a" /* CartitemPage */]);
        this.menuCtrl.close();
    };
    MyApp.prototype.gotowishlistpage = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_10__pages_mywishlist_mywishlist__["a" /* MyWishlistPage */]);
        this.menuCtrl.close();
    };
    MyApp.prototype.gotosignpage = function () {
        localStorage.removeItem("loginotp");
        localStorage.removeItem("loginuserid");
        localStorage.removeItem("islogin");
        localStorage.removeItem("backtosigninpage");
        this.menuCtrl.close();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_9__pages_signup_signup__["a" /* SignupPage */]);
        this.logoutshow = 0;
        this.commonservice.successalert("Logout sucessfully");
        this.events.publish('allpagecommon');
    };
    MyApp.prototype.signpage = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_9__pages_signup_signup__["a" /* SignupPage */]);
        this.menuCtrl.close();
    };
    MyApp.prototype.gotomyaccount = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_13__pages_myAccount_myAccount__["a" /* MyAccountPage */]);
        this.menuCtrl.close();
    };
    MyApp.prototype.gotofaq = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_8__pages_faq_faq__["a" /* FaqPage */]);
        this.menuCtrl.close();
    };
    MyApp.prototype.allpagecommon = function () {
        var _this = this;
        if (localStorage.getItem("islogin")) {
            this.servicedata = "view_profile?userid=" + localStorage.getItem("loginuserid") + "&lang=" + localStorage.getItem("applanguage");
            this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
                _this.data = res;
                console.log(_this.data);
                console.log(_this.data.data.firstname);
                if (_this.data.data.firstname == "" || _this.data.data.firstname == null) {
                    _this.username = "Guest user";
                }
                else {
                    _this.username = _this.data.data.firstname + " " + _this.data.data.lastname;
                }
                _this.useraddress = _this.data.data.address;
                _this.logoutshow = 1;
            });
        }
        else {
            this.username = "Guest user";
            this.logoutshow = 0;
            //this.email="admmin@gmail.com";
        }
    };
    MyApp.prototype.allservicecalldata = function () {
        var _this = this;
        this.servicedata = "category_list/" + localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            console.log(_this.data);
            //  console.log('category_list');
            localStorage.setItem("setcatlist", JSON.stringify(_this.data));
            _this.allproductdata();
        });
    };
    MyApp.prototype.allproductdata = function () {
        var _this = this;
        this.servicedata = "allcategory_product/" + localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            //   console.log('allproductdata');
            console.log(_this.data);
            _this.allproductdatatwo();
            localStorage.setItem("allcategoryproductlist", JSON.stringify(_this.data.data));
        });
    };
    MyApp.prototype.allproductdatatwo = function () {
        var _this = this;
        this.servicedata = "allproduct_list/" + localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            //  console.log('allproductdatatwo');
            console.log(_this.data);
            localStorage.setItem("allproductlist", JSON.stringify(_this.data.data));
        });
    };
    MyApp.prototype.changelaguagefunction = function () {
        this.allservicecalldata();
        if (this.lang == false) {
            this.translate.setDefaultLang('en');
            localStorage.setItem("applanguage", 'en');
            this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_dashboard_dashboard__["a" /* DashboardPage */]);
            //this.nav.setRoot(DashboardPage);
            this.menuCtrl.close();
        }
        else {
            this.translate.setDefaultLang('er');
            localStorage.setItem("applanguage", 'er');
            this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_dashboard_dashboard__["a" /* DashboardPage */]);
            //  this.nav.setRoot(DashboardPage);
            this.menuCtrl.close();
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/app/app.html"*/'<ion-menu [content]="content">\n\n\n<ion-content>\n    <div>\n        <div class="media" style="z-index:888; background-color:#ae9435;     height: 86px;padding: 10px">\n            <div class="media-body" style=" display: block;\n            width: auto;">\n                <img src="{{userimage}}" class="align-self-center" style="    display: inline-block;\n                vertical-align: middle;width:60px">\n                <h4 class="profileh4" style="    display: inline-block;\n                width: calc(100% - 70px);\n                margin: auto;\n                text-align: center;\n                vertical-align: middle;"> {{username}} </h4>\n                \n            </div>\n           \n           \n        </div>\n        <div class="cmedia mt-1" style="margin: 10px 0; ">\n            <div class="cmedia-body">\n                <div style="width: 60%;display: flex;padding-left: 12px;margin:0 auto;">\n                    <div class="lpsize" >English</div>\n                    <!-- <label style="width: 28%;float: left" class="switch"> -->\n                        <!-- <input id="checkboxid" type="radio" ng-reflect-model="false" value="false" (click)="gotohomepage(\'HomePage\')" checked="false" (change)="changelaguagefunction()" [(ngModel)]="lang" data-toggle="toggle" > -->\n                        <!-- <input type="checkbox" id="checkboxid" ng-reflect-model="false"  value="false" (click)="gotohomepage(\'HomePage\')" (change)="changelaguagefunction()" [(ngModel)]="lang" >\n                        <span class="slider round"></span>  -->\n                        <ion-toggle checked="false" (click)="gotohomepage(\'HomePage\')" (ionChange)="changelaguagefunction()" [(ngModel)]="lang"></ion-toggle>\n                    <!-- </label> -->\n                    <div class="lpsize" style="margin-left: 15px;">Arabic</div>\n                </div>\n               \n                <div  *ngIf="logoutshow==1" style="font-size:15px; text-align: -webkit-center; font-weight:400; color:#818181; margin: 10px 10px 5px;">\n                    <ion-icon (click)="gotomyaccount()"  name="pin"></ion-icon> \n                    {{useraddress}}\n                    <ion-icon (click)="gotomyaccount()"  name="create"></ion-icon>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <ion-list>\n        <hr>\n        <a  style="padding-left: 15px;color:black; display: block;" (click)="gotohomepage(\'HomePage\')"> <img style="    width: 23px;" src="assets/imgs/icon-images/home.png">&nbsp;&nbsp; &nbsp;&nbsp; {{"HOME" | translate}} </a>\n        <br>\n        <a  style="padding-left: 15px;color:black; display: block;" (click)="gotocategorypage(\'HomePage\')"> <img style="    width: 23px;" src="assets/imgs/icon-images/category.png">&nbsp;&nbsp; &nbsp;&nbsp; {{"CATEGORY" | translate}} </a>\n        <br *ngIf="logoutshow==1">\n        <a *ngIf="logoutshow==1" style="padding-left: 15px;color:black;  display: block;" (click)="gotoorderpage(\'MyOrderPage\')"><img style="    width: 23px;" src="assets/imgs/icon-images/list.png">&nbsp;&nbsp; &nbsp;&nbsp; {{"MYORDER" | translate}} </a>\n        <br>\n        <a style="padding-left: 15px;color:black;    display: block;" (click)="gotocartpage(\'myCartPage\')"><img style="    width: 23px;" src="assets/imgs/icon-images/shopping-cart.png">&nbsp;&nbsp; &nbsp;&nbsp; {{"MYCART" | translate}} </a>\n        <br *ngIf="logoutshow==1">\n        <a *ngIf="logoutshow==1" style="padding-left: 15px;color:black;    display: block;"(click)="gotowishlistpage(\'MyWishlistPage\')"> <img style="    width: 23px;" src="assets/imgs/icon-images/tag (1).png"> &nbsp;&nbsp;&nbsp;&nbsp; {{"MYWISHLIST" | translate}} </a>\n        <br>\n        <a *ngIf="logoutshow==1" style="padding-left: 15px;color:black;" (click)="gotomyaccount()"><img style="    width: 23px;" src="assets/imgs/icon-images/customer-service.png">&nbsp;&nbsp; &nbsp;&nbsp; {{"MYACCOUNT" | translate}} </a>\n        <br *ngIf="logoutshow==1" ><br *ngIf="logoutshow==1">\n        <a style="padding-left: 15px;color:black;" (click)="gotofaq()"><img style="    width: 23px;" src="assets/imgs/icon-images/faq.png">&nbsp;&nbsp; &nbsp;&nbsp; {{"FAQS" | translate}} </a>\n         <br><br>\n        <a *ngIf="logoutshow==1" style="padding-left: 15px;color:black;    display: block;" (click)="gotosignpage(\'SignupPage\')"> <img src="assets/imgs/icon-images/logout.png">&nbsp;&nbsp; &nbsp;&nbsp; {{"LOGOUT" | translate}} </a>\n       <br><br>\n        <a *ngIf="logoutshow==0" style="padding-left: 15px;color:black;    display: block;" (click)="signpage(\'SignupPage\')"> <img src="assets/imgs/icon-images/logout.png">&nbsp;&nbsp; &nbsp;&nbsp; {{"SIGNUP" | translate}}</a>\n    </ion-list>\n</ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_4__providers_commonservice_commonservice__["a" /* CommonserviceProvider */], __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 435:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListPage = /** @class */ (function () {
    function ListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    ListPage_1 = ListPage;
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(ListPage_1, {
            item: item
        });
    };
    ListPage = ListPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/list/list.html"*/'<ion-header>\n  <!-- <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{"LIST" | translate}}</ion-title>\n  </ion-navbar> -->\n    <div class="headerbg_wh">\n  <div class="container">\n      <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>{{"LIST" | translate}}</ion-title>\n    </div>\n    </div>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n      <ion-icon [name]="item.icon" item-start></ion-icon>\n      {{item.title}}\n      <div class="item-note" item-end>\n        {{item.note}}\n      </div>\n    </button>\n  </ion-list>\n  <div *ngIf="selectedItem" padding>\n    You navigated here from <b>{{selectedItem.title}}</b>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/list/list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavParams */]])
    ], ListPage);
    return ListPage;
    var ListPage_1;
}());

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 437:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShoppingCartPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_commonservice_commonservice__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__addtocart_addToCart__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__mycart_myCart__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__dashboard_dashboard__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__search_search__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__myAccount_myAccount__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__cartitem_cartitem__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__home_home__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_jquery__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_jquery__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var ShoppingCartPage = /** @class */ (function () {
    function ShoppingCartPage(platform, navCtrl, commonservice, events) {
        var _this = this;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.commonservice = commonservice;
        this.events = events;
        this.error = "";
        this.image = "assets/imgs/product-des.png";
        this.name = "ambuj";
        this.protype = "SAR 139 / Meter";
        this.promaterial = "slikone";
        this.color = "black";
        this.proquenty = "1";
        this.price = "1000";
        this.categorydetils = "";
        this.sku = "";
        this.jan = "";
        this.productitem = "";
        this.productid = "";
        events.publish('allpagecommon');
        this.loadpage();
        this.platform.registerBackButtonAction(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__addtocart_addToCart__["a" /* AddtocartPage */]);
        });
    }
    ShoppingCartPage.prototype.loadpage = function () {
        var i = 0;
        this.productitem = JSON.parse(localStorage.getItem("cart" + "11"));
        console.log(this.productitem);
        if (this.productitem != null) {
            for (var _i = 0, _a = this.productitem; _i < _a.length; _i++) {
                var obj = _a[_i];
                if (obj[0] == localStorage.getItem("details_pdid")) {
                    this.data = obj;
                }
                i++;
            }
            console.log(this.data[8]);
            this.productid = this.data[0];
            this.name = this.data[1];
            this.price = this.data[2];
            this.image = this.data[3];
            this.proquenty = this.data[4];
            this.promaterial = this.data[5];
            this.protype = this.data[6];
            this.color = this.data[7];
            this.sku = "BRI43535";
            this.jan = "78";
            this.description();
        }
        //his.data=localStorage.getItem("cart"+"11");
    };
    ShoppingCartPage.prototype.description = function () {
        var _this = this;
        this.servicedata = "productDetail/" + localStorage.getItem("details_pdid") + "?lang=" + localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            _this.description = _this.data.data.description;
            __WEBPACK_IMPORTED_MODULE_10_jquery__("#categorydetils").append(_this.data.data.description);
        });
    };
    ShoppingCartPage.prototype.gotoaddtocart = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__cartitem_cartitem__["a" /* CartitemPage */]);
    };
    ShoppingCartPage.prototype.gotomycart = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__mycart_myCart__["a" /* myCartPage */]);
    };
    ShoppingCartPage.prototype.gotohome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__dashboard_dashboard__["a" /* DashboardPage */]);
    };
    ShoppingCartPage.prototype.gotosearch = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__search_search__["a" /* SearchPage */]);
    };
    ShoppingCartPage.prototype.gotocat = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__home_home__["a" /* HomePage */]);
    };
    ShoppingCartPage.prototype.gotobag = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__mycart_myCart__["a" /* myCartPage */]);
    };
    ShoppingCartPage.prototype.gotoaccount = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__myAccount_myAccount__["a" /* MyAccountPage */]);
    };
    ShoppingCartPage.prototype.gotoback = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__addtocart_addToCart__["a" /* AddtocartPage */]);
    };
    ShoppingCartPage.prototype.addwishlist = function () {
        var _this = this;
        this.commonservice.waitloadershow();
        this.servicedata = "add_wishlist?product_id=" + localStorage.getItem("details_pdid") + "&userid=" + localStorage.getItem("loginuserid") + "/" + localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            console.log(res);
            _this.data = res;
            _this.commonservice.waitloaderhide();
            if (_this.data.status = "true") {
                _this.commonservice.successalert("Product added in your wishlist sucessfully");
            }
            else {
                _this.commonservice.erroralert("Something went wrong");
            }
        });
    };
    ShoppingCartPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-shoppingCart',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/shoppingcart/shoppingCart.html"*/'\n\n<ion-header>\n\n        <div class="headerbg_wh">\n\n    <div class="container" >\n\n            <!-- style=" padding-top: 20px; padding-bottom: 10px;" -->\n\n        <span style="font-size:20px; cursor:pointer; " ><i class="fa fa-arrow-left" (click)="gotoback()" aria-hidden="true" style="font-size: 20px;  padding-left: 10px;"></i> &nbsp; &nbsp; &nbsp;<small style="font-weight: 500;">{{"SHOPPINGCART" | translate}}</small> </span>\n\n        <span style="font-size: 24px;cursor:pointer;float: right;  " ><i class="fa fa-search" (click)="gotosearch()" aria-hidden="true" style="font-size: 16px;  padding-left: 10px;"></i></span>\n\n    </div>\n\n    </div>\n\n</ion-header>\n\n<ion-content style="background-color: rgb(245, 244, 244);">\n\n    \n\n    <div class="container-fluid" style="    height: 100vh;margin-top: 23px;">\n\n        \n\n        <div class="media" >\n\n            <div class="media-body" style="text-align: -webkit-center;">\n\n                <img class="" src="{{image}}" style="height: 204px;    width:100%;">\n\n            </div>\n\n        </div>\n\n        <div class="row" style="padding: 17px;text-align: center;    margin-top: -9px;"> \n\n            <div style="width: 100%; margin-top: -9px;"><h2 style="text-align: center;font-size: 18px;font-weight: 800" >{{name}}</h2></div>\n\n            <div style="width: 88%; margin-top: -9px;"><h3 style="text-align: center;font-size: 16px;    padding-left: 30px;">{{protype}}</h3></div>\n\n        </div>\n\n        <div class="row" style="margin-top: -10px">\n\n            <div class="col" style=" font-size:12px; text-align:right; padding-left:0px; padding-right:0px;">{{"COLOR" | translate}} :</div>\n\n            <div class="col" style="font-weight:bold; font-size:12px; text-align:left; padding-left:0px; padding-right:0px;">{{color}}</div>\n\n           <!-- <div class="col" style=" font-size:12px; text-align:right; padding-left:0px; padding-right:0px;">{{"MATERIAL" | translate}} :</div>\n\n            <div class="col" style=" font-weight:bold; font-size:12px; text-align:left; padding-left:0px; padding-right:0px;">{{promaterial}}</div>-->\n\n        </div>\n\n        <div class="card" style="margin-top: -10px;padding: 10px">\n\n                <div class="card-body text-center">\n\n                    <h4 class="myorderh4 font-weight-bold" > <strong>{{"SKU" | translate}} </strong>: {{sku}} </h4>\n\n                    <h4 class="myorderh4 font-weight-bold">{{"DESCRIPTION" | translate}}\n\n                        <span style="font-weight:400;" id="categorydetils"></span>\n\n                    </h4>\n\n                </div>\n\n            </div> \n\n        \n\n        <br>    \n\n    </div>\n\n    \n\n</ion-content>\n\n<ion-footer style="padding: 0;">\n\n  \n\n    <div style="position: fixed;bottom: -6px;background-color:#ffffff;    z-index: 1;    width: 100%;">\n\n        <div style="width: 100%">\n\n            <div class="col" style=" margin:0px;" style="width: 50%;float: left;padding:0px;">\n\n               <!-- <button type="button" class="btn btn-primary btn-block Update-Cart-btn" style="height:50px;" (click)="gotoaddtocart()">{{"VIEWCART" | translate}}</button>-->\n\n                <button type="button" class="btn btn-primary btn-block Update-Cart-btn" style="height:50px;" (click)="addwishlist()">{{"addwishlist" | translate}}</button>\n\n            </div>\n\n            <div class="col"  style="width: 50%;float: left;padding:0px;">\n\n                 <button type="button" class="btn btn-primary btn-block CheckOut-btn" style="height:50px;" (click)="gotomycart()">{{"CHECKOUT" | translate}}</button> \n\n            </div>\n\n        </div>\n\n    </div>\n\n</ion-footer>'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/shoppingcart/shoppingCart.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_commonservice_commonservice__["a" /* CommonserviceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */]])
    ], ShoppingCartPage);
    return ShoppingCartPage;
}());

//# sourceMappingURL=shoppingCart.js.map

/***/ }),

/***/ 438:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DelieveryTimelinePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DelieveryTimelinePage = /** @class */ (function () {
    function DelieveryTimelinePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    DelieveryTimelinePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-delieveryTimeline',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/delieveryTimeline/delieveryTimeline.html"*/'<ion-content >\n\n<br>\n\n<div class="container-fluid">\n\n	<div class="media" style="border-bottom:2px solid #d6dadb;  padding:10px;">\n\n		<img src="assets/images/product-des.png" class="align-self-center mr-3" style="width:60px">\n\n		<div class="media-body">\n\n			<h4 class="trach4">Arriving 20 October</h4>  \n\n		</div>\n\n	</div>\n\n	<button type="button" class="btn btn-primary next btn-block" (click)="orderDetails()">{{"NEXT" | translate}}</button>\n\n	<br><br>\n\n\n\n	<div class="container" align="center"> \n\n		<div class="col-md-12">\n\n			<div class="vl">\n\n				<div class="media height">\n\n					<div class="active">\n\n						<i class="fa fa-check" aria-hidden="true" style="color:white;"></i> \n\n					</div>\n\n					<div class="media-body">\n\n						<h4 class="timelineh4 activeh4">Order Saturday, 11 October</h4>\n\n					</div>\n\n				</div>\n\n\n\n				<div class="media height">\n\n					<div class="rectancle">\n\n					</div>\n\n					<div class="media-body">\n\n						<h4 class="timelineh4">{{"SHIPPED" | translate}}</h4>\n\n					</div>\n\n				</div>\n\n\n\n				<div class="media height">\n\n					<div class="rectancle">\n\n					</div>\n\n					<div class="media-body">\n\n						<h4 class="timelineh4">{{"OUTFORDELIVERY" | translate}}</h4>\n\n					</div>\n\n				</div>\n\n\n\n				<div class="media">\n\n					<div class="rectancle">\n\n					</div>\n\n					<div class="media-body">\n\n						<h4 class="timelineh4">Arriving 13 October - 20 October</h4>\n\n					</div>\n\n				</div>\n\n			</div>\n\n		</div>\n\n\n\n		<div class="col-md-12">\n\n			<br>\n\n			<div class="border">\n\n			</div>\n\n		</div>\n\n\n\n		<div class="col-md-12">\n\n			<br>\n\n			<h4 class="tracadd4">{{"SHIPPINGADD" | translate}}</h4>  \n\n			<div class="displayAddressDiv">\n\n				<ul class="displayAddressUL">\n\n				</ul>\n\n			</div>\n\n\n\n		</div>\n\n	</div>  \n\n</div>\n\n</ion-content >\n\n'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/delieveryTimeline/delieveryTimeline.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */]])
    ], DelieveryTimelinePage);
    return DelieveryTimelinePage;
}());

//# sourceMappingURL=delieveryTimeline.js.map

/***/ }),

/***/ 439:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__search_search__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__myAccount_myAccount__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__mycart_myCart__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






// import { myCartPage } from '../mycart/mycart';

var NotificationPage = /** @class */ (function () {
    function NotificationPage(platform, navCtrl) {
        var _this = this;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.platform.registerBackButtonAction(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
        });
    }
    NotificationPage.prototype.gotohome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    NotificationPage.prototype.gotosearch = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__search_search__["a" /* SearchPage */]);
    };
    NotificationPage.prototype.gotocat = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    NotificationPage.prototype.gotobag = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__mycart_myCart__["a" /* myCartPage */]);
    };
    NotificationPage.prototype.gotoaccount = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__myAccount_myAccount__["a" /* MyAccountPage */]);
    };
    NotificationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-notification',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/notification/notification.html"*/'<ion-content >\n\n<div class="container-fluid">\n\n<div class="container">\n\n<div class="row" >\n\n<h4 style="color:black; font-size:16px; font-weight:bold; padding-bottom:0px; margin-bottom:0px; margin-top:10px;">\n\n<i class="fa fa-arrow-left" (click)="goBack()" aria-hidden="true" style="margin-right:30px;"></i>\n\n{{"NOTIFICATION" | translate}}</h4>\n\n</div>\n\n</div>\n\n<hr style="color:black;">\n\n\n\n\n\n<div class="card bg-light">\n\n    <div class="card-body text-center">\n\n    \n\n    <h3 class="notificationh3">Join Laistraila Apps to get more offer</h3>\n\n    <p class="card-text notificationp">\n\n    Today at 11:25 pa\n\n    </p>\n\n      <p class="card-text notificationp">\n\n      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum rutrum turpis id luctus vulputate. Donec vehicula fringilla fermentum. Ut tempor, ipsum eu semper vehicula, ex libero viverra est, in congue erat est sed dui. Proin libero lorem, pretium molestie nibh at, congue fringilla lectus. Aliquam lacinia nisl sollicitudin,\n\n      \n\n      </p>\n\n    </div>\n\n  </div>\n\n  \n\n  \n\n  <div class="card bg-light">\n\n    <div class="card-body text-center">\n\n    \n\n    <h3 class="notificationh3">Join Laistraila Apps to get more offer</h3>\n\n    <p class="card-text notificationp">\n\n    Today at 11:25 pa\n\n    </p>\n\n      <p class="card-text notificationp">\n\n      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum rutrum turpis id luctus vulputate. Donec vehicula fringilla fermentum. Ut tempor, ipsum eu semper vehicula, ex libero viverra est, in congue erat est sed dui. Proin libero lorem, pretium molestie nibh at, congue fringilla lectus. Aliquam lacinia nisl sollicitudin,\n\n      \n\n      </p>\n\n    </div>\n\n  </div>\n\n\n\n\n\n</div>\n\n</ion-content >\n\n<ion-footer>\n\n    <ion-toolbar style="padding-top: 42px;    width: 103%;">\n\n        <div class="row shadowfooter" >\n\n            <div style="text-align:center; width:20%; float:left;">\n\n                <a class="iconname" (click)="gotohome()" style="color:black;font-size: 12px;"> \n\n                    <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/home.png">\n\n                    <br>\n\n                    {{"HOME" | translate}}  \n\n                </a>\n\n            </div> \n\n            <div style="text-align:center; width:20%; float:left;">\n\n                <a class="iconname" (click)="gotosearch()" style="color:black;font-size: 12px;"> \n\n                    <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/search.png">\n\n                    <br>\n\n                    {{"SEARCH" | translate}}</a>\n\n            </div>\n\n            <div  style="text-align:center; width:20%; float:left;">\n\n                <a class="iconname" (click)="gotocat()" style="color:black;font-size: 12px;">  \n\n                    <img  style="    height: 18px;" src="assets/imgs/icon-images/list.png">\n\n                    <br>\n\n                    {{"CATEGORY" | translate}} </a>\n\n            </div>\n\n            <div style="text-align:center; width:20%; float:left;">\n\n                <a (click)="gotobag()" class="iconname" style="color:black;font-size: 12px;"> \n\n                    <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/shopping-bag.png">\n\n                    <br>\n\n                    {{"BAG" | translate}}\n\n                </a>\n\n            </div>\n\n            <div  style="text-align:center; width:20%; float:left;">\n\n                <a  (click)="gotoaccount()"  class="iconname" style="color:black;font-size: 12px;"> \n\n                    <img class="img-fluid" style="    height: 18px;"  src="assets/imgs/icon-images/round-account-button-with-user-inside.png">\n\n                    <br>\n\n                    {{"MYACCOUNT" | translate}} </a>\n\n            </div>\n\n        </div>\n\n    \n\n    </ion-toolbar>\n\n</ion-footer>\n\n'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/notification/notification.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */]])
    ], NotificationPage);
    return NotificationPage;
}());

//# sourceMappingURL=notification.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_commonservice_commonservice__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__verification_verification__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__orderConfirmed_orderConfirmed__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__dashboard_dashboard__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_jquery__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_jquery__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__cartitem_cartitem__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var PaymentPage = /** @class */ (function () {
    function PaymentPage(platform, alertCtrl, navCtrl, events, commonservice, toastCtrl) {
        var _this = this;
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.events = events;
        this.commonservice = commonservice;
        this.toastCtrl = toastCtrl;
        this.ct = 0;
        this.error = "";
        this.fname = "";
        this.nocart = 0;
        this.lname = "";
        this.email = "";
        this.emailone = "";
        this.emailtwo = "@gmail.com";
        this.mobile = "";
        this.address = "";
        this.city = "";
        this.emailtwoto = '';
        this.postcode = "";
        this.country = "";
        this.bcngemail = "0";
        this.bcngaddress = "0";
        this.bcngemaillast = "0";
        this.bcngaddresslast = "0";
        this.updateaddress = 0;
        this.shippingmethod = 0;
        this.cartitems = [];
        this.productitem = [];
        this.totalamount = 0;
        this.tax = 0;
        this.countrylist = [];
        this.citylist = [];
        this.opendom = 1;
        this.couponshow = 0;
        this.total_weight = 0;
        this.couponamount = 0;
        this.couponcode = "";
        this.subtotalpay = 0;
        this.taxablevalue = 0;
        this.discount = 0;
        this.c_address = "";
        this.c_zipcode = "";
        this.c_city = "";
        this.c_country = "";
        this.c_id = "0";
        this.c_a_array = [];
        this.c_status = "0";
        this.c_all_address = [];
        this.total_address = 0;
        this.shipping_charge = 0;
        this.orderconfirmarray = [];
        events.publish('allpagecommon');
        this.loadpage();
        this.mobile = localStorage.getItem("ship_mobile");
        this.postcode = "";
        this.platform.registerBackButtonAction(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__cartitem_cartitem__["a" /* CartitemPage */]);
        });
    }
    PaymentPage.prototype.isValidEmailAddress = function (emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    };
    /*opentab(tabnumber)
    {
        if(tabnumber == 1)
        {
            $("#two").css("display","none");
            $("#three").css("display","none");
            $("#one").css("display","block");
            $(".two").css("border-bottom","none");
            $(".three").css("border-bottom","none");
            $(".one").css("border-bottom","3px solid");
            $("#twofooter").css("display","none");
            $("#threefooter").css("display","none");
            $("#onefooter").css("display","block");
        }
        else if(tabnumber == 2)
        {
            if(this.updateaddress == 1)
            {
                $("#one").css("display","none");
                $("#three").css("display","none");
                $("#two").css("display","block");
                $(".one").css("border-bottom","none");
                $(".three").css("border-bottom","none");
                $(".two").css("border-bottom","3px solid");
                $("#onefooter").css("display","none");
                $("#threefooter").css("display","none");
                $("#twofooter").css("display","block");
            }
        }
        else
        {
            if(this.updateaddress == 1)
            {
                $("#two").css("display","none");
                $("#one").css("display","none");
                $("#three").css("display","block");
                $(".one").css("border-bottom","none");
                $(".two").css("border-bottom","none");
                $(".three").css("border-bottom","3px solid");
                $("#twofooter").css("display","none");
                $("#onefooter").css("display","none");
                $("#threefooter").css("display","block");
            }
        }
    }*/
    PaymentPage.prototype.opentab = function (tabnumber) {
        if (tabnumber == 1) {
            __WEBPACK_IMPORTED_MODULE_6_jquery__("#three").css("display", "none");
            __WEBPACK_IMPORTED_MODULE_6_jquery__("#one").css("display", "block");
            __WEBPACK_IMPORTED_MODULE_6_jquery__(".three").css("border-bottom", "none");
            __WEBPACK_IMPORTED_MODULE_6_jquery__(".one").css("border-bottom", "3px solid");
            __WEBPACK_IMPORTED_MODULE_6_jquery__("#threefooter").css("display", "none");
            __WEBPACK_IMPORTED_MODULE_6_jquery__("#onefooter").css("display", "block");
            __WEBPACK_IMPORTED_MODULE_6_jquery__(".two").css("border-bottom", "none");
            __WEBPACK_IMPORTED_MODULE_6_jquery__("#twofooter").css("display", "none");
            __WEBPACK_IMPORTED_MODULE_6_jquery__("#two").css("display", "none");
        }
        else if (tabnumber == 2) {
            if (this.updateaddress == 1) {
                //this.commonservice.waitloadershow();
                // this.commonservice.waitloaderhide();
                //  this.cartitem();
                __WEBPACK_IMPORTED_MODULE_6_jquery__("#one").css("display", "none");
                __WEBPACK_IMPORTED_MODULE_6_jquery__("#three").css("display", "none");
                __WEBPACK_IMPORTED_MODULE_6_jquery__("#two").css("display", "block");
                __WEBPACK_IMPORTED_MODULE_6_jquery__(".one").css("border-bottom", "none");
                __WEBPACK_IMPORTED_MODULE_6_jquery__(".three").css("border-bottom", "none");
                __WEBPACK_IMPORTED_MODULE_6_jquery__(".two").css("border-bottom", "3px solid");
                __WEBPACK_IMPORTED_MODULE_6_jquery__("#onefooter").css("display", "none");
                __WEBPACK_IMPORTED_MODULE_6_jquery__("#threefooter").css("display", "none");
                __WEBPACK_IMPORTED_MODULE_6_jquery__("#twofooter").css("display", "block");
            }
        }
        else {
            if (this.updateaddress == 1) {
                __WEBPACK_IMPORTED_MODULE_6_jquery__("#one").css("display", "none");
                __WEBPACK_IMPORTED_MODULE_6_jquery__("#three").css("display", "block");
                __WEBPACK_IMPORTED_MODULE_6_jquery__("#two").css("display", "none");
                __WEBPACK_IMPORTED_MODULE_6_jquery__(".one").css("border-bottom", "none");
                __WEBPACK_IMPORTED_MODULE_6_jquery__(".three").css("border-bottom", "3px solid");
                __WEBPACK_IMPORTED_MODULE_6_jquery__("#onefooter").css("display", "none");
                __WEBPACK_IMPORTED_MODULE_6_jquery__("#threefooter").css("display", "block");
                __WEBPACK_IMPORTED_MODULE_6_jquery__(".two").css("border-bottom", "none");
                __WEBPACK_IMPORTED_MODULE_6_jquery__("#twofooter").css("display", "none");
            }
        }
    };
    PaymentPage.prototype.gotoadresssection = function () {
        __WEBPACK_IMPORTED_MODULE_6_jquery__("#three").css("display", "none");
        __WEBPACK_IMPORTED_MODULE_6_jquery__("#two").css("display", "none");
        __WEBPACK_IMPORTED_MODULE_6_jquery__("#one").css("display", "block");
        __WEBPACK_IMPORTED_MODULE_6_jquery__(".three").css("border-bottom", "none");
        __WEBPACK_IMPORTED_MODULE_6_jquery__(".two").css("border-bottom", "none");
        __WEBPACK_IMPORTED_MODULE_6_jquery__(".one").css("border-bottom", "3px solid");
        __WEBPACK_IMPORTED_MODULE_6_jquery__("#threefooter").css("display", "none");
        __WEBPACK_IMPORTED_MODULE_6_jquery__("#twofooter").css("display", "none");
        __WEBPACK_IMPORTED_MODULE_6_jquery__("#onefooter").css("display", "block");
        this.fname = "";
        this.lname = "";
        this.email = "";
        this.emailone = "";
        this.emailtwo = "@gmail.com";
        this.address = "";
        this.city = "";
        this.emailtwoto = '';
        this.postcode = "";
        this.country = "";
        this.updateaddress = 0;
    };
    PaymentPage.prototype.goorderconfirm = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__verification_verification__["a" /* VerificationPage */]);
    };
    PaymentPage.prototype.gotoback = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__cartitem_cartitem__["a" /* CartitemPage */]);
        // this.navCtrl.setRoot(DashboardPage);
    };
    PaymentPage.prototype.goshippingmethod = function () {
        this.updateaddress = 1;
        this.shippingmethod = 1;
        this.opentab(3);
    };
    PaymentPage.prototype.changedom = function () {
        if (this.emailtwo == 'other') {
            this.opendom = 0;
            //    alert("dfdfdf");
        }
    };
    PaymentPage.prototype.loadpage = function () {
        var _this = this;
        if (localStorage.getItem("islogin")) {
            this.commonservice.waitloadershow();
            this.servicedata = "view_profile?userid=" + localStorage.getItem("loginuserid") + "&lang=" + localStorage.getItem("applanguage");
            this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
                _this.commonservice.waitloaderhide();
                _this.data = res;
                console.log(_this.data.data);
                _this.fname = _this.data.data.firstname;
                _this.lname = _this.data.data.lastname;
                //this.email=this.data.data.email;
                _this.mobile = _this.data.data.telephone;
                _this.address = _this.data.data.address;
                _this.addressship = _this.data.data.address;
                _this.emailship = _this.data.data.email;
                _this.emailmetod = _this.data.data.email;
                _this.addressmethod = _this.data.data.address;
                if (localStorage.getItem("ship_saveinformation")) {
                    _this.fname = localStorage.getItem("ship_fname");
                    _this.lname = localStorage.getItem("ship_lname");
                    _this.emailone = localStorage.getItem("ship_emailone");
                    _this.emailtwo = localStorage.getItem("ship_emailtwo");
                    _this.mobile = localStorage.getItem("ship_mobile");
                    _this.address = localStorage.getItem("ship_address");
                    _this.information = true;
                    _this.city = localStorage.getItem("ship_city");
                    _this.country = localStorage.getItem("ship_country");
                    _this.postcode = localStorage.getItem("ship_postcode");
                    _this.addressship = localStorage.getItem("ship_address");
                    _this.emailship = localStorage.getItem("ship_email");
                    _this.emailmetod = localStorage.getItem("ship_email");
                    _this.addressmethod = localStorage.getItem("ship_address");
                    _this.email = localStorage.getItem("ship_email");
                    _this.address = localStorage.getItem("ship_address");
                }
                //  this.cartitem();
                _this.countrylistad();
            });
        }
        else {
            if (localStorage.getItem("ship_saveinformation")) {
                this.fname = localStorage.getItem("ship_fname");
                this.lname = localStorage.getItem("ship_lname");
                this.emailone = localStorage.getItem("ship_emailone");
                this.emailtwo = localStorage.getItem("ship_emailtwo");
                this.mobile = localStorage.getItem("ship_mobile");
                this.address = localStorage.getItem("ship_address");
                this.information = true;
                this.city = localStorage.getItem("ship_city");
                this.country = localStorage.getItem("ship_country");
                this.postcode = localStorage.getItem("ship_postcode");
                this.addressship = localStorage.getItem("ship_address");
                this.emailship = localStorage.getItem("ship_email");
                this.emailmetod = localStorage.getItem("ship_email");
                this.addressmethod = localStorage.getItem("ship_address");
                this.email = localStorage.getItem("ship_email");
                this.address = localStorage.getItem("ship_address");
            }
            // this.cartitem();
            this.countrylistad();
        }
    };
    PaymentPage.prototype.countrylistad = function () {
        var _this = this;
        this.servicedata = "country?lang=" + localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            _this.countrylist = _this.data.data;
            _this.countrycode = "";
            _this.citylistad();
        });
    };
    PaymentPage.prototype.underprocess = function () {
        var toast = this.toastCtrl.create({
            message: 'In Process',
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            //console.log('Dismissed toast');
        });
        toast.present();
    };
    PaymentPage.prototype.citylistad = function () {
        var _this = this;
        this.servicedata = "city?lang=" + localStorage.getItem("applanguage");
        ;
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            _this.citylist = _this.data.data;
            //console.log(this.citylist);
        });
    };
    // getTotal ()
    // {
    //     var total = 0;
    //     for(var i = 0; i < this.cartitems.length; i++)
    //     {
    //         var product = this.cartitems[i];
    //         total += (product.price* product.quen);
    //     }
    //     this.tax=(total * 0.05);
    //     this.totalamount=(total * 0.05) + total;
    //     console.log(this.tax);
    // }
    PaymentPage.prototype.getTotal = function () {
        var _this = this;
        var total = 0;
        var taxprice = 0;
        var weight = 0;
        for (var i = 0; i < this.cartitems.length; i++) {
            var product = this.cartitems[i];
            if (product.is_discount == 1) {
                total += (product.discount_price * product.quen);
                weight += (product.weight * product.quen);
            }
            else {
                total += (product.price * product.quen);
                weight += (product.weight * product.quen);
            }
        }
        this.total_weight = weight;
        console.log(this.total_weight);
        this.servicedata = "shippingratecal?user_id=" + localStorage.getItem("loginuserid") + "&product_array=" + JSON.stringify(this.cartitems) + "&city=" + this.city + "&country_code=" + this.countrycode + "&total_weight=" + this.total_weight;
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            console.log(_this.data);
            console.log(_this.couponcode);
            _this.shipping_charge = _this.data.TotalAmount.Value;
            // if(localStorage.getItem("couponcode") == "0" || localStorage.getItem("couponcode") == "" || localStorage.getItem("couponcode") == null || localStorage.getItem("couponcode") == undefined ){  
            if (_this.coupon_code == null || _this.coupon_code == '') {
                console.log("null enter");
                _this.couponcode = "";
                _this.ct = 0;
                _this.couponshow = 0;
                _this.coupon_code = "";
                _this.totalamount = total;
                _this.taxablevalue = _this.totalamount * 0.05;
                _this.subtotalpay = _this.taxablevalue + _this.totalamount + _this.shipping_charge;
            }
            else {
                console.log("coupn enter enter");
                _this.couponshow = 1;
                _this.totalamount = total;
                _this.taxablevalue = _this.totalamount * 0.05;
                if (_this.coupontype == "P") {
                    console.log("percentgae enter");
                    _this.ct = _this.totalamount * _this.couponamount;
                    console.log(_this.shipping_charge);
                    _this.subtotalpay = (_this.taxablevalue + _this.totalamount + _this.shipping_charge) - _this.ct;
                    //   localStorage.setItem("couponamount",this.ct);
                }
                else {
                    console.log("flat amount enter");
                    _this.ct = _this.couponamount;
                    _this.subtotalpay = (_this.taxablevalue + _this.totalamount + _this.shipping_charge) - _this.ct;
                    //localStorage.setItem("couponamount",this.ct);
                }
                // this.couponcode=localStorage.getItem("couponcode");
                // this.servicedata="coupon_code?coupon_code="+localStorage.getItem("couponcode")+"&user_id="+localStorage.getItem("loginuserid");
                // this.commonservice.serverdataget(this.servicedata).subscribe(
                //      res => {
                //          this.data=res;
                //          console.log(this.data);
                //          this.coupontype =this.data.Response.type;
                //          this.couponamount=this.data.Response.amount;
                //          if(this.coupontype == "P"){
                //             console.log("percentgae enter");          
                //             this.ct = this.totalamount * this.couponamount;
                //             console.log(this.shipping_charge);                      
                //             this.subtotalpay=  (this.taxablevalue + this.totalamount + this.shipping_charge) - this.ct;
                //          //   localStorage.setItem("couponamount",this.ct);
                //             }
                //             else{
                //                 console.log("flat amount enter");
                //                this.ct = this.couponamount;                          
                //                 this.subtotalpay=  (this.taxablevalue + this.totalamount + this.shipping_charge) - this.ct;
                //                 //localStorage.setItem("couponamount",this.ct);
                //             }
                //      }
                // )
            }
        });
        localStorage.setItem("subtotalpay", JSON.stringify(this.subtotalpay));
        localStorage.setItem("tax", JSON.stringify(this.taxablevalue));
    };
    PaymentPage.prototype.minusitem = function (val, id, pr, color) {
        console.log(color);
        console.log(val);
        console.log(id);
        console.log(pr);
        var min = localStorage.getItem(pr);
        console.log('crt_min' + min);
        var nid = id;
        if (val > min) {
            var m = parseFloat(val) - parseFloat(min);
        }
        else {
            var m = parseFloat(val);
        }
        nid['' + pr] = m;
        console.log(m);
        console.log(pr);
        this.updatequenty(m, pr, color);
    };
    PaymentPage.prototype.plusitem = function (val, id, pr, stock, color) {
        console.log(color);
        console.log(val);
        console.log(id);
        console.log(pr);
        var quantity_stock = stock;
        var min = localStorage.getItem(pr);
        if (parseFloat(val) < parseFloat(stock)) {
            console.log("if call");
            var nid = id;
            var m = parseFloat(val) + parseFloat(min);
            nid['' + pr] = m;
        }
        else {
            console.log("else call");
            this.commonservice.erroralert("You reach max quntity");
            var nid = id;
            var m = parseFloat(val);
            nid['' + pr] = m;
        }
        //  var m = parseFloat(val) +parseFloat(min);
        console.log('crt_pls' + min);
        var nid = id;
        // var m = parseFloat(val) +parseFloat(min);
        nid['' + pr] = m;
        console.log(m);
        console.log(pr);
        this.updatequenty(m, pr, color);
    };
    PaymentPage.prototype.updatequenty = function (m, pid, color) {
        var a = [];
        var b = JSON.parse(localStorage.getItem("cart" + "11"));
        for (var _i = 0, b_1 = b; _i < b_1.length; _i++) {
            var val = b_1[_i];
            if (pid == val[0]) {
                if (color == val[7]) {
                    val[4] = m;
                    a.push(val);
                }
                else {
                    a.push(val);
                }
            }
            else {
                a.push(val);
            }
        }
        ;
        localStorage.setItem("cart" + "11", JSON.stringify(a));
        this.allitem();
    };
    PaymentPage.prototype.allitem = function () {
        console.log("allitem function call start;");
        this.cartitems = [];
        this.productitem = JSON.parse(localStorage.getItem("cart" + "11"));
        console.log(this.productitem);
        if (this.productitem != null) {
            if (this.productitem.length > 0) {
                console.log(this.productitem.length);
                var i = 0;
                for (var _i = 0, _a = this.productitem; _i < _a.length; _i++) {
                    var val = _a[_i];
                    i++;
                    this.cartitems.push({ "id": val[0], "name": val[1], "price": val[2], "image": val[3], "quen": val[4], "material": val[5], "color": val[7], "is_discount": val[9], "discount_price": val[10], "quantity_stock": val[11], "weight": val[12], "coupon_amount": val[13] });
                    console.log(this.cartitems);
                }
                this.nocart = 1;
            }
            else {
                this.nocart = 0;
            }
        }
        else {
            this.nocart = 0;
        }
        this.getTotal();
        // this.shiipingratecal();
        console.log(this.nocart);
        localStorage.setItem("cartproductcount", JSON.stringify(this.cartitems.length));
        console.log("allitem function call end;");
    };
    PaymentPage.prototype.deleteConfirm = function (id, color) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm',
            message: 'Do you want to Delete this product in your cart?',
            buttons: [{
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Ok',
                    handler: function () {
                        _this.deleteitem(id, color);
                    }
                }
            ]
        });
        alert.present();
    };
    PaymentPage.prototype.deleteitem = function (id, color) {
        var a = [];
        var b = JSON.parse(localStorage.getItem("cart" + "11"));
        for (var _i = 0, b_2 = b; _i < b_2.length; _i++) {
            var val = b_2[_i];
            if (id != val[0] || (val[7] != color)) {
                a.push(val);
            }
        }
        ;
        localStorage.setItem("cart" + "11", JSON.stringify(a));
        //  localStorage.removeItem(id);
        this.allitem();
    };
    PaymentPage.prototype.coupon = function () {
        var _this = this;
        if (this.couponcode == null || this.couponcode == '') {
            this.couponshow = 0;
            this.coupon_code = "";
            this.couponcode = "";
            this.getTotal();
            //localStorage.setItem("couponcode","");
        }
        else {
            console.log(this.couponcode);
            this.commonservice.waitloadershow();
            this.servicedata = "coupon_code?coupon_code=" + this.couponcode + "&user_id=" + localStorage.getItem("loginuserid");
            this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
                _this.data = res;
                _this.commonservice.waitloaderhide();
                console.log(_this.data);
                if (_this.data.status == 1) {
                    _this.coupontype = _this.data.Response.type;
                    _this.couponamount = _this.data.Response.amount;
                    _this.couponshow = 1;
                    _this.coupon_code = _this.couponcode;
                    // localStorage.setItem("couponcode",this.couponcode);  
                }
                else {
                    _this.couponshow = 0;
                    _this.coupon_code = "";
                    _this.couponcode = "";
                    _this.commonservice.erroralert("Invalid coupon code");
                }
                _this.getTotal();
            });
        }
    };
    PaymentPage.prototype.removecoupon = function () {
        this.couponshow = 0;
        this.coupon_code = "";
        this.couponcode = "";
        this.getTotal();
    };
    PaymentPage.prototype.cartitem = function () {
        this.productitem = JSON.parse(localStorage.getItem("cart" + "11"));
        console.log(this.productitem);
        if (this.productitem != null) {
            if (this.productitem.length > 0) {
                var i = 0;
                for (var k = 0; k < this.productitem.length; k++) {
                    var temp = this.productitem[k];
                    this.cartitems.push({ "id": temp.pid, "name": temp.name,
                        "price": temp.price, "image": temp.image,
                        "quen": temp.qty, "material": '', "color": temp.color,
                        "is_discount": temp.isDiscount, "discount_price": temp.discount_price,
                        "quantity_stock": temp.stock, "weight": temp.weight });
                }
                this.nocart = 1;
            }
            else {
                this.nocart = 0;
            }
            console.log(this.cartitems);
        }
        else {
            this.nocart = 0;
        }
        this.getTotal();
        // this.shiipingratecal();
    };
    PaymentPage.prototype.savetoinformation = function () {
        if (this.emailtwo == 'other') {
            this.email = this.emailone + "" + this.emailtwoto;
            // alert("sdsdsdsdsdsdsd");
        }
        else {
            this.email = this.emailone + "" + this.emailtwo;
        }
        // console.log(this.email);
        this.error = "";
        if (!this.fname) {
            this.error = "First Name is required";
        }
        else if (!this.lname) {
            this.error = "Last Name is required";
        }
        else if (!this.email) {
            this.error = "Email is required";
        }
        else if (this.isValidEmailAddress(this.email) == false) {
            this.error = "Please Enter Valid Email Address And For other porvide Full E-Mail Address E.x admin@admin.com";
        }
        else if (!this.mobile) {
            this.error = "Mobile Number is required";
        }
        else if (!this.address) {
            this.error = "Address is required";
        }
        else if (!this.city) {
            this.error = "City is required";
        }
        else if (!this.country) {
            this.error = "Country is required";
        }
        else if (!this.postcode || !(__WEBPACK_IMPORTED_MODULE_6_jquery__["isNumeric"](this.postcode))) {
            this.error = "Post Code is required";
        }
        if (this.error == "") {
            localStorage.setItem("ship_fname", this.fname);
            localStorage.setItem("ship_lname", this.lname);
            localStorage.setItem("ship_emailone", this.emailone);
            localStorage.setItem("ship_emailtwo", this.emailtwo);
            localStorage.setItem("ship_mobile", this.mobile);
            localStorage.setItem("ship_address", this.address);
            localStorage.setItem("ship_city", this.city);
            localStorage.setItem("ship_country", this.country);
            localStorage.setItem("ship_postcode", this.postcode);
        }
        else {
            this.commonservice.erroralert(this.error);
            this.information = false;
        }
    };
    PaymentPage.prototype.savetoinformationto = function () {
        this.cartitem();
        if (this.emailtwo == "other") {
            this.email = this.emailone;
            console.log(this.email);
        }
        else {
            this.email = this.emailone + '' + this.emailtwo;
            console.log(this.email);
        }
        //console.log(this.email);
        this.error = "";
        if (this.fname == "") {
            this.error = "First Name is required";
        }
        else if (this.lname == "") {
            this.error = "Last Name is required";
        }
        else if (this.email == "") {
            this.error = "Email is required";
        }
        else if (this.isValidEmailAddress(this.email) == false) {
            this.error = "For other porvide Full E-Mail Address Like E.x admin@admin.com";
            // this.error="Please Enter Valid Email Address";
        }
        else if (this.mobile == "") {
            this.error = "Mobile Number is required";
        }
        else if (this.address == "") {
            this.error = "Address is required";
        }
        else if (this.city == "") {
            this.error = "City is required";
        }
        else if (this.country == "") {
            this.error = "Country is required";
        }
        // else if(this.postcode=="")
        // {
        //     this.error="Post Code is required";
        // }
        if (this.error == "") {
            localStorage.setItem("ship_fname", this.fname);
            localStorage.setItem("ship_lname", this.lname);
            localStorage.setItem("ship_emailone", this.emailone);
            localStorage.setItem("ship_emailtwo", this.emailtwo);
            localStorage.setItem("ship_mobile", this.mobile);
            localStorage.setItem("ship_address", this.address);
            localStorage.setItem("ship_city", this.city);
            localStorage.setItem("ship_country", this.country);
            localStorage.setItem("ship_postcode", this.postcode);
            this.shiipingratecal();
        }
        else {
            this.commonservice.erroralert(this.error);
            this.information = false;
        }
    };
    PaymentPage.prototype.changeinemail = function () {
        this.bcngemail = "1";
    };
    PaymentPage.prototype.changeinaddress = function () {
        this.bcngaddress = "1";
    };
    PaymentPage.prototype.savechangeinemail = function () {
        if (this.email == "") {
            this.error = "Email is required";
        }
        else if (this.isValidEmailAddress(this.email) == false) {
            this.error = "Please Enter Valid Email Address";
        }
        if (this.error == "") {
            localStorage.setItem("ship_email", this.email);
            this.bcngemail = "0";
            this.addressship = localStorage.getItem("ship_address");
            this.emailship = localStorage.getItem("ship_email");
            this.emailmetod = localStorage.getItem("ship_email");
            this.addressmethod = localStorage.getItem("ship_address");
            this.email = localStorage.getItem("ship_email");
            this.address = localStorage.getItem("ship_address");
        }
        else {
            this.commonservice.erroralert(this.error);
        }
    };
    PaymentPage.prototype.savechangeinaddress = function () {
        //console.log("changeinaddress");
        if (this.address == "") {
            this.error = "Address is required";
        }
        if (this.error == "") {
            localStorage.setItem("ship_address", this.address);
            this.bcngaddress = "0";
            this.addressship = localStorage.getItem("ship_address");
            this.emailship = localStorage.getItem("ship_email");
            this.emailmetod = localStorage.getItem("ship_email");
            this.addressmethod = localStorage.getItem("ship_address");
            this.email = localStorage.getItem("ship_email");
            this.address = localStorage.getItem("ship_address");
        }
        else {
            this.commonservice.erroralert(this.error);
        }
    };
    PaymentPage.prototype.changeinemaillast = function () {
        this.bcngemaillast = "1";
    };
    PaymentPage.prototype.changeinaddresslast = function () {
        this.bcngaddresslast = "1";
    };
    PaymentPage.prototype.savechangeinemaillast = function () {
        if (this.email == "") {
            this.error = "Email is required";
        }
        else if (this.isValidEmailAddress(this.email) == false) {
            this.error = "Please Enter Valid Email Address";
        }
        if (this.error == "") {
            localStorage.setItem("ship_email", this.email);
            this.bcngemaillast = "0";
            this.addressship = localStorage.getItem("ship_address");
            this.emailship = localStorage.getItem("ship_email");
            this.emailmetod = localStorage.getItem("ship_email");
            this.addressmethod = localStorage.getItem("ship_address");
            this.email = localStorage.getItem("ship_email");
            this.address = localStorage.getItem("ship_address");
        }
        else {
            this.commonservice.erroralert(this.error);
        }
    };
    PaymentPage.prototype.savechangeinaddresslast = function () {
        if (this.address == "") {
            this.error = "Address is required";
        }
        if (this.error == "") {
            localStorage.setItem("ship_address", this.address);
            this.bcngaddresslast = "0";
            this.addressship = localStorage.getItem("ship_address");
            this.emailship = localStorage.getItem("ship_email");
            this.emailmetod = localStorage.getItem("ship_email");
            this.addressmethod = localStorage.getItem("ship_address");
            this.email = localStorage.getItem("ship_email");
            this.address = localStorage.getItem("ship_address");
        }
        else {
            this.commonservice.erroralert(this.error);
        }
    };
    PaymentPage.prototype.completeorder = function () {
        // this.commonservice.waitloadershow();
        this.completeorderto();
        // this.servicedata="login?mobile="+this.mobile+"&lang="+localStorage.getItem("applanguage");
        // this.commonservice.serverdataget(this.servicedata).subscribe(
        //     res => {
        //         this.commonservice.waitloaderhide();
        //         this.data=res;
        //         console.log(this.data);
        //         if(this.data.status="true")
        //         {
        //             localStorage.setItem("loginotp", this.data.otp);
        //             localStorage.setItem("loginuserid", this.data.userid);
        //         }
        //         else
        //         {
        //             this.commonservice.erroralert("Something went wrong");   
        //         }
        //     }
        // )
    };
    PaymentPage.prototype.completeorderto = function () {
        if (localStorage.getItem("ship_emailtwo") == "other") {
            this.email = localStorage.getItem("ship_emailone");
            console.log(this.email);
            // alert("dfdsdssf");
        }
        else {
            this.email = localStorage.getItem("ship_emailone") + localStorage.getItem("ship_emailtwo");
            console.log(this.email);
        }
        // this.commonservice.waitloadershow();
        //    this.shipping_charge= "00";
        console.log(JSON.stringify(this.cartitems));
        this.getTotal();
        this.orderconfirmarray.push({ "tax": this.taxablevalue, "totalamount": this.subtotalpay, "shipping_charge": this.shipping_charge, "subtotal": this.totalamount, "email": this.email, "coupon_amount": this.ct, "coupon_code": this.coupon_code, "cartitems": JSON.stringify(this.cartitems) });
        console.log(this.orderconfirmarray);
        localStorage.setItem("orderconifrmdetails", JSON.stringify(this.orderconfirmarray));
        //this.commonservice.successalert("The order cannot be cancelled after 24 hours of confirmation");
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__orderConfirmed_orderConfirmed__["a" /* OrderConfirmedPage */]);
        // this.servicedata="placeorder?firstname="+localStorage.getItem("ship_fname")+"&tax="+m.tax+ "&totalamount="+m.subtotalpay + "&shipping_charge="+m.shipping_charge +"&subtotal=" + m.totalamount + "&lastname="+localStorage.getItem("ship_lname")+"&telephone="+localStorage.getItem("ship_mobile")+"&email="+m.email+"&address="+localStorage.getItem("ship_address")+"&postcode="+localStorage.getItem("ship_postcode")+"&city="+localStorage.getItem("ship_city")+"&shipping_country="+localStorage.getItem("ship_country")+"&customer_id="+localStorage.getItem("loginuserid")+"&product_array="+m.cartitems+"&lang="+localStorage.getItem("applanguage")+"&coupon_amount="+m.ct+"&coupon_code="+m.couponcode;    
    };
    PaymentPage.prototype.newaddress = function () {
        var _this = this;
        if (this.c_country == null || this.c_country == '' || this.c_address == '' || this.c_address == null || this.c_city == null || this.c_city == '' || this.c_zipcode == null || this.c_zipcode == '' || !(__WEBPACK_IMPORTED_MODULE_6_jquery__["isNumeric"](this.c_zipcode))) {
            if (this.c_address == '' || this.c_address == null) {
                this.commonservice.erroralert("New Address Required");
            }
            else if (this.c_city == null || this.c_city == '') {
                this.commonservice.erroralert("City Name Required");
            }
            else if (this.c_country == null || this.c_country == '') {
                this.commonservice.erroralert("Country Name Required");
            }
            else if (this.c_zipcode == null || this.c_zipcode == '') {
                this.commonservice.erroralert("Postal Code Required");
            }
            else if (!(__WEBPACK_IMPORTED_MODULE_6_jquery__["isNumeric"](this.c_zipcode))) {
                this.commonservice.erroralert("Postal Code Have Only Numeric Value");
            }
        }
        else {
            console.log(this.c_country);
            this.servicedata = "add_user_address?user_id=" + localStorage.getItem("loginuserid") + "&lang=" + localStorage.getItem("applanguage") + "&city=" + this.c_city + "&country=" + this.c_country + "&zipcode=" + this.c_zipcode + "&address=" + this.c_address;
            this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
                _this.data = res;
                console.log(_this.data);
            });
            //  this.address = this.c_address;
            //  this.city =this.c_city ;
            //  this.country =this.c_country ;
            // this.postcode = this.c_zipcode;
            __WEBPACK_IMPORTED_MODULE_6_jquery__(".tab-content #menu1").removeClass("active");
            __WEBPACK_IMPORTED_MODULE_6_jquery__(".tab-content #menu1").removeClass("in");
            __WEBPACK_IMPORTED_MODULE_6_jquery__(".tab-content #home").addClass("active");
            __WEBPACK_IMPORTED_MODULE_6_jquery__(".tab-content #home").addClass("in");
            __WEBPACK_IMPORTED_MODULE_6_jquery__(".modal-body ul li").eq(1).addClass("active");
            __WEBPACK_IMPORTED_MODULE_6_jquery__(".modal-body ul li").eq(2).removeClass("active");
            __WEBPACK_IMPORTED_MODULE_6_jquery__("#firsttab").addClass("active");
            __WEBPACK_IMPORTED_MODULE_6_jquery__("#secounftab").removeClass("active");
            this.custom_address();
            this.c_zipcode = "";
            this.c_city = "";
            this.c_address = "";
            this.c_country = "";
            //$(".tab-content #home").addClass("active");
        }
    };
    PaymentPage.prototype.custom_address = function () {
        var _this = this;
        // $("body").addClass("modal-open");
        this.servicedata = "isAddressAvailable?user_id=" + localStorage.getItem("loginuserid");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            console.log(_this.data.total_address);
            _this.total_address = _this.data.total_address;
            if (_this.data.status == 1) {
                _this.c_status = _this.data.status;
                _this.c_all_address = _this.data.Response;
                // for(let b of this.data.Response){
                // this.c_address=b.address;
                // this.c_id=b.id;
                // this.c_city=b.city;
                // this.c_country=b.country;
                // this.c_zipcode=b.zipcode;                   
                // console.log(this.c_address);
                // }
            }
            else {
                _this.total_address = 0;
                if (_this.data.total_address >= 2) {
                    _this.commonservice.erroralert("You Can Add Only 2 Address");
                    __WEBPACK_IMPORTED_MODULE_6_jquery__("#secounftab").css("display", "none");
                }
                _this.c_status = _this.data.status;
                // this.commonservice.erroralert("something went wrong");
            }
            console.log(_this.data);
        });
    };
    PaymentPage.prototype.selectaddress = function (id) {
        var _this = this;
        this.servicedata = "isAddressAvailable?user_id=" + localStorage.getItem("loginuserid");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            if (_this.data.status == 1) {
                for (var _i = 0, _a = _this.data.Response; _i < _a.length; _i++) {
                    var b = _a[_i];
                    _this.c_id = b.id;
                    // console.log(id);
                    // console.log(b.id);
                    if (id == b.id) {
                        _this.address = b.address;
                        _this.city = b.city;
                        _this.country = b.country;
                        console.log(_this.country);
                        _this.postcode = b.zipcode;
                        _this.countrycode = b.iso;
                        // this.shiipingratecal();
                        //$('#exampleModa789l').hide();
                        // var $ :any;
                        //    var el = document.getElementById('#exampleModa789l') as HTMLDivElement;
                        //    el. modal('hide');
                        //   document.getElementById("exampleModa789l").style.display = "none";
                        //  $('#exampleModa789l').modal('hide');
                        //location.reload();
                    }
                }
            }
            else {
                _this.c_status = _this.data.status;
                //  this.commonservice.erroralert("something went wrong");
            }
            console.log(_this.data);
        });
        // $(".modalcancel").append("<button type='button' data-dismiss='modal'> </button>"); 
        // let alert = this.alertCtrl.create
        // ({
        //     title: 'Confirm',
        //     message: 'Confirm Your Shippping Address',
        //     buttons: 
        // [{
        //     text: 'Cancel',
        //     role: 'cancel',
        //     handler: () => {
        //     }
        //     },
        //     {
        //         text: 'Ok',
        //         cssClass: 'modalcancel',
        //         handler: () => 
        //         {
        //             //$('.modalcancel').html("<div id='mySecondDiv'></div>");
        //            // $(".modalcancel").append("<button type='button' data-dismiss='modal'> </button>"); 
        //         // $(".alert-button").append("<button type='button' data-dismiss='modal'> </button>"); 
        //           this.confirmnewaddress(id);
        //         }
        //     }
        // ]});
        // alert.present();
    };
    // confirmnewaddress(id){        
    //     this.servicedata="isAddressAvailable?user_id="+localStorage.getItem("loginuserid");
    //     this.commonservice.serverdataget(this.servicedata).subscribe(
    //         res=>{
    //             this.data=res;
    //             if(this.data.status == 1){
    //                 for(let b of this.data.Response){
    //                 this.c_id=b.id;
    //                 // console.log(id);
    //                 // console.log(b.id);
    //                 if( id == b.id){
    //                     this.address=b.address;
    //                     this.city=b.city;
    //                     this.country=b.country;
    //                     this.postcode=b.zipcode;  
    //                    $('#exampleModa789l').hide();
    //                   // var $ :any;
    //                 //    var el = document.getElementById('#exampleModa789l') as HTMLDivElement;
    //                 //    el. modal('hide');
    //                 //   document.getElementById("exampleModa789l").style.display = "none";
    //            //  $('#exampleModa789l').modal('hide');
    //                  //location.reload();
    //                 }
    //                 }
    //             }
    //             else{
    //                 this.c_status=this.data.status;
    //               //  this.commonservice.erroralert("something went wrong");
    //             }
    //             console.log(this.data);
    //         }
    //     ) 
    // }
    PaymentPage.prototype.selectcountry = function (id) {
        this.countrycode = id;
    };
    PaymentPage.prototype.shiipingratecal = function () {
        var _this = this;
        // for(var i = 0; i < this.cartitems.length; i++)
        // {
        //     var weight;
        //     var product = this.cartitems[i];
        //     weight += (product.weight* product.quen);      
        // }   
        // this.total_weight=weight;  
        this.commonservice.waitloadershow();
        this.servicedata = "shippingratecal?user_id=" + localStorage.getItem("loginuserid") + "&product_array=" + JSON.stringify(this.cartitems) + "&city=" + this.city + "&country_code=" + this.countrycode + "&total_weight=" + this.total_weight;
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            console.log(_this.data);
            _this.commonservice.waitloaderhide();
            if (_this.data.HasErrors == false) {
                _this.shipping_charge = _this.data.TotalAmount.Value;
                _this.updateaddress = 1;
                _this.opentab(2);
            }
            else {
                _this.opentab(1);
                _this.commonservice.erroralert("Enter valid city and country name");
            }
        });
        console.log(this.shipping_charge);
    };
    PaymentPage.prototype.deleteaddress = function (id) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm',
            message: 'Do you want to delete this address?',
            buttons: [{
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Ok',
                    handler: function () {
                        _this.deltaddress(id);
                    }
                }
            ]
        });
        alert.present();
    };
    PaymentPage.prototype.deltaddress = function (id) {
        var _this = this;
        this.servicedata = "delete_user_address?user_id=" + localStorage.getItem("loginuserid") + "&address_id=" + id;
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            if (_this.data.status == 1) {
                _this.commonservice.successalert("Deleted succussfully");
            }
            else {
                // this.commonservice.erroralert("Something Went Wrong");
            }
        });
        this.custom_address();
    };
    PaymentPage.prototype.gohome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__dashboard_dashboard__["a" /* DashboardPage */]);
    };
    PaymentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-payment',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/payment/payment.html"*/'<ion-header>\n\n    <!-- <ion-navbar> -->\n\n            <!-- style="width: 100%;padding-top: 20px; padding-bottom: 17px;"\n\n            style="width: 80%;float: left;    margin-top: -4px;"<div class="container" style="width: 100%; ">\n\n         style="width: 20%;float: left;background-color: transparent;" -->\n\n         <div class="headerbg_wh">\n\n      <div class="container" style="    display: flex;\n\n        width: 100%;">\n\n         <button icon-only   style="    flex: 1; background-color: transparent;" >\n\n            <i (click)="gotoback()" class="fa fa-arrow-left" aria-hidden="true" style="font-size: 20px;"></i>\n\n        </button>\n\n        <ion-title style="flex: 9;" >{{"CUSTOMERINFO" | translate}}</ion-title>\n\n    </div>\n\n    <!-- </ion-navbar> -->\n\n    </div>\n\n</ion-header>\n\n<ion-content>\n\n   \n\n    <div class="container" style=" margin-top: 30px; padding:0;border-bottom: 1px solid #d8d0d0;" >\n\n        <div class="tab">\n\n            <button class="tablinks one" style="border-bottom: 3px solid;    width: 33.33%;" (click)="opentab(\'1\')"  >{{"CUSTOMERINFO" | translate}}</button>\n\n            <button class="tablinks two" style="width: 33.33%;" (click)="opentab(\'2\')" >{{"Ordersummary" | translate}}</button>\n\n            <!-- {{"SHIPPINGMETHOD" | translate}} -->\n\n            <button class="tablinks three" style="width: 33.33%;  padding-left: 0px; padding-right: 0px;" (click)="opentab(\'3\')"  >{{"PAYMENT" | translate}}</button>\n\n        </div>\n\n    </div>\n\n    <div class="container" style="padding-top: 24px;">\n\n        <div id="one" style="margin-bottom:30px;">\n\n            <div class="container-fluid pt-5"> \n\n                <div class="loginpage">\n\n                    <div class="form-group" style=\'padding-bottom: 24px;width: 100%\' >\n\n                        <input  class="line-animation" placeholder=\'{{"EMAIL" | translate}}\' type="text" style="width: 45%;float: left" [(ngModel)]="emailone" >\n\n                        <input *ngIf="opendom==0"  class="line-animation" placeholder="@gmail.com" type="text" style="float: left" [(ngModel)]="emailtwoto" >\n\n                        <ion-select *ngIf="opendom==1"  class="line-animation" [(ngModel)]="emailtwo"  style="color:black; width: 45%; float: left;margin-left: 25px;font-size: 14px;padding: 0 0 3px;margin-top: 0px;" (change)=\'changedom()\' >\n\n                            <ion-option value="@gmail.com">@gmail.com</ion-option>\n\n                            <ion-option value="@hotmail.com">@hotmail.com</ion-option>\n\n                            <ion-option value="other">Other</ion-option>\n\n                        </ion-select>\n\n                        <div class="line"></div>\n\n                    </div>\n\n                    <div class="form-group"  >\n\n                        <input  class="line-animation"  placeholder=\'{{"FNAME" | translate}}\'  type="text" [(ngModel)]="fname" style="width:45%;float: left">\n\n\n\n                        <input  class="line-animation" placeholder=\'{{"LNAME" | translate}}\'   type="text" [(ngModel)]="lname" style="width:45%;float: left;margin-left: 25px !important ">\n\n                        <div class="line"></div>\n\n                    </div>\n\n                    <div class="form-group" style=\'padding-bottom: 0px;\'>\n\n                        <input  class="line-animation" placeholder=\'{{"ADDRESS" | translate}}\'  [(ngModel)]="address" type="text">\n\n                        <div class="line"></div>\n\n                    </div>\n\n                    <div class="form-group" style=\'padding-bottom: 10px;\'>\n\n                        <!-- <ion-select class="line-animation" [(ngModel)]="city" style="color:black; max-width: 100%;width:100%;float: left;font-size: 14px;padding: 0 0px 5px;" >\n\n                            <ion-option  value="">{{"CHOOSEYOURCITY" | translate}} </ion-option>\n\n                            <ion-option *ngFor="let ci of citylist" value="{{ci}}">{{ci}}</ion-option>\n\n                        </ion-select> -->\n\n                        <input  class="line-animation" placeholder=\'{{"City" | translate}}\' [(ngModel)]="city"  type="text">\n\n                        <!-- <select class="line-animation" [(ngModel)]="city" style="color:black; width:100%;float: left;font-size: 14px;" >\n\n                            <option  value="">{{"CHOOSEYOURCITY" | translate}} </option>\n\n                            <option *ngFor="let ci of citylist" value="{{ci}}">{{ci}}</option>\n\n                        </select> -->\n\n                        <div class="line"></div>\n\n                    </div>\n\n\n\n                    \n\n                     <div class="form-group s_quntity" style="margin: 15px 0 0 !important;">\n\n                        <ion-item>\n\n                            <ion-label>{{"Selectcountry" | translate}}</ion-label>\n\n                        <ion-select class="line-animation" [(ngModel)]="country" style="color:black; max-width: 100%;width:100%;float: left;font-size: 14px;padding: 0 0px 5px;" >\n\n                            <ion-option  value="">{{"CHOOSEYOURCOUNTRY" | translate}} </ion-option>\n\n                            <ion-option (ionSelect)="selectcountry(co.iso)" *ngFor="let co of countrylist" value="{{co.phonecode}}">{{co.name}}</ion-option>\n\n                        </ion-select>\n\n                    </ion-item>\n\n                     <!--     <select class="line-animation" [(ngModel)]="country" style="color:black; width:35%;float: left;font-size: 14px;" >\n\n                            <option  value="">{{"CHOOSEYOURCOUNTRY" | translate}} </option>\n\n                            <option *ngFor="let co of countrylist" value="{{co.phonecode}}">{{co.name}}</option>\n\n                        </select> -->\n\n\n\n                        <!-- <input  class="line-animation" placeholder=\'{{"POSTCODE" | translate}}\'  [(ngModel)]="postcode" type="text" style="width:47%;float: left;margin-left: 25px "> -->\n\n                        <div class="line"></div>\n\n                    </div>\n\n                    <div class="form-group" style="margin: 10px 0 0 !important;">\n\n                            <input  pattern="[0-9]*" decimal="true" allow-multiple-decimals="true" class="line-animation" placeholder=\'{{"POSTCODE" | translate}}\'  [(ngModel)]="postcode" type="number" >\n\n                            <div class="line"></div>\n\n                    </div>\n\n                    <div class="form-group" style=\'padding-bottom: 0px;\' style="margin: 10px 0 0 !important;">\n\n                        <input  class="line-animation" placeholder=\'{{"MOBILENUMBER" | translate}}\'  [(ngModel)]="mobile" type="text" readonly="true">\n\n                        <div class="line"></div>\n\n                    </div>\n\n                    <div class="form-check-inline" style=\'padding-top: 15px;\'>\n\n                        <label class="form-check-label" style="font-size:12px;">\n\n                            <input type="checkbox"  (click)="savetoinformation()" [(ngModel)]="information"  class="form-check-input" id="myCheck" > &nbsp;&nbsp;{{"SAVEINFO" | translate}} <a  style="text-align:right;"> {{"REFUNDPOLICY" | translate}}</a>\n\n                        </label>\n\n                    </div>\n\n\n\n                    <button type="button" style="border: 0;\n\n                    font-size: 12px;\n\n                    margin-top: 10px;\n\n                    letter-spacing: 0.6px;" (click)="custom_address()" class="btn btn-primary" data-toggle="modal" data-target="#exampleModa789l">\n\n                            {{"Address Option" | translate}}\n\n                      </button>\n\n                      \n\n                      <!-- Modal -->\n\n                      \n\n                      <div class="modal fade secound_address" id="exampleModa789l" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">\n\n                        <div class="modal-dialog" role="document">\n\n                          <div class="modal-content">\n\n                            <!-- <div class="modal-header">\n\n                              <h5 class="modal-title" id="exampleModalLabel">Add New Address</h5>\n\n                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">\n\n                                <span aria-hidden="true">&times;</span>\n\n                              </button>\n\n                            </div> -->\n\n                            <div class="modal-body">\n\n                                    \n\n                                          <ul class="nav nav-tabs">\n\n                                                <li id="firsttab"  class="active"><a data-toggle="tab" href="#home"> {{"Save address" | translate}}</a></li>\n\n                                                <li *ngIf="2 > total_address" id="secounftab"  ><a data-toggle="tab" href="#menu1">{{"Add New address" | translate}}</a></li>\n\n\n\n                                             \n\n                                              </ul>\n\n                                              \n\n                                              <div class="tab-content">\n\n                                                <div id="home" class="tab-pane fade in active">\n\n                                                        <div *ngIf="c_status == 0" >\n\n                                                                {{"Add New address" | translate}}\n\n                                                             </div>\n\n                                                             <div style="overflow-y:auto;max-height: 300px;" *ngIf="c_status == 1" >                                                    \n\n            \n\n                <!-- <div *ngFor="let b of c_all_address" class="c_addres"> \n\n                    <div >\n\n                            !-- <input (click)="selectaddress(b.id)" style="width:20px;height:20px;display: inline-block; vertical-align: middle;" type="radio" name="gender" value="male"> \n\n                            <span class="addresinner">\n\n                                    <div class="deletaddress" (click)="deleteaddress(b.id)"><ion-icon ios="ios-close" md="md-close"></ion-icon> </div>\n\n                                            <div>                                 \n\n                                                <div>{{b.address}}</div><span>{{b.city}}</span><span>{{b.country}}</span><span>{{b.zipcode}}</span>\n\n                                            </div>\n\n                                    </span> -->\n\n                            \n\n                        <!-- <input style="display: inline-block; vertical-align: middle;" type="radio" name="gender" value="female">\n\n                        <span class="addresinner">\n\n                        <div class="deletaddress" (click)="deleteaddress(b.id)"><ion-icon ios="ios-close" md="md-close"></ion-icon> </div>\n\n                                <div >\n\n                        - (click)="selectaddress(b.id)" --\n\n                                    <div>{{b.address}}</div><span>{{b.city}}</span><span>{{b.country}}</span><span>{{b.zipcode}}</span>\n\n                                </div>\n\n                        </span> --\n\n                    </div>                    \n\n                </div> -->\n\n                <ion-list radio-group  class="optionaddress">\n\n                        <ion-item  *ngFor="let b of c_all_address">\n\n                          <!-- <ion-label>{{b.address}} {{b.city}} {{b.country}} {{b.zipcode}}</ion-label>\n\n                          <ion-radio value="friends"></ion-radio> -->\n\n                          <ion-radio  (ionSelect)="selectaddress(b.id)"></ion-radio>                          \n\n                          <ion-label> <div class="deletaddress" (click)="deleteaddress(b.id)"><ion-icon ios="ios-close" md="md-close"></ion-icon> </div>{{b.address}},<br> {{b.city}} ,{{b.country_name}}, {{b.zipcode}} </ion-label>                          \n\n                        </ion-item>     \n\n                </ion-list>\n\n\n\n                <button type="button" style="text-align: center; display: block;background-color: #ae9435; color: #fff;margin: 30px auto 15px;" class="btn btn-secondary" data-dismiss="modal">{{"SAVE" | translate}}</button>\n\n                \n\n            \n\n                                                            </div>\n\n                                                </div>\n\n                                                <div id="menu1" class="tab-pane fade">\n\n                                                    \n\n                                                        <div class="form-group" style=\'padding-bottom: 0px;\'>\n\n                                                                <input  class="line-animation" placeholder=\'{{"ADDRESS" | translate}}\'  [(ngModel)]="c_address" type="text">\n\n                                                                <div class="line"></div>\n\n                                                            </div>\n\n                                                            <div class="form-group" style=\'padding-bottom: 10px;\'>\n\n                                                                <input  class="line-animation" placeholder=\'{{"City" | translate}}\' [(ngModel)]="c_city"  type="text">\n\n                                                                <div class="line"></div>\n\n                                                            </div>\n\n                                                            \n\n                                                             <div class="form-group s_quntity" >\n\n                                                                    <ion-item>\n\n                                                                    <ion-label>{{"Selectcountry" | translate}} </ion-label>\n\n                                                                <ion-select class="line-animation" [(ngModel)]="c_country" style="color:black; max-width: 100%;width:100%;float: left;font-size: 14px;padding: 0 0px 5px;" >\n\n                                                                    <ion-option  value="">{{"CHOOSEYOURCOUNTRY" | translate}} </ion-option>\n\n                                                                    <ion-option *ngFor="let co of countrylist" value="{{co.phonecode}}">{{co.name}}</ion-option>\n\n                                                                </ion-select>\n\n                                                            </ion-item>\n\n                                                                \n\n                                                            </div>\n\n                                                            <div class="form-group">\n\n                                                                    <input pattern="[0-9]*" decimal="true" allow-multiple-decimals="true" class="line-animation" placeholder=\'{{"POSTCODE" | translate}}\'  [(ngModel)]="c_zipcode" type="number">\n\n                                                                    <div class="line"></div>\n\n\n\n                                                            </div>\n\n                                                            <button style="margin-top: 15px;" type="button" (click)="newaddress()" class="btn btn-primary">{{"savenewaddress" | translate}}</button>\n\n                                                </div>\n\n                                             \n\n                                              </div>\n\n                                \n\n                            </div>\n\n                            <div class="modal-footer">\n\n                              <button type="button" id="closemodal" class="btn btn-secondary" data-dismiss="modal">{{"close" | translate}}</button>\n\n                              \n\n                            </div>\n\n                          </div>\n\n                        </div>\n\n                      </div>\n\n<!-- modal -->\n\n                </div>\n\n            </div> \n\n            \n\n        </div>       \n\n        <div id="two" style="display: none">\n\n            <div *ngIf="nocart == 0" style="    text-align: center; margin-top: 83px; font-size: 20px; color: #848484;">{{"NOITEMINCART" | translate}}</div>\n\n            <div class="pt-5" *ngIf="nocart==1">\n\n                    <div class="" style="padding-top: 20px; padding-bottom: 10px;height: 100vh;">\n\n                            <div  style="width: 100%" >\n\n                                <div class="cart_bl" *ngFor="let ci of cartitems">\n\n                                    <div class="col-xs-4" style="padding: 0px;width:20%;">\n\n                                        <img src="{{ci.image}}" style="height: 105px; width: 105px;">\n\n                                    </div>\n\n                                    <div class="col-xs-4" style="padding:0 10px;width:40%;">\n\n                                        <div  style="    margin: 0 0 3px;\n\n                                        line-height: 18px;">\n\n                                            <span style="width: 60%; font-weight: bold;">{{ci.name | slice: 0:25}}</span>  \n\n                                        </div>\n\n                                        <!-- <p style=\'margin:0;\'>\n\n                                            <span style="color: red;font-weight: bold;">{{"SAR" | translate}} {{ci.price}}</span>\n\n                                        </p>  -->\n\n                                        <div *ngIf="ci.is_discount == 1">\n\n                                            <p  style="color: red;\n\n                                            font-weight: bold;\n\n                                            margin: 0;\n\n                                            line-height: 20px;\n\n                                            font-size: 14px;">{{"SAR" | translate}} {{ci.discount_price}}</p>\n\n                                            <span  style="text-decoration: line-through;\n\n                                            color: grey;\n\n                                            font-size: 12px;\n\n                                            margin: 0;\n\n                                            line-height: 16px;"> {{"SAR" | translate}} {{ci.price}}  </span>  <span style="font-size:12px;">/{{"METER" | translate}}</span>                        \n\n                                           </div>\n\n                                            <div *ngIf="ci.is_discount == 0">\n\n                                            <p class="text-center" style="color: red;font-weight: bold; line-height: 20px;"> {{"SAR" | translate}} {{ci.price}} </p>/{{"METER" | translate}}\n\n                                           </div>\n\n                                        <p style=\'margin:0;\'>\n\n                                            <span style="font-weight: bold;">{{"COLOR" | translate}} : <span  style="width: 15px;height: 15px;padding: 0px 8px;\n\n                                                margin: 0 5px; " [ngStyle]="{\'background-color\': ci.color}"></span> </span>\n\n                                        </p> \n\n                                    </div>\n\n                                    <div class="col-xs-4" style="width:40%;padding:5px 0 0;">\n\n                                        <a (click)="deleteConfirm(ci.id,ci.color)" ><i  class="fa fa-trash-o" aria-hidden="true" style="font-size:20px; margin-left: 60px;color: black;"></i></a>\n\n                                        <a class="addtocart1">\n\n                                            <span class="minus bg-dark" (click)="minusitem(ci.quen,ci,ci.id,ci.color);"  style="display: inline-block; width:22%;text-align: center;">-</span>\n\n                                            <input type="tel" class="count" name="qty"  placeholder="{{ci.quen | number : \'1.1-2\'}}" style="background-color:#fadf12;width: 50%;text-align: center;border: antiquewhite" readonly>\n\n                                            <span class="plus bg-dark" (click)="plusitem(ci.quen,ci,ci.id,ci.quantity_stock,ci.color);" style="display: inline-block; width:22%;text-align: center;">+</span>\n\n                                                                </a>\n\n                                                                {{"METER" | translate}}\n\n                                    </div>\n\n                    \n\n                                    <!-- <div class="col-xs-12" style=\'padding: 0;\'><hr style="border-color: #c3a1a1 !important;"></div> -->\n\n                                </div>           \n\n                            </div>\n\n                            <!-- coupn code start -->\n\n                            <label class = "coupon item item-input">\n\n                                <ion-input  class="custom_input" [(ngModel)]="couponcode" placeholder=\'{{"EnterCouponCode" | translate}}\' name="coupon" autocorrect="on" clearInput="true" clearOnEdit="true" type="text" inputmode="text"></ion-input>\n\n                                <button *ngIf="couponshow==0" (click)="coupon()"> {{"ApplyCouponCode" | translate}}</button>\n\n                                <div *ngIf="couponshow==1" (click)="removecoupon()" style="color: #ff2626;    margin-left: 15px;float: right;text-align: center; line-height: 27px;font-size: 24px;"><i  class="fa fa-trash-o" aria-hidden="true" style="    font-size: 24px;\n\n                                    color: #d41e1e;"></i> </div>   \n\n                                <button *ngIf="couponshow==1" (click)="coupon()" style="cursor: not-allowed;\n\n                                pointer-events: none; text-shadow: none; box-shadow: none;opacity: 0.75;">{{"AppliedCouponCode" | translate}}</button>\n\n                             </label> \n\n                             <div *ngIf="couponshow==1" style="display: block; margin-bottom: 15px;width: 100%;background-color: #ABEBC6;color: #014c0b;padding: 10px;text-align: center; letter-spacing: 0.5px;" > {{"AppliedCouponCode" | translate}}</div>\n\n                            \n\n                            <!--  coupon code end-->\n\n                    <p style="    border-bottom: 1px solid #0000006b;\n\n                    color: #000;\n\n                    font-weight: 600;\n\n                    font-size: 15px;">{{"COSTSTRUCTURE" | translate}} </p>\n\n                            <div style="width: 100%">\n\n                                <div class="col-xs-6" style="padding: 0px;">\n\n                                    <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"SUBTOTAL" | translate}}</h4>\n\n                                    <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"TAXCART" | translate}}</h4>\n\n                                    <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"ShippingCharges" | translate}}</h4>\n\n                                    <h4 *ngIf="couponshow ==1" class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"COUPONAMOUNT" | translate}}</h4>\n\n                                    <h4 style="    border-top: 1px solid #000;\n\n                                    padding-top: 8px;\n\n                                    font-size: 15px;" class="myorderh4 font-weight-bold">{{"PAYABLEAMOUNT" | translate}}</h4>\n\n                                </div>\n\n                                <div class="col-xs-6" style="padding: 0px;">\n\n                                    <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"SAR" | translate}} : {{totalamount | number:\'1.1-2\'}}</h4>\n\n                                    <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"SAR" | translate}} : {{taxablevalue | number:\'1.1-2\'}} </h4>\n\n                                    <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"SAR" | translate}} : {{shipping_charge | number:\'1.1-2\'}} </h4>\n\n                                    <h4 *ngIf="couponshow ==1" class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"SAR" | translate}} : - {{ct | number:\'1.1-2\'}}</h4>\n\n                                    <h4 style="    border-top: 1px solid #000;\n\n                                    padding-top: 8px;\n\n                                    font-size: 15px;" class="myorderh4 font-weight-bold">{{"SAR" | translate}} : {{subtotalpay | number:\'1.1-2\'}}</h4>\n\n                                </div>\n\n                            </div>        \n\n                        </div> \n\n                <!-- <div class="loginpage">\n\n                    <div class="form-group"  style=\'width: 100%;padding-bottom: 30px;\'>\n\n                        <input class="line-animation" id="changeemail" [(ngModel)]="emailone" type="text" *ngIf="bcngemail==0" readonly style="width:40%;float: left;">\n\n                        <input class="line-animation" id="changeemail" [(ngModel)]="emailtwo" type="text" *ngIf="bcngemail==0" readonly style="width:40%;float: left;">\n\n                        <input class="line-animation" id="changeemail" [(ngModel)]="emailone" type="text" *ngIf="bcngemail==1" style="width:40%;float: left;">\n\n                        <input class="line-animation" id="changeemail" [(ngModel)]="emailtwo" type="text" *ngIf="bcngemail==1" style="width:40%;float: left;">\n\n                        <label style="color:#0CF; font-size:13px; width:19%;float: left;"><a *ngIf="bcngemail==0"  style="color: #10cfcf !important;" (click)="changeinemail()">{{"CHANGE" | translate}}</a><a *ngIf="bcngemail==1"  style="color: #10cfcf !important;" (click)="savechangeinemail()">{{"SAVE" | translate}}</a></label>\n\n                        <div class="line"></div>\n\n                    </div>\n\n                    <div class="form-group" style=\'width: 100%;padding-bottom: 24px;\'>\n\n                        <input  class="line-animation" id="changeaddress" [(ngModel)]="address" *ngIf="bcngaddress==0" readonly type="text" style="width:80%;float: left;">\n\n                        <input  class="line-animation" id="changeaddress" [(ngModel)]="address" *ngIf="bcngaddress==1"  type="text" style="width:80%;float: left;">\n\n                        <label style="color:#0CF; font-size:13px; width:19%;float: left;"><a *ngIf="bcngaddress==0" style="color: #10cfcf !important;" (click)="changeinaddress()">{{"CHANGE" | translate}}</a><a *ngIf="bcngaddress==1"  style="color: #10cfcf !important;" (click)="savechangeinaddress()">{{"SAVE" | translate}}</a></label>\n\n                        <div class="line"></div>\n\n                    </div>\n\n                   \n\n                   \n\n                    <br><br>\n\n                    <div class="row">\n\n                        <a style="float: left;width: 100%">\n\n                            <i class="fa fa-angle-left" aria-hidden="true" style="color:#10cfcf; font-size:20px;float: left;width: 10%"></i><p style="font-size:16px; color:#0CC; margin-left:-10px;float: left;width: 70%">{{"RETURNTOCUSTOMERINFO" | translate}}</p>\n\n                        </a>\n\n                    </div>\n\n                    \n\n                </div> -->\n\n            </div>\n\n        </div>\n\n        \n\n        <div id="three" style="display: none">\n\n            <div class="container-fluid pt-5">\n\n                <!--<div class="loginpage">\n\n                    <div class="form-group"  style=\'width: 100%;padding-bottom: 30px;\'>\n\n                        <input class="line-animation" id="changeemaillast" [(ngModel)]="emailone" type="text" *ngIf="bcngemaillast==0" readonly style="width:40%;float: left;">\n\n                        <input class="line-animation" id="changeemaillast" [(ngModel)]="emailtwo" type="text" *ngIf="bcngemaillast==0" readonly style="width:40%;float: left;">\n\n                        <input class="line-animation" id="changeemaillast" [(ngModel)]="emailone" type="text" *ngIf="bcngemaillast==1" style="width:40%;float: left;">\n\n                        <input class="line-animation" id="changeemaillast" [(ngModel)]="emailtwo" type="text" *ngIf="bcngemaillast==1" style="width:40%;float: left;">\n\n                        <label style="color:#0CF; font-size:13px; width:19%;float: left;"><a *ngIf="bcngemaillast==0"  style="color: #10cfcf !important;" (click)="changeinemaillast()">{{"CHANGE" | translate}}</a><a *ngIf="bcngemaillast==1"  style="color: #10cfcf !important;" (click)="savechangeinemaillast()">{{"SAVE" | translate}}</a></label>\n\n                        <div class="line"></div>\n\n                    </div>\n\n                    <div class="form-group" style=\'width: 100%;padding-bottom: 0px;\'>\n\n                        <input  class="line-animation" id="changeaddresslast" [(ngModel)]="address" *ngIf="bcngaddresslast==0"  readonly type="text" style="width:80%;float: left;">\n\n                        <input  class="line-animation" id="changeaddresslast" [(ngModel)]="address"  *ngIf="bcngaddresslast==1"  type="text" style="width:80%;float: left;">\n\n                        <label style="color:#0CF; font-size:13px; width:19%;float: left;"><a *ngIf="bcngaddresslast==0" style="color: #10cfcf !important;" (click)="changeinaddresslast()">{{"CHANGE" | translate}}</a><a *ngIf="bcngaddresslast==1"  style="color: #10cfcf !important;" (click)="savechangeinaddresslast()">{{"SAVE" | translate}}</a></label>\n\n                        <div class="line"></div>\n\n                    </div>\n\n                   \n\n                </div>-->\n\n                <br><br>\n\n                <h4 style="color:black; font-weight:bold; font-size:12px;     margin-top: -5px;"><a style="font-size: 17px;color: black;"> {{"PAYMENTINFORMATION" | translate}}</a></h4>    \n\n                <form style="font-size:12px;padding: 10px;">\n\n                    <input type="radio" name="gender" value="male" checked> <span style="font-size:15px;margin-left: 10px;"> {{"PAYMENTONDELIVERY" | translate}}</span><br>\n\n                    <div (click)="underprocess()"><input type="radio" name="gender" value="visa"  disabled=\'disabled\'> <span style="font-size:15px;margin-left: 10px;" > {{"VISA" | translate}} <img src="assets/imgs/visa.png" style="    margin-top: 0px;     width: 20%;"> </span> </div><br>\n\n                    <div  style="margin-top: -20px;"(click)="underprocess()"><input type="radio" name="gender" value="sadad" disabled=\'disabled\'><span style="font-size:15px;margin-left: 10px;" >  {{"SADAD" | translate}} <img src="assets/imgs/sadad.png" style="    margin-top: 0px;     width: 17%;"> </span></div>\n\n                </form> \n\n                <h4 style="color:black; font-weight:bold; font-size:14px; margin:15px; margin-left:0px; "><a style="font-size: 17px;color: black;">{{"SHIPPINGMETHOD" | translate}}	</a></h4>    \n\n                <form style="font-size:12px;padding: 10px;">\n\n                    <input type="radio" name="gender" value="male" checked><span style="font-size:15px;margin-left: 10px;"> {{"SAMEASSHIPPINGADD" | translate}}</span> <br>\n\n                    <div (click)="gotoadresssection()"><input type="radio" name="gender" value="female" disabled=\'disabled\'><span style="font-size:15px;margin-left: 10px;">  {{"USEADIFFRENTADD" | translate}} </span></div><br>\n\n                </form>\n\n                <div class="row" style="position:fixed; bottom:0px; width:105%;    margin-left: -34px;">\n\n                    \n\n                </div>\n\n            </div>\n\n            \n\n        </div>\n\n    </div>\n\n</ion-content>\n\n<ion-footer style="padding: 0;">\n\n    <div id="onefooter" class="container-fluid" style="padding-right: 0px;padding-left: 0px;">\n\n        <div class="row" >\n\n            <div class="col" style="padding:0px; margin:0px;">\n\n                <button (click)="gotoback()" class="btn btn-primary btn-block Update-Cart-btn" style="height:50px;">{{"RETURNTOCART" | translate}}</button> \n\n            </div>\n\n            <div  class="col" style="padding:0px; margin:0px;">\n\n              <!--position: sticky;bottom: 0;   <button  class="btn btn-primary btn-block CheckOut-btn" style="height:50px;" (click)="savetoinformationto()">{{"SAMEASSHIPPINGADD" | translate}}</button>--> \n\n                <button  class="btn btn-primary btn-block CheckOut-btn" style="height:50px;" (click)="savetoinformationto()">{{"PAYMENT" | translate}}</button> \n\n            </div>\n\n        </div>\n\n    </div>\n\n   <div id="twofooter" class="row" style="width:100%;display: none;padding: 0;">\n\n    <div class="row" >\n\n    <div *ngIf="nocart== 1" class="col" style="padding:0px; margin:0px;">\n\n        <button  class="btn btn-primary btn-block CheckOut-btn" style="height:50px;" (click)="goshippingmethod()">{{"Countinuetopayment" | translate}} </button> \n\n    </div>\n\n    <div *ngIf="nocart== 0" class="col" style="padding:0px; margin:0px;">\n\n        <button  class="btn btn-primary btn-block CheckOut-btn" style="height:50px;" (click)="gohome()">{{"ADDMOREITEM" | translate}}  </button> \n\n    </div>\n\n</div>\n\n        <!-- {{"CONTINUESHIPPINGMETHOD" | translate}} -->\n\n    </div>\n\n    <div id="threefooter" class="container-fluid" style="padding-right: 0px;padding-left: 0px;width: 100%;display: none">\n\n        <div class="row" >\n\n            <div class="col" style="padding:0px; margin:0px;">\n\n               <button  class="btn btn-primary btn-block CheckOut-btn" style="height:50px;" (click)="completeorder()">{{"COMPLETEORDER" | translate}}</button> \n\n            </div>\n\n        </div>\n\n    </div>\n\n</ion-footer>\n\n'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/payment/payment.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */], __WEBPACK_IMPORTED_MODULE_2__providers_commonservice_commonservice__["a" /* CommonserviceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* ToastController */]])
    ], PaymentPage);
    return PaymentPage;
}());

//# sourceMappingURL=payment.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return myCartPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_commonservice_commonservice__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__addtocart_addToCart__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__cartitem_cartitem__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// import { AddtocartPage } from '../addtocart/addtocart';



var myCartPage = /** @class */ (function () {
    function myCartPage(platform, navCtrl, events, commonservice, alertCtrl) {
        var _this = this;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.events = events;
        this.commonservice = commonservice;
        this.alertCtrl = alertCtrl;
        this.error = "";
        this.image = "";
        this.name = "";
        this.protype = "";
        this.promaterial = "";
        this.color = "";
        this.proquenty = "";
        this.price = "";
        this.categorydetils = "";
        this.sku = "";
        this.jan = '';
        this.totalamount = 0;
        this.discount_price = "0";
        this.is_discount = 0;
        this.quantity_stock = "0";
        events.publish('allpagecommon');
        this.loadpage();
        this.platform.registerBackButtonAction(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__addtocart_addToCart__["a" /* AddtocartPage */]);
        });
    }
    myCartPage.prototype.loadpage = function () {
        var i = 0;
        this.productitem = JSON.parse(localStorage.getItem("cart" + "11"));
        if (this.productitem != null) {
            console.log('leng ' + this.productitem.length);
            var length_1 = this.productitem.length;
            length_1 = length_1 - 1;
            this.data = this.productitem[length_1];
            console.log('test data ' + JSON.stringify(this.data));
            // for (let obj of this.productitem) 
            // {
            //     if(obj[0]==localStorage.getItem("details_pdid"))
            //     {                   
            //         this.data=obj;                    
            //     }
            //     i++;
            // }
            //    console.log(this.data);
            this.productid = this.data.pid;
            this.name = this.data.name;
            this.price = this.data.price;
            this.image = this.data.image;
            this.proquenty = this.data.qty;
            // this.promaterial=this.data.;
            // this.protype=this.data[6];
            this.discount_price = this.data.discount_price;
            this.is_discount = this.data.isDiscount;
            this.quantity_stock = this.data.qty;
            this.color = this.data.color;
            this.sku = "BRI43535";
            this.jan = "78";
            this.description();
            if (this.is_discount == 1) {
                this.totalamount = parseFloat(this.proquenty) * parseFloat(this.discount_price);
            }
            else {
                this.totalamount = parseFloat(this.proquenty) * parseFloat(this.price);
            }
        }
        else {
            this.commonservice.erroralert("Product not add in your cart");
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
        }
        // this.totalamount=parseFloat(this.proquenty)* parseFloat(this.price);
        //his.data=localStorage.getItem("cart"+"11");
    };
    myCartPage.prototype.description = function () {
        var _this = this;
        this.servicedata = "productDetail/" + localStorage.getItem("details_pdid");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            _this.description = _this.data.data.description;
            $("#categorydetils").append(_this.data.data.description);
        });
    };
    myCartPage.prototype.gotoaddtocart = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__addtocart_addToCart__["a" /* AddtocartPage */]);
    };
    myCartPage.prototype.gotomycart = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__cartitem_cartitem__["a" /* CartitemPage */]);
    };
    myCartPage.prototype.gotoback = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__addtocart_addToCart__["a" /* AddtocartPage */]);
    };
    myCartPage.prototype.deletecrtitem = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm',
            message: 'Do you want to Delete this product in your cart?',
            buttons: [{
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Ok',
                    handler: function () {
                        _this.deleteitem(localStorage.getItem("details_pdid"));
                    }
                }
            ]
        });
        alert.present();
        //
    };
    myCartPage.prototype.deleteitem = function (id) {
        var a = [];
        var b = JSON.parse(localStorage.getItem("cart" + "11"));
        for (var _i = 0, b_1 = b; _i < b_1.length; _i++) {
            var val = b_1[_i];
            if (id != val[0]) {
                a.push(val);
            }
        }
        ;
        localStorage.setItem("cart" + "11", JSON.stringify(a));
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__cartitem_cartitem__["a" /* CartitemPage */]);
    };
    myCartPage.prototype.addwishlist = function () {
        var _this = this;
        this.commonservice.waitloadershow();
        this.servicedata = "add_wishlist?product_id=" + localStorage.getItem("details_pdid") + "&userid=" + localStorage.getItem("loginuserid") + "&lang=" + localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            _this.commonservice.waitloaderhide();
            if (_this.data.status = "true") {
                _this.commonservice.successalert("Product add in your wishlist sucessfully");
            }
            else {
                _this.commonservice.erroralert("Something went wrong");
            }
        });
    };
    myCartPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-myCart',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/mycart/myCart.html"*/'\n\n<ion-header>\n\n        <div class="headerbg_wh">\n\n    <div class="container" >\n\n            <!-- style=" padding-top: 20px; padding-bottom: 15px;" -->\n\n        <span (click)="gotoback()"  style="font-size:20px; cursor:pointer; " ><i class="fa fa-arrow-left" aria-hidden="true" style="font-size: 20px;  padding-left: 10px;"></i> &nbsp; &nbsp; &nbsp;<small style="font-weight: 500;">{{"MYCART" | translate}} </small> </span>\n\n        <span (click)="deletecrtitem()"  style="font-size:20px;cursor:pointer;float: right;" ><i class="fa fa-trash-o"   aria-hidden="true" style="font-size: 24px;color: #e4af33;"></i></span>\n\n    </div>\n\n    </div>\n\n</ion-header>\n\n<ion-content style="background-color: rgb(245, 244, 244);">\n\n    <div class="container-fluid" style="margin-top: 23px;">\n\n            <div class="media" >\n\n                <div class="media-body" style="text-align: -webkit-center;">\n\n                    <img class="" src="{{image}}" style="height: 204px;    width: 100%;padding-top: 6px;">\n\n                </div>\n\n            </div>\n\n            <div class="row" style="padding: 17px;text-align: center;margin-top: -9px;"> \n\n                <div style="width: 100%;margin-top: -9px;"><h2 style="text-align: center;font-size: 18px;font-weight: 800" >{{name}}</h2></div>\n\n                <div style="width: 88%;margin-top: -9px;"><h3 style="text-align: center;font-size: 16px;    padding-left: 30px;">{{protype}}</h3></div>\n\n            </div>\n\n            <div class="form-group form-inline" style="width: 100%;margin-bottom: 10px;">\n\n                <label style="width:40px; font-size:12px; font-weight:400; ">{{"COLOR" | translate}}</label>\n\n                <label   [ngStyle]="{\'background-color\': color}" style="margin-left:8px;margin-bottom:0px;font-size: 12px; font-weight: 400;width:15px;height: 15px;"></label>\n\n                   \n\n                <!--<label style="width:20%; float: left; margin-left:50px; font-size:12px; font-weight:400;">{{"MATERIAL" | translate}}</label>\n\n                <label style="width: 16%;float: left; margin-left: 8px;font-size: 12px; font-weight: 400;border: 1px solid;border-radius: 12px;padding-left: 8px;">{{promaterial}}</label>-->\n\n            </div>\n\n       <br>\n\n        <h4 class="myorderh4 font-weight-bold" style="margin-top: -9px;">{{"DESCRIPTION" | translate}} </h4>\n\n        <p id="categorydetils" style="margin-top: -9px;"> </p>\n\n        <hr style="border-color:#CCC;">\n\n        <h4 class="myorderh4 font-weight-bold">{{"SUBTOTAL" | translate}}: \n\n            <span style="font-weight:400;">{{"SAR" | translate}} : {{totalamount}}</span>        \n\n        </h4>\n\n        <h4 class="myorderh4 font-weight-bold">{{"SHIPPING" | translate}} :\n\n            <span style="font-weight:400;">{{"SHIPPINGCONTENT" | translate}} </span>\n\n        </h4>\n\n        <h4 class="myorderh4 font-weight-bold">{{"TOTAL" | translate}}: {{"SAR" | translate}} : {{totalamount}}      \n\n        </h4>\n\n    </div>\n\n    <br>\n\n    \n\n    <br>    \n\n    \n\n</ion-content>\n\n<ion-footer style="padding: 0;">    \n\n    <div style="background-color:#ffffff;    z-index: 1;    width: 100%;">\n\n        <div style="width: 100%">\n\n            <div class="col" style="width: 50%;float: left;padding:0px;">\n\n                 <button type="button" class="btn btn-primary btn-block Update-Cart-btn" style="height:50px;" (click)="gotoaddtocart()">{{"UPDATECART" | translate}}</button>\n\n            </div>\n\n            <div class="col" style="width: 50%;float: left;padding:0px;">\n\n                <button type="button" class="btn btn-primary btn-block CheckOut-btn" style="height:50px" (click)="gotomycart()">{{"CHECKOUT" | translate}}</button> \n\n            </div>\n\n        </div>\n\n    </div>\n\n</ion-footer>\n\n'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/mycart/myCart.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */], __WEBPACK_IMPORTED_MODULE_2__providers_commonservice_commonservice__["a" /* CommonserviceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], myCartPage);
    return myCartPage;
}());

//# sourceMappingURL=myCart.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signinOtp_signinOtp__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_commonservice_commonservice__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SignupPage = /** @class */ (function () {
    function SignupPage(platform, navCtrl, commonservice, events) {
        var _this = this;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.commonservice = commonservice;
        this.events = events;
        this.error = "";
        this.mobilenumber = "";
        this.numbervalue = '';
        this.countrycode = "966";
        this.loadpage();
        this.platform.registerBackButtonAction(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__["a" /* DashboardPage */]);
        });
        document.addEventListener('paste', function (e) {
            console.log(e.clipboardData.getData('Text'));
            _this.mg = e.clipboardData.getData('Text');
            $('#telephone').val(_this.mg).trigger('.telephone');
            e.preventDefault();
            e.stopPropagation();
        });
    }
    SignupPage.prototype.loadpage = function () {
        var _this = this;
        this.commonservice.waitloadershow();
        this.servicedata = "country?lang=" + localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.commonservice.waitloaderhide();
            _this.data = res;
            _this.countrylist = _this.data.data;
            // this.countrycode="191";
            _this.countrycode = _this.data.data[2].phonecode;
            console.log(_this.data);
            console.log(_this.countrycode);
        });
    };
    SignupPage.prototype.login = function () {
        var _this = this;
        if (this.mobilenumber != null) {
            var number = this.mobilenumber;
            var numbertostr = number.toString();
            var numberlength = numbertostr.length;
            console.log(numberlength);
            if (Number(numberlength) <= 9) {
                this.numbervalue = '1';
            }
            else if (Number(numberlength) >= 13) {
                this.numbervalue = '1';
            }
            else {
                this.numbervalue = '0';
            }
        }
        console.log(this.numbervalue);
        if (!this.mobilenumber) {
            this.error = "Mobile number is required";
        }
        if (this.error == "") {
            this.commonservice.waitloadershow();
            this.servicedata = "login?mobile=" + this.mobilenumber + "&countrycode=" + this.countrycode + "&lang=" + localStorage.getItem("applanguage");
            this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
                _this.commonservice.waitloaderhide();
                _this.data = res;
                console.log(_this.data);
                if (_this.data.status = "true") {
                    localStorage.setItem("loginotp", _this.data.otp);
                    localStorage.setItem("loginuserid", _this.data.userid);
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__signinOtp_signinOtp__["a" /* SigninOtpPage */]);
                }
                else {
                    _this.commonservice.erroralert("Something went wrong");
                }
            });
        }
        else {
            this.commonservice.erroralert(this.error);
        }
    };
    SignupPage.prototype.gohome = function () {
        this.events.publish('allpagecommon');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__["a" /* DashboardPage */]);
    };
    SignupPage.prototype.checkFocus = function (e) {
        $("#telephone").on("click", function (e) {
            $(".login_footer").addClass("active");
            console.log("active btnb");
            e.stopPropagation();
        });
        $(document).on("click", function (e) {
            e.stopPropagation();
            if ($(e.target).is(".ogin_footer") === false) {
                console.log("remove");
                $(".login_footer").removeClass("active");
            }
        });
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-signup',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/signup/signup.html"*/'<ion-header>\n        <div class="headerbg_wh">\n    <div class="container" style="width: 100%;">\n        <button style="text-align: left; width: 20%;float: left;background-color: transparent;" icon-only >\n            <i (click)="gohome()" class="fa fa-arrow-left" aria-hidden="true" style="font-size: 20px;"></i>\n        </button>\n        <ion-title style="width: 80%;font-size: 20px; float: left;">{{"SIGNUP" | translate}}</ion-title>\n    </div>\n    </div>\n</ion-header>\n<ion-content class="signuppage" style="background-color: rgb(245, 244, 244);">    \n    <div class="container-fluid pt-5 align_content" style="margin-top: 61px;">\n        <div class="loginpage " style="padding:0px 15px;padding-top: 0px;">\n             <div class="form-group form-inline" style="width: 100%">\n                <label style="vertical-align: middle;line-height: 30px;width:12%;margin:0;float: left"><img src="assets/imgs/icon-images/flag.png" style="color: #292727;height:24px;"></label>\n                <!--<input name="userCcountry" class="line-animation" placeholder="Saudi Arabia" type="text"  style="color:black; width:90%;float: left" >-->\n            \n                <ion-select [(ngModel)]="countrycode" class="line-animation" style="border-bottom: 2px solid #a9a9a9;max-width: 100%;padding: 0 10px 10px;color:black; width:80%;float: left;font-size: 17px;" >\n                  \n                    <ion-option *ngFor="let co of countrylist" value="{{co.phonecode}}">{{co.name}}</ion-option>\n                </ion-select>\n                <div class="line"></div>\n            </div>\n           \n            <br><br>\n            <div class="telephone_inner form-group form-inline"  (click)="checkFocus()"  style="width: 100%;margin-top: 25px;display: block;overflow: hidden;">\n                <p style="position: absolute;font-size: 18px;  margin-top: 2px; margin-left: 40px;">+ {{countrycode}}</p>\n                <label style="width:12%;float: left;font-size: 17px;"><img src="assets/imgs/icon-images/call-answer.png" style="    margin-top: 0px; height:24px;"></label>\n                <ion-input  pattern="[0-9]*" decimal="true" allow-multiple-decimals="true" name="userCcountry" id="telephone" class="line-animation telephone" placeholder="" type="number"  style="color:black; width:78%;float: left;font-size: 17px; padding-left: 59px;" [(ngModel)]="mobilenumber" ></ion-input>\n                <!-- (ngModelChange) ="modhlcg()" -->\n                <div class="line"></div>\n            </div>\n            <br><br>\n            <div class="row" style="width: 95%;padding-top: 30px; margin: 0 auto;">\n                <div class="col-md-12 col-lg-12" style="float:left;width:100%">\n                    <button class="btn btn-primary  btn-block register" style="border-radius: 21px;" (click)="login()" >{{"REGISTER" | translate}}</button>\n                </div>\n            </div>\n            <div class="row" style="width: 95%;margin: 35px auto 0;">\n                <div class="col-md-12 col-lg-12" style="float: left;width:100%">\n                    <button class="btn btn-primary  btn-block register" style="border-radius: 21px;background-color: white !important;color: black" (click)="gohome()">{{"SKIP" | translate}}</button>\n                </div>\n            </div>\n\n\n            <br>\n           <!--<h3 style="color:#666; font-size:14px; text-align:center;color: #292727;">Already Registerd ? Login Here </h3>-->\n        </div>\n    </div>\n\n</ion-content> \n<ion-footer style="padding: 10px 0;">\n        <div class="" style="width: 100%;padding: 0 15px; ">\n                <div style="float: left;width: 40%;">\n                    <span style="font-size:16px;color: #292727;">{{"sharewith" | translate}}</span>\n                </div>\n                <div style="float: left;width: 50% ">\n                  <a href="https://www.facebook.com/La-Estrella-online-2061855837269923/?modal=admin_todo_tour" ><img style="margin-left: 0px;width: 25px;" alt="facebook" src="assets/imgs/facebooklogo.png"></a>\n                  <a href="https://twitter.com/laestrella_on" ><img style="margin-left: 20px; width: 25px;" alt="twitter"  src="assets/imgs/Twitter-icon.png"></a>\n                  <a href="https://www.instagram.com/laestrella.online/"><img style="margin-left: 20px;width: 25px;" alt="instagram"  src="assets/imgs/instagram.png"></a>\n                </div>\n            </div>\n</ion-footer>\n<!-- <div class="login_footer row m-0" style="background-color: #f5f4f4;position: absolute; bottom: 0; width:100%;padding:10px 15px;z-index:999;margin: 0;">\n      \n        <div class="col" style="width: 100%">\n            <div style="float: left;width: 40%;">\n                <span style="font-size:16px;color: #292727;">{{"LOGIN_WITH" | translate}}</span>\n            </div>\n            <div style="float: left;width: 50% ">\n                <img style="margin-left: 0px;width: 25px;" src="assets/imgs/facebooklogo.png">\n                <img style="margin-left: 20px; width: 25px;"   src="assets/imgs/Twitter-icon.png">\n                <img style="margin-left: 20px;width: 25px;"  src="assets/imgs/google-logo.png">\n            </div>\n        </div>\n    </div> -->\n'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/signup/signup.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__providers_commonservice_commonservice__["a" /* CommonserviceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 62:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__addtocart_addToCart__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_commonservice_commonservice__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__search_search__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__myAccount_myAccount__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__cartitem_cartitem__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__home_home__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var CategoryDetailsPage = /** @class */ (function () {
    function CategoryDetailsPage(platform, navCtrl, navParams, commonservice, events) {
        var _this = this;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.commonservice = commonservice;
        this.events = events;
        this.error = "";
        this.catdetailslist = [];
        events.publish('allpagecommon');
        this.loadpage();
        console.log(localStorage.getItem("cat_name"));
        this.platform.registerBackButtonAction(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__home_home__["a" /* HomePage */]);
        });
    }
    CategoryDetailsPage.prototype.ionViewDidEnter = function () {
        var id = this.navParams.get("id") || null;
        if (id != null && id != undefined) {
            document.getElementById(id).scrollIntoView();
            this.navParams.data.id = null;
        }
    };
    CategoryDetailsPage.prototype.gotoback = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__home_home__["a" /* HomePage */]);
    };
    CategoryDetailsPage.prototype.gocatgorydetails = function (id) {
        localStorage.setItem("Addtocartback", 'CategoryDetailsPage');
        localStorage.setItem("details_pdid", id);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__addtocart_addToCart__["a" /* AddtocartPage */]);
    };
    /*loadpage()
    {
        this.commonservice.waitloadershow();
        this.servicedata="productList/"+localStorage.getItem("cat_id");
        this.commonservice.serverdataget(this.servicedata).subscribe(
            res => {
                this.commonservice.waitloaderhide();
                this.data=res;
                console.log(res);
                this.catdetailslist=this.data.data;
            }
        )
    }*/
    CategoryDetailsPage.prototype.loadpage = function () {
        var b = JSON.parse(localStorage.getItem("allcategoryproductlist"));
        console.log("allcategoryproductlist" + b);
        if (localStorage.getItem("applanguage") == "er") {
            var setLang = 2;
        }
        else {
            var setLang = 1;
        }
        if (b != null) {
            var i = 0;
            for (var _i = 0, b_1 = b; _i < b_1.length; _i++) {
                var bp = b_1[_i];
                if (bp.category_id == localStorage.getItem("cat_id")) {
                    if (bp.language_id == setLang) {
                        this.catdetailslist.push({ "product_id": bp.product_id, "image": bp.image, "name": bp.name, "price": bp.price, "model": bp.model, "sku": bp.sku, "jan": bp.jan, "language_id": bp.language_id, "colorImages": bp.mycolorImg, "isColorSelected": false });
                    }
                }
            }
        }
        this.catname = localStorage.getItem("cat_name");
    };
    CategoryDetailsPage.prototype.gotohome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__["a" /* DashboardPage */]);
    };
    CategoryDetailsPage.prototype.gotosearch = function () {
        localStorage.setItem('lastpageserch', 'CategoryDetailsPage');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__search_search__["a" /* SearchPage */]);
    };
    CategoryDetailsPage.prototype.gotocat = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__home_home__["a" /* HomePage */]);
    };
    CategoryDetailsPage.prototype.gotobag = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__cartitem_cartitem__["a" /* CartitemPage */]);
    };
    CategoryDetailsPage.prototype.gotoaccount = function () {
        // this.navCtrl.setRoot(HomePage);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__myAccount_myAccount__["a" /* MyAccountPage */]);
    };
    CategoryDetailsPage.prototype.onColorSelect = function (col, prod_id) {
        this.checkColor = col;
        for (var i = 0; i < this.catdetailslist.length; i++) {
            if (prod_id == this.catdetailslist[i].product_id) {
                for (var j = 0; j < this.catdetailslist[i].colorImages.length; j++) {
                    if (col == this.catdetailslist[i].colorImages[j].color) {
                        this.checkedId = prod_id;
                        this.catdetailslist[i].image = "";
                        this.catdetailslist[i].image = this.catdetailslist[i].colorImages[j].img;
                    }
                }
            }
        }
    };
    CategoryDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-categoryDetails',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/categorydetails/categoryDetails.html"*/'<ion-header >\n\n        <div class="headerbg_wh">\n\n    <div class="container">\n\n            <!-- style=" padding-top: 20px; padding-bottom: 20px;" -->\n\n        <span (click)="gotoback()" style="font-size:20px; cursor:pointer; " ><i class="fa fa-arrow-left"  aria-hidden="true" style="font-size: 20px;  padding-left: 10px;"></i> &nbsp; &nbsp; &nbsp;<small class="cttitle" style="font-weight: 500;font-size: 20px;">{{catname}}</small> </span>\n\n        <span (click)="gotosearch()" style="font-size:20px;cursor:pointer;float: right;" ><i class="fa fa-search"  aria-hidden="true" style="font-size: 20px;  padding-left: 10px;"></i></span>\n\n    </div> \n\n    </div>\n\n</ion-header>\n\n\n\n<ion-content padding style="background-color: rgb(245, 244, 244);">\n\n\n\n    <div class="container-fluid" style="padding:0px 3px; display: -webkit-flex;display: flex;-webkit-flex-wrap: wrap;flex-wrap: wrap; ">\n\n        \n\n            <div class="col-xs-6 col-sm-6 col-md-6" *ngFor="let cdl of catdetailslist" style="flex-basis: 49%;-webkit-flex-basis:49%; width:49%;padding:0px 3px;margin-bottom: 15px; ">\n\n               \n\n                <div id="{{cdl.product_id}}"  class="card" style="padding:5px;background-color:white">\n\n                    <a> <img class="card-img-top image" (click)="gocatgorydetails(cdl.product_id)" src="{{cdl.image}}" style="height: 22vh;" alt="Photo of sunset"></a>\n\n                    <p class="text-center"><strong >{{cdl.name | slice: 0:15 | translate}}</strong></p>\n\n                    <div class="color_boxes custom_center">\n\n                    <div class="colorBox ion-text-center" [ngClass]="{\'active\': checkColor == clr.color && checkedId == cdl.product_id}" *ngFor="let clr of cdl.colorImages" [ngStyle]="{\'background-color\': clr.color}" (click)="onColorSelect(clr.color,cdl.product_id)">\n\n                    </div>\n\n                    </div>\n\n                    <hr style="border-color: #c3bfbf !important;">\n\n                    <p class="text-center font-weight-bold productname" style="margin-top: -14px;">{{cdl.model | slice: 0:15}}</p>\n\n                    <div col class="ion-text-center" (click)="gocatgorydetails(cdl.product_id)">\n\n                        <a class="btn ordernow"> {{"ADDTOCRT" | translate}}</a>\n\n                    </div>\n\n                </div>\n\n           \n\n            </div>\n\n        \n\n     </div>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n        <div style="display:flex;background-color: #f0f0f0;padding-top:8px;padding-bottom:8px;">\n\n           <div (click)="gotohome()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname"  style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/home.png">\n\n                   <br>\n\n                   {{"HOME" | translate}} \n\n               </a>\n\n           </div> \n\n           <div (click)="gotosearch()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/search.png">\n\n                   <br>\n\n                   {{"SEARCH" | translate}}</a>\n\n           </div>\n\n           <div (click)="gotocat()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;">  \n\n                   <img  style="    height: 18px;" src="assets/imgs/icon-images/list.png">\n\n                   <br>\n\n                   {{"CATEGORY" | translate}} </a>\n\n           </div>\n\n           <div (click)="gotobag()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="height: 18px;" src="assets/imgs/icon-images/shopping-bag.png">\n\n                   <br>\n\n                   {{"BAG" | translate}}\n\n               </a>\n\n           </div>\n\n           <div (click)="gotoaccount()"  style=\'flex:1;text-align:center\'>\n\n               <a   class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="height: 18px;"  src="assets/imgs/icon-images/round-account-button-with-user-inside.png">\n\n                   <br>\n\n                   {{"MYACCOUNT" | translate}} </a>\n\n           </div>\n\n        </div>\n\n</ion-footer>\n\n'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/categorydetails/categoryDetails.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_commonservice_commonservice__["a" /* CommonserviceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */]])
    ], CategoryDetailsPage);
    return CategoryDetailsPage;
}());

//# sourceMappingURL=categoryDetails.js.map

/***/ }),

/***/ 73:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyOrderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__orderDetails_orderDetails__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__search_search__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__myAccount_myAccount__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_commonservice_commonservice__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__cartitem_cartitem__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__home_home__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// import { OrderDetailsPage } from '../orderdetails/orderdetails';



// import { myCartPage } from '../mycart/mycart';



var MyOrderPage = /** @class */ (function () {
    function MyOrderPage(platform, navCtrl, events, commonservice) {
        var _this = this;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.events = events;
        this.commonservice = commonservice;
        this.error = "";
        this.orderlist = [];
        this.norecourd = '1';
        events.publish('allpagecommon');
        this.loadpage();
        this.platform.registerBackButtonAction(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__["a" /* DashboardPage */]);
        });
    }
    MyOrderPage.prototype.loadpage = function () {
        var _this = this;
        this.commonservice.waitloadershow();
        this.servicedata = "order_history?user_id=" + localStorage.getItem("loginuserid") + "&lang=" + localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.commonservice.waitloaderhide();
            _this.data = res;
            console.log(_this.data);
            console.log(_this.data.data);
            console.log(_this.data.order_status);
            if (_this.data.status != false) {
                console.log('1');
                _this.norecourd = '0';
                _this.orderlist = _this.data.data;
            }
            else {
                console.log('2');
                _this.orderlist = [];
            }
        });
    };
    MyOrderPage.prototype.goOrderDetails = function (id) {
        localStorage.setItem("orderid", id);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__orderDetails_orderDetails__["a" /* OrderDetailsPage */]);
    };
    MyOrderPage.prototype.gotoback = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__["a" /* DashboardPage */]);
    };
    MyOrderPage.prototype.gotohome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__["a" /* DashboardPage */]);
    };
    MyOrderPage.prototype.gotosearch = function () {
        localStorage.setItem('lastpageserch', 'myorderPage');
        console.log(localStorage.getItem('lastpageserch'));
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__search_search__["a" /* SearchPage */]);
    };
    MyOrderPage.prototype.gotocat = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__home_home__["a" /* HomePage */]);
    };
    MyOrderPage.prototype.gotobag = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__cartitem_cartitem__["a" /* CartitemPage */]);
    };
    MyOrderPage.prototype.gotoaccount = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__myAccount_myAccount__["a" /* MyAccountPage */]);
    };
    MyOrderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-myOrder',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/myorder/myOrder.html"*/'<ion-header>\n\n        <div class="headerbg_wh">\n\n    <div class="container" style="display: flex;justify-content: center;align-items: center;">\n\n        <span style="font-size:20px; cursor:pointer;flex: 1;"  ><i class="fa fa-arrow-left" (click)="gotoback()" aria-hidden="true" style="    padding: 0 10px;font-size: 20px;"></i> &nbsp; {{"MYORDER" | translate}}</span>\n\n        <!--style=" padding-top: 20px; padding-bottom: 20px;" <span style="font-size: 24px;cursor:pointer;float: right;" ><i class="fa fa-search" (click)="gotosearch()" aria-hidden="true" style="font-size: 16px;"></i></span> -->\n\n        \n\n                <span (click)="gotosearch()" class="myorder_search" style="flex: 1;"> \n\n                    <div class="form-group" style="margin: 0;">\n\n                        <div class="input-group">\n\n                           \n\n                            <input id="text" type="text" class="form-control Ccontrol" name="text"\n\n                                placeholder="{{\'SEARCHT\' | translate}}" style="    border-radius: 5px 0 0 5px !important;  border-left: 1px solid #b8b8b8 !important;height: 30px;" required>\n\n                                <span class="input-group-addon Caddon"style="      border-left: 0 !important;\n\n                                border-radius: 0 5px 5px 0 !important;border-right: 1px solid #b8b8b8 !important;" ><img class="searchimg" src="assets/images/search.png"></span>\n\n                        </div>\n\n                    </div>\n\n                </span> \n\n           \n\n    </div> \n\n    </div>\n\n</ion-header>\n\n<ion-content style="background-color: rgb(245, 244, 244);">\n\n    \n\n    <div style="padding-left: 10px;padding-right: 10px;" *ngIf="orderlist">\n\n        <div   *ngFor="let ol of orderlist"  style="margin-top: 20px;box-sizing: border-box;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);border-radius: 12px;padding: 5px;    padding-bottom: 14px;">\n\n            <div class="container-fluid" style="padding-right: 0px;margin-top: 10px;">\n\n                <div class="card bg-light">\n\n                    <div class="card-body text-center deliverycss">    \n\n                        <div class="notificationh3" style="    font-size: 18px;\n\n                        border-bottom: 1px solid #ae9435;\n\n                        padding: 0 0 10px;">\n\n                    <span>{{"ORDERSTATUS" | translate}}</span>\n\n                    <span style=" color: green;float: right;" *ngIf="ol.order_status==\'Complete\' "> {{"COMPLETEORDER" | translate}}\n\n                        <!-- <img src="assets/imgs/comlete.png"> -->\n\n                    </span>\n\n                    <span style=" color: orange;float: right;" *ngIf="ol.order_status==\'Pending\' "> {{"ORDERPROCESSING" | translate}}</span>\n\n                    <span style=" color: red;float: right;" *ngIf="ol.order_status==\'Canceled\' "> {{"CANCELORDER" | translate}}</span>\n\n                    </div>\n\n                        <h4 class="myorderh4">{{"TRANSACTIONID" | translate}} : {{ol.order_id}}</h4> \n\n                        <h4 class="myorderh4">{{"PLACEORDER" | translate}} : {{ol.order_date}}</h4>\n\n                        <!-- <h4 class="myorderh4" [ngClass]="{\'ordercomplte\' : \'{{ol.order_status}}\' == Complete}"  >{{"ORDERprocessing" | translate}} : {{ol.order_status}}</h4> -->\n\n                        <!--<h4 class="myorderh4">{{ol.product_quantity}}/ Products</h4> -->\n\n                    </div>\n\n                </div> \n\n                <div class="container-fluid" >\n\n                    <div class="row">\n\n                        <div class="col-8">\n\n                            <h3 class="notificationh3" style="margin-bottom:10px;font-size: 16px">{{"SHIPMENT" | translate}} : {{ol.product_quantity}} {{"PRODUCTS" | translate}}</h3> </div>\n\n                        <div class="col" style="text-align:right;"><a style="font-size:16px; float: right;width: 66%;font-weight: 400;color: #337ab7 !important;" (click)="goOrderDetails(ol.order_id)" >\n\n                            {{"VIEWDETAILS" | translate}}</a></div>         \n\n                    </div>\n\n                    <!-- <div class="row" style="width: 100%;">           \n\n                        <div *ngFor="let pd of ol.product" class="col-sm-6" style="width: 49%;padding-left: 5px;padding-right: 0px;border-radius: 7px;">\n\n                            <img  style="padding: 10px;" src="{{pd.image}}" alt="Photo of sunset">\n\n                        </div>\n\n                    </div>\n\n                    <br> -->\n\n                    <!-- <div >\n\n                        <div >\n\n                            <button class="btn btn-primary  btn-block register" style="border-radius: 34px;width: 90%" (click)="goOrderDetails(ol.order_id)" >{{"REORDER" | translate}}</button>\n\n                        </div>\n\n                    </div>  -->\n\n                </div>\n\n            </div>\n\n        </div>\n\n    </div>\n\n    <div *ngIf="norecourd==\'1\'"  style="margin-top: 136px;    margin-top: 136px;   position: absolute;  top: 0px;  font-size: 36px;   left: 61px;  color: #c3cccc;" >{{"NORECORDFOUND" | translate}}</div>\n\n    \n\n</ion-content>\n\n<!-- <ion-footer>\n\n    <ion-toolbar>\n\n            <div (click)="gotohome()" style="text-align:center; width:20%; float:left;">\n\n                <a class="iconname"  style="color:black;font-size: 12px;"> \n\n                    <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/home.png">\n\n                    <br>\n\n                    {{"HOME" | translate}}  \n\n                </a>\n\n            </div> \n\n            <div (click)="gotosearch()"  style="text-align:center; width:20%; float:left;">\n\n                <a class="iconname" style="color:black;font-size: 12px;"> \n\n                    <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/search.png">\n\n                    <br>\n\n                    {{"SEARCH" | translate}}</a>\n\n            </div>\n\n            <div (click)="gotocat()" style="text-align:center; width:20%; float:left;">\n\n                <a class="iconname"  style="color:black;font-size: 12px;">  \n\n                    <img  style="    height: 18px;" src="assets/imgs/icon-images/list.png">\n\n                    <br>\n\n                    {{"CATEGORY" | translate}} </a>\n\n            </div>\n\n            <div (click)="gotobag()"  style="text-align:center; width:20%; float:left;">\n\n                <a class="iconname" style="color:black;font-size: 12px;"> \n\n                    <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/shopping-bag.png">\n\n                    <br>\n\n                    {{"BAG" | translate}}\n\n                </a>\n\n            </div>\n\n            <div  (click)="gotoaccount()"  style="text-align:center; width:20%; float:left;">\n\n                <a   class="iconname" style="color:black;font-size: 12px;"> \n\n                    <img class="img-fluid" style="    height: 18px;"  src="assets/imgs/icon-images/round-account-button-with-user-inside.png">\n\n                    <br>\n\n                    {{"MYACCOUNT" | translate}} </a>\n\n            </div>\n\n       \n\n    \n\n    </ion-toolbar>\n\n</ion-footer> -->\n\n<ion-footer>\n\n        <div style="display:flex;">\n\n           <div (click)="gotohome()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname"  style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="height: 18px;" src="assets/imgs/icon-images/home.png">\n\n                   <br>\n\n                   {{"HOME" | translate}} \n\n               </a>\n\n           </div> \n\n           <div (click)="gotosearch()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/search.png">\n\n                   <br>\n\n                   {{"SEARCH" | translate}}</a>\n\n           </div>\n\n           <div (click)="gotocat()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;">  \n\n                   <img  style="    height: 18px;" src="assets/imgs/icon-images/list.png">\n\n                   <br>\n\n                   {{"CATEGORY" | translate}} </a>\n\n           </div>\n\n           <div (click)="gotobag()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="height: 18px;" src="assets/imgs/icon-images/shopping-bag.png">\n\n                   <br>\n\n                   {{"BAG" | translate}}\n\n               </a>\n\n           </div>\n\n           <div (click)="gotoaccount()"  style=\'flex:1;text-align:center\'>\n\n               <a   class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="height: 18px;"  src="assets/imgs/icon-images/round-account-button-with-user-inside.png">\n\n                   <br>\n\n                   {{"MYACCOUNT" | translate}} </a>\n\n           </div>\n\n        </div>\n\n</ion-footer>'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/myorder/myOrder.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */], __WEBPACK_IMPORTED_MODULE_6__providers_commonservice_commonservice__["a" /* CommonserviceProvider */]])
    ], MyOrderPage);
    return MyOrderPage;
}());

//# sourceMappingURL=myOrder.js.map

/***/ }),

/***/ 74:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup_signup__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__search_search__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__myAccount_myAccount__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__myorder_myOrder__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__addtocart_addToCart__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_commonservice_commonservice__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__cartitem_cartitem__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__home_home__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var OrderDetailsPage = /** @class */ (function () {
    function OrderDetailsPage(platform, alertCtrl, navCtrl, events, commonservice) {
        var _this = this;
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.events = events;
        this.commonservice = commonservice;
        this.address = "";
        this.amount = 0;
        this.subtotal = 0;
        this.tax = 0;
        this.cancelcheck = 0;
        this.coupon_amount = 0;
        this.is_cancelled = 0;
        this.shipping_charge = 0;
        events.publish('allpagecommon');
        this.loadpage();
        this.platform.registerBackButtonAction(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__myorder_myOrder__["a" /* MyOrderPage */]);
        });
    }
    OrderDetailsPage_1 = OrderDetailsPage;
    OrderDetailsPage.prototype.loadpage = function () {
        var _this = this;
        this.commonservice.waitloadershow();
        this.servicedata = "order_history_detail?order_id=" + localStorage.getItem("orderid") + "&lang=" + localStorage.getItem("applanguage");
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.commonservice.waitloaderhide();
            _this.data = res;
            console.log(_this.data);
            _this.date = _this.data.data.date;
            _this.tr_id = _this.data.data.order_id;
            _this.products = _this.data.data.product;
            _this.cancelcheck = _this.data.data.is_cancelled;
            _this.totalamount = _this.data.data.total;
            _this.subtotal = _this.data.data.sub_total;
            _this.is_cancelled = _this.data.data.is_cancelled;
            console.log(_this.is_cancelled);
            _this.address = _this.data.data.delivery_address;
            _this.orderstatus = _this.data.data.orderstatus;
            _this.tax = _this.data.data.tax;
            _this.shipping_charge = _this.data.data.shipping;
            console.log(_this.shipping_charge);
            if (_this.data.data.coupon_code == null || _this.data.data.coupon_code == '') {
                _this.coupon_code = 0;
            }
            else {
                _this.coupon_code = _this.data.data.coupon_code;
                _this.coupon_amount = _this.data.data.coupon_amount;
            }
        });
        //  var amount= (this.totalamount)-(this.tax);
    };
    OrderDetailsPage.prototype.gotocancel = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm',
            message: 'Do you really want to cancel your order? You can cancel your order within 24Hrs',
            buttons: [{
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Ok',
                    handler: function () {
                        _this.cancleorder();
                    }
                }
            ]
        });
        alert.present();
    };
    OrderDetailsPage.prototype.cancleorder = function () {
        var _this = this;
        this.servicedata = "cancel?order_id=" + localStorage.getItem("orderid") + "&orderstatus=7";
        this.navCtrl.setRoot(OrderDetailsPage_1);
        this.commonservice.serverdataget(this.servicedata).subscribe(function (res) {
            _this.data = res;
            console.log(_this.data.cancel);
            // console.log(this.data); 
            // console.log(this.data.cancel);                 
            // if(this.data.cancel == 1){
            //     this.commonservice.successalert("Your Order Has Been Canceled"); 
            // }else{
            //     this.commonservice.erroralert("You Can Cancel Your Order Within 24hrs. Please Contact To Customer Care");  
            // }               
        });
    };
    OrderDetailsPage.prototype.goOrderDetails = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__signup_signup__["a" /* SignupPage */]);
    };
    OrderDetailsPage.prototype.gotoback = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__myorder_myOrder__["a" /* MyOrderPage */]);
    };
    OrderDetailsPage.prototype.gotohome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__["a" /* DashboardPage */]);
    };
    OrderDetailsPage.prototype.gotosearch = function () {
        localStorage.setItem('lastpageserch', 'orderdetilaspage');
        console.log(localStorage.getItem('lastpageserch'));
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__search_search__["a" /* SearchPage */]);
    };
    OrderDetailsPage.prototype.details = function (id) {
        localStorage.setItem("details_pdid", id);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__addtocart_addToCart__["a" /* AddtocartPage */]);
    };
    OrderDetailsPage.prototype.gotocat = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__home_home__["a" /* HomePage */]);
    };
    OrderDetailsPage.prototype.gotobag = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__cartitem_cartitem__["a" /* CartitemPage */]);
    };
    OrderDetailsPage.prototype.gotoaccount = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__myAccount_myAccount__["a" /* MyAccountPage */]);
    };
    OrderDetailsPage = OrderDetailsPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-orderDetails',template:/*ion-inline-start:"/home/lipl-223/Documents/Laestrellac App/src/pages/orderDetails/orderDetails.html"*/'<ion-header>\n\n        <div class="headerbg_wh">\n\n    <div class="container">\n\n            <!-- style=" padding-top: 20px; padding-bottom: 20px;" -->\n\n        <span style="font-size:20px; cursor:pointer;" ><i class="fa fa-arrow-left" (click)="gotoback()" aria-hidden="true" style="padding: 0 10px;font-size: 20px;"></i> &nbsp; {{"MYORDERDETAILS" | translate}}</span>\n\n        <!-- <span style="font-size: 20px;cursor:pointer;float: right;" ><i class="fa fa-search" (click)="gotosearch()" aria-hidden="true" style="font-size: 20px;"></i></span> -->\n\n    </div>\n\n    </div>\n\n</ion-header>\n\n\n\n<ion-content style="background-color: rgb(245, 244, 244);">\n\n    \n\n    <div class="container-fluid" style=" margin-top: 11px;">\n\n        <div class="card bg-light"> \n\n            <div class="card-body text-center deliverycss">    \n\n                <h3 class="cancel" [ngClass]="{\'is-complete\' : orderstatus == 7}" style="font-size: 23px; color:red;">{{"YourOrderHasBeenCanceled" | translate}}</h3>\n\n                <h3 class="deli" [ngClass]="{\'deliveredtext\' : orderstatus == 5}" style="font-size: 23px; color:#155724;">{{"YourOrderisDelivered" | translate}}</h3>\n\n                <div class="notificationh3" style="font-size: 16px;">\n\n                    <span>{{"ORDERSTATUS" | translate}}</span>\n\n                    <span style="float: right;" *ngIf="is_cancelled == 1 && orderstatus != 7 && orderstatus != 5">\n\n                            <div (click)="gotocancel()"  [ngClass]="{\'ordercanceltrack\' : cancelcheck == 0 || orderstatus == 7 || orderstatus == 5 || orderstatus == 3}">\n\n                                    <button type="button" class="btn btn-primary btn-block Cancel-btn" style="height: 30px;\n\n                                    width: 80px;\n\n                                    letter-spacing: 0.6px;\n\n                                    background-color: #a50000;\n\n                                    border: 0;\n\n                                    outline: 0;" >{{"cancel" | translate}}</button> \n\n                                </div>\n\n                    </span>\n\n                </div>  \n\n                <h4 class="myorderh4" style="font-weight: 500;"><b>{{"TRANSACTIONID" | translate}} :  </b> {{tr_id}}</h4>\n\n                <h4 class="myorderh4" style="font-weight: 400;"><b>{{"PLACEORDER" | translate}} </b>{{date}} </h4>\n\n                <h4 class="myorderh4" style="font-weight: 400;"><b style="display: block;margin-bottom: 8px;">{{"SHIPPINGADD" | translate}} :</b> {{address}}</h4> \n\n                <!-- <div (click)="gotocancel()" class="col" [ngClass]="{\'ordercanceltrack\' : orderstatus == 7 || orderstatus == 5 || orderstatus == 3}"  style="width: 50%;float: left;padding:0px;">\n\n                        <button type="button" class="btn btn-primary btn-block Cancel-btn" style="height:50px" >{{"cancel" | translate}}</button> \n\n                    </div> -->\n\n            </div>\n\n        </div>\n\n    </div> \n\n    <!-- <div style="padding-left: 13px;"><h4 style="font-size:15px;font-weight: 600">{{"TOTALAMOUNT" | translate}} :  SAR &nbsp;&nbsp;&nbsp;{{totalamount}} </h4></div>\n\n    <div style="padding-left: 13px;"><h4 style="font-size:15px;font-weight: 600">tax :  SAR {{tax}}</h4></div> -->\n\n    <div  class="container-fluid"> \n\n        <!-- style="    padding-left: 32px;"  -->\n\n    <div *ngFor="let ps of products" (click)="details(ps.product_id)" class="row"  style="    margin-bottom: 10px;    margin-top: 8px;    border-radius: 4px;width: 100%;box-sizing: border-box;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); margin: 10px 0;">    \n\n        <div class="col-6" style="width: 35%;border-radius: 7px;padding:10px;background-position: center;background-size: cover;">\n\n            <img  style="padding: 10px 0;" src="{{ps.image}}" alt="Photo of sunset">\n\n        </div>\n\n        <div class="col-6" style="width: 63%;padding: 10px 0px;">\n\n            <div class="card product" style="box-shadow:none; border:none;margin: 0;">\n\n                <!-- <h3 class="notificationh3" style="  font-size: 16px;"> {{"COTTONFABRICS" | translate}}  </h3> -->\n\n                 <h3 class="notificationh3" style="margin: 0 0 5px; font-size: 16px;"> {{ps.name}} </h3>\n\n                <!-- <span style="font-size: 14px;"> {{ps.name}} </span> -->\n\n                <span style="display:block;font-size: 13px;"><b>{{"COLOR" | translate}}:</b> <label  [ngStyle]="{\'background-color\': ps.model}" style="vertical-align: middle; margin-left:8px;margin-bottom:0px;font-size: 12px; font-weight: 400;width:15px;height: 15px;"></label></span>\n\n                <span style="font-size: 14px;"><b>{{"QTY" | translate}}:</b> {{ps.quantity}},</span>\n\n                \n\n                <span style="display:block;font-size: 13px;"><b>{{"PRoductAmount" | translate}}:</b> SAR {{ps.price}}</span>\n\n                <span style="display:block;font-size: 14px;font-weight: 600;"><b>Final Product Amount:</b> SAR {{ps.totlal_price}}</span>\n\n            </div>\n\n        </div>\n\n    </div>         \n\n</div> \n\n        <div style="width: 100%;display: block;overflow: hidden;padding: 0 15px;" >\n\n            <p style="border-bottom: 1px solid #0000006b;color: #000;font-weight: 600;font-size: 15px;">{{"COSTSTRUCTURE" | translate}} </p>\n\n            <div class="col-xs-6" style="padding: 0px;">\n\n                <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"SUBTOTAL" | translate}}</h4>\n\n                <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"TAXCART" | translate}}</h4>\n\n                <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"ShippingCharges" | translate}}</h4>\n\n                <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"COUPONAMOUNT" | translate}}</h4>\n\n                <h4 style="border-top: 1px solid #000;padding-top: 8px;font-size: 15px;" class="myorderh4 font-weight-bold">{{"PAYABLEAMOUNT" | translate}}</h4>\n\n            </div>\n\n            <div class="col-xs-6" style="padding: 0px;">\n\n                <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"SAR" | translate}} :  {{subtotal}} </h4>\n\n                <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"SAR" | translate}} : {{tax}} </h4>\n\n                <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;">{{"SAR" | translate}} : {{shipping_charge}}</h4>\n\n                <h4 class="myorderh4 font-weight-bold" style="font-size: 12px;font-weight: 500;"> {{"SAR" | translate}} : - {{coupon_amount}} </h4>\n\n                <h4 style="border-top: 1px solid #000;padding-top: 8px;font-size: 15px;" class="myorderh4 font-weight-bold">{{"SAR" | translate}} : {{totalamount}}</h4>\n\n            </div>\n\n        </div> \n\n   \n\n\n\n<ul class="progress-tracker" [ngClass]="{\'ordercanceltrack\' : orderstatus == 7}" style="padding: 35px">\n\n    <li class="progress-step is-complete">\n\n        <span class="progress-marker"><p style="margin-top: 88px; color: black;font-weight: 800;"> {{"ORDERPROCESSING" | translate}}</p></span>\n\n    </li>\n\n  \n\n    <li class="progress-step" [ngClass]="{\'is-complete\' : orderstatus == 5 || orderstatus == 3 }">\n\n      <span class="progress-marker"><p style="margin-top: 88px; color: black;font-weight: 800;"> {{"SHIPPED" | translate}}</p></span>\n\n    </li>\n\n    <li class="progress-step" [ngClass]="{\'is-complete\' : orderstatus == 5}">\n\n        <span class="progress-marker"><p style="margin-top: 88px; color: black;font-weight: 800;">{{"DELIVERED" | translate}}</p></span>\n\n    </li>\n\n</ul>\n\n    \n\n	\n\n</ion-content>\n\n<!-- <ion-footer>\n\n    <ion-toolbar>\n\n            <div (click)="gotohome()"  style="text-align:center; width:20%; float:left;">\n\n                <a class="iconname" style="color:black;font-size: 12px;"> \n\n                    <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/home.png">\n\n                    <br>\n\n                    {{"HOME" | translate}} \n\n                </a>\n\n            </div> \n\n            <div (click)="gotosearch()"  style="text-align:center; width:20%; float:left;">\n\n                <a class="iconname" style="color:black;font-size: 12px;"> \n\n                    <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/search.png">\n\n                    <br>\n\n                    {{"SEARCH" | translate}}</a>\n\n            </div>\n\n            <div  (click)="gotocat()"  style="text-align:center; width:20%; float:left;">\n\n                <a class="iconname" style="color:black;font-size: 12px;">  \n\n                    <img  style="    height: 18px;" src="assets/imgs/icon-images/list.png">\n\n                    <br>\n\n                    {{"CATEGORY" | translate}} </a>\n\n            </div>\n\n            <div (click)="gotobag()" style="text-align:center; width:20%; float:left;">\n\n                <a  class="iconname" style="color:black;font-size: 12px;"> \n\n                    <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/shopping-bag.png">\n\n                    <br>\n\n                    {{"BAG" | translate}}\n\n                </a>\n\n            </div>\n\n            <div  (click)="gotoaccount()"  style="text-align:center; width:20%; float:left;">\n\n                <a  class="iconname" style="color:black;font-size: 12px;"> \n\n                    <img class="img-fluid" style="    height: 18px;"  src="assets/imgs/icon-images/round-account-button-with-user-inside.png">\n\n                    <br>\n\n                    {{"MYACCOUNT" | translate}} </a>\n\n            </div>\n\n        \n\n    </ion-toolbar>\n\n</ion-footer> -->\n\n<ion-footer>\n\n        <div style="display:flex;">\n\n           <div (click)="gotohome()" style=\'flex:1;text-align:center\'>\n\n               <a class="iconname"  style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/home.png">\n\n                   <br>\n\n                   {{"HOME" | translate}} \n\n               </a>\n\n           </div> \n\n           <div (click)="gotosearch()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="    height: 18px;" src="assets/imgs/icon-images/search.png">\n\n                   <br>\n\n                   {{"SEARCH" | translate}}</a>\n\n           </div>\n\n           <div (click)="gotocat()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;">  \n\n                   <img  style="    height: 18px;" src="assets/imgs/icon-images/list.png">\n\n                   <br>\n\n                   {{"CATEGORY" | translate}} </a>\n\n           </div>\n\n           <div (click)="gotobag()"  style=\'flex:1;text-align:center\'>\n\n               <a class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="height: 18px;" src="assets/imgs/icon-images/shopping-bag.png">\n\n                   <br>\n\n                   {{"BAG" | translate}}\n\n               </a>\n\n           </div>\n\n           <div (click)="gotoaccount()"  style=\'flex:1;text-align:center\'>\n\n               <a   class="iconname" style="color:black;font-size: 10px;"> \n\n                   <img class="img-fluid" style="height: 18px;"  src="assets/imgs/icon-images/round-account-button-with-user-inside.png">\n\n                   <br>\n\n                   {{"MYACCOUNT" | translate}} </a>\n\n           </div>\n\n        </div>\n\n</ion-footer>\n\n'/*ion-inline-end:"/home/lipl-223/Documents/Laestrellac App/src/pages/orderDetails/orderDetails.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */], __WEBPACK_IMPORTED_MODULE_8__providers_commonservice_commonservice__["a" /* CommonserviceProvider */]])
    ], OrderDetailsPage);
    return OrderDetailsPage;
    var OrderDetailsPage_1;
}());

//# sourceMappingURL=orderDetails.js.map

/***/ })

},[276]);
//# sourceMappingURL=main.js.map